/**************************************************************************
 * TournamentPoolFightUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.test.utils.*;



import com.curseblade.entity.FightPlayer;
import com.curseblade.fixture.FightPlayerDataLoader;
import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.fixture.TournamentPoolFightDataLoader;
import java.util.ArrayList;

public abstract class TournamentPoolFightUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static TournamentPoolFight generateRandom(Context ctx){
		TournamentPoolFight tournamentPoolFight = new TournamentPoolFight();

		tournamentPoolFight.setId(TestUtils.generateRandomInt(0,100) + 1);
		tournamentPoolFight.setNodeLevel(TestUtils.generateRandomInt(0,100));
		ArrayList<FightPlayer> winners =
			new ArrayList<FightPlayer>(FightPlayerDataLoader.getInstance(ctx).getMap().values());
		if (!winners.isEmpty()) {
			tournamentPoolFight.setWinner(winners.get(TestUtils.generateRandomInt(0, winners.size())));
		}
		ArrayList<TournamentPoolFight> parentPools =
			new ArrayList<TournamentPoolFight>(TournamentPoolFightDataLoader.getInstance(ctx).getMap().values());
		if (!parentPools.isEmpty()) {
			tournamentPoolFight.setParentPool(parentPools.get(TestUtils.generateRandomInt(0, parentPools.size())));
		}
		ArrayList<TournamentPoolFight> leftPools =
			new ArrayList<TournamentPoolFight>(TournamentPoolFightDataLoader.getInstance(ctx).getMap().values());
		if (!leftPools.isEmpty()) {
			tournamentPoolFight.setLeftPool(leftPools.get(TestUtils.generateRandomInt(0, leftPools.size())));
		}
		ArrayList<TournamentPoolFight> rightPools =
			new ArrayList<TournamentPoolFight>(TournamentPoolFightDataLoader.getInstance(ctx).getMap().values());
		if (!rightPools.isEmpty()) {
			tournamentPoolFight.setRightPool(rightPools.get(TestUtils.generateRandomInt(0, rightPools.size())));
		}

		return tournamentPoolFight;
	}

	public static boolean equals(TournamentPoolFight tournamentPoolFight1, TournamentPoolFight tournamentPoolFight2){
		boolean ret = true;
		Assert.assertNotNull(tournamentPoolFight1);
		Assert.assertNotNull(tournamentPoolFight2);
		if (tournamentPoolFight1!=null && tournamentPoolFight2 !=null){
			Assert.assertEquals(tournamentPoolFight1.getId(), tournamentPoolFight2.getId());
			Assert.assertEquals(tournamentPoolFight1.getNodeLevel(), tournamentPoolFight2.getNodeLevel());
			if (tournamentPoolFight1.getWinner() != null
					&& tournamentPoolFight2.getWinner() != null) {
				Assert.assertEquals(tournamentPoolFight1.getWinner().getId(),
						tournamentPoolFight2.getWinner().getId());

			}
			if (tournamentPoolFight1.getParentPool() != null
					&& tournamentPoolFight2.getParentPool() != null) {
				Assert.assertEquals(tournamentPoolFight1.getParentPool().getId(),
						tournamentPoolFight2.getParentPool().getId());

			}
			if (tournamentPoolFight1.getLeftPool() != null
					&& tournamentPoolFight2.getLeftPool() != null) {
				Assert.assertEquals(tournamentPoolFight1.getLeftPool().getId(),
						tournamentPoolFight2.getLeftPool().getId());

			}
			if (tournamentPoolFight1.getRightPool() != null
					&& tournamentPoolFight2.getRightPool() != null) {
				Assert.assertEquals(tournamentPoolFight1.getRightPool().getId(),
						tournamentPoolFight2.getRightPool().getId());

			}
		}

		return ret;
	}
}

