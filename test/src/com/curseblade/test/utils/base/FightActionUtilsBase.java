/**************************************************************************
 * FightActionUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.FightAction;
import com.curseblade.test.utils.*;



import com.curseblade.entity.Fight;
import com.curseblade.fixture.FightDataLoader;
import com.curseblade.entity.FightPlayer;
import com.curseblade.fixture.FightPlayerDataLoader;
import com.curseblade.entity.FightActionType;
import java.util.ArrayList;

public abstract class FightActionUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static FightAction generateRandom(Context ctx){
		FightAction fightAction = new FightAction();

		fightAction.setId(TestUtils.generateRandomInt(0,100) + 1);
		ArrayList<Fight> fights =
			new ArrayList<Fight>(FightDataLoader.getInstance(ctx).getMap().values());
		if (!fights.isEmpty()) {
			fightAction.setFight(fights.get(TestUtils.generateRandomInt(0, fights.size())));
		}
		ArrayList<FightPlayer> senders =
			new ArrayList<FightPlayer>(FightPlayerDataLoader.getInstance(ctx).getMap().values());
		if (!senders.isEmpty()) {
			fightAction.setSender(senders.get(TestUtils.generateRandomInt(0, senders.size())));
		}
		ArrayList<FightPlayer> receivers =
			new ArrayList<FightPlayer>(FightPlayerDataLoader.getInstance(ctx).getMap().values());
		if (!receivers.isEmpty()) {
			fightAction.setReceiver(receivers.get(TestUtils.generateRandomInt(0, receivers.size())));
		}
		fightAction.setActionType(FightActionType.values()[TestUtils.generateRandomInt(0,FightActionType.values().length)]);
		fightAction.setBeforeValue(TestUtils.generateRandomInt(0,100));
		fightAction.setAfterValue(TestUtils.generateRandomInt(0,100));

		return fightAction;
	}

	public static boolean equals(FightAction fightAction1, FightAction fightAction2){
		boolean ret = true;
		Assert.assertNotNull(fightAction1);
		Assert.assertNotNull(fightAction2);
		if (fightAction1!=null && fightAction2 !=null){
			Assert.assertEquals(fightAction1.getId(), fightAction2.getId());
			if (fightAction1.getFight() != null
					&& fightAction2.getFight() != null) {
				Assert.assertEquals(fightAction1.getFight().getId(),
						fightAction2.getFight().getId());

			}
			if (fightAction1.getSender() != null
					&& fightAction2.getSender() != null) {
				Assert.assertEquals(fightAction1.getSender().getId(),
						fightAction2.getSender().getId());

			}
			if (fightAction1.getReceiver() != null
					&& fightAction2.getReceiver() != null) {
				Assert.assertEquals(fightAction1.getReceiver().getId(),
						fightAction2.getReceiver().getId());

			}
			Assert.assertEquals(fightAction1.getActionType(), fightAction2.getActionType());
			Assert.assertEquals(fightAction1.getBeforeValue(), fightAction2.getBeforeValue());
			Assert.assertEquals(fightAction1.getAfterValue(), fightAction2.getAfterValue());
		}

		return ret;
	}
}

