/**************************************************************************
 * SpellUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.Spell;
import com.curseblade.test.utils.*;




public abstract class SpellUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static Spell generateRandom(Context ctx){
		Spell spell = new Spell();

		spell.setId(TestUtils.generateRandomInt(0,100) + 1);
		spell.setName("name_"+TestUtils.generateRandomString(10));
		spell.setBaseAttack(TestUtils.generateRandomDouble(0,100));
		spell.setCriticalAttack(TestUtils.generateRandomDouble(0,100));

		return spell;
	}

	public static boolean equals(Spell spell1, Spell spell2){
		boolean ret = true;
		Assert.assertNotNull(spell1);
		Assert.assertNotNull(spell2);
		if (spell1!=null && spell2 !=null){
			Assert.assertEquals(spell1.getId(), spell2.getId());
			Assert.assertEquals(spell1.getName(), spell2.getName());
			Assert.assertEquals(spell1.getBaseAttack(), spell2.getBaseAttack());
			Assert.assertEquals(spell1.getCriticalAttack(), spell2.getCriticalAttack());
		}

		return ret;
	}
}

