/**************************************************************************
 * CharacterPlayerUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.test.utils.*;



import com.curseblade.entity.ItemArmury;
import com.curseblade.fixture.ItemArmuryDataLoader;
import com.curseblade.entity.Spell;
import com.curseblade.fixture.SpellDataLoader;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.fixture.ItemWeaponDataLoader;
import java.util.ArrayList;

public abstract class CharacterPlayerUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static CharacterPlayer generateRandom(Context ctx){
		CharacterPlayer characterPlayer = new CharacterPlayer();

		characterPlayer.setId(TestUtils.generateRandomInt(0,100) + 1);
		characterPlayer.setPseudo("pseudo_"+TestUtils.generateRandomString(10));
		characterPlayer.setLife(TestUtils.generateRandomInt(0,100));
		characterPlayer.setCreatedAt(TestUtils.generateRandomDate());
		characterPlayer.setLevel(TestUtils.generateRandomInt(0,100));
		ArrayList<ItemArmury> armuryEquipedItemss =
			new ArrayList<ItemArmury>(ItemArmuryDataLoader.getInstance(ctx).getMap().values());
		ArrayList<ItemArmury> relatedArmuryEquipedItemss = new ArrayList<ItemArmury>();
		if (!armuryEquipedItemss.isEmpty()) {
			relatedArmuryEquipedItemss.add(armuryEquipedItemss.get(TestUtils.generateRandomInt(0, armuryEquipedItemss.size())));
			characterPlayer.setArmuryEquipedItems(relatedArmuryEquipedItemss);
		}
		ArrayList<Spell> equipedSpellss =
			new ArrayList<Spell>(SpellDataLoader.getInstance(ctx).getMap().values());
		ArrayList<Spell> relatedEquipedSpellss = new ArrayList<Spell>();
		if (!equipedSpellss.isEmpty()) {
			relatedEquipedSpellss.add(equipedSpellss.get(TestUtils.generateRandomInt(0, equipedSpellss.size())));
			characterPlayer.setEquipedSpells(relatedEquipedSpellss);
		}
		ArrayList<ItemWeapon> weaponUseds =
			new ArrayList<ItemWeapon>(ItemWeaponDataLoader.getInstance(ctx).getMap().values());
		if (!weaponUseds.isEmpty()) {
			characterPlayer.setWeaponUsed(weaponUseds.get(TestUtils.generateRandomInt(0, weaponUseds.size())));
		}

		return characterPlayer;
	}

	public static boolean equals(CharacterPlayer characterPlayer1, CharacterPlayer characterPlayer2){
		boolean ret = true;
		Assert.assertNotNull(characterPlayer1);
		Assert.assertNotNull(characterPlayer2);
		if (characterPlayer1!=null && characterPlayer2 !=null){
			Assert.assertEquals(characterPlayer1.getId(), characterPlayer2.getId());
			Assert.assertEquals(characterPlayer1.getPseudo(), characterPlayer2.getPseudo());
			Assert.assertEquals(characterPlayer1.getLife(), characterPlayer2.getLife());
			Assert.assertEquals(characterPlayer1.getCreatedAt(), characterPlayer2.getCreatedAt());
			Assert.assertEquals(characterPlayer1.getLevel(), characterPlayer2.getLevel());
			if (characterPlayer1.getArmuryEquipedItems() != null
					&& characterPlayer2.getArmuryEquipedItems() != null) {
				Assert.assertEquals(characterPlayer1.getArmuryEquipedItems().size(),
					characterPlayer2.getArmuryEquipedItems().size());
				for (int i=0;i<characterPlayer1.getArmuryEquipedItems().size();i++){
					Assert.assertEquals(characterPlayer1.getArmuryEquipedItems().get(i).getId(),
								characterPlayer2.getArmuryEquipedItems().get(i).getId());
				}
			}
			if (characterPlayer1.getEquipedSpells() != null
					&& characterPlayer2.getEquipedSpells() != null) {
				Assert.assertEquals(characterPlayer1.getEquipedSpells().size(),
					characterPlayer2.getEquipedSpells().size());
				for (int i=0;i<characterPlayer1.getEquipedSpells().size();i++){
					Assert.assertEquals(characterPlayer1.getEquipedSpells().get(i).getId(),
								characterPlayer2.getEquipedSpells().get(i).getId());
				}
			}
			if (characterPlayer1.getWeaponUsed() != null
					&& characterPlayer2.getWeaponUsed() != null) {
				Assert.assertEquals(characterPlayer1.getWeaponUsed().getId(),
						characterPlayer2.getWeaponUsed().getId());

			}
		}

		return ret;
	}
}

