/**************************************************************************
 * ItemWeaponTypeUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.ItemWeaponType;
import com.curseblade.test.utils.*;




public abstract class ItemWeaponTypeUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static ItemWeaponType generateRandom(Context ctx){
		ItemWeaponType itemWeaponType = new ItemWeaponType();

		itemWeaponType.setId(TestUtils.generateRandomInt(0,100) + 1);
		itemWeaponType.setTitle("title_"+TestUtils.generateRandomString(10));

		return itemWeaponType;
	}

	public static boolean equals(ItemWeaponType itemWeaponType1, ItemWeaponType itemWeaponType2){
		boolean ret = true;
		Assert.assertNotNull(itemWeaponType1);
		Assert.assertNotNull(itemWeaponType2);
		if (itemWeaponType1!=null && itemWeaponType2 !=null){
			Assert.assertEquals(itemWeaponType1.getId(), itemWeaponType2.getId());
			Assert.assertEquals(itemWeaponType1.getTitle(), itemWeaponType2.getTitle());
		}

		return ret;
	}
}

