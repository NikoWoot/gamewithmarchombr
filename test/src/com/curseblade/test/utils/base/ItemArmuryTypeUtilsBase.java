/**************************************************************************
 * ItemArmuryTypeUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.ItemArmuryType;
import com.curseblade.test.utils.*;




public abstract class ItemArmuryTypeUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static ItemArmuryType generateRandom(Context ctx){
		ItemArmuryType itemArmuryType = new ItemArmuryType();

		itemArmuryType.setId(TestUtils.generateRandomInt(0,100) + 1);
		itemArmuryType.setTitle("title_"+TestUtils.generateRandomString(10));

		return itemArmuryType;
	}

	public static boolean equals(ItemArmuryType itemArmuryType1, ItemArmuryType itemArmuryType2){
		boolean ret = true;
		Assert.assertNotNull(itemArmuryType1);
		Assert.assertNotNull(itemArmuryType2);
		if (itemArmuryType1!=null && itemArmuryType2 !=null){
			Assert.assertEquals(itemArmuryType1.getId(), itemArmuryType2.getId());
			Assert.assertEquals(itemArmuryType1.getTitle(), itemArmuryType2.getTitle());
		}

		return ret;
	}
}

