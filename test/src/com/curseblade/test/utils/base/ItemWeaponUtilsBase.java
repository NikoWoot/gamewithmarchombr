/**************************************************************************
 * ItemWeaponUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.test.utils.*;



import com.curseblade.entity.ItemWeaponType;

public abstract class ItemWeaponUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static ItemWeapon generateRandom(Context ctx){
		ItemWeapon itemWeapon = new ItemWeapon();

		itemWeapon.setId(TestUtils.generateRandomInt(0,100) + 1);
		itemWeapon.setName("name_"+TestUtils.generateRandomString(10));
		itemWeapon.setBaseAttack(TestUtils.generateRandomDouble(0,100));
		itemWeapon.setWeaponType(ItemWeaponType.values()[TestUtils.generateRandomInt(0,ItemWeaponType.values().length)]);

		return itemWeapon;
	}

	public static boolean equals(ItemWeapon itemWeapon1, ItemWeapon itemWeapon2){
		boolean ret = true;
		Assert.assertNotNull(itemWeapon1);
		Assert.assertNotNull(itemWeapon2);
		if (itemWeapon1!=null && itemWeapon2 !=null){
			Assert.assertEquals(itemWeapon1.getId(), itemWeapon2.getId());
			Assert.assertEquals(itemWeapon1.getName(), itemWeapon2.getName());
			Assert.assertEquals(itemWeapon1.getBaseAttack(), itemWeapon2.getBaseAttack());
			Assert.assertEquals(itemWeapon1.getWeaponType(), itemWeapon2.getWeaponType());
		}

		return ret;
	}
}

