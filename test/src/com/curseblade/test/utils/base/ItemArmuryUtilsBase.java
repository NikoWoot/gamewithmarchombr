/**************************************************************************
 * ItemArmuryUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.ItemArmury;
import com.curseblade.test.utils.*;



import com.curseblade.entity.ItemArmuryType;

public abstract class ItemArmuryUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static ItemArmury generateRandom(Context ctx){
		ItemArmury itemArmury = new ItemArmury();

		itemArmury.setId(TestUtils.generateRandomInt(0,100) + 1);
		itemArmury.setName("name_"+TestUtils.generateRandomString(10));
		itemArmury.setDefValue(TestUtils.generateRandomInt(0,100));
		itemArmury.setArmuryType(ItemArmuryType.values()[TestUtils.generateRandomInt(0,ItemArmuryType.values().length)]);

		return itemArmury;
	}

	public static boolean equals(ItemArmury itemArmury1, ItemArmury itemArmury2){
		boolean ret = true;
		Assert.assertNotNull(itemArmury1);
		Assert.assertNotNull(itemArmury2);
		if (itemArmury1!=null && itemArmury2 !=null){
			Assert.assertEquals(itemArmury1.getId(), itemArmury2.getId());
			Assert.assertEquals(itemArmury1.getName(), itemArmury2.getName());
			Assert.assertEquals(itemArmury1.getDefValue(), itemArmury2.getDefValue());
			Assert.assertEquals(itemArmury1.getArmuryType(), itemArmury2.getArmuryType());
		}

		return ret;
	}
}

