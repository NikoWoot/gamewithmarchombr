/**************************************************************************
 * TournamentUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.Tournament;
import com.curseblade.test.utils.*;



import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.fixture.TournamentPoolFightDataLoader;
import java.util.ArrayList;

public abstract class TournamentUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static Tournament generateRandom(Context ctx){
		Tournament tournament = new Tournament();

		tournament.setId(TestUtils.generateRandomInt(0,100) + 1);
		tournament.setMaxElements(TestUtils.generateRandomInt(0,100));
		ArrayList<TournamentPoolFight> poolFightss =
			new ArrayList<TournamentPoolFight>(TournamentPoolFightDataLoader.getInstance(ctx).getMap().values());
		ArrayList<TournamentPoolFight> relatedPoolFightss = new ArrayList<TournamentPoolFight>();
		if (!poolFightss.isEmpty()) {
			relatedPoolFightss.add(poolFightss.get(TestUtils.generateRandomInt(0, poolFightss.size())));
			tournament.setPoolFights(relatedPoolFightss);
		}

		return tournament;
	}

	public static boolean equals(Tournament tournament1, Tournament tournament2){
		boolean ret = true;
		Assert.assertNotNull(tournament1);
		Assert.assertNotNull(tournament2);
		if (tournament1!=null && tournament2 !=null){
			Assert.assertEquals(tournament1.getId(), tournament2.getId());
			Assert.assertEquals(tournament1.getMaxElements(), tournament2.getMaxElements());
			if (tournament1.getPoolFights() != null
					&& tournament2.getPoolFights() != null) {
				Assert.assertEquals(tournament1.getPoolFights().size(),
					tournament2.getPoolFights().size());
				for (int i=0;i<tournament1.getPoolFights().size();i++){
					Assert.assertEquals(tournament1.getPoolFights().get(i).getId(),
								tournament2.getPoolFights().get(i).getId());
				}
			}
		}

		return ret;
	}
}

