/**************************************************************************
 * HarmonyRssItemUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.HarmonyRssItem;
import com.curseblade.test.utils.*;




public abstract class HarmonyRssItemUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static HarmonyRssItem generateRandom(Context ctx){
		HarmonyRssItem harmonyRssItem = new HarmonyRssItem();

		harmonyRssItem.setId(TestUtils.generateRandomInt(0,100) + 1);
		harmonyRssItem.setHash(TestUtils.generateRandomInt(0,100));
		harmonyRssItem.setGuid("guid_"+TestUtils.generateRandomString(10));
		harmonyRssItem.setTitle("title_"+TestUtils.generateRandomString(10));
		harmonyRssItem.setLink("link_"+TestUtils.generateRandomString(10));
		harmonyRssItem.setDescription("description_"+TestUtils.generateRandomString(10));
		harmonyRssItem.setEnclosure("enclosure_"+TestUtils.generateRandomString(10));
		harmonyRssItem.setAuthor("author_"+TestUtils.generateRandomString(10));
		harmonyRssItem.setPubDate(TestUtils.generateRandomDateTime());
		harmonyRssItem.setCategories("categories_"+TestUtils.generateRandomString(10));

		return harmonyRssItem;
	}

	public static boolean equals(HarmonyRssItem harmonyRssItem1, HarmonyRssItem harmonyRssItem2){
		boolean ret = true;
		Assert.assertNotNull(harmonyRssItem1);
		Assert.assertNotNull(harmonyRssItem2);
		if (harmonyRssItem1!=null && harmonyRssItem2 !=null){
			Assert.assertEquals(harmonyRssItem1.getId(), harmonyRssItem2.getId());
			Assert.assertEquals(harmonyRssItem1.getHash(), harmonyRssItem2.getHash());
			Assert.assertEquals(harmonyRssItem1.getGuid(), harmonyRssItem2.getGuid());
			Assert.assertEquals(harmonyRssItem1.getTitle(), harmonyRssItem2.getTitle());
			Assert.assertEquals(harmonyRssItem1.getLink(), harmonyRssItem2.getLink());
			Assert.assertEquals(harmonyRssItem1.getDescription(), harmonyRssItem2.getDescription());
			Assert.assertEquals(harmonyRssItem1.getEnclosure(), harmonyRssItem2.getEnclosure());
			Assert.assertEquals(harmonyRssItem1.getAuthor(), harmonyRssItem2.getAuthor());
			Assert.assertEquals(harmonyRssItem1.getPubDate(), harmonyRssItem2.getPubDate());
			Assert.assertEquals(harmonyRssItem1.getCategories(), harmonyRssItem2.getCategories());
		}

		return ret;
	}
}

