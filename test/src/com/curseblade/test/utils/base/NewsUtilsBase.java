/**************************************************************************
 * NewsUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.News;
import com.curseblade.test.utils.*;

import com.curseblade.entity.HarmonyRssItem;


public abstract class NewsUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static News generateRandom(Context ctx){
		News news = new News();
		HarmonyRssItem harmonyRssItem = HarmonyRssItemUtils.generateRandom(ctx);
		news.setId(harmonyRssItem.getId());
		news.setHash(harmonyRssItem.getHash());
		news.setGuid(harmonyRssItem.getGuid());
		news.setTitle(harmonyRssItem.getTitle());
		news.setLink(harmonyRssItem.getLink());
		news.setDescription(harmonyRssItem.getDescription());
		news.setEnclosure(harmonyRssItem.getEnclosure());
		news.setAuthor(harmonyRssItem.getAuthor());
		news.setPubDate(harmonyRssItem.getPubDate());
		news.setCategories(harmonyRssItem.getCategories());


		return news;
	}

	public static boolean equals(News news1, News news2){
		boolean ret = true;
		Assert.assertNotNull(news1);
		Assert.assertNotNull(news2);
		if (news1!=null && news2 !=null){
		}

		return ret;
	}
}

