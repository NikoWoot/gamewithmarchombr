/**************************************************************************
 * FightPlayerUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.FightPlayer;
import com.curseblade.test.utils.*;



import com.curseblade.entity.Fight;
import com.curseblade.fixture.FightDataLoader;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.fixture.CharacterPlayerDataLoader;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.fixture.ItemWeaponDataLoader;
import com.curseblade.entity.Spell;
import com.curseblade.fixture.SpellDataLoader;
import java.util.ArrayList;

public abstract class FightPlayerUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static FightPlayer generateRandom(Context ctx){
		FightPlayer fightPlayer = new FightPlayer();

		fightPlayer.setId(TestUtils.generateRandomInt(0,100) + 1);
		fightPlayer.setTeamId(TestUtils.generateRandomInt(0,100));
		ArrayList<Fight> fights =
			new ArrayList<Fight>(FightDataLoader.getInstance(ctx).getMap().values());
		if (!fights.isEmpty()) {
			fightPlayer.setFight(fights.get(TestUtils.generateRandomInt(0, fights.size())));
		}
		fightPlayer.setPseudo("pseudo_"+TestUtils.generateRandomString(10));
		fightPlayer.setLife(TestUtils.generateRandomInt(0,100));
		fightPlayer.setDead(TestUtils.generateRandomBool());
		ArrayList<CharacterPlayer> baseCharacters =
			new ArrayList<CharacterPlayer>(CharacterPlayerDataLoader.getInstance(ctx).getMap().values());
		if (!baseCharacters.isEmpty()) {
			fightPlayer.setBaseCharacter(baseCharacters.get(TestUtils.generateRandomInt(0, baseCharacters.size())));
		}
		ArrayList<ItemWeapon> baseWeapons =
			new ArrayList<ItemWeapon>(ItemWeaponDataLoader.getInstance(ctx).getMap().values());
		if (!baseWeapons.isEmpty()) {
			fightPlayer.setBaseWeapon(baseWeapons.get(TestUtils.generateRandomInt(0, baseWeapons.size())));
		}
		ArrayList<Spell> baseSpellss =
			new ArrayList<Spell>(SpellDataLoader.getInstance(ctx).getMap().values());
		ArrayList<Spell> relatedBaseSpellss = new ArrayList<Spell>();
		if (!baseSpellss.isEmpty()) {
			relatedBaseSpellss.add(baseSpellss.get(TestUtils.generateRandomInt(0, baseSpellss.size())));
			fightPlayer.setBaseSpells(relatedBaseSpellss);
		}

		return fightPlayer;
	}

	public static boolean equals(FightPlayer fightPlayer1, FightPlayer fightPlayer2){
		boolean ret = true;
		Assert.assertNotNull(fightPlayer1);
		Assert.assertNotNull(fightPlayer2);
		if (fightPlayer1!=null && fightPlayer2 !=null){
			Assert.assertEquals(fightPlayer1.getId(), fightPlayer2.getId());
			Assert.assertEquals(fightPlayer1.getTeamId(), fightPlayer2.getTeamId());
			if (fightPlayer1.getFight() != null
					&& fightPlayer2.getFight() != null) {
				Assert.assertEquals(fightPlayer1.getFight().getId(),
						fightPlayer2.getFight().getId());

			}
			Assert.assertEquals(fightPlayer1.getPseudo(), fightPlayer2.getPseudo());
			Assert.assertEquals(fightPlayer1.getLife(), fightPlayer2.getLife());
			Assert.assertEquals(fightPlayer1.isDead(), fightPlayer2.isDead());
			if (fightPlayer1.getBaseCharacter() != null
					&& fightPlayer2.getBaseCharacter() != null) {
				Assert.assertEquals(fightPlayer1.getBaseCharacter().getId(),
						fightPlayer2.getBaseCharacter().getId());

			}
			if (fightPlayer1.getBaseWeapon() != null
					&& fightPlayer2.getBaseWeapon() != null) {
				Assert.assertEquals(fightPlayer1.getBaseWeapon().getId(),
						fightPlayer2.getBaseWeapon().getId());

			}
			if (fightPlayer1.getBaseSpells() != null
					&& fightPlayer2.getBaseSpells() != null) {
				Assert.assertEquals(fightPlayer1.getBaseSpells().size(),
					fightPlayer2.getBaseSpells().size());
				for (int i=0;i<fightPlayer1.getBaseSpells().size();i++){
					Assert.assertEquals(fightPlayer1.getBaseSpells().get(i).getId(),
								fightPlayer2.getBaseSpells().get(i).getId());
				}
			}
		}

		return ret;
	}
}

