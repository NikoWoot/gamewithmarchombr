/**************************************************************************
 * FightUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils.base;

import android.content.Context;
import junit.framework.Assert;
import com.curseblade.entity.Fight;
import com.curseblade.test.utils.*;



import com.curseblade.entity.FightPlayer;
import com.curseblade.fixture.FightPlayerDataLoader;
import com.curseblade.entity.FightAction;
import com.curseblade.fixture.FightActionDataLoader;
import java.util.ArrayList;

public abstract class FightUtilsBase {

	// If you have enums, you may have to override this method to generate the random enums values
	/**
	 * Generate a random entity
	 *
	 * @return The randomly generated entity
	 */
	public static Fight generateRandom(Context ctx){
		Fight fight = new Fight();

		fight.setId(TestUtils.generateRandomInt(0,100) + 1);
		ArrayList<FightPlayer> starterFighterss =
			new ArrayList<FightPlayer>(FightPlayerDataLoader.getInstance(ctx).getMap().values());
		ArrayList<FightPlayer> relatedStarterFighterss = new ArrayList<FightPlayer>();
		if (!starterFighterss.isEmpty()) {
			relatedStarterFighterss.add(starterFighterss.get(TestUtils.generateRandomInt(0, starterFighterss.size())));
			fight.setStarterFighters(relatedStarterFighterss);
		}
		ArrayList<FightPlayer> alternatedFighterss =
			new ArrayList<FightPlayer>(FightPlayerDataLoader.getInstance(ctx).getMap().values());
		ArrayList<FightPlayer> relatedAlternatedFighterss = new ArrayList<FightPlayer>();
		if (!alternatedFighterss.isEmpty()) {
			relatedAlternatedFighterss.add(alternatedFighterss.get(TestUtils.generateRandomInt(0, alternatedFighterss.size())));
			fight.setAlternatedFighters(relatedAlternatedFighterss);
		}
		ArrayList<FightPlayer> survivorFighterss =
			new ArrayList<FightPlayer>(FightPlayerDataLoader.getInstance(ctx).getMap().values());
		ArrayList<FightPlayer> relatedSurvivorFighterss = new ArrayList<FightPlayer>();
		if (!survivorFighterss.isEmpty()) {
			relatedSurvivorFighterss.add(survivorFighterss.get(TestUtils.generateRandomInt(0, survivorFighterss.size())));
			fight.setSurvivorFighters(relatedSurvivorFighterss);
		}
		ArrayList<FightAction> actionss =
			new ArrayList<FightAction>(FightActionDataLoader.getInstance(ctx).getMap().values());
		ArrayList<FightAction> relatedActionss = new ArrayList<FightAction>();
		if (!actionss.isEmpty()) {
			relatedActionss.add(actionss.get(TestUtils.generateRandomInt(0, actionss.size())));
			fight.setActions(relatedActionss);
		}
		ArrayList<FightPlayer> senderPlayers =
			new ArrayList<FightPlayer>(FightPlayerDataLoader.getInstance(ctx).getMap().values());
		if (!senderPlayers.isEmpty()) {
			fight.setSenderPlayer(senderPlayers.get(TestUtils.generateRandomInt(0, senderPlayers.size())));
		}
		ArrayList<FightPlayer> receiverPlayers =
			new ArrayList<FightPlayer>(FightPlayerDataLoader.getInstance(ctx).getMap().values());
		if (!receiverPlayers.isEmpty()) {
			fight.setReceiverPlayer(receiverPlayers.get(TestUtils.generateRandomInt(0, receiverPlayers.size())));
		}
		fight.setCurrentElement(TestUtils.generateRandomInt(0,100));

		return fight;
	}

	public static boolean equals(Fight fight1, Fight fight2){
		boolean ret = true;
		Assert.assertNotNull(fight1);
		Assert.assertNotNull(fight2);
		if (fight1!=null && fight2 !=null){
			Assert.assertEquals(fight1.getId(), fight2.getId());
			if (fight1.getStarterFighters() != null
					&& fight2.getStarterFighters() != null) {
				Assert.assertEquals(fight1.getStarterFighters().size(),
					fight2.getStarterFighters().size());
				for (int i=0;i<fight1.getStarterFighters().size();i++){
					Assert.assertEquals(fight1.getStarterFighters().get(i).getId(),
								fight2.getStarterFighters().get(i).getId());
				}
			}
			if (fight1.getAlternatedFighters() != null
					&& fight2.getAlternatedFighters() != null) {
				Assert.assertEquals(fight1.getAlternatedFighters().size(),
					fight2.getAlternatedFighters().size());
				for (int i=0;i<fight1.getAlternatedFighters().size();i++){
					Assert.assertEquals(fight1.getAlternatedFighters().get(i).getId(),
								fight2.getAlternatedFighters().get(i).getId());
				}
			}
			if (fight1.getSurvivorFighters() != null
					&& fight2.getSurvivorFighters() != null) {
				Assert.assertEquals(fight1.getSurvivorFighters().size(),
					fight2.getSurvivorFighters().size());
				for (int i=0;i<fight1.getSurvivorFighters().size();i++){
					Assert.assertEquals(fight1.getSurvivorFighters().get(i).getId(),
								fight2.getSurvivorFighters().get(i).getId());
				}
			}
			if (fight1.getActions() != null
					&& fight2.getActions() != null) {
				Assert.assertEquals(fight1.getActions().size(),
					fight2.getActions().size());
				for (int i=0;i<fight1.getActions().size();i++){
					Assert.assertEquals(fight1.getActions().get(i).getId(),
								fight2.getActions().get(i).getId());
				}
			}
			if (fight1.getSenderPlayer() != null
					&& fight2.getSenderPlayer() != null) {
				Assert.assertEquals(fight1.getSenderPlayer().getId(),
						fight2.getSenderPlayer().getId());

			}
			if (fight1.getReceiverPlayer() != null
					&& fight2.getReceiverPlayer() != null) {
				Assert.assertEquals(fight1.getReceiverPlayer().getId(),
						fight2.getReceiverPlayer().getId());

			}
			Assert.assertEquals(fight1.getCurrentElement(), fight2.getCurrentElement());
		}

		return ret;
	}
}

