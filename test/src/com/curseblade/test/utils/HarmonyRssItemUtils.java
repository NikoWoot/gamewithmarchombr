/**************************************************************************
 * HarmonyRssItemUtils.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.utils;

import com.curseblade.test.utils.base.HarmonyRssItemUtilsBase;

public abstract class HarmonyRssItemUtils extends HarmonyRssItemUtilsBase {

}
