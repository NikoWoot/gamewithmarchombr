/**************************************************************************
 * NewsTestProviderBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.base;

import android.test.suitebuilder.annotation.SmallTest;

import com.curseblade.provider.NewsProviderAdapter;

import com.curseblade.data.NewsSQLiteAdapter;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;
import com.curseblade.entity.News;

import com.curseblade.fixture.NewsDataLoader;
import com.curseblade.fixture.NewsDataLoader;

import java.util.ArrayList;
import com.curseblade.test.utils.*;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import junit.framework.Assert;

/** News database test abstract class <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project with Harmony.
 * You should edit NewsTestDB class instead of this one or you will lose all your modifications.</i></b>
 */
public abstract class NewsTestProviderBase extends TestDBBase {
	protected Context ctx;

	protected NewsSQLiteAdapter adapter;

	protected News entity;
	protected ContentResolver provider;

	protected ArrayList<News> entities;

	protected int nbEntities = 0;
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();

		this.ctx = this.getMockContext();

		this.adapter = new NewsSQLiteAdapter(this.ctx);

		this.entities = new ArrayList<News>(NewsDataLoader.getInstance(this.ctx).getMap().values());
		if (this.entities.size()>0) {
			this.entity = this.entities.get(TestUtils.generateRandomInt(0,entities.size()-1));
		}

		this.nbEntities += NewsDataLoader.getInstance(this.ctx).getMap().size();
		this.provider = this.getMockContext().getContentResolver();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/** Test case Create Entity */
	@SmallTest
	public void testCreate() {
		Uri result = null;
		if (this.entity != null) {
			News news = NewsUtils.generateRandom(this.ctx);

			try {
				ContentValues values = this.adapter.itemToContentValues(news);
				values.remove(HarmonyRssItemSQLiteAdapter.COL_ID);
				result = this.provider.insert(NewsProviderAdapter.NEWS_URI, values);

			} catch (Exception e) {
				e.printStackTrace();
			}

			Assert.assertNotNull(result);
			Assert.assertTrue(Integer.valueOf(result.getEncodedPath().substring(result.getEncodedPath().lastIndexOf("/")+1)) > 0);
		}
	}

	/** Test case Read Entity */
	@SmallTest
	public void testRead() {
		News result = null;

		if (this.entity != null) {
			try {
				Cursor c = this.provider.query(Uri.parse(NewsProviderAdapter.NEWS_URI + "/" + this.entity.getId()), this.adapter.getCols(), null, null, null);
				c.moveToFirst();
				result = this.adapter.cursorToItem(c);
				c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			NewsUtils.equals(this.entity, result);
		}
	}

	/** Test case ReadAll Entity */
	@SmallTest
	public void testReadAll() {
		ArrayList<News> result = null;
		try {
			Cursor c = this.provider.query(NewsProviderAdapter.NEWS_URI, this.adapter.getCols(), null, null, null);
			result = this.adapter.cursorToItems(c);
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertNotNull(result);
		if (result != null) {
			Assert.assertEquals(result.size(), this.nbEntities);
		}
	}

	/** Test case Update Entity */
	@SmallTest
	public void testUpdate() {
		int result = -1;
		if (this.entity != null) {
			News news = NewsUtils.generateRandom(this.ctx);

			try {
				news.setId(this.entity.getId());

				ContentValues values = this.adapter.itemToContentValues(news);
				result = this.provider.update(
					Uri.parse(NewsProviderAdapter.NEWS_URI
						+ "/"
						+ news.getId()),
					values,
					null,
					null);

			} catch (Exception e) {
				e.printStackTrace();
			}

			Assert.assertTrue(result > 0);
		}
	}

	/** Test case UpdateAll Entity */
	@SmallTest
	public void testUpdateAll() {
		int result = -1;
		if (this.entities.size() > 0) {
			News news = NewsUtils.generateRandom(this.ctx);

			try {
				ContentValues values = this.adapter.itemToContentValues(news);
				values.remove(HarmonyRssItemSQLiteAdapter.COL_ID);

				result = this.provider.update(NewsProviderAdapter.NEWS_URI, values, null, null);
			} catch (Exception e) {
				e.printStackTrace();
			}

			Assert.assertEquals(result, this.nbEntities);
		}
	}

	/** Test case Delete Entity */
	@SmallTest
	public void testDelete() {
		int result = -1;
		if (this.entity != null) {
			try {
				result = this.provider.delete(Uri.parse(NewsProviderAdapter.NEWS_URI + "/" + this.entity.getId()), null, null);

			} catch (Exception e) {
				e.printStackTrace();
			}
			Assert.assertTrue(result >= 0);
		}

	}

	/** Test case DeleteAll Entity */
	@SmallTest
	public void testDeleteAll() {
		int result = -1;
		if (this.entities.size() > 0) {

			try {
				result = this.provider.delete(NewsProviderAdapter.NEWS_URI, null, null);

			} catch (Exception e) {
				e.printStackTrace();
			}

			Assert.assertEquals(result, this.nbEntities);
		}
	}
}
