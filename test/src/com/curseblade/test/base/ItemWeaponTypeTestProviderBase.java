/**************************************************************************
 * ItemWeaponTypeTestProviderBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.base;

import android.test.suitebuilder.annotation.SmallTest;

import com.curseblade.provider.ItemWeaponTypeProviderAdapter;

import com.curseblade.data.ItemWeaponTypeSQLiteAdapter;

import com.curseblade.entity.ItemWeaponType;

import com.curseblade.fixture.ItemWeaponTypeDataLoader;
import com.curseblade.fixture.ItemWeaponTypeDataLoader;

import java.util.ArrayList;
import com.curseblade.test.utils.*;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import junit.framework.Assert;

/** ItemWeaponType database test abstract class <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project with Harmony.
 * You should edit ItemWeaponTypeTestDB class instead of this one or you will lose all your modifications.</i></b>
 */
public abstract class ItemWeaponTypeTestProviderBase extends TestDBBase {
	protected Context ctx;

	protected ItemWeaponTypeSQLiteAdapter adapter;

	protected ItemWeaponType entity;
	protected ContentResolver provider;

	protected ArrayList<ItemWeaponType> entities;

	protected int nbEntities = 0;
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();

		this.ctx = this.getMockContext();

		this.adapter = new ItemWeaponTypeSQLiteAdapter(this.ctx);

		this.entities = new ArrayList<ItemWeaponType>(ItemWeaponTypeDataLoader.getInstance(this.ctx).getMap().values());
		if (this.entities.size()>0) {
			this.entity = this.entities.get(TestUtils.generateRandomInt(0,entities.size()-1));
		}

		this.nbEntities += ItemWeaponTypeDataLoader.getInstance(this.ctx).getMap().size();
		this.provider = this.getMockContext().getContentResolver();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/** Test case Create Entity */
	@SmallTest
	public void testCreate() {
		Uri result = null;
		if (this.entity != null) {
			ItemWeaponType itemWeaponType = ItemWeaponTypeUtils.generateRandom(this.ctx);

			try {
				ContentValues values = this.adapter.itemToContentValues(itemWeaponType);
				values.remove(ItemWeaponTypeSQLiteAdapter.COL_ID);
				result = this.provider.insert(ItemWeaponTypeProviderAdapter.ITEMWEAPONTYPE_URI, values);

			} catch (Exception e) {
				e.printStackTrace();
			}

			Assert.assertNotNull(result);
			Assert.assertTrue(Integer.valueOf(result.getEncodedPath().substring(result.getEncodedPath().lastIndexOf("/")+1)) > 0);
		}
	}

	/** Test case Read Entity */
	@SmallTest
	public void testRead() {
		ItemWeaponType result = null;

		if (this.entity != null) {
			try {
				Cursor c = this.provider.query(Uri.parse(ItemWeaponTypeProviderAdapter.ITEMWEAPONTYPE_URI + "/" + this.entity.getId()), this.adapter.getCols(), null, null, null);
				c.moveToFirst();
				result = this.adapter.cursorToItem(c);
				c.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			ItemWeaponTypeUtils.equals(this.entity, result);
		}
	}

	/** Test case ReadAll Entity */
	@SmallTest
	public void testReadAll() {
		ArrayList<ItemWeaponType> result = null;
		try {
			Cursor c = this.provider.query(ItemWeaponTypeProviderAdapter.ITEMWEAPONTYPE_URI, this.adapter.getCols(), null, null, null);
			result = this.adapter.cursorToItems(c);
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertNotNull(result);
		if (result != null) {
			Assert.assertEquals(result.size(), this.nbEntities);
		}
	}

	/** Test case Update Entity */
	@SmallTest
	public void testUpdate() {
		int result = -1;
		if (this.entity != null) {
			ItemWeaponType itemWeaponType = ItemWeaponTypeUtils.generateRandom(this.ctx);

			try {
				itemWeaponType.setId(this.entity.getId());

				ContentValues values = this.adapter.itemToContentValues(itemWeaponType);
				result = this.provider.update(
					Uri.parse(ItemWeaponTypeProviderAdapter.ITEMWEAPONTYPE_URI
						+ "/"
						+ itemWeaponType.getId()),
					values,
					null,
					null);

			} catch (Exception e) {
				e.printStackTrace();
			}

			Assert.assertTrue(result > 0);
		}
	}

	/** Test case UpdateAll Entity */
	@SmallTest
	public void testUpdateAll() {
		int result = -1;
		if (this.entities.size() > 0) {
			ItemWeaponType itemWeaponType = ItemWeaponTypeUtils.generateRandom(this.ctx);

			try {
				ContentValues values = this.adapter.itemToContentValues(itemWeaponType);
				values.remove(ItemWeaponTypeSQLiteAdapter.COL_ID);
				values.remove(ItemWeaponTypeSQLiteAdapter.COL_ID);

				result = this.provider.update(ItemWeaponTypeProviderAdapter.ITEMWEAPONTYPE_URI, values, null, null);
			} catch (Exception e) {
				e.printStackTrace();
			}

			Assert.assertEquals(result, this.nbEntities);
		}
	}

	/** Test case Delete Entity */
	@SmallTest
	public void testDelete() {
		int result = -1;
		if (this.entity != null) {
			try {
				result = this.provider.delete(Uri.parse(ItemWeaponTypeProviderAdapter.ITEMWEAPONTYPE_URI + "/" + this.entity.getId()), null, null);

			} catch (Exception e) {
				e.printStackTrace();
			}
			Assert.assertTrue(result >= 0);
		}

	}

	/** Test case DeleteAll Entity */
	@SmallTest
	public void testDeleteAll() {
		int result = -1;
		if (this.entities.size() > 0) {

			try {
				result = this.provider.delete(ItemWeaponTypeProviderAdapter.ITEMWEAPONTYPE_URI, null, null);

			} catch (Exception e) {
				e.printStackTrace();
			}

			Assert.assertEquals(result, this.nbEntities);
		}
	}
}
