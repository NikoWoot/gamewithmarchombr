/**************************************************************************
 * ItemWeaponTypeTestDBBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.test.base;

import android.test.suitebuilder.annotation.SmallTest;

import com.curseblade.data.ItemWeaponTypeSQLiteAdapter;
import com.curseblade.entity.ItemWeaponType;

import com.curseblade.fixture.ItemWeaponTypeDataLoader;

import java.util.ArrayList;

import com.curseblade.test.utils.*;
import android.content.Context;
import junit.framework.Assert;

/** ItemWeaponType database test abstract class <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project with Harmony.
 * You should edit ItemWeaponTypeTestDB class instead of this one or you will lose all your modifications.</i></b>
 */
public abstract class ItemWeaponTypeTestDBBase extends TestDBBase {
	protected Context ctx;

	protected ItemWeaponTypeSQLiteAdapter adapter;

	protected ItemWeaponType entity;
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();

		this.ctx = this.getMockContext();

		this.adapter = new ItemWeaponTypeSQLiteAdapter(this.ctx);
		this.adapter.open();

		ArrayList<ItemWeaponType> entities = new ArrayList<ItemWeaponType>(ItemWeaponTypeDataLoader.getInstance(this.ctx).getMap().values());
		if (entities.size()>0){
			this.entity = entities.get(TestUtils.generateRandomInt(0,entities.size()-1));
		}
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		this.adapter.close();

		super.tearDown();
	}

	/** Test case Create Entity */
	@SmallTest
	public void testCreate() {
		int result = -1;
		if (this.entity != null) {
			ItemWeaponType itemWeaponType = ItemWeaponTypeUtils.generateRandom(this.ctx);

			result = (int)this.adapter.insert(itemWeaponType);

			Assert.assertTrue(result >= 0);
		}
	}

	/** Test case Read Entity */
	@SmallTest
	public void testRead() {
		ItemWeaponType result = null;
		if (this.entity != null) {
			result = this.adapter.getByID(this.entity.getId()); // TODO Generate by @Id annotation

			ItemWeaponTypeUtils.equals(result, this.entity);
		}
	}

	/** Test case Update Entity */
	@SmallTest
	public void testUpdate() {
		int result = -1;
		if (this.entity != null) {
			ItemWeaponType itemWeaponType = ItemWeaponTypeUtils.generateRandom(this.ctx);
			itemWeaponType.setId(this.entity.getId());

			result = (int)this.adapter.update(itemWeaponType);

			Assert.assertTrue(result >= 0);
		}
	}

	/** Test case Update Entity */
	@SmallTest
	public void testDelete() {
		int result = -1;
		if (this.entity != null) {
			result = (int)this.adapter.remove(this.entity.getId());
			Assert.assertTrue(result >= 0);
		}
	}
}
