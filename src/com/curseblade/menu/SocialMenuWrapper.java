package com.curseblade.menu;

import com.actionbarsherlock.internal.view.menu.ActionMenuItem;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.ShareActionProvider;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.curseblade.start.mainFrags.PlayerFragment;
import com.curseblade.view.characterplayer.CharacterPlayerShowFragment;	
import com.curseblade.menu.base.MenuWrapperBase;

public class SocialMenuWrapper implements MenuWrapperBase{
	private MenuItem socialItem;
	private final static String menuName = "Share";
	/** Menu Visibility. */
	private boolean visible = true;

	@Override
	public void initializeMenu(Menu menu,
			FragmentActivity activity,
			Fragment fragment,
			Context ctx) {
		this.socialItem 	= menu.add(0, CursebladeMenu.SOCIAL , Menu.NONE, menuName);
		this.socialItem.setShowAsAction(ActionMenuItem.SHOW_AS_ACTION_IF_ROOM|ActionMenuItem.SHOW_AS_ACTION_WITH_TEXT);
		this.socialItem.setVisible(false);
	}

	@Override
	public void updateMenu(Menu menu, FragmentActivity activity,
			Fragment fragment, Context context) {
		int currentFragmentHashCode;
		if (fragment!=null) {
			currentFragmentHashCode = fragment.getClass().hashCode();
		} else {
			currentFragmentHashCode = -1;
		}
			
		if (currentFragmentHashCode == CharacterPlayerShowFragment.class.hashCode()
				|| currentFragmentHashCode == PlayerFragment.class.hashCode()){
			/** Getting the actionprovider associated with the menu item whose id is share */
	        ShareActionProvider mShareActionProvider = new ShareActionProvider(context);
	        
	        /** Getting the target intent */
	        Intent shareIntent = new Intent(Intent.ACTION_SEND);
	        shareIntent.setType("text/plain");
	        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "SUBJECT");
	        shareIntent.putExtra(Intent.EXTRA_TEXT, "Sample Content !!!");
	        
	        /** Setting a share intent */
	        if (shareIntent!=null)
	            mShareActionProvider.setShareIntent(shareIntent);
			
	        this.socialItem.setActionProvider(mShareActionProvider);
	        this.socialItem.setVisible(this.visible);
	    }
		
	}

	@Override
	public boolean dispatch(MenuItem item, Context ctx, Fragment fragment) {
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode,
			Intent data, Context context, Fragment fragment) {
	}

	@Override
	public void hide(Menu menu, FragmentActivity activity, Fragment fragment,
			Context ctx) {
		this.visible = false;
	}

	@Override
	public void show(Menu menu, FragmentActivity activity, Fragment fragment,
			Context ctx) {
		this.visible = true;
	}

	@Override
	public void clear(Menu menu,
			FragmentActivity activity,
			Fragment fragment,
			Context ctx) {
		menu.removeItem(CursebladeMenu.SOCIAL);
	}
}
