/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start.tutoFrags;

import com.curseblade.auth.activity.SignUpActivity;
import com.curseblade.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Fragment for last screen of TutoActivity
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class TutoFragment3 extends Fragment {

	/**
	 * {@inheritDoc}
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Inflate the view
		View v = inflater.inflate(R.layout.fragment_tuto3, null);

		// Search start button into the view
		Button button = (Button) v.findViewById(R.id.startButton);

		// Add an event on click on the button
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				
				TutoFragment3.this.getActivity().finish();
				
				Intent intent = new Intent(v.getContext(), SignUpActivity.class);
				startActivity(intent);
			}
		});

		return v;
	}
}
