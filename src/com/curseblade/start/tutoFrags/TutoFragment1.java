/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start.tutoFrags;

import com.curseblade.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment for first screen of TutoActivity
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class TutoFragment1 extends Fragment {

	/**
	 * {@inheritDoc}
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.fragment_tuto1, null);
	}
}
