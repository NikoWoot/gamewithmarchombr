/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start;

import java.util.ArrayList;

import com.viewpagerindicator.PageIndicator;
import com.viewpagerindicator.TitlePageIndicator;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import com.curseblade.R;
import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.start.mainFrags.InventoryFragment;
import com.curseblade.start.mainFrags.PlayerFragment;
import com.curseblade.start.mainFrags.ShopFragment;
import com.curseblade.start.mainFrags.SpellFragment;

/**
 * Activity for the main menu with Home, Campagne, Pvp and Tournament
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class MainActivity extends HarmonyFragmentActivity {

	private FragmentAdapter mAdapter;
	private ViewPager mPager;
	private PageIndicator mIndicator;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ArrayList<FragmentAdapterElement> elements = new ArrayList<FragmentAdapterElement>();
		elements.add(new FragmentAdapterElement(this
				.getString(R.string.shop_title), new ShopFragment()));
		elements.add(new FragmentAdapterElement(this
				.getString(R.string.home_title), new PlayerFragment()));
		elements.add(new FragmentAdapterElement(this
				.getString(R.string.inventory_title), new InventoryFragment()));
		elements.add(new FragmentAdapterElement(this
				.getString(R.string.spell_title), new SpellFragment()));

		mAdapter = new FragmentAdapter(getSupportFragmentManager(), elements);

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);

		// Set the second Page and not the first one
		mPager.setCurrentItem(1);

		mIndicator = (TitlePageIndicator) findViewById(R.id.indicator);
		mIndicator.setViewPager(mPager);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// super.onSaveInstanceState(outState);
	}

}
