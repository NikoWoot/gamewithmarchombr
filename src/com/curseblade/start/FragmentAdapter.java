/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start;

import java.util.ArrayList;
import java.util.List;

import com.viewpagerindicator.IconPagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Class for Fragment mecanism in FragmentedActivity
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class FragmentAdapter extends FragmentPagerAdapter implements
		IconPagerAdapter {

	private List<FragmentAdapterElement> fragmentElements;

	/**
	 * Default Constructor
	 * 
	 * @param fm
	 *            FragmentManager of my activity
	 * @param fragmentElements
	 *            Array of Fragments
	 */
	public FragmentAdapter(FragmentManager fm,
			ArrayList<FragmentAdapterElement> fragmentElements) {
		super(fm);

		this.fragmentElements = fragmentElements;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getIconResId(int index) {
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Fragment getItem(int position) {

		FragmentAdapterElement adapterResult = null;
		Fragment result = null;

		try {
			adapterResult = this.fragmentElements.get(position);

			if (adapterResult != null) {
				result = adapterResult.getFragment();
			}
		} catch (Exception ex) {

		}

		// Return selected Fragment
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCount() {
		return this.fragmentElements.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CharSequence getPageTitle(int position) {

		FragmentAdapterElement adapterResult = null;
		String result = null;

		try {
			adapterResult = this.fragmentElements.get(position);

			if (adapterResult != null) {
				result = adapterResult.getTitle();
			}
		} catch (Exception ex) {

		}

		// Return title of selected Fragment
		return result;

	}

}
