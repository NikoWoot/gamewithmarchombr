package com.curseblade.start;

import java.io.IOException;

import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.anddev.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
import org.anddev.andengine.ui.activity.BaseGameActivity;
import org.anddev.andengine.util.Debug;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

public class CharRepresentationActivity extends BaseGameActivity implements
		IAccelerometerListener {

	private Camera camera;
	private static final int CAMERA_LARGEUR = 480;
	private static final int CAMERA_HAUTEUR = 800;
	private BitmapTextureAtlas archerTextureAtk;
	private BitmapTextureAtlas archerTextureMov;
	private TiledTextureRegion archerAtk;
	private TiledTextureRegion archerMov;
	private AnimatedSprite animArcherAtk;
	private AnimatedSprite animArcherMov;
	private Sound peasant;
	private Sound gland;
	private Music myMusic;
	private Scene scene;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onLoadComplete() {
		// Start musique at start of avatar screen
		myMusic.play();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Engine onLoadEngine() {
		Engine engine;
		EngineOptions engineOptions;

		// Set vision field size of the map with camera and its position
		camera = new Camera(0, 0, CAMERA_LARGEUR, CAMERA_HAUTEUR);

		// Here we put the engine with camera and precise orientation of screen
		engineOptions = new EngineOptions(true, ScreenOrientation.PORTRAIT,
				new RatioResolutionPolicy(CAMERA_LARGEUR, CAMERA_HAUTEUR),
				camera);

		// Enable tu play music and sounds
		engineOptions.setNeedsMusic(true);
		engineOptions.setNeedsSound(true);

		engine = new Engine(engineOptions);

		return engine;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onLoadResources() {
		// Bitmap used to define page size of the sprite (thanks it, cuts images
		// at the good size)
		// Height and width have to be multiple of 2
		archerTextureAtk = new BitmapTextureAtlas(1024, 512,
				TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		archerTextureMov = new BitmapTextureAtlas(1024, 512,
				TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		// set container which store following animations
		archerAtk = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(archerTextureAtk, this,
						"gfx/Archer_Atk.png", 0, 0, 8, 1);
		archerMov = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(archerTextureMov, this,
						"gfx/Archer_Mov.png", 0, 0, 13, 1);
		getEngine().getTextureManager().loadTextures(archerTextureAtk);
		getEngine().getTextureManager().loadTextures(archerTextureMov);

		// set sounds
		try {
			peasant = SoundFactory.createSoundFromAsset(
					this.mEngine.getSoundManager(), this,
					"mfx/PeasantReady1.wav");
			gland = SoundFactory.createSoundFromAsset(
					this.mEngine.getSoundManager(), this,
					"mfx/PriestPissed4.wav");
		} catch (final IOException e) {
			Debug.e(e);
		}

		// set music
		MusicFactory.setAssetBasePath("mfx/");
		try {
			this.myMusic = MusicFactory.createMusicFromAsset(
					this.mEngine.getMusicManager(), this, "HordeFirepole.ogg");
			this.myMusic.setLooping(true);
		} catch (final IOException e) {
			Debug.e(e);
		}

		// create white ugly buttons and put it on the screen
		createControllers();

		// Activation of the accelerometer
		this.enableAccelerometerSensor(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Scene onLoadScene() {
		// 1 scene = 1 calque
		scene = new Scene();
		// Set position on screen
		animArcherMov = new AnimatedSprite(55, 63, archerMov);

		// time between each animations and if they have to loop
		animArcherMov.animate(120, true);

		// linking animation on scene
		scene.attachChild(animArcherMov);

		scene.setBackground(new ColorBackground(0.52f, 0.75f, 0.03f));

		return scene;
	}

	// Function to create white ugly buttons
	private void createControllers() {
		HUD yourHud = new HUD();

		// Pret a travailler!
		final Rectangle left = new Rectangle(20, 720, 60, 60) {
			public boolean onAreaTouched(TouchEvent touchEvent, float X, float Y) {
				if (touchEvent.isActionUp()) {
					peasant.play();
				}
				return true;
			};
		};

		// Home music
		final Rectangle play = new Rectangle(100, 730, 40, 40) {
			public boolean onAreaTouched(TouchEvent touchEvent, float X, float Y) {
				if (touchEvent.isActionUp()) {
					if (myMusic.isPlaying()) {
						myMusic.pause();
						myMusic.seekTo(0);
					} else {
						myMusic.play();
					}
				}
				return true;
			};
		};

		// Gland
		final Rectangle right = new Rectangle(160, 720, 60, 60) {
			public boolean onAreaTouched(TouchEvent touchEvent, float X, float Y) {
				if (touchEvent.isActionUp()) {
					gland.play();
				}
				return true;
			};
		};

		// Attack avatar animation
		final Rectangle atk = new Rectangle(320, 720, 60, 60) {
			public boolean onAreaTouched(TouchEvent touchEvent, float X, float Y) {
				if (touchEvent.isActionUp()) {
					// flush screen
					scene.detachChildren();
					scene.clearChildScene();

					// set archer position on the screen
					animArcherAtk = new AnimatedSprite(50, 50, archerAtk);

					// time between each animations and if they have to loop
					animArcherAtk.animate(120, false);

					// linking animation on scene
					scene.attachChild(animArcherAtk);
				}
				return true;
			};
		};

		// Move avatar animation
		final Rectangle mov = new Rectangle(400, 720, 60, 60) {
			public boolean onAreaTouched(TouchEvent touchEvent, float X, float Y) {
				if (touchEvent.isActionUp()) {
					// flush screen
					scene.detachChildren();
					scene.clearChildScene();

					// set archer position on the screen
					animArcherMov = new AnimatedSprite(55, 63, archerMov);

					// time between each animations and if they have to loop
					animArcherMov.animate(120, false);

					// linking animation on scene
					scene.attachChild(animArcherMov);
				}
				return true;
			};
		};

		// Registration of all area that need tu be "touch sensitive"
		yourHud.registerTouchArea(left);
		yourHud.registerTouchArea(play);
		yourHud.registerTouchArea(right);
		yourHud.registerTouchArea(atk);
		yourHud.registerTouchArea(mov);

		// Put all those area in HUD pool of camera which is like a minimap or
		// other interface button of a game
		yourHud.attachChild(left);
		yourHud.attachChild(play);
		yourHud.attachChild(right);
		yourHud.attachChild(atk);
		yourHud.attachChild(mov);

		camera.setHUD(yourHud);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onAccelerometerChanged(AccelerometerData myAccelerometerData) 
	{
		// Function to move avatar with accelerometer only if he is not out of the screen size
		if (animArcherMov.getX() + myAccelerometerData.getX() < CAMERA_LARGEUR
				- animArcherMov.getWidth()
				&& animArcherMov.getX() + myAccelerometerData.getX() > 0) {
			animArcherMov.setPosition(animArcherMov.getX()
					+ myAccelerometerData.getX(), animArcherMov.getY());
		}
		
		if (animArcherMov.getY() + myAccelerometerData.getY() < CAMERA_HAUTEUR
				- animArcherMov.getHeight()
				&& animArcherMov.getY() + myAccelerometerData.getY() > 0) {
			animArcherMov.setPosition(animArcherMov.getX(),
					animArcherMov.getY() + myAccelerometerData.getY());
		}
	}
}