/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start.mainFrags;

import com.curseblade.CursebladeApplication;
import com.curseblade.HarmonyHomeActivity;
import com.curseblade.R;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.provider.utils.CharacterPlayerProviderUtils;
import com.curseblade.start.CampaignActivity;
import com.curseblade.start.CharRepresentationActivity;
import com.curseblade.start.FightGameActivity;
import com.curseblade.start.PvpActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Class for PlayerFragment into MainActivity
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class PlayerFragment extends HarmonyFragment implements OnClickListener {

	/**
	 * {@inheritDoc}
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_player, null);

		// Add events on button
		this.assignOnClickEventOnButtons(v, this, R.id.btn_betatest,
				R.id.btn_pvp, R.id.btn_campaign, R.id.btn_avatar);

		// Get my Character
		CharacterPlayerProviderUtils u = new CharacterPlayerProviderUtils(this.getActivity());
		CharacterPlayer player = u.query(CursebladeApplication.selectedCharacterId);
		
		// Set my surname and my level in my view
		TextView txtView = (TextView) v.findViewById(R.id.txtview_profilpseudo);
		txtView.setText(player.getPseudo());
		
		txtView = (TextView) v.findViewById(R.id.txtview_profillevel);
		txtView.setText(player.getLevel().toString());
		
		return v;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onClick(View v) {

		Class<? extends Activity> nextClass = null;

		// SwitchCase to know senderButton
		switch (v.getId()) {

		case R.id.btn_betatest:
			nextClass = HarmonyHomeActivity.class;
			break;

		case R.id.btn_pvp:
			nextClass = FightGameActivity.class;
			break;

		case R.id.btn_campaign:
			nextClass = CampaignActivity.class;
			break;
			
		case R.id.btn_avatar:
			nextClass = CharRepresentationActivity.class;
			break;
		}

		// If an nextClass has selected
		if (nextClass != null) {

			// Start New Activity
			Intent intent = new Intent(v.getContext(), nextClass);
			startActivity(intent);
		}

	}
}
