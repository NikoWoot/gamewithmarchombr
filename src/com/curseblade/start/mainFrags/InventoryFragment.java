/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start.mainFrags;

import com.curseblade.CursebladeApplication;
import com.curseblade.criterias.ItemArmuryCriterias;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.ItemArmurySQLiteAdapter;
import com.curseblade.harmony.view.HarmonyListFragment;
import com.curseblade.provider.ItemArmuryProviderAdapter;
import com.curseblade.view.itemarmury.ItemArmuryListFragment;
import com.curseblade.view.itemarmury.ItemArmuryListLoader;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Class to show inventory on my character
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class InventoryFragment extends ItemArmuryListFragment 
			 implements HarmonyListFragment.OnLoadCallback {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = super.onCreateView(inflater, container, savedInstanceState);
		
		this.loadCallback = this;
		
		this.setHasOptionsMenu(false);
		
		
		return v;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		Loader<Cursor> result = null;
		ItemArmuryCriterias crit = new ItemArmuryCriterias(GroupType.AND);
		crit.add(ItemArmurySQLiteAdapter.COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL,
					CursebladeApplication.selectedCharacterId.toString());

		result = new ItemArmuryListLoader(this.getActivity(),
				ItemArmuryProviderAdapter.ITEMARMURY_URI,
				ItemArmurySQLiteAdapter.ALIASED_COLS, crit, null);

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onListLoaded() {
		// Hack by Niko (stop crash fragment)
		
	}

}
