/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start.mainFrags;

import com.curseblade.R;
import com.curseblade.harmony.view.HarmonyFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Class for TournamentFragment into MainActivity
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class TournamentFragment extends HarmonyFragment {

	/**
	 * {@inheritDoc}
	 */
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.fragment_tournament, null);
	}
}
