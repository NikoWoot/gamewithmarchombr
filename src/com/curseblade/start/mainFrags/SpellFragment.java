/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.start.mainFrags;

import com.curseblade.CursebladeApplication;
import com.curseblade.criterias.SpellCriterias;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.harmony.view.HarmonyListFragment;
import com.curseblade.provider.SpellProviderAdapter;
import com.curseblade.view.spell.SpellListFragment;
import com.curseblade.view.spell.SpellListLoader;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Class to represent all spells of my character
 * @author Nicolas GAUTIER
 *
 */
public class SpellFragment extends SpellListFragment
			implements HarmonyListFragment.OnLoadCallback{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View v = super.onCreateView(inflater, container, savedInstanceState);
		
		this.loadCallback = this;
		
		this.setHasOptionsMenu(false);
				
		return v;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		Loader<Cursor> result = null;
		SpellCriterias crit = new SpellCriterias(GroupType.AND);
		crit.add(SpellSQLiteAdapter.COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL,
					CursebladeApplication.selectedCharacterId.toString());

		result = new SpellListLoader(this.getActivity(),
				SpellProviderAdapter.SPELL_URI,
				SpellSQLiteAdapter.ALIASED_COLS, crit, null);

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onListLoaded() {
		// Hack by Niko (stop crash fragment)
		
	}
}
