/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start;

import com.curseblade.CursebladeApplicationBase;
import com.curseblade.R;
import com.curseblade.auth.AccountUtils;
import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.notification.NotificationUtils;

import android.accounts.Account;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

/**
 * Activity for SplashScreen
 * 
 * @author Alexandre Lebrun
 * 
 */
public class SplashScreenActivity extends HarmonyFragmentActivity {

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 3000;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity

				Class<?> nextActivity = null;

				// Else

				try {
					AccountUtils accountUtils = new AccountUtils(
							getBaseContext());

					Account myAccount = accountUtils.getFirstAccount();

					// If i already registred, Go to HomeActivity
					if (myAccount != null) {
						nextActivity = MainActivity.class;

					} else { // Go to TutorialActivity

						nextActivity = TutorialActivity.class;

						NotificationUtils notifUtils = new NotificationUtils(SplashScreenActivity.this);
						notifUtils.createShortNotification(
								SplashScreenActivity.this
										.getString(R.string.notification_welcome_shortmessage),
								"CurseBlade",
								SplashScreenActivity.this
										.getString(R.string.notification_welcome_longmessage));
					}

					// Launch the nextActivity
					Intent i = new Intent(SplashScreenActivity.this,
							nextActivity);
					startActivity(i);
					finish();

				} catch (Exception e) {
					Log.e(CursebladeApplicationBase.TAG, e.getMessage());
				}
			}
		}, SPLASH_TIME_OUT);
	}
}
