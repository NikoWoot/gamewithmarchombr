/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.curseblade.CursebladeApplication;
import com.curseblade.R;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.Fight;
import com.curseblade.entity.FightAction;
import com.curseblade.entity.FightActionType;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.Spell;
import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.job.FightSimpleIA;
import com.curseblade.provider.utils.CharacterPlayerProviderUtils;

/**
 * Class to execute fight
 * 
 * @author Alexandre Lebrun
 * 
 */
public class FightGameActivity extends HarmonyFragmentActivity implements
		OnClickListener {

	private Fight fight;
	private CharacterPlayer realCharacter;
	private CharacterPlayer computerCharacter;
	private FightPlayer realFightPlayer;
	private FightPlayer computerFightPlayer;
	private FightSimpleIA computerPlayerIa;

	// Count by team
	private int firstTeamCount = 1;
	private int secondTeamCount = 1;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);

		this.setContentView(R.layout.activity_fightgame);
		LinearLayout yourLayout = (LinearLayout) findViewById(R.id.spell_bar);
		
		// Get my Character
		CharacterPlayerProviderUtils adaptChara = new CharacterPlayerProviderUtils(this);

		this.realCharacter = adaptChara
				.query(CursebladeApplication.selectedCharacterId);
		this.computerCharacter = adaptChara
				.query(CursebladeApplication.debugCharacterId);

		this.fight = new Fight();
		this.realFightPlayer = new FightPlayer(this.realCharacter, this.fight,
				1);
		this.computerFightPlayer = new FightPlayer(this.computerCharacter,
				this.fight, 2);

		this.fight.getStarterFighters().add(this.realFightPlayer);
		this.fight.getStarterFighters().add(this.computerFightPlayer);

		this.computerPlayerIa = new FightSimpleIA(fight, computerFightPlayer);

		// Prepare the fight
		try {
			this.fight.prepare();
		} catch (Exception e) {
			Log.e(CursebladeApplication.TAG, e.getMessage());
		}

		// Launch the first Turn
		this.fight.startTurn();

		// Init btnList
		for (int i = 0; i < realCharacter.getEquipedSpells().size(); i++) {
			Button btn = new Button(this);

			btn.setText(realCharacter.getEquipedSpells().get(i).getName());
			btn.setTag(i);
			btn.setOnClickListener(this);

			yourLayout.addView(btn);
		}

		//Initialize number of alive player in each team
		firstTeamCount = 1;
		secondTeamCount = 1;

		//Set behavior of the weapon attack button
		Button btnWeap = (Button) findViewById(R.id.fight_atk_btn);
		btnWeap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				turnResolve(v, "Weapon");
			}
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onClick(View v) {
		turnResolve(v, "Spell");
	}

	/**
	 * Add logs on TextView
	 */
	private void addLog() {
		//Reset of display values
		TextView nfo = (TextView) findViewById(R.id.nfo);
		nfo.setText("");

		//loop all passed action in this fight and display it
		for (FightAction fa : fight.getActions()) {

			if (fa.getActionType() == FightActionType.ACTION_FIGHTER) {
				String str = String.format(this.getString(R.string.fight_log),
						fa.getSender().getPseudo(), fa.getAfterValue()
								.toString(), fa.getReceiver().getPseudo());
				str += ".\n";
				nfo.append(str);
			}
		}
		
		//Display a congratulation message if any team have one
		if (firstTeamCount == 0) {
			nfo.append(fight.getSurvivorFighters().get(1).getPseudo()
					+ " a gagner la partie.");
			Toast.makeText(
					this,
					String.format(this.getString(R.string.toast_victory), fight
							.getSurvivorFighters().get(1).getPseudo()),
					Toast.LENGTH_LONG).show();
		} else if (secondTeamCount == 0) {
			nfo.append(fight.getSurvivorFighters().get(0).getPseudo()
					+ " a gagner la partie.");
			Toast.makeText(
					this,
					String.format(this.getString(R.string.toast_victory), fight
							.getSurvivorFighters().get(0).getPseudo()),
					Toast.LENGTH_LONG).show();
		}
	}

	/**
	 * Make all action after chosing a spell. Attack ennemy and the, he attack too
	 * @param v : Often contain the button who raise an attack(spell or weapon or other attack)
	 * @param buttonType : Informe function of the type of button rise the attack
	 */
	private void turnResolve(View v, String buttonType) {
		if (firstTeamCount != 0 && secondTeamCount != 0) {
			// Get button, spellId (by buttonTag) and Spell (by Spellid)
			Button b = (Button) v;

			// MyCharacter cast a spell
			try {
				if (buttonType == "Spell") {
					int spellId = (Integer) b.getTag();
					Spell launchedSpell = fight.getSenderPlayer()
							.getBaseSpells().get(spellId);
					fight.attack(fight.getSenderPlayer(),
							fight.getReceiverPlayer(), launchedSpell);
				} else if (buttonType == "Weapon") {
					fight.attack(fight.getSenderPlayer(), fight
							.getReceiverPlayer(), fight.getSenderPlayer()
							.getBaseWeapon());
				}
			} catch (Exception e) {
				Log.e(CursebladeApplication.TAG, e.getMessage());
			}

			// End Turn
			fight.endTurn();

			// Actualize the teamCount
			firstTeamCount = fight.countTeam(1, fight.getAlternatedFighters());
			secondTeamCount = fight.countTeam(2, fight.getAlternatedFighters());

			// Test if fight is finish
			if (firstTeamCount != 0 && secondTeamCount != 0) {
				// Start turn
				fight.startTurn();

			} else {
				// End of fight
				fight.endFight();
			}

			// ComputerPlayer in Action
			if (firstTeamCount != 0 && secondTeamCount != 0
					&& fight.getSenderPlayer() == this.computerFightPlayer) {

				// Execute turn of the botPlayer
				try {
					this.computerPlayerIa.playTurn();
				} catch (Exception e) {
					Log.e(CursebladeApplication.TAG, e.getMessage());
				}

				// EndTurn of bot
				fight.endTurn();

				// Actualize the teamCount
				firstTeamCount = fight.countTeam(1,
						fight.getAlternatedFighters());
				secondTeamCount = fight.countTeam(2,
						fight.getAlternatedFighters());

				// Test if fight is finish
				if (firstTeamCount != 0 && secondTeamCount != 0) {
					// Start turn
					fight.startTurn();

				} else {
					// End of fight
					fight.endFight();
				}
			}
		}
		
		// Add log on TextView
		addLog();
	}
}
