/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;

import com.curseblade.R;
import com.curseblade.start.tutoFrags.TutoFragment1;
import com.curseblade.start.tutoFrags.TutoFragment2;
import com.curseblade.start.tutoFrags.TutoFragment3;
import com.viewpagerindicator.LinePageIndicator;
import com.viewpagerindicator.PageIndicator;

/**
 * Fragmented Activity with TutoFragment1, TutoFragment2, TutoFragment3
 * 
 * @author Alexandre Lebrun
 * 
 */
public class TutorialActivity extends FragmentActivity {

	private FragmentAdapter mAdapter;
	private ViewPager mPager;
	private PageIndicator mIndicator;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tutorial);

		ArrayList<FragmentAdapterElement> elements = new ArrayList<FragmentAdapterElement>();
		elements.add(new FragmentAdapterElement("Page1", new TutoFragment1()));
		elements.add(new FragmentAdapterElement("Page2", new TutoFragment2()));
		elements.add(new FragmentAdapterElement("Page3", new TutoFragment3()));

		mAdapter = new FragmentAdapter(getSupportFragmentManager(), elements);

		mPager = (ViewPager) findViewById(R.id.tutoPager);
		mPager.setAdapter(mAdapter);

		mIndicator = (LinePageIndicator) findViewById(R.id.tutoIndicator);
		mIndicator.setViewPager(mPager);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		// super.onSaveInstanceState(outState);
	}
}
