/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
 **/
package com.curseblade.start;

import android.support.v4.app.Fragment;

/**
 * Class to symbolize an Fragment in my activity (Title + Fragment)
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class FragmentAdapterElement {

	private String title;
	private Fragment fragment;

	/**
	 * Default constructor
	 * 
	 * @param title
	 *            title of my fragment
	 * @param fragment
	 *            fragment for my view
	 */
	public FragmentAdapterElement(String title, Fragment fragment) {
		this.title = title;
		this.fragment = fragment;
	}

	/**
	 * 
	 * @return Return title of my fragment
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 
	 * @return Return Fragment for my adapter
	 */
	public Fragment getFragment() {
		return fragment;
	}

}
