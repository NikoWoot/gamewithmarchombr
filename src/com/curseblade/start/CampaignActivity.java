/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.start;

import com.curseblade.R;
import com.curseblade.harmony.view.HarmonyFragmentActivity;

import android.os.Bundle;

/**
 * Class for CampaignFragment into MainActivity
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class CampaignActivity extends HarmonyFragmentActivity {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		this.setContentView(R.layout.fragment_campaignitem);
	}
	


}
