/**************************************************************************
 * FightCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.Fight;

/**
 * FightCriterias.
 *
 * This class can help you forge requests for your Fight Entity.
 * For more details, see CriteriasBase.
 */
public class FightCriterias extends CriteriasBase<Fight> {
	/** String to parcel fightCriteria. */
	public static final String PARCELABLE =
			"fightCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public FightCriterias(final GroupType type) {
		super(type);
	}
}
