/**************************************************************************
 * FightPlayerCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.FightPlayer;

/**
 * FightPlayerCriterias.
 *
 * This class can help you forge requests for your FightPlayer Entity.
 * For more details, see CriteriasBase.
 */
public class FightPlayerCriterias extends CriteriasBase<FightPlayer> {
	/** String to parcel fightPlayerCriteria. */
	public static final String PARCELABLE =
			"fightPlayerCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public FightPlayerCriterias(final GroupType type) {
		super(type);
	}
}
