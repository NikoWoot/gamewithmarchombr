/**************************************************************************
 * CriteriaValue.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias.base.value;

import java.io.Serializable;

import com.curseblade.criterias.base.ICriteria;

/**
 * Criteria value.
 * Abstract class for Values classes.
 */
public abstract class CriteriaValue implements ICriteria, Serializable {

}

