/**************************************************************************
 * FightActionCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.FightAction;

/**
 * FightActionCriterias.
 *
 * This class can help you forge requests for your FightAction Entity.
 * For more details, see CriteriasBase.
 */
public class FightActionCriterias extends CriteriasBase<FightAction> {
	/** String to parcel fightActionCriteria. */
	public static final String PARCELABLE =
			"fightActionCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public FightActionCriterias(final GroupType type) {
		super(type);
	}
}
