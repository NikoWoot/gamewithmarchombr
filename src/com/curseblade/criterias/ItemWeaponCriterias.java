/**************************************************************************
 * ItemWeaponCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.ItemWeapon;

/**
 * ItemWeaponCriterias.
 *
 * This class can help you forge requests for your ItemWeapon Entity.
 * For more details, see CriteriasBase.
 */
public class ItemWeaponCriterias extends CriteriasBase<ItemWeapon> {
	/** String to parcel itemWeaponCriteria. */
	public static final String PARCELABLE =
			"itemWeaponCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public ItemWeaponCriterias(final GroupType type) {
		super(type);
	}
}
