/**************************************************************************
 * NewsCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.News;

/**
 * NewsCriterias.
 *
 * This class can help you forge requests for your News Entity.
 * For more details, see CriteriasBase.
 */
public class NewsCriterias extends CriteriasBase<News> {
	/** String to parcel newsCriteria. */
	public static final String PARCELABLE =
			"newsCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public NewsCriterias(final GroupType type) {
		super(type);
	}
}
