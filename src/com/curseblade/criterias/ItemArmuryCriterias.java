/**************************************************************************
 * ItemArmuryCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.ItemArmury;

/**
 * ItemArmuryCriterias.
 *
 * This class can help you forge requests for your ItemArmury Entity.
 * For more details, see CriteriasBase.
 */
public class ItemArmuryCriterias extends CriteriasBase<ItemArmury> {
	/** String to parcel itemArmuryCriteria. */
	public static final String PARCELABLE =
			"itemArmuryCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public ItemArmuryCriterias(final GroupType type) {
		super(type);
	}
}
