/**************************************************************************
 * SpellCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.Spell;

/**
 * SpellCriterias.
 *
 * This class can help you forge requests for your Spell Entity.
 * For more details, see CriteriasBase.
 */
public class SpellCriterias extends CriteriasBase<Spell> {
	/** String to parcel spellCriteria. */
	public static final String PARCELABLE =
			"spellCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public SpellCriterias(final GroupType type) {
		super(type);
	}
}
