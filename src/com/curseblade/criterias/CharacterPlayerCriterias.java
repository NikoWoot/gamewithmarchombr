/**************************************************************************
 * CharacterPlayerCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.CharacterPlayer;

/**
 * CharacterPlayerCriterias.
 *
 * This class can help you forge requests for your CharacterPlayer Entity.
 * For more details, see CriteriasBase.
 */
public class CharacterPlayerCriterias extends CriteriasBase<CharacterPlayer> {
	/** String to parcel characterPlayerCriteria. */
	public static final String PARCELABLE =
			"characterPlayerCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public CharacterPlayerCriterias(final GroupType type) {
		super(type);
	}
}
