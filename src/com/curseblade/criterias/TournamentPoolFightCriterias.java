/**************************************************************************
 * TournamentPoolFightCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.TournamentPoolFight;

/**
 * TournamentPoolFightCriterias.
 *
 * This class can help you forge requests for your TournamentPoolFight Entity.
 * For more details, see CriteriasBase.
 */
public class TournamentPoolFightCriterias extends CriteriasBase<TournamentPoolFight> {
	/** String to parcel tournamentPoolFightCriteria. */
	public static final String PARCELABLE =
			"tournamentPoolFightCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public TournamentPoolFightCriterias(final GroupType type) {
		super(type);
	}
}
