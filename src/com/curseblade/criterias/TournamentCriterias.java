/**************************************************************************
 * TournamentCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.Tournament;

/**
 * TournamentCriterias.
 *
 * This class can help you forge requests for your Tournament Entity.
 * For more details, see CriteriasBase.
 */
public class TournamentCriterias extends CriteriasBase<Tournament> {
	/** String to parcel tournamentCriteria. */
	public static final String PARCELABLE =
			"tournamentCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public TournamentCriterias(final GroupType type) {
		super(type);
	}
}
