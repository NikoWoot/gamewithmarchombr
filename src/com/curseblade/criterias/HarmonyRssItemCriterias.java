/**************************************************************************
 * HarmonyRssItemCriterias.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.criterias;

import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.Criteria;

import com.curseblade.entity.HarmonyRssItem;

/**
 * HarmonyRssItemCriterias.
 *
 * This class can help you forge requests for your HarmonyRssItem Entity.
 * For more details, see CriteriasBase.
 */
public class HarmonyRssItemCriterias extends CriteriasBase<HarmonyRssItem> {
	/** String to parcel harmonyRssItemCriteria. */
	public static final String PARCELABLE =
			"harmonyRssItemCriteriaPARCEL";

	/**
	 * Constructor.
	 * @param type The Criteria's GroupType
	 */
	public HarmonyRssItemCriterias(final GroupType type) {
		super(type);
	}
}
