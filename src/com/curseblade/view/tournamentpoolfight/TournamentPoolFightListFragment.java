/**************************************************************************
 * TournamentPoolFightListFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournamentpoolfight;

import java.util.ArrayList;

import com.curseblade.criterias.TournamentPoolFightCriterias;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;
import com.curseblade.menu.CrudCreateMenuWrapper.CrudCreateMenuInterface;
import com.curseblade.provider.TournamentPoolFightProviderAdapter;
import com.curseblade.harmony.view.HarmonyListFragment;
import com.google.android.pinnedheader.headerlist.PinnedHeaderListView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import com.curseblade.R;
import com.curseblade.entity.TournamentPoolFight;

/** TournamentPoolFight list fragment.
 *
 * This fragment gives you an interface to list all your TournamentPoolFights.
 *
 * @see android.app.Fragment
 */
public class TournamentPoolFightListFragment
		extends HarmonyListFragment<TournamentPoolFight>
		implements CrudCreateMenuInterface {

	/** The adapter which handles list population. */
	protected TournamentPoolFightListAdapter mAdapter;

	@Override
	public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {

		final View view =
				inflater.inflate(R.layout.fragment_tournamentpoolfight_list,
						null);

		this.initializeHackCustomList(view,
				R.id.tournamentpoolfightProgressLayout,
				R.id.tournamentpoolfightListContainer);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Give some text to display if there is no data.  In a real
		// application this would come from a resource.
		this.setEmptyText(
				getString(
						R.string.tournamentpoolfight_empty_list));

		// Create an empty adapter we will use to display the loaded data.
		((PinnedHeaderListView) this.getListView())
					.setPinnedHeaderEnabled(false);
		this.mAdapter = new TournamentPoolFightListAdapter(this.getActivity());

		// Start out with a progress indicator.
		this.setListShown(false);

		// Prepare the loader.  Either re-connect with an existing one,
		// or start a new one.
		getLoaderManager().initLoader(0, null, this);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		/* Do click action inside your fragment here. */
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		Loader<Cursor> result = null;
		TournamentPoolFightCriterias crit = null;
		if (bundle != null) {
			crit = (TournamentPoolFightCriterias) bundle.get(
						TournamentPoolFightCriterias.PARCELABLE);
		}

		if (crit != null) {
			result = new TournamentPoolFightListLoader(this.getActivity(),
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
				crit,
				null);
		} else {
			result = new TournamentPoolFightListLoader(this.getActivity(),
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);
		}
		return result;
	}

	@Override
	public void onLoadFinished(
			Loader<Cursor> loader,
			Cursor data) {

		// Set the new data in the adapter.
		data.setNotificationUri(this.getActivity().getContentResolver(),
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI);

		ArrayList<TournamentPoolFight> users = new TournamentPoolFightSQLiteAdapter(
				this.getActivity()).cursorToItems(data);
		this.mAdapter.setNotifyOnChange(false);
		this.mAdapter.setData(
				new TournamentPoolFightListAdapter
					.TournamentPoolFightSectionIndexer(users));
		this.mAdapter.setNotifyOnChange(true);
		this.mAdapter.notifyDataSetChanged();
		this.mAdapter.setPinnedPartitionHeadersEnabled(false);
		this.mAdapter.setSectionHeaderDisplayEnabled(false);

		if (this.getListAdapter() == null) {
			this.setListAdapter(this.mAdapter);
		}

		// The list should now be shown.
		if (this.isResumed()) {
			this.setListShown(true);
		} else {
			this.setListShownNoAnimation(true);
		}

		super.onLoadFinished(loader, data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		// Clear the data in the adapter.
		this.mAdapter.clear();
	}

	@Override
	public void onClickAdd() {
		Intent intent = new Intent(this.getActivity(),
					TournamentPoolFightCreateActivity.class);
		this.startActivity(intent);
	}

}
