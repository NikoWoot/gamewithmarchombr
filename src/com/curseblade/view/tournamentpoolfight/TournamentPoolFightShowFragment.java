/**************************************************************************
 * TournamentPoolFightShowFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournamentpoolfight;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.curseblade.R;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.harmony.view.DeleteDialog;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.view.MultiLoader;
import com.curseblade.harmony.view.MultiLoader.UriLoadedCallback;
import com.curseblade.menu.CrudEditDeleteMenuWrapper.CrudEditDeleteMenuInterface;
import com.curseblade.provider.utils.TournamentPoolFightProviderUtils;
import com.curseblade.provider.TournamentPoolFightProviderAdapter;

/** TournamentPoolFight show fragment.
 *
 * This fragment gives you an interface to show a TournamentPoolFight.
 * 
 * @see android.app.Fragment
 */
public class TournamentPoolFightShowFragment
		extends HarmonyFragment
		implements CrudEditDeleteMenuInterface,
				DeleteDialog.DeleteDialogCallback {
	/** Model data. */
	protected TournamentPoolFight model;

	/** DeleteCallback. */
	protected DeleteCallback deleteCallback;

	/* This entity's fields views */
	/** winner View. */
	protected TextView winnerView;
	/** parentPool View. */
	protected TextView parentPoolView;
	/** leftPool View. */
	protected TextView leftPoolView;
	/** rightPool View. */
	protected TextView rightPoolView;
	/** Data layout. */
	protected RelativeLayout dataLayout;
	/** Text view for no TournamentPoolFight. */
	protected TextView emptyText;


    /** Initialize view of curr.fields.
     *
     * @param view The layout inflating
     */
    protected void initializeComponent(final View view) {
		this.winnerView =
			(TextView) view.findViewById(
					R.id.tournamentpoolfight_winner);
		this.parentPoolView =
			(TextView) view.findViewById(
					R.id.tournamentpoolfight_parentpool);
		this.leftPoolView =
			(TextView) view.findViewById(
					R.id.tournamentpoolfight_leftpool);
		this.rightPoolView =
			(TextView) view.findViewById(
					R.id.tournamentpoolfight_rightpool);

		this.dataLayout =
				(RelativeLayout) view.findViewById(
						R.id.tournamentpoolfight_data_layout);
		this.emptyText =
				(TextView) view.findViewById(
						R.id.tournamentpoolfight_empty);
    }

    /** Load data from model to fields view. */
    public void loadData() {
    	if (this.model != null) {

    		this.dataLayout.setVisibility(View.VISIBLE);
    		this.emptyText.setVisibility(View.GONE);


		if (this.model.getWinner() != null) {
			this.winnerView.setText(
					String.valueOf(this.model.getWinner().getId()));
		}
		if (this.model.getParentPool() != null) {
			this.parentPoolView.setText(
					String.valueOf(this.model.getParentPool().getId()));
		}
		if (this.model.getLeftPool() != null) {
			this.leftPoolView.setText(
					String.valueOf(this.model.getLeftPool().getId()));
		}
		if (this.model.getRightPool() != null) {
			this.rightPoolView.setText(
					String.valueOf(this.model.getRightPool().getId()));
		}
		} else {
    		this.dataLayout.setVisibility(View.GONE);
    		this.emptyText.setVisibility(View.VISIBLE);
    	}
    }

    @Override
    public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {

    	// Inflate the layout for this fragment
        final View view =
        		inflater.inflate(
        				R.layout.fragment_tournamentpoolfight_show,
        				container,
        				false);
        
        if (this.getActivity() instanceof DeleteCallback) {
        	this.deleteCallback = (DeleteCallback) this.getActivity();
        }

        this.initializeComponent(view);
        
        final Intent intent =  getActivity().getIntent();
        this.update((TournamentPoolFight) intent.getParcelableExtra(TournamentPoolFight.PARCEL));

        return view;
    }

	/**
	 * Updates the view with the given data.
	 *
	 * @param item The TournamentPoolFight to get the data from.
	 */
	public void update(TournamentPoolFight item) {
    	this.model = item;
    	
		this.loadData();
		
		if (this.model != null) {
			MultiLoader loader = new MultiLoader(this);
			String baseUri = 
					TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI 
					+ "/" 
					+ this.model.getId();

			loader.addUri(Uri.parse(baseUri), new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					TournamentPoolFightShowFragment.this.onTournamentPoolFightLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/winner"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					TournamentPoolFightShowFragment.this.onWinnerLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/parentpool"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					TournamentPoolFightShowFragment.this.onParentPoolLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/leftpool"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					TournamentPoolFightShowFragment.this.onLeftPoolLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/rightpool"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					TournamentPoolFightShowFragment.this.onRightPoolLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.init();
		}
    }

	/**
	 * Called when the entity has been loaded.
	 * 
	 * @param c The cursor of this entity
	 */
	public void onTournamentPoolFightLoaded(Cursor c) {
		if (c.getCount() > 0) {
			c.moveToFirst();
			new TournamentPoolFightSQLiteAdapter(getActivity()).cursorToItem(
						c,
						this.model);
			this.loadData();
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onWinnerLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setWinner(
							new FightPlayerSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setWinner(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onParentPoolLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setParentPool(
							new TournamentPoolFightSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setParentPool(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onLeftPoolLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setLeftPool(
							new TournamentPoolFightSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setLeftPool(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onRightPoolLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setRightPool(
							new TournamentPoolFightSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setRightPool(null);
					this.loadData();
			}
		}
	}

	/**
	 * Calls the TournamentPoolFightEditActivity.
	 */
	@Override
	public void onClickEdit() {
		final Intent intent = new Intent(getActivity(),
									TournamentPoolFightEditActivity.class);
		Bundle extras = new Bundle();
		extras.putParcelable("TournamentPoolFight", this.model);
		intent.putExtras(extras);

		this.getActivity().startActivity(intent);
	}

	/**
	 * Shows a confirmation dialog.
	 */
	@Override
	public void onClickDelete() {
		new DeleteDialog(this.getActivity(), this).show();
	}

	@Override
	public void onDeleteDialogClose(boolean ok) {
		if (ok) {
			new DeleteTask(this.getActivity(), this.model).execute();
		}
	}
	
	/** 
	 * Called when delete task is done.
	 */	
	public void onPostDelete() {
		if (this.deleteCallback != null) {
			this.deleteCallback.onItemDeleted();
		}
	}

	/**
	 * This class will remove the entity into the DB.
	 * It runs asynchronously.
	 */
	private class DeleteTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private Context ctx;
		/** Entity to delete. */
		private TournamentPoolFight item;

		/**
		 * Constructor of the task.
		 * @param item The entity to remove from DB
		 * @param ctx A context to build TournamentPoolFightSQLiteAdapter
		 */
		public DeleteTask(final Context ctx,
					final TournamentPoolFight item) {
			super();
			this.ctx = ctx;
			this.item = item;
		}

		@Override
		protected Integer doInBackground(Void... params) {
			int result = -1;

			result = new TournamentPoolFightProviderUtils(this.ctx)
					.delete(this.item);

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result >= 0) {
				TournamentPoolFightShowFragment.this.onPostDelete();
			}
			super.onPostExecute(result);
		}
		
		

	}
	
	/**
	 * Callback for item deletion.
	 */ 
	public interface DeleteCallback {
		/** Called when current item has been deleted. */
		void onItemDeleted();
	}
}

