/**************************************************************************
 * TournamentPoolFightShowActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournamentpoolfight;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.view.tournamentpoolfight.TournamentPoolFightShowFragment.DeleteCallback;

import android.os.Bundle;

/** TournamentPoolFight show Activity.
 *
 * This only contains a TournamentPoolFightShowFragment.
 *
 * @see android.app.Activity
 */
public class TournamentPoolFightShowActivity 
		extends HarmonyFragmentActivity 
		implements DeleteCallback {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_tournamentpoolfight_show);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onItemDeleted() {
		this.finish();
	}
}
