/**************************************************************************
 * TournamentPoolFightListAdapter.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournamentpoolfight;

import java.util.List;

import com.curseblade.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.google.android.pinnedheader.SelectionItemView;
import com.google.android.pinnedheader.headerlist.HeaderAdapter;
import com.google.android.pinnedheader.headerlist.HeaderSectionIndexer;
import com.google.android.pinnedheader.headerlist.PinnedHeaderListView.PinnedHeaderAdapter;
import com.curseblade.entity.TournamentPoolFight;

/**
 * List adapter for TournamentPoolFight entity.
 */
public class TournamentPoolFightListAdapter
		extends HeaderAdapter<TournamentPoolFight>
		implements PinnedHeaderAdapter {
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public TournamentPoolFightListAdapter(Context ctx) {
		super(ctx);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param resource The resource
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public TournamentPoolFightListAdapter(Context context,
			int resource,
			int textViewResourceId,
			List<TournamentPoolFight> objects) {
		super(context, resource, textViewResourceId, objects);
	}

	/**
     * Constructor.
     *
	 * @param context The context
	 * @param resource The resource
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public TournamentPoolFightListAdapter(Context context,
			int resource,
			int textViewResourceId,
			TournamentPoolFight[] objects) {
		super(context, resource, textViewResourceId, objects);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param resource The resource
	 * @param textViewResourceId The resource id of the text view
	 */
	public TournamentPoolFightListAdapter(Context context,
			int resource,
			int textViewResourceId) {
		super(context, resource, textViewResourceId);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public TournamentPoolFightListAdapter(Context context,
			int textViewResourceId,
			List<TournamentPoolFight> objects) {
		super(context, textViewResourceId, objects);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public TournamentPoolFightListAdapter(Context context,
			int textViewResourceId,
			TournamentPoolFight[] objects) {
		super(context, textViewResourceId, objects);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param textViewResourceId The resource id of the text view
	 */
	public TournamentPoolFightListAdapter(Context context,
			int textViewResourceId) {
		super(context, textViewResourceId);
	}

	/** Holder row. */
	private static class ViewHolder extends SelectionItemView {

		/**
		 * Constructor.
		 *
		 * @param context The context
		 */
		public ViewHolder(Context context) {
			this(context, null);
		}
		
		/**
		 * Constructor.
		 *
		 * @param context The context
		 * @param attrs The attribute set
		 */
		public ViewHolder(Context context, AttributeSet attrs) {
			super(context, attrs, R.layout.row_tournamentpoolfight);
		}

		/** Populate row with a TournamentPoolFight.
		 *
		 * @param model TournamentPoolFight data
		 */
		public void populate(final TournamentPoolFight model) {
			View convertView = this.getInnerLayout();
			TextView winnerView =
				(TextView) convertView.findViewById(
						R.id.row_tournamentpoolfight_winner);
			TextView parentPoolView =
				(TextView) convertView.findViewById(
						R.id.row_tournamentpoolfight_parentpool);
			TextView leftPoolView =
				(TextView) convertView.findViewById(
						R.id.row_tournamentpoolfight_leftpool);
			TextView rightPoolView =
				(TextView) convertView.findViewById(
						R.id.row_tournamentpoolfight_rightpool);


			winnerView.setText(
					String.valueOf(model.getWinner().getId()));
			parentPoolView.setText(
					String.valueOf(model.getParentPool().getId()));
			leftPoolView.setText(
					String.valueOf(model.getLeftPool().getId()));
			rightPoolView.setText(
					String.valueOf(model.getRightPool().getId()));
		}
	}

	/** Section indexer for this entity's list. */
	public static class TournamentPoolFightSectionIndexer
					extends HeaderSectionIndexer<TournamentPoolFight>
					implements SectionIndexer {

		/**
		 * Constructor.
		 * @param items The items of the indexer
		 */
		public TournamentPoolFightSectionIndexer(List<TournamentPoolFight> items) {
			super(items);
		}
		
		@Override
		protected String getHeaderText(TournamentPoolFight item) {
			return "Your entity's header name here";
		}
	}

	@Override
    protected View bindView(View itemView,
				int partition,
				TournamentPoolFight item,
				int position) {
    	final ViewHolder view;
    	
    	if (itemView != null) {
    		view = (ViewHolder) itemView;
    	} else {
    		view = new ViewHolder(this.getContext());
		}

    	if (!((HarmonyFragmentActivity) this.getContext()).isDualMode()) {
    		view.setActivatedStateSupported(false);
		}
    	
    	view.populate(item);
        this.bindSectionHeaderAndDivider(view, position);
        
        return view;
    }

	@Override
	public int getPosition(TournamentPoolFight item) {
		int result = -1;
		if (item != null) {
			for (int i = 0; i < this.getCount(); i++) {
				if (item.getId() == this.getItem(i).getId()) {
					result = i;
				}
			}
		}
		return result;
	}
}
