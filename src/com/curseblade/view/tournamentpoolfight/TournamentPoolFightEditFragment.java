/**************************************************************************
 * TournamentPoolFightEditFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournamentpoolfight;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.database.sqlite.SQLiteException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.entity.FightPlayer;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;

import com.curseblade.harmony.widget.SingleEntityWidget;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;

import com.curseblade.provider.utils.TournamentPoolFightProviderUtils;
import com.curseblade.provider.utils.FightPlayerProviderUtils;

/** TournamentPoolFight create fragment.
 *
 * This fragment gives you an interface to edit a TournamentPoolFight.
 *
 * @see android.app.Fragment
 */
public class TournamentPoolFightEditFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected TournamentPoolFight model = new TournamentPoolFight();

	/** curr.fields View. */
	/** The winner chooser component. */
	protected SingleEntityWidget winnerWidget;
	/** The winner Adapter. */
	protected SingleEntityWidget.EntityAdapter<FightPlayer>
			winnerAdapter;
	/** The parentPool chooser component. */
	protected SingleEntityWidget parentPoolWidget;
	/** The parentPool Adapter. */
	protected SingleEntityWidget.EntityAdapter<TournamentPoolFight>
			parentPoolAdapter;
	/** The leftPool chooser component. */
	protected SingleEntityWidget leftPoolWidget;
	/** The leftPool Adapter. */
	protected SingleEntityWidget.EntityAdapter<TournamentPoolFight>
			leftPoolAdapter;
	/** The rightPool chooser component. */
	protected SingleEntityWidget rightPoolWidget;
	/** The rightPool Adapter. */
	protected SingleEntityWidget.EntityAdapter<TournamentPoolFight>
			rightPoolAdapter;

	/** Initialize view of curr.fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(View view) {
		this.winnerAdapter =
				new SingleEntityWidget.EntityAdapter<FightPlayer>() {
			@Override
			public String entityToString(FightPlayer item) {
				return String.valueOf(item.getId());
			}
		};
		this.winnerWidget =
			(SingleEntityWidget) view.findViewById(R.id.tournamentpoolfight_winner_button);
		this.winnerWidget.setAdapter(this.winnerAdapter);
		this.parentPoolAdapter =
				new SingleEntityWidget.EntityAdapter<TournamentPoolFight>() {
			@Override
			public String entityToString(TournamentPoolFight item) {
				return String.valueOf(item.getId());
			}
		};
		this.parentPoolWidget =
			(SingleEntityWidget) view.findViewById(R.id.tournamentpoolfight_parentpool_button);
		this.parentPoolWidget.setAdapter(this.parentPoolAdapter);
		this.leftPoolAdapter =
				new SingleEntityWidget.EntityAdapter<TournamentPoolFight>() {
			@Override
			public String entityToString(TournamentPoolFight item) {
				return String.valueOf(item.getId());
			}
		};
		this.leftPoolWidget =
			(SingleEntityWidget) view.findViewById(R.id.tournamentpoolfight_leftpool_button);
		this.leftPoolWidget.setAdapter(this.leftPoolAdapter);
		this.rightPoolAdapter =
				new SingleEntityWidget.EntityAdapter<TournamentPoolFight>() {
			@Override
			public String entityToString(TournamentPoolFight item) {
				return String.valueOf(item.getId());
			}
		};
		this.rightPoolWidget =
			(SingleEntityWidget) view.findViewById(R.id.tournamentpoolfight_rightpool_button);
		this.rightPoolWidget.setAdapter(this.rightPoolAdapter);
	}

	/** Load data from model to curr.fields view. */
	public void loadData() {


		new LoadTask(this).execute();
	}

	/** Save data from curr.fields view to model. */
	public void saveData() {

		this.model.setWinner(this.winnerAdapter.getSelectedItem());

		this.model.setParentPool(this.parentPoolAdapter.getSelectedItem());

		this.model.setLeftPool(this.leftPoolAdapter.getSelectedItem());

		this.model.setRightPool(this.rightPoolAdapter.getSelectedItem());

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}
	@Override
	public View onCreateView(
				LayoutInflater inflater,
				ViewGroup container,
				Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		final View view =
				inflater.inflate(R.layout.fragment_tournamentpoolfight_edit,
						container,
						false);

		final Intent intent =  getActivity().getIntent();
		this.model = (TournamentPoolFight) intent.getParcelableExtra(
				TournamentPoolFight.PARCEL);

		this.initializeComponent(view);
		this.loadData();

		return view;
	}

	/**
	 * This class will update the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class EditTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to update. */
		private final TournamentPoolFight entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public EditTask(final TournamentPoolFightEditFragment fragment,
					final TournamentPoolFight entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.tournamentpoolfight_progress_save_title),
					this.ctx.getString(
							R.string.tournamentpoolfight_progress_save_message));
		}

		@Override
		protected Integer doInBackground(Void... params) {
			Integer result = -1;

			try {
				result = new TournamentPoolFightProviderUtils(this.ctx).update(
					this.entity);
			} catch (SQLiteException e) {
				Log.e("TournamentPoolFightEditFragment", e.getMessage());
			}

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);

			if (result > 0) {
				final HarmonyFragmentActivity activity =
						(HarmonyFragmentActivity) this.ctx;
				activity.setResult(HarmonyFragmentActivity.RESULT_OK);
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(this.ctx.getString(
						R.string.tournamentpoolfight_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
																int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}


	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private TournamentPoolFightEditFragment fragment;
		/** winner list. */
		private ArrayList<FightPlayer> winnerList;
		/** parentPool list. */
		private ArrayList<TournamentPoolFight> parentPoolList;
		/** leftPool list. */
		private ArrayList<TournamentPoolFight> leftPoolList;
		/** rightPool list. */
		private ArrayList<TournamentPoolFight> rightPoolList;

		/**
		 * Constructor of the task.
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final TournamentPoolFightEditFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
				this.ctx.getString(
					R.string.tournamentpoolfight_progress_load_relations_title),
				this.ctx.getString(
					R.string.tournamentpoolfight_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			this.winnerList = 
				new FightPlayerProviderUtils(this.ctx).queryAll();
			this.parentPoolList = 
				new TournamentPoolFightProviderUtils(this.ctx).queryAll();
			this.leftPoolList = 
				new TournamentPoolFightProviderUtils(this.ctx).queryAll();
			this.rightPoolList = 
				new TournamentPoolFightProviderUtils(this.ctx).queryAll();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			this.fragment.onWinnerLoaded(this.winnerList);
			this.fragment.onParentPoolLoaded(this.parentPoolList);
			this.fragment.onLeftPoolLoaded(this.leftPoolList);
			this.fragment.onRightPoolLoaded(this.rightPoolList);

			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new EditTask(this, this.model).execute();
		}
	}

	/**
	 * Called when winner have been loaded.
	 * @param items The loaded items
	 */
	protected void onWinnerLoaded(ArrayList<FightPlayer> items) {
		this.winnerAdapter.loadData(items);
		
		for (FightPlayer item : items) {
			if (item.getId() == this.model.getWinner().getId()) {
				this.winnerAdapter.selectItem(item);
			}
		}
	}
	/**
	 * Called when parentPool have been loaded.
	 * @param items The loaded items
	 */
	protected void onParentPoolLoaded(ArrayList<TournamentPoolFight> items) {
		this.parentPoolAdapter.loadData(items);
		
		for (TournamentPoolFight item : items) {
			if (item.getId() == this.model.getParentPool().getId()) {
				this.parentPoolAdapter.selectItem(item);
			}
		}
	}
	/**
	 * Called when leftPool have been loaded.
	 * @param items The loaded items
	 */
	protected void onLeftPoolLoaded(ArrayList<TournamentPoolFight> items) {
		this.leftPoolAdapter.loadData(items);
		
		for (TournamentPoolFight item : items) {
			if (item.getId() == this.model.getLeftPool().getId()) {
				this.leftPoolAdapter.selectItem(item);
			}
		}
	}
	/**
	 * Called when rightPool have been loaded.
	 * @param items The loaded items
	 */
	protected void onRightPoolLoaded(ArrayList<TournamentPoolFight> items) {
		this.rightPoolAdapter.loadData(items);
		
		for (TournamentPoolFight item : items) {
			if (item.getId() == this.model.getRightPool().getId()) {
				this.rightPoolAdapter.selectItem(item);
			}
		}
	}
}
