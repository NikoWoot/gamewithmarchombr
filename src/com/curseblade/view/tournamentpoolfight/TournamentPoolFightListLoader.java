/**************************************************************************
 * TournamentPoolFightListLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournamentpoolfight;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.CursorLoader;

import com.curseblade.criterias.TournamentPoolFightCriterias;

/**
 * TournamentPoolFight Loader.
 */
public class TournamentPoolFightListLoader
				extends CursorLoader {

	/**
	 * Constructor.
	 * @param ctx context
	 * @param crit TournamentPoolFightCriterias
	 */
	public TournamentPoolFightListLoader(
			final Context ctx,
			final TournamentPoolFightCriterias crit) {
		super(ctx);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param uri The URI associated with this loader
	 * @param projection The projection to use
	 * @param selection The selection
	 * @param selectionArgs The selection Args
	 * @param sortOrder The sort order
	 */
	public TournamentPoolFightListLoader(
					Context ctx,
					Uri uri,
					String[] projection,
					String selection,
					String[] selectionArgs,
					String sortOrder) {
		super(ctx,
				uri,
				projection,
				selection,
				selectionArgs,
				sortOrder);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param uri The URI associated with this loader
	 * @param projection The projection to use
	 * @param criterias TournamentPoolFightCriterias
	 * @param sortOrder The sort order
	 */
	public TournamentPoolFightListLoader(
					Context ctx,
					Uri uri,
					String[] projection,
					TournamentPoolFightCriterias criterias,
					String sortOrder) {
		super(ctx,
				uri,
				projection,
				criterias.toSQLiteSelection(),
				criterias.toSQLiteSelectionArgs(),
				sortOrder);
	}
}
