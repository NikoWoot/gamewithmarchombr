/**************************************************************************
 * ItemWeaponEditFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.itemweapon;



import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.database.sqlite.SQLiteException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.entity.ItemWeaponType;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;

import com.curseblade.harmony.widget.EnumSpinner;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;

import com.curseblade.provider.utils.ItemWeaponProviderUtils;

/** ItemWeapon create fragment.
 *
 * This fragment gives you an interface to edit a ItemWeapon.
 *
 * @see android.app.Fragment
 */
public class ItemWeaponEditFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected ItemWeapon model = new ItemWeapon();

	/** curr.fields View. */
	/** name View. */
	protected EditText nameView;
	/** baseAttack View. */
	protected EditText baseAttackView;
	/** weaponType View. */
	protected EnumSpinner weaponTypeView;

	/** Initialize view of curr.fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(View view) {
		this.nameView = (EditText) view.findViewById(
				R.id.itemweapon_name);
		this.baseAttackView = (EditText) view.findViewById(
				R.id.itemweapon_baseattack);
		this.weaponTypeView = (EnumSpinner) view.findViewById(
				R.id.itemweapon_weapontype);
		this.weaponTypeView.setEnum(ItemWeaponType.class);
	}

	/** Load data from model to curr.fields view. */
	public void loadData() {

		if (this.model.getName() != null) {
			this.nameView.setText(this.model.getName());
		}
		if (this.model.getBaseAttack() != null) {
			this.baseAttackView.setText(String.valueOf(this.model.getBaseAttack()));
		}
		if (this.model.getWeaponType() != null) {
			this.weaponTypeView.setSelectedItem(this.model.getWeaponType());
		}


	}

	/** Save data from curr.fields view to model. */
	public void saveData() {

		this.model.setName(this.nameView.getEditableText().toString());

		this.model.setBaseAttack(Double.parseDouble(
					this.baseAttackView.getEditableText().toString()));

		this.model.setWeaponType((ItemWeaponType) this.weaponTypeView.getSelectedItem());

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

		if (Strings.isNullOrEmpty(
					this.nameView.getText().toString().trim())) {
			error = R.string.itemweapon_name_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.baseAttackView.getText().toString().trim())) {
			error = R.string.itemweapon_baseattack_invalid_field_error;
		}
	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}
	@Override
	public View onCreateView(
				LayoutInflater inflater,
				ViewGroup container,
				Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		final View view =
				inflater.inflate(R.layout.fragment_itemweapon_edit,
						container,
						false);

		final Intent intent =  getActivity().getIntent();
		this.model = (ItemWeapon) intent.getParcelableExtra(
				ItemWeapon.PARCEL);

		this.initializeComponent(view);
		this.loadData();

		return view;
	}

	/**
	 * This class will update the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class EditTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to update. */
		private final ItemWeapon entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public EditTask(final ItemWeaponEditFragment fragment,
					final ItemWeapon entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.itemweapon_progress_save_title),
					this.ctx.getString(
							R.string.itemweapon_progress_save_message));
		}

		@Override
		protected Integer doInBackground(Void... params) {
			Integer result = -1;

			try {
				result = new ItemWeaponProviderUtils(this.ctx).update(
					this.entity);
			} catch (SQLiteException e) {
				Log.e("ItemWeaponEditFragment", e.getMessage());
			}

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);

			if (result > 0) {
				final HarmonyFragmentActivity activity =
						(HarmonyFragmentActivity) this.ctx;
				activity.setResult(HarmonyFragmentActivity.RESULT_OK);
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(this.ctx.getString(
						R.string.itemweapon_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
																int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}



	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new EditTask(this, this.model).execute();
		}
	}

}
