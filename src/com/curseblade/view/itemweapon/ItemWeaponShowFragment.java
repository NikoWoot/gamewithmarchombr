/**************************************************************************
 * ItemWeaponShowFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.itemweapon;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.curseblade.R;
import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.harmony.view.DeleteDialog;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.view.MultiLoader;
import com.curseblade.harmony.view.MultiLoader.UriLoadedCallback;
import com.curseblade.menu.CrudEditDeleteMenuWrapper.CrudEditDeleteMenuInterface;
import com.curseblade.provider.utils.ItemWeaponProviderUtils;
import com.curseblade.provider.ItemWeaponProviderAdapter;

/** ItemWeapon show fragment.
 *
 * This fragment gives you an interface to show a ItemWeapon.
 * 
 * @see android.app.Fragment
 */
public class ItemWeaponShowFragment
		extends HarmonyFragment
		implements CrudEditDeleteMenuInterface,
				DeleteDialog.DeleteDialogCallback {
	/** Model data. */
	protected ItemWeapon model;

	/** DeleteCallback. */
	protected DeleteCallback deleteCallback;

	/* This entity's fields views */
	/** name View. */
	protected TextView nameView;
	/** baseAttack View. */
	protected TextView baseAttackView;
	/** weaponType View. */
	protected TextView weaponTypeView;
	/** Data layout. */
	protected RelativeLayout dataLayout;
	/** Text view for no ItemWeapon. */
	protected TextView emptyText;


    /** Initialize view of curr.fields.
     *
     * @param view The layout inflating
     */
    protected void initializeComponent(final View view) {
		this.nameView =
			(TextView) view.findViewById(
					R.id.itemweapon_name);
		this.baseAttackView =
			(TextView) view.findViewById(
					R.id.itemweapon_baseattack);
		this.weaponTypeView =
			(TextView) view.findViewById(
					R.id.itemweapon_weapontype);

		this.dataLayout =
				(RelativeLayout) view.findViewById(
						R.id.itemweapon_data_layout);
		this.emptyText =
				(TextView) view.findViewById(
						R.id.itemweapon_empty);
    }

    /** Load data from model to fields view. */
    public void loadData() {
    	if (this.model != null) {

    		this.dataLayout.setVisibility(View.VISIBLE);
    		this.emptyText.setVisibility(View.GONE);


		if (this.model.getName() != null) {
			this.nameView.setText(this.model.getName());
		}
		if (this.model.getBaseAttack() != null) {
			this.baseAttackView.setText(String.valueOf(this.model.getBaseAttack()));
		}
		if (this.model.getWeaponType() != null) {
			this.weaponTypeView.setText(this.model.getWeaponType().toString());
		}
		} else {
    		this.dataLayout.setVisibility(View.GONE);
    		this.emptyText.setVisibility(View.VISIBLE);
    	}
    }

    @Override
    public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {

    	// Inflate the layout for this fragment
        final View view =
        		inflater.inflate(
        				R.layout.fragment_itemweapon_show,
        				container,
        				false);
        
        if (this.getActivity() instanceof DeleteCallback) {
        	this.deleteCallback = (DeleteCallback) this.getActivity();
        }

        this.initializeComponent(view);
        
        final Intent intent =  getActivity().getIntent();
        this.update((ItemWeapon) intent.getParcelableExtra(ItemWeapon.PARCEL));

        return view;
    }

	/**
	 * Updates the view with the given data.
	 *
	 * @param item The ItemWeapon to get the data from.
	 */
	public void update(ItemWeapon item) {
    	this.model = item;
    	
		this.loadData();
		
		if (this.model != null) {
			MultiLoader loader = new MultiLoader(this);
			String baseUri = 
					ItemWeaponProviderAdapter.ITEMWEAPON_URI 
					+ "/" 
					+ this.model.getId();

			loader.addUri(Uri.parse(baseUri), new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					ItemWeaponShowFragment.this.onItemWeaponLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.init();
		}
    }

	/**
	 * Called when the entity has been loaded.
	 * 
	 * @param c The cursor of this entity
	 */
	public void onItemWeaponLoaded(Cursor c) {
		if (c.getCount() > 0) {
			c.moveToFirst();
			new ItemWeaponSQLiteAdapter(getActivity()).cursorToItem(
						c,
						this.model);
			this.loadData();
		}
	}

	/**
	 * Calls the ItemWeaponEditActivity.
	 */
	@Override
	public void onClickEdit() {
		final Intent intent = new Intent(getActivity(),
									ItemWeaponEditActivity.class);
		Bundle extras = new Bundle();
		extras.putParcelable("ItemWeapon", this.model);
		intent.putExtras(extras);

		this.getActivity().startActivity(intent);
	}

	/**
	 * Shows a confirmation dialog.
	 */
	@Override
	public void onClickDelete() {
		new DeleteDialog(this.getActivity(), this).show();
	}

	@Override
	public void onDeleteDialogClose(boolean ok) {
		if (ok) {
			new DeleteTask(this.getActivity(), this.model).execute();
		}
	}
	
	/** 
	 * Called when delete task is done.
	 */	
	public void onPostDelete() {
		if (this.deleteCallback != null) {
			this.deleteCallback.onItemDeleted();
		}
	}

	/**
	 * This class will remove the entity into the DB.
	 * It runs asynchronously.
	 */
	private class DeleteTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private Context ctx;
		/** Entity to delete. */
		private ItemWeapon item;

		/**
		 * Constructor of the task.
		 * @param item The entity to remove from DB
		 * @param ctx A context to build ItemWeaponSQLiteAdapter
		 */
		public DeleteTask(final Context ctx,
					final ItemWeapon item) {
			super();
			this.ctx = ctx;
			this.item = item;
		}

		@Override
		protected Integer doInBackground(Void... params) {
			int result = -1;

			result = new ItemWeaponProviderUtils(this.ctx)
					.delete(this.item);

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result >= 0) {
				ItemWeaponShowFragment.this.onPostDelete();
			}
			super.onPostExecute(result);
		}
		
		

	}
	
	/**
	 * Callback for item deletion.
	 */ 
	public interface DeleteCallback {
		/** Called when current item has been deleted. */
		void onItemDeleted();
	}
}

