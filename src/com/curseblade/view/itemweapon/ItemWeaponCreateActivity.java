/**************************************************************************
 * ItemWeaponCreateActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.itemweapon;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;

import android.os.Bundle;

/** 
 * ItemWeapon create Activity.
 *
 * This only contains a ItemWeaponCreateFragment.
 *
 * @see android.app.Activity
 */
public class ItemWeaponCreateActivity extends HarmonyFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_itemweapon_create);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
