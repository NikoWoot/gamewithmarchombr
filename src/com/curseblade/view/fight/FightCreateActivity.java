/**************************************************************************
 * FightCreateActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fight;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;

import android.os.Bundle;

/** 
 * Fight create Activity.
 *
 * This only contains a FightCreateFragment.
 *
 * @see android.app.Activity
 */
public class FightCreateActivity extends HarmonyFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_fight_create);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
