/**************************************************************************
 * FightShowFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fight;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.curseblade.R;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.data.FightActionSQLiteAdapter;
import com.curseblade.entity.Fight;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.FightAction;
import com.curseblade.harmony.view.DeleteDialog;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.view.MultiLoader;
import com.curseblade.harmony.view.MultiLoader.UriLoadedCallback;
import com.curseblade.menu.CrudEditDeleteMenuWrapper.CrudEditDeleteMenuInterface;
import com.curseblade.provider.utils.FightProviderUtils;
import com.curseblade.provider.FightProviderAdapter;

/** Fight show fragment.
 *
 * This fragment gives you an interface to show a Fight.
 * 
 * @see android.app.Fragment
 */
public class FightShowFragment
		extends HarmonyFragment
		implements CrudEditDeleteMenuInterface,
				DeleteDialog.DeleteDialogCallback {
	/** Model data. */
	protected Fight model;

	/** DeleteCallback. */
	protected DeleteCallback deleteCallback;

	/* This entity's fields views */
	/** starterFighters View. */
	protected TextView starterFightersView;
	/** alternatedFighters View. */
	protected TextView alternatedFightersView;
	/** survivorFighters View. */
	protected TextView survivorFightersView;
	/** actions View. */
	protected TextView actionsView;
	/** senderPlayer View. */
	protected TextView senderPlayerView;
	/** receiverPlayer View. */
	protected TextView receiverPlayerView;
	/** currentElement View. */
	protected TextView currentElementView;
	/** Data layout. */
	protected RelativeLayout dataLayout;
	/** Text view for no Fight. */
	protected TextView emptyText;


    /** Initialize view of curr.fields.
     *
     * @param view The layout inflating
     */
    protected void initializeComponent(final View view) {
		this.starterFightersView =
			(TextView) view.findViewById(
					R.id.fight_starterfighters);
		this.alternatedFightersView =
			(TextView) view.findViewById(
					R.id.fight_alternatedfighters);
		this.survivorFightersView =
			(TextView) view.findViewById(
					R.id.fight_survivorfighters);
		this.actionsView =
			(TextView) view.findViewById(
					R.id.fight_actions);
		this.senderPlayerView =
			(TextView) view.findViewById(
					R.id.fight_senderplayer);
		this.receiverPlayerView =
			(TextView) view.findViewById(
					R.id.fight_receiverplayer);
		this.currentElementView =
			(TextView) view.findViewById(
					R.id.fight_currentelement);

		this.dataLayout =
				(RelativeLayout) view.findViewById(
						R.id.fight_data_layout);
		this.emptyText =
				(TextView) view.findViewById(
						R.id.fight_empty);
    }

    /** Load data from model to fields view. */
    public void loadData() {
    	if (this.model != null) {

    		this.dataLayout.setVisibility(View.VISIBLE);
    		this.emptyText.setVisibility(View.GONE);


		if (this.model.getStarterFighters() != null) {
			String starterFightersValue = "";
			for (FightPlayer item : this.model.getStarterFighters()) {
				starterFightersValue += item.getId() + ",";
			}
			this.starterFightersView.setText(starterFightersValue);
		}
		if (this.model.getAlternatedFighters() != null) {
			String alternatedFightersValue = "";
			for (FightPlayer item : this.model.getAlternatedFighters()) {
				alternatedFightersValue += item.getId() + ",";
			}
			this.alternatedFightersView.setText(alternatedFightersValue);
		}
		if (this.model.getSurvivorFighters() != null) {
			String survivorFightersValue = "";
			for (FightPlayer item : this.model.getSurvivorFighters()) {
				survivorFightersValue += item.getId() + ",";
			}
			this.survivorFightersView.setText(survivorFightersValue);
		}
		if (this.model.getActions() != null) {
			String actionsValue = "";
			for (FightAction item : this.model.getActions()) {
				actionsValue += item.getId() + ",";
			}
			this.actionsView.setText(actionsValue);
		}
		if (this.model.getSenderPlayer() != null) {
			this.senderPlayerView.setText(
					String.valueOf(this.model.getSenderPlayer().getId()));
		}
		if (this.model.getReceiverPlayer() != null) {
			this.receiverPlayerView.setText(
					String.valueOf(this.model.getReceiverPlayer().getId()));
		}
		if (this.model.getCurrentElement() != null) {
			this.currentElementView.setText(String.valueOf(this.model.getCurrentElement()));
		}
		} else {
    		this.dataLayout.setVisibility(View.GONE);
    		this.emptyText.setVisibility(View.VISIBLE);
    	}
    }

    @Override
    public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {

    	// Inflate the layout for this fragment
        final View view =
        		inflater.inflate(
        				R.layout.fragment_fight_show,
        				container,
        				false);
        
        if (this.getActivity() instanceof DeleteCallback) {
        	this.deleteCallback = (DeleteCallback) this.getActivity();
        }

        this.initializeComponent(view);
        
        final Intent intent =  getActivity().getIntent();
        this.update((Fight) intent.getParcelableExtra(Fight.PARCEL));

        return view;
    }

	/**
	 * Updates the view with the given data.
	 *
	 * @param item The Fight to get the data from.
	 */
	public void update(Fight item) {
    	this.model = item;
    	
		this.loadData();
		
		if (this.model != null) {
			MultiLoader loader = new MultiLoader(this);
			String baseUri = 
					FightProviderAdapter.FIGHT_URI 
					+ "/" 
					+ this.model.getId();

			loader.addUri(Uri.parse(baseUri), new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightShowFragment.this.onFightLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/starterfighters"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightShowFragment.this.onStarterFightersLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/alternatedfighters"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightShowFragment.this.onAlternatedFightersLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/survivorfighters"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightShowFragment.this.onSurvivorFightersLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/actions"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightShowFragment.this.onActionsLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/senderplayer"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightShowFragment.this.onSenderPlayerLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/receiverplayer"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightShowFragment.this.onReceiverPlayerLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.init();
		}
    }

	/**
	 * Called when the entity has been loaded.
	 * 
	 * @param c The cursor of this entity
	 */
	public void onFightLoaded(Cursor c) {
		if (c.getCount() > 0) {
			c.moveToFirst();
			new FightSQLiteAdapter(getActivity()).cursorToItem(
						c,
						this.model);
			this.loadData();
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onStarterFightersLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				this.model.setStarterFighters(
						new FightPlayerSQLiteAdapter(getActivity()).cursorToItems(c));
					this.loadData();
			} else {
				this.model.setStarterFighters(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onAlternatedFightersLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				this.model.setAlternatedFighters(
						new FightPlayerSQLiteAdapter(getActivity()).cursorToItems(c));
					this.loadData();
			} else {
				this.model.setAlternatedFighters(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onSurvivorFightersLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				this.model.setSurvivorFighters(
						new FightPlayerSQLiteAdapter(getActivity()).cursorToItems(c));
					this.loadData();
			} else {
				this.model.setSurvivorFighters(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onActionsLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				this.model.setActions(
						new FightActionSQLiteAdapter(getActivity()).cursorToItems(c));
					this.loadData();
			} else {
				this.model.setActions(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onSenderPlayerLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setSenderPlayer(
							new FightPlayerSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setSenderPlayer(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onReceiverPlayerLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setReceiverPlayer(
							new FightPlayerSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setReceiverPlayer(null);
					this.loadData();
			}
		}
	}

	/**
	 * Calls the FightEditActivity.
	 */
	@Override
	public void onClickEdit() {
		final Intent intent = new Intent(getActivity(),
									FightEditActivity.class);
		Bundle extras = new Bundle();
		extras.putParcelable("Fight", this.model);
		intent.putExtras(extras);

		this.getActivity().startActivity(intent);
	}

	/**
	 * Shows a confirmation dialog.
	 */
	@Override
	public void onClickDelete() {
		new DeleteDialog(this.getActivity(), this).show();
	}

	@Override
	public void onDeleteDialogClose(boolean ok) {
		if (ok) {
			new DeleteTask(this.getActivity(), this.model).execute();
		}
	}
	
	/** 
	 * Called when delete task is done.
	 */	
	public void onPostDelete() {
		if (this.deleteCallback != null) {
			this.deleteCallback.onItemDeleted();
		}
	}

	/**
	 * This class will remove the entity into the DB.
	 * It runs asynchronously.
	 */
	private class DeleteTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private Context ctx;
		/** Entity to delete. */
		private Fight item;

		/**
		 * Constructor of the task.
		 * @param item The entity to remove from DB
		 * @param ctx A context to build FightSQLiteAdapter
		 */
		public DeleteTask(final Context ctx,
					final Fight item) {
			super();
			this.ctx = ctx;
			this.item = item;
		}

		@Override
		protected Integer doInBackground(Void... params) {
			int result = -1;

			result = new FightProviderUtils(this.ctx)
					.delete(this.item);

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result >= 0) {
				FightShowFragment.this.onPostDelete();
			}
			super.onPostExecute(result);
		}
		
		

	}
	
	/**
	 * Callback for item deletion.
	 */ 
	public interface DeleteCallback {
		/** Called when current item has been deleted. */
		void onItemDeleted();
	}
}

