/**************************************************************************
 * FightCreateFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fight;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.Fight;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.FightAction;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.widget.MultiEntityWidget;
import com.curseblade.harmony.widget.SingleEntityWidget;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;
import com.curseblade.provider.utils.FightProviderUtils;
import com.curseblade.provider.utils.FightPlayerProviderUtils;
import com.curseblade.provider.utils.FightActionProviderUtils;

/**
 * Fight create fragment.
 *
 * This fragment gives you an interface to create a Fight.
 */
public class FightCreateFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected Fight model = new Fight();

	/** Fields View. */
	/** The starterFighters chooser component. */
	protected MultiEntityWidget starterFightersWidget;
	/** The starterFighters Adapter. */
	protected MultiEntityWidget.EntityAdapter<FightPlayer> 
				starterFightersAdapter;
	/** The alternatedFighters chooser component. */
	protected MultiEntityWidget alternatedFightersWidget;
	/** The alternatedFighters Adapter. */
	protected MultiEntityWidget.EntityAdapter<FightPlayer> 
				alternatedFightersAdapter;
	/** The survivorFighters chooser component. */
	protected MultiEntityWidget survivorFightersWidget;
	/** The survivorFighters Adapter. */
	protected MultiEntityWidget.EntityAdapter<FightPlayer> 
				survivorFightersAdapter;
	/** The actions chooser component. */
	protected MultiEntityWidget actionsWidget;
	/** The actions Adapter. */
	protected MultiEntityWidget.EntityAdapter<FightAction> 
				actionsAdapter;
	/** The senderPlayer chooser component. */
	protected SingleEntityWidget senderPlayerWidget;
	/** The senderPlayer Adapter. */
	protected SingleEntityWidget.EntityAdapter<FightPlayer> 
				senderPlayerAdapter;
	/** The receiverPlayer chooser component. */
	protected SingleEntityWidget receiverPlayerWidget;
	/** The receiverPlayer Adapter. */
	protected SingleEntityWidget.EntityAdapter<FightPlayer> 
				receiverPlayerAdapter;
	/** currentElement View. */
	protected EditText currentElementView;

	/** Initialize view of fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(final View view) {
		this.starterFightersAdapter = 
				new MultiEntityWidget.EntityAdapter<FightPlayer>() {
			@Override
			public String entityToString(FightPlayer item) {
				return String.valueOf(item.getId());
			}
		};
		this.starterFightersWidget =
			(MultiEntityWidget) view.findViewById(R.id.fight_starterfighters_button);
		this.starterFightersWidget.setAdapter(this.starterFightersAdapter);
		this.alternatedFightersAdapter = 
				new MultiEntityWidget.EntityAdapter<FightPlayer>() {
			@Override
			public String entityToString(FightPlayer item) {
				return String.valueOf(item.getId());
			}
		};
		this.alternatedFightersWidget =
			(MultiEntityWidget) view.findViewById(R.id.fight_alternatedfighters_button);
		this.alternatedFightersWidget.setAdapter(this.alternatedFightersAdapter);
		this.survivorFightersAdapter = 
				new MultiEntityWidget.EntityAdapter<FightPlayer>() {
			@Override
			public String entityToString(FightPlayer item) {
				return String.valueOf(item.getId());
			}
		};
		this.survivorFightersWidget =
			(MultiEntityWidget) view.findViewById(R.id.fight_survivorfighters_button);
		this.survivorFightersWidget.setAdapter(this.survivorFightersAdapter);
		this.actionsAdapter = 
				new MultiEntityWidget.EntityAdapter<FightAction>() {
			@Override
			public String entityToString(FightAction item) {
				return String.valueOf(item.getId());
			}
		};
		this.actionsWidget =
			(MultiEntityWidget) view.findViewById(R.id.fight_actions_button);
		this.actionsWidget.setAdapter(this.actionsAdapter);
		this.senderPlayerAdapter = 
				new SingleEntityWidget.EntityAdapter<FightPlayer>() {
			@Override
			public String entityToString(FightPlayer item) {
				return String.valueOf(item.getId());
			}
		};
		this.senderPlayerWidget =
			(SingleEntityWidget) view.findViewById(R.id.fight_senderplayer_button);
		this.senderPlayerWidget.setAdapter(this.senderPlayerAdapter);
		this.receiverPlayerAdapter = 
				new SingleEntityWidget.EntityAdapter<FightPlayer>() {
			@Override
			public String entityToString(FightPlayer item) {
				return String.valueOf(item.getId());
			}
		};
		this.receiverPlayerWidget =
			(SingleEntityWidget) view.findViewById(R.id.fight_receiverplayer_button);
		this.receiverPlayerWidget.setAdapter(this.receiverPlayerAdapter);
		this.currentElementView =
			(EditText) view.findViewById(R.id.fight_currentelement);
	}

	/** Load data from model to fields view. */
	public void loadData() {

		if (this.model.getCurrentElement() != null) {
			this.currentElementView.setText(String.valueOf(this.model.getCurrentElement()));
		}

		new LoadTask(this).execute();
	}

	/** Save data from fields view to model. */
	public void saveData() {

		this.model.setStarterFighters(this.starterFightersAdapter.getCheckedItems());

		this.model.setAlternatedFighters(this.alternatedFightersAdapter.getCheckedItems());

		this.model.setSurvivorFighters(this.survivorFightersAdapter.getCheckedItems());

		this.model.setActions(this.actionsAdapter.getCheckedItems());

		this.model.setSenderPlayer(this.senderPlayerAdapter.getSelectedItem());

		this.model.setReceiverPlayer(this.receiverPlayerAdapter.getSelectedItem());

		this.model.setCurrentElement(Integer.parseInt(
					this.currentElementView.getEditableText().toString()));

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

		if (Strings.isNullOrEmpty(
					this.currentElementView.getText().toString().trim())) {
			error = R.string.fight_currentelement_invalid_field_error;
		}
	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}

	@Override
	public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		final View view = inflater.inflate(
				R.layout.fragment_fight_create,
				container,
				false);

		this.initializeComponent(view);
		this.loadData();
		return view;
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class CreateTask extends AsyncTask<Void, Void, Uri> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to persist. */
		private final Fight entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public CreateTask(final FightCreateFragment fragment,
				final Fight entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.fight_progress_save_title),
					this.ctx.getString(
							R.string.fight_progress_save_message));
		}

		@Override
		protected Uri doInBackground(Void... params) {
			Uri result = null;

			result = new FightProviderUtils(this.ctx).insert(
						this.entity);

			return result;
		}

		@Override
		protected void onPostExecute(Uri result) {
			super.onPostExecute(result);
			if (result != null) {
				final HarmonyFragmentActivity activity =
										 (HarmonyFragmentActivity) this.ctx;
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(
						this.ctx.getString(
								R.string.fight_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private FightCreateFragment fragment;
		/** starterFighters list. */
		private ArrayList<FightPlayer> starterFightersList;
		/** alternatedFighters list. */
		private ArrayList<FightPlayer> alternatedFightersList;
		/** survivorFighters list. */
		private ArrayList<FightPlayer> survivorFightersList;
		/** actions list. */
		private ArrayList<FightAction> actionsList;
		/** senderPlayer list. */
		private ArrayList<FightPlayer> senderPlayerList;
		/** receiverPlayer list. */
		private ArrayList<FightPlayer> receiverPlayerList;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final FightCreateFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.fight_progress_load_relations_title),
					this.ctx.getString(
							R.string.fight_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			this.starterFightersList = 
				new FightPlayerProviderUtils(this.ctx).queryAll();
			this.alternatedFightersList = 
				new FightPlayerProviderUtils(this.ctx).queryAll();
			this.survivorFightersList = 
				new FightPlayerProviderUtils(this.ctx).queryAll();
			this.actionsList = 
				new FightActionProviderUtils(this.ctx).queryAll();
			this.senderPlayerList = 
				new FightPlayerProviderUtils(this.ctx).queryAll();
			this.receiverPlayerList = 
				new FightPlayerProviderUtils(this.ctx).queryAll();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			this.fragment.starterFightersAdapter.loadData(this.starterFightersList);
			this.fragment.alternatedFightersAdapter.loadData(this.alternatedFightersList);
			this.fragment.survivorFightersAdapter.loadData(this.survivorFightersList);
			this.fragment.actionsAdapter.loadData(this.actionsList);
			this.fragment.senderPlayerAdapter.loadData(this.senderPlayerList);
			this.fragment.receiverPlayerAdapter.loadData(this.receiverPlayerList);
			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new CreateTask(this, this.model).execute();
		}
	}
}
