/**************************************************************************
 * FightPlayerShowActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fightplayer;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.view.fightplayer.FightPlayerShowFragment.DeleteCallback;

import android.os.Bundle;

/** FightPlayer show Activity.
 *
 * This only contains a FightPlayerShowFragment.
 *
 * @see android.app.Activity
 */
public class FightPlayerShowActivity 
		extends HarmonyFragmentActivity 
		implements DeleteCallback {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_fightplayer_show);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onItemDeleted() {
		this.finish();
	}
}
