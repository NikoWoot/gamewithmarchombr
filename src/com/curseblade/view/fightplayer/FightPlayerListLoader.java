/**************************************************************************
 * FightPlayerListLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fightplayer;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.CursorLoader;

import com.curseblade.criterias.FightPlayerCriterias;

/**
 * FightPlayer Loader.
 */
public class FightPlayerListLoader
				extends CursorLoader {

	/**
	 * Constructor.
	 * @param ctx context
	 * @param crit FightPlayerCriterias
	 */
	public FightPlayerListLoader(
			final Context ctx,
			final FightPlayerCriterias crit) {
		super(ctx);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param uri The URI associated with this loader
	 * @param projection The projection to use
	 * @param selection The selection
	 * @param selectionArgs The selection Args
	 * @param sortOrder The sort order
	 */
	public FightPlayerListLoader(
					Context ctx,
					Uri uri,
					String[] projection,
					String selection,
					String[] selectionArgs,
					String sortOrder) {
		super(ctx,
				uri,
				projection,
				selection,
				selectionArgs,
				sortOrder);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param uri The URI associated with this loader
	 * @param projection The projection to use
	 * @param criterias FightPlayerCriterias
	 * @param sortOrder The sort order
	 */
	public FightPlayerListLoader(
					Context ctx,
					Uri uri,
					String[] projection,
					FightPlayerCriterias criterias,
					String sortOrder) {
		super(ctx,
				uri,
				projection,
				criterias.toSQLiteSelection(),
				criterias.toSQLiteSelectionArgs(),
				sortOrder);
	}
}
