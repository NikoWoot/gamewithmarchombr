/**************************************************************************
 * FightPlayerShowFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fightplayer;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.curseblade.R;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.Spell;
import com.curseblade.harmony.view.DeleteDialog;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.view.MultiLoader;
import com.curseblade.harmony.view.MultiLoader.UriLoadedCallback;
import com.curseblade.menu.CrudEditDeleteMenuWrapper.CrudEditDeleteMenuInterface;
import com.curseblade.provider.utils.FightPlayerProviderUtils;
import com.curseblade.provider.FightPlayerProviderAdapter;

/** FightPlayer show fragment.
 *
 * This fragment gives you an interface to show a FightPlayer.
 * 
 * @see android.app.Fragment
 */
public class FightPlayerShowFragment
		extends HarmonyFragment
		implements CrudEditDeleteMenuInterface,
				DeleteDialog.DeleteDialogCallback {
	/** Model data. */
	protected FightPlayer model;

	/** DeleteCallback. */
	protected DeleteCallback deleteCallback;

	/* This entity's fields views */
	/** teamId View. */
	protected TextView teamIdView;
	/** fight View. */
	protected TextView fightView;
	/** pseudo View. */
	protected TextView pseudoView;
	/** life View. */
	protected TextView lifeView;
	/** dead View. */
	protected CheckBox deadView;
	/** baseCharacter View. */
	protected TextView baseCharacterView;
	/** baseWeapon View. */
	protected TextView baseWeaponView;
	/** baseSpells View. */
	protected TextView baseSpellsView;
	/** Data layout. */
	protected RelativeLayout dataLayout;
	/** Text view for no FightPlayer. */
	protected TextView emptyText;


    /** Initialize view of curr.fields.
     *
     * @param view The layout inflating
     */
    protected void initializeComponent(final View view) {
		this.teamIdView =
			(TextView) view.findViewById(
					R.id.fightplayer_teamid);
		this.fightView =
			(TextView) view.findViewById(
					R.id.fightplayer_fight);
		this.pseudoView =
			(TextView) view.findViewById(
					R.id.fightplayer_pseudo);
		this.lifeView =
			(TextView) view.findViewById(
					R.id.fightplayer_life);
		this.deadView =
			(CheckBox) view.findViewById(
					R.id.fightplayer_dead);
		this.deadView.setEnabled(false);
		this.baseCharacterView =
			(TextView) view.findViewById(
					R.id.fightplayer_basecharacter);
		this.baseWeaponView =
			(TextView) view.findViewById(
					R.id.fightplayer_baseweapon);
		this.baseSpellsView =
			(TextView) view.findViewById(
					R.id.fightplayer_basespells);

		this.dataLayout =
				(RelativeLayout) view.findViewById(
						R.id.fightplayer_data_layout);
		this.emptyText =
				(TextView) view.findViewById(
						R.id.fightplayer_empty);
    }

    /** Load data from model to fields view. */
    public void loadData() {
    	if (this.model != null) {

    		this.dataLayout.setVisibility(View.VISIBLE);
    		this.emptyText.setVisibility(View.GONE);


		if (this.model.getTeamId() != null) {
			this.teamIdView.setText(String.valueOf(this.model.getTeamId()));
		}
		if (this.model.getFight() != null) {
			this.fightView.setText(
					String.valueOf(this.model.getFight().getId()));
		}
		if (this.model.getPseudo() != null) {
			this.pseudoView.setText(this.model.getPseudo());
		}
		if (this.model.getLife() != null) {
			this.lifeView.setText(String.valueOf(this.model.getLife()));
		}
		this.deadView.setChecked(this.model.isDead());
		if (this.model.getBaseCharacter() != null) {
			this.baseCharacterView.setText(
					String.valueOf(this.model.getBaseCharacter().getId()));
		}
		if (this.model.getBaseWeapon() != null) {
			this.baseWeaponView.setText(
					String.valueOf(this.model.getBaseWeapon().getId()));
		}
		if (this.model.getBaseSpells() != null) {
			String baseSpellsValue = "";
			for (Spell item : this.model.getBaseSpells()) {
				baseSpellsValue += item.getId() + ",";
			}
			this.baseSpellsView.setText(baseSpellsValue);
		}
		} else {
    		this.dataLayout.setVisibility(View.GONE);
    		this.emptyText.setVisibility(View.VISIBLE);
    	}
    }

    @Override
    public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {

    	// Inflate the layout for this fragment
        final View view =
        		inflater.inflate(
        				R.layout.fragment_fightplayer_show,
        				container,
        				false);
        
        if (this.getActivity() instanceof DeleteCallback) {
        	this.deleteCallback = (DeleteCallback) this.getActivity();
        }

        this.initializeComponent(view);
        
        final Intent intent =  getActivity().getIntent();
        this.update((FightPlayer) intent.getParcelableExtra(FightPlayer.PARCEL));

        return view;
    }

	/**
	 * Updates the view with the given data.
	 *
	 * @param item The FightPlayer to get the data from.
	 */
	public void update(FightPlayer item) {
    	this.model = item;
    	
		this.loadData();
		
		if (this.model != null) {
			MultiLoader loader = new MultiLoader(this);
			String baseUri = 
					FightPlayerProviderAdapter.FIGHTPLAYER_URI 
					+ "/" 
					+ this.model.getId();

			loader.addUri(Uri.parse(baseUri), new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightPlayerShowFragment.this.onFightPlayerLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/fight"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightPlayerShowFragment.this.onFightLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/basecharacter"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightPlayerShowFragment.this.onBaseCharacterLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/baseweapon"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightPlayerShowFragment.this.onBaseWeaponLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/basespells"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					FightPlayerShowFragment.this.onBaseSpellsLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.init();
		}
    }

	/**
	 * Called when the entity has been loaded.
	 * 
	 * @param c The cursor of this entity
	 */
	public void onFightPlayerLoaded(Cursor c) {
		if (c.getCount() > 0) {
			c.moveToFirst();
			new FightPlayerSQLiteAdapter(getActivity()).cursorToItem(
						c,
						this.model);
			this.loadData();
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onFightLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setFight(
							new FightSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setFight(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onBaseCharacterLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setBaseCharacter(
							new CharacterPlayerSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setBaseCharacter(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onBaseWeaponLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setBaseWeapon(
							new ItemWeaponSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setBaseWeapon(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onBaseSpellsLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				this.model.setBaseSpells(
						new SpellSQLiteAdapter(getActivity()).cursorToItems(c));
					this.loadData();
			} else {
				this.model.setBaseSpells(null);
					this.loadData();
			}
		}
	}

	/**
	 * Calls the FightPlayerEditActivity.
	 */
	@Override
	public void onClickEdit() {
		final Intent intent = new Intent(getActivity(),
									FightPlayerEditActivity.class);
		Bundle extras = new Bundle();
		extras.putParcelable("FightPlayer", this.model);
		intent.putExtras(extras);

		this.getActivity().startActivity(intent);
	}

	/**
	 * Shows a confirmation dialog.
	 */
	@Override
	public void onClickDelete() {
		new DeleteDialog(this.getActivity(), this).show();
	}

	@Override
	public void onDeleteDialogClose(boolean ok) {
		if (ok) {
			new DeleteTask(this.getActivity(), this.model).execute();
		}
	}
	
	/** 
	 * Called when delete task is done.
	 */	
	public void onPostDelete() {
		if (this.deleteCallback != null) {
			this.deleteCallback.onItemDeleted();
		}
	}

	/**
	 * This class will remove the entity into the DB.
	 * It runs asynchronously.
	 */
	private class DeleteTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private Context ctx;
		/** Entity to delete. */
		private FightPlayer item;

		/**
		 * Constructor of the task.
		 * @param item The entity to remove from DB
		 * @param ctx A context to build FightPlayerSQLiteAdapter
		 */
		public DeleteTask(final Context ctx,
					final FightPlayer item) {
			super();
			this.ctx = ctx;
			this.item = item;
		}

		@Override
		protected Integer doInBackground(Void... params) {
			int result = -1;

			result = new FightPlayerProviderUtils(this.ctx)
					.delete(this.item);

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result >= 0) {
				FightPlayerShowFragment.this.onPostDelete();
			}
			super.onPostExecute(result);
		}
		
		

	}
	
	/**
	 * Callback for item deletion.
	 */ 
	public interface DeleteCallback {
		/** Called when current item has been deleted. */
		void onItemDeleted();
	}
}

