/**************************************************************************
 * FightPlayerListAdapter.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fightplayer;

import java.util.List;

import com.curseblade.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.google.android.pinnedheader.SelectionItemView;
import com.google.android.pinnedheader.headerlist.HeaderAdapter;
import com.google.android.pinnedheader.headerlist.HeaderSectionIndexer;
import com.google.android.pinnedheader.headerlist.PinnedHeaderListView.PinnedHeaderAdapter;
import com.curseblade.entity.FightPlayer;

/**
 * List adapter for FightPlayer entity.
 */
public class FightPlayerListAdapter
		extends HeaderAdapter<FightPlayer>
		implements PinnedHeaderAdapter {
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public FightPlayerListAdapter(Context ctx) {
		super(ctx);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param resource The resource
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public FightPlayerListAdapter(Context context,
			int resource,
			int textViewResourceId,
			List<FightPlayer> objects) {
		super(context, resource, textViewResourceId, objects);
	}

	/**
     * Constructor.
     *
	 * @param context The context
	 * @param resource The resource
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public FightPlayerListAdapter(Context context,
			int resource,
			int textViewResourceId,
			FightPlayer[] objects) {
		super(context, resource, textViewResourceId, objects);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param resource The resource
	 * @param textViewResourceId The resource id of the text view
	 */
	public FightPlayerListAdapter(Context context,
			int resource,
			int textViewResourceId) {
		super(context, resource, textViewResourceId);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public FightPlayerListAdapter(Context context,
			int textViewResourceId,
			List<FightPlayer> objects) {
		super(context, textViewResourceId, objects);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public FightPlayerListAdapter(Context context,
			int textViewResourceId,
			FightPlayer[] objects) {
		super(context, textViewResourceId, objects);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param textViewResourceId The resource id of the text view
	 */
	public FightPlayerListAdapter(Context context,
			int textViewResourceId) {
		super(context, textViewResourceId);
	}

	/** Holder row. */
	private static class ViewHolder extends SelectionItemView {

		/**
		 * Constructor.
		 *
		 * @param context The context
		 */
		public ViewHolder(Context context) {
			this(context, null);
		}
		
		/**
		 * Constructor.
		 *
		 * @param context The context
		 * @param attrs The attribute set
		 */
		public ViewHolder(Context context, AttributeSet attrs) {
			super(context, attrs, R.layout.row_fightplayer);
		}

		/** Populate row with a FightPlayer.
		 *
		 * @param model FightPlayer data
		 */
		public void populate(final FightPlayer model) {
			View convertView = this.getInnerLayout();
			TextView teamIdView =
				(TextView) convertView.findViewById(
						R.id.row_fightplayer_teamid);
			TextView fightView =
				(TextView) convertView.findViewById(
						R.id.row_fightplayer_fight);
			TextView pseudoView =
				(TextView) convertView.findViewById(
						R.id.row_fightplayer_pseudo);
			TextView lifeView =
				(TextView) convertView.findViewById(
						R.id.row_fightplayer_life);
			CheckBox deadView =
				(CheckBox) convertView.findViewById(
						R.id.row_fightplayer_dead);
			deadView.setEnabled(false);
			TextView baseCharacterView =
				(TextView) convertView.findViewById(
						R.id.row_fightplayer_basecharacter);
			TextView baseWeaponView =
				(TextView) convertView.findViewById(
						R.id.row_fightplayer_baseweapon);


			if (model.getTeamId() != null) {
				teamIdView.setText(String.valueOf(model.getTeamId()));
			}
			fightView.setText(
					String.valueOf(model.getFight().getId()));
			if (model.getPseudo() != null) {
				pseudoView.setText(model.getPseudo());
			}
			if (model.getLife() != null) {
				lifeView.setText(String.valueOf(model.getLife()));
			}
			deadView.setChecked(model.isDead());
			baseCharacterView.setText(
					String.valueOf(model.getBaseCharacter().getId()));
			baseWeaponView.setText(
					String.valueOf(model.getBaseWeapon().getId()));
		}
	}

	/** Section indexer for this entity's list. */
	public static class FightPlayerSectionIndexer
					extends HeaderSectionIndexer<FightPlayer>
					implements SectionIndexer {

		/**
		 * Constructor.
		 * @param items The items of the indexer
		 */
		public FightPlayerSectionIndexer(List<FightPlayer> items) {
			super(items);
		}
		
		@Override
		protected String getHeaderText(FightPlayer item) {
			return "Your entity's header name here";
		}
	}

	@Override
    protected View bindView(View itemView,
				int partition,
				FightPlayer item,
				int position) {
    	final ViewHolder view;
    	
    	if (itemView != null) {
    		view = (ViewHolder) itemView;
    	} else {
    		view = new ViewHolder(this.getContext());
		}

    	if (!((HarmonyFragmentActivity) this.getContext()).isDualMode()) {
    		view.setActivatedStateSupported(false);
		}
    	
    	view.populate(item);
        this.bindSectionHeaderAndDivider(view, position);
        
        return view;
    }

	@Override
	public int getPosition(FightPlayer item) {
		int result = -1;
		if (item != null) {
			for (int i = 0; i < this.getCount(); i++) {
				if (item.getId() == this.getItem(i).getId()) {
					result = i;
				}
			}
		}
		return result;
	}
}
