/**************************************************************************
 * FightPlayerCreateFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fightplayer;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.Fight;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.entity.Spell;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.widget.MultiEntityWidget;
import com.curseblade.harmony.widget.SingleEntityWidget;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;
import com.curseblade.provider.utils.FightPlayerProviderUtils;
import com.curseblade.provider.utils.FightProviderUtils;
import com.curseblade.provider.utils.CharacterPlayerProviderUtils;
import com.curseblade.provider.utils.ItemWeaponProviderUtils;
import com.curseblade.provider.utils.SpellProviderUtils;

/**
 * FightPlayer create fragment.
 *
 * This fragment gives you an interface to create a FightPlayer.
 */
public class FightPlayerCreateFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected FightPlayer model = new FightPlayer();

	/** Fields View. */
	/** teamId View. */
	protected EditText teamIdView;
	/** The fight chooser component. */
	protected SingleEntityWidget fightWidget;
	/** The fight Adapter. */
	protected SingleEntityWidget.EntityAdapter<Fight> 
				fightAdapter;
	/** pseudo View. */
	protected EditText pseudoView;
	/** life View. */
	protected EditText lifeView;
	/** dead View. */
	protected CheckBox deadView;
	/** The baseCharacter chooser component. */
	protected SingleEntityWidget baseCharacterWidget;
	/** The baseCharacter Adapter. */
	protected SingleEntityWidget.EntityAdapter<CharacterPlayer> 
				baseCharacterAdapter;
	/** The baseWeapon chooser component. */
	protected SingleEntityWidget baseWeaponWidget;
	/** The baseWeapon Adapter. */
	protected SingleEntityWidget.EntityAdapter<ItemWeapon> 
				baseWeaponAdapter;
	/** The baseSpells chooser component. */
	protected MultiEntityWidget baseSpellsWidget;
	/** The baseSpells Adapter. */
	protected MultiEntityWidget.EntityAdapter<Spell> 
				baseSpellsAdapter;

	/** Initialize view of fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(final View view) {
		this.teamIdView =
			(EditText) view.findViewById(R.id.fightplayer_teamid);
		this.fightAdapter = 
				new SingleEntityWidget.EntityAdapter<Fight>() {
			@Override
			public String entityToString(Fight item) {
				return String.valueOf(item.getId());
			}
		};
		this.fightWidget =
			(SingleEntityWidget) view.findViewById(R.id.fightplayer_fight_button);
		this.fightWidget.setAdapter(this.fightAdapter);
		this.pseudoView =
			(EditText) view.findViewById(R.id.fightplayer_pseudo);
		this.lifeView =
			(EditText) view.findViewById(R.id.fightplayer_life);
		this.deadView =
				(CheckBox) view.findViewById(R.id.fightplayer_dead);
		this.baseCharacterAdapter = 
				new SingleEntityWidget.EntityAdapter<CharacterPlayer>() {
			@Override
			public String entityToString(CharacterPlayer item) {
				return String.valueOf(item.getId());
			}
		};
		this.baseCharacterWidget =
			(SingleEntityWidget) view.findViewById(R.id.fightplayer_basecharacter_button);
		this.baseCharacterWidget.setAdapter(this.baseCharacterAdapter);
		this.baseWeaponAdapter = 
				new SingleEntityWidget.EntityAdapter<ItemWeapon>() {
			@Override
			public String entityToString(ItemWeapon item) {
				return String.valueOf(item.getId());
			}
		};
		this.baseWeaponWidget =
			(SingleEntityWidget) view.findViewById(R.id.fightplayer_baseweapon_button);
		this.baseWeaponWidget.setAdapter(this.baseWeaponAdapter);
		this.baseSpellsAdapter = 
				new MultiEntityWidget.EntityAdapter<Spell>() {
			@Override
			public String entityToString(Spell item) {
				return String.valueOf(item.getId());
			}
		};
		this.baseSpellsWidget =
			(MultiEntityWidget) view.findViewById(R.id.fightplayer_basespells_button);
		this.baseSpellsWidget.setAdapter(this.baseSpellsAdapter);
	}

	/** Load data from model to fields view. */
	public void loadData() {

		if (this.model.getTeamId() != null) {
			this.teamIdView.setText(String.valueOf(this.model.getTeamId()));
		}
		if (this.model.getPseudo() != null) {
			this.pseudoView.setText(this.model.getPseudo());
		}
		if (this.model.getLife() != null) {
			this.lifeView.setText(String.valueOf(this.model.getLife()));
		}
		this.deadView.setChecked(this.model.isDead());

		new LoadTask(this).execute();
	}

	/** Save data from fields view to model. */
	public void saveData() {

		this.model.setTeamId(Integer.parseInt(
					this.teamIdView.getEditableText().toString()));

		this.model.setFight(this.fightAdapter.getSelectedItem());

		this.model.setPseudo(this.pseudoView.getEditableText().toString());

		this.model.setLife(Integer.parseInt(
					this.lifeView.getEditableText().toString()));

		this.model.setDead(this.deadView.isChecked());

		this.model.setBaseCharacter(this.baseCharacterAdapter.getSelectedItem());

		this.model.setBaseWeapon(this.baseWeaponAdapter.getSelectedItem());

		this.model.setBaseSpells(this.baseSpellsAdapter.getCheckedItems());

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

		if (Strings.isNullOrEmpty(
					this.teamIdView.getText().toString().trim())) {
			error = R.string.fightplayer_teamid_invalid_field_error;
		}
		if (this.fightAdapter.getSelectedItem() == null) {
			error = R.string.fightplayer_fight_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.pseudoView.getText().toString().trim())) {
			error = R.string.fightplayer_pseudo_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.lifeView.getText().toString().trim())) {
			error = R.string.fightplayer_life_invalid_field_error;
		}
		if (this.baseCharacterAdapter.getSelectedItem() == null) {
			error = R.string.fightplayer_basecharacter_invalid_field_error;
		}
		if (this.baseWeaponAdapter.getSelectedItem() == null) {
			error = R.string.fightplayer_baseweapon_invalid_field_error;
		}
		if (this.baseSpellsAdapter.getCheckedItems().isEmpty()) {
			error = R.string.fightplayer_basespells_invalid_field_error;
		}
	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}

	@Override
	public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		final View view = inflater.inflate(
				R.layout.fragment_fightplayer_create,
				container,
				false);

		this.initializeComponent(view);
		this.loadData();
		return view;
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class CreateTask extends AsyncTask<Void, Void, Uri> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to persist. */
		private final FightPlayer entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public CreateTask(final FightPlayerCreateFragment fragment,
				final FightPlayer entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.fightplayer_progress_save_title),
					this.ctx.getString(
							R.string.fightplayer_progress_save_message));
		}

		@Override
		protected Uri doInBackground(Void... params) {
			Uri result = null;

			result = new FightPlayerProviderUtils(this.ctx).insert(
						this.entity);

			return result;
		}

		@Override
		protected void onPostExecute(Uri result) {
			super.onPostExecute(result);
			if (result != null) {
				final HarmonyFragmentActivity activity =
										 (HarmonyFragmentActivity) this.ctx;
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(
						this.ctx.getString(
								R.string.fightplayer_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private FightPlayerCreateFragment fragment;
		/** fight list. */
		private ArrayList<Fight> fightList;
		/** baseCharacter list. */
		private ArrayList<CharacterPlayer> baseCharacterList;
		/** baseWeapon list. */
		private ArrayList<ItemWeapon> baseWeaponList;
		/** baseSpells list. */
		private ArrayList<Spell> baseSpellsList;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final FightPlayerCreateFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.fightplayer_progress_load_relations_title),
					this.ctx.getString(
							R.string.fightplayer_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			this.fightList = 
				new FightProviderUtils(this.ctx).queryAll();
			this.baseCharacterList = 
				new CharacterPlayerProviderUtils(this.ctx).queryAll();
			this.baseWeaponList = 
				new ItemWeaponProviderUtils(this.ctx).queryAll();
			this.baseSpellsList = 
				new SpellProviderUtils(this.ctx).queryAll();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			this.fragment.fightAdapter.loadData(this.fightList);
			this.fragment.baseCharacterAdapter.loadData(this.baseCharacterList);
			this.fragment.baseWeaponAdapter.loadData(this.baseWeaponList);
			this.fragment.baseSpellsAdapter.loadData(this.baseSpellsList);
			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new CreateTask(this, this.model).execute();
		}
	}
}
