/**************************************************************************
 * FightPlayerCreateActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fightplayer;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;

import android.os.Bundle;

/** 
 * FightPlayer create Activity.
 *
 * This only contains a FightPlayerCreateFragment.
 *
 * @see android.app.Activity
 */
public class FightPlayerCreateActivity extends HarmonyFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_fightplayer_create);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
