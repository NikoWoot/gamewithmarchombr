/**************************************************************************
 * FightPlayerEditFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.fightplayer;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.Fight;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.entity.Spell;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.widget.MultiEntityWidget;
import com.curseblade.harmony.widget.SingleEntityWidget;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;
import com.curseblade.provider.FightPlayerProviderAdapter;
import com.curseblade.provider.utils.FightPlayerProviderUtils;
import com.curseblade.provider.utils.FightProviderUtils;
import com.curseblade.provider.utils.CharacterPlayerProviderUtils;
import com.curseblade.provider.utils.ItemWeaponProviderUtils;
import com.curseblade.provider.utils.SpellProviderUtils;
import com.curseblade.data.SpellSQLiteAdapter;

/** FightPlayer create fragment.
 *
 * This fragment gives you an interface to edit a FightPlayer.
 *
 * @see android.app.Fragment
 */
public class FightPlayerEditFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected FightPlayer model = new FightPlayer();

	/** curr.fields View. */
	/** teamId View. */
	protected EditText teamIdView;
	/** The fight chooser component. */
	protected SingleEntityWidget fightWidget;
	/** The fight Adapter. */
	protected SingleEntityWidget.EntityAdapter<Fight>
			fightAdapter;
	/** pseudo View. */
	protected EditText pseudoView;
	/** life View. */
	protected EditText lifeView;
	/** dead View. */
	protected CheckBox deadView;
	/** The baseCharacter chooser component. */
	protected SingleEntityWidget baseCharacterWidget;
	/** The baseCharacter Adapter. */
	protected SingleEntityWidget.EntityAdapter<CharacterPlayer>
			baseCharacterAdapter;
	/** The baseWeapon chooser component. */
	protected SingleEntityWidget baseWeaponWidget;
	/** The baseWeapon Adapter. */
	protected SingleEntityWidget.EntityAdapter<ItemWeapon>
			baseWeaponAdapter;
	/** The baseSpells chooser component. */
	protected MultiEntityWidget baseSpellsWidget;
	/** The baseSpells Adapter. */
	protected MultiEntityWidget.EntityAdapter<Spell>
			baseSpellsAdapter;

	/** Initialize view of curr.fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(View view) {
		this.teamIdView = (EditText) view.findViewById(
				R.id.fightplayer_teamid);
		this.fightAdapter =
				new SingleEntityWidget.EntityAdapter<Fight>() {
			@Override
			public String entityToString(Fight item) {
				return String.valueOf(item.getId());
			}
		};
		this.fightWidget =
			(SingleEntityWidget) view.findViewById(R.id.fightplayer_fight_button);
		this.fightWidget.setAdapter(this.fightAdapter);
		this.pseudoView = (EditText) view.findViewById(
				R.id.fightplayer_pseudo);
		this.lifeView = (EditText) view.findViewById(
				R.id.fightplayer_life);
		this.deadView = (CheckBox) view.findViewById(
				R.id.fightplayer_dead);
		this.baseCharacterAdapter =
				new SingleEntityWidget.EntityAdapter<CharacterPlayer>() {
			@Override
			public String entityToString(CharacterPlayer item) {
				return String.valueOf(item.getId());
			}
		};
		this.baseCharacterWidget =
			(SingleEntityWidget) view.findViewById(R.id.fightplayer_basecharacter_button);
		this.baseCharacterWidget.setAdapter(this.baseCharacterAdapter);
		this.baseWeaponAdapter =
				new SingleEntityWidget.EntityAdapter<ItemWeapon>() {
			@Override
			public String entityToString(ItemWeapon item) {
				return String.valueOf(item.getId());
			}
		};
		this.baseWeaponWidget =
			(SingleEntityWidget) view.findViewById(R.id.fightplayer_baseweapon_button);
		this.baseWeaponWidget.setAdapter(this.baseWeaponAdapter);
		this.baseSpellsAdapter =
				new MultiEntityWidget.EntityAdapter<Spell>() {
			@Override
			public String entityToString(Spell item) {
				return String.valueOf(item.getId());
			}
		};
		this.baseSpellsWidget = (MultiEntityWidget) view.findViewById(
						R.id.fightplayer_basespells_button);
		this.baseSpellsWidget.setAdapter(this.baseSpellsAdapter);
	}

	/** Load data from model to curr.fields view. */
	public void loadData() {

		if (this.model.getTeamId() != null) {
			this.teamIdView.setText(String.valueOf(this.model.getTeamId()));
		}
		if (this.model.getPseudo() != null) {
			this.pseudoView.setText(this.model.getPseudo());
		}
		if (this.model.getLife() != null) {
			this.lifeView.setText(String.valueOf(this.model.getLife()));
		}
		this.deadView.setChecked(this.model.isDead());

		new LoadTask(this).execute();
	}

	/** Save data from curr.fields view to model. */
	public void saveData() {

		this.model.setTeamId(Integer.parseInt(
					this.teamIdView.getEditableText().toString()));

		this.model.setFight(this.fightAdapter.getSelectedItem());

		this.model.setPseudo(this.pseudoView.getEditableText().toString());

		this.model.setLife(Integer.parseInt(
					this.lifeView.getEditableText().toString()));

		this.model.setDead(this.deadView.isChecked());

		this.model.setBaseCharacter(this.baseCharacterAdapter.getSelectedItem());

		this.model.setBaseWeapon(this.baseWeaponAdapter.getSelectedItem());

		this.model.setBaseSpells(this.baseSpellsAdapter.getCheckedItems());

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

		if (Strings.isNullOrEmpty(
					this.teamIdView.getText().toString().trim())) {
			error = R.string.fightplayer_teamid_invalid_field_error;
		}
		if (this.fightAdapter.getSelectedItem() == null) {
			error = R.string.fightplayer_fight_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.pseudoView.getText().toString().trim())) {
			error = R.string.fightplayer_pseudo_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.lifeView.getText().toString().trim())) {
			error = R.string.fightplayer_life_invalid_field_error;
		}
		if (this.baseCharacterAdapter.getSelectedItem() == null) {
			error = R.string.fightplayer_basecharacter_invalid_field_error;
		}
		if (this.baseWeaponAdapter.getSelectedItem() == null) {
			error = R.string.fightplayer_baseweapon_invalid_field_error;
		}
		if (this.baseSpellsAdapter.getCheckedItems().isEmpty()) {
			error = R.string.fightplayer_basespells_invalid_field_error;
		}
	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}
	@Override
	public View onCreateView(
				LayoutInflater inflater,
				ViewGroup container,
				Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		final View view =
				inflater.inflate(R.layout.fragment_fightplayer_edit,
						container,
						false);

		final Intent intent =  getActivity().getIntent();
		this.model = (FightPlayer) intent.getParcelableExtra(
				FightPlayer.PARCEL);

		this.initializeComponent(view);
		this.loadData();

		return view;
	}

	/**
	 * This class will update the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class EditTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to update. */
		private final FightPlayer entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public EditTask(final FightPlayerEditFragment fragment,
					final FightPlayer entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.fightplayer_progress_save_title),
					this.ctx.getString(
							R.string.fightplayer_progress_save_message));
		}

		@Override
		protected Integer doInBackground(Void... params) {
			Integer result = -1;

			try {
				result = new FightPlayerProviderUtils(this.ctx).update(
					this.entity);
			} catch (SQLiteException e) {
				Log.e("FightPlayerEditFragment", e.getMessage());
			}

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);

			if (result > 0) {
				final HarmonyFragmentActivity activity =
						(HarmonyFragmentActivity) this.ctx;
				activity.setResult(HarmonyFragmentActivity.RESULT_OK);
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(this.ctx.getString(
						R.string.fightplayer_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
																int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}


	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private FightPlayerEditFragment fragment;
		/** fight list. */
		private ArrayList<Fight> fightList;
		/** baseCharacter list. */
		private ArrayList<CharacterPlayer> baseCharacterList;
		/** baseWeapon list. */
		private ArrayList<ItemWeapon> baseWeaponList;
		/** baseSpells list. */
		private ArrayList<Spell> baseSpellsList;
	/** baseSpells list. */
		private ArrayList<Spell> associatedBaseSpellsList;

		/**
		 * Constructor of the task.
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final FightPlayerEditFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
				this.ctx.getString(
					R.string.fightplayer_progress_load_relations_title),
				this.ctx.getString(
					R.string.fightplayer_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			this.fightList = 
				new FightProviderUtils(this.ctx).queryAll();
			this.baseCharacterList = 
				new CharacterPlayerProviderUtils(this.ctx).queryAll();
			this.baseWeaponList = 
				new ItemWeaponProviderUtils(this.ctx).queryAll();
			this.baseSpellsList = 
				new SpellProviderUtils(this.ctx).queryAll();
			Uri baseSpellsUri = FightPlayerProviderAdapter.FIGHTPLAYER_URI;
			baseSpellsUri = Uri.withAppendedPath(baseSpellsUri, 
									String.valueOf(this.fragment.model.getId()));
			baseSpellsUri = Uri.withAppendedPath(baseSpellsUri, "baseSpells");
			Cursor baseSpellsCursor = 
					this.ctx.getContentResolver().query(
							baseSpellsUri,
							new String[]{SpellSQLiteAdapter.ALIASED_COL_ID},
							null,
							null, 
							null);
			
			if (baseSpellsCursor != null && baseSpellsCursor.getCount() > 0) {
				this.associatedBaseSpellsList = new ArrayList<Spell>();
				while (baseSpellsCursor.moveToNext()) {
					int baseSpellsId = baseSpellsCursor.getInt(
							baseSpellsCursor.getColumnIndex(
									SpellSQLiteAdapter.COL_ID));
					for (Spell baseSpells : this.baseSpellsList) {
						if (baseSpells.getId() == baseSpellsId) {
							this.associatedBaseSpellsList.add(baseSpells);
						}
					}
				}
				baseSpellsCursor.close();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			this.fragment.onFightLoaded(this.fightList);
			this.fragment.onBaseCharacterLoaded(this.baseCharacterList);
			this.fragment.onBaseWeaponLoaded(this.baseWeaponList);
			this.fragment.model.setBaseSpells(this.associatedBaseSpellsList);
			this.fragment.onBaseSpellsLoaded(this.baseSpellsList);

			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new EditTask(this, this.model).execute();
		}
	}

	/**
	 * Called when fight have been loaded.
	 * @param items The loaded items
	 */
	protected void onFightLoaded(ArrayList<Fight> items) {
		this.fightAdapter.loadData(items);
		
		for (Fight item : items) {
			if (item.getId() == this.model.getFight().getId()) {
				this.fightAdapter.selectItem(item);
			}
		}
	}
	/**
	 * Called when baseCharacter have been loaded.
	 * @param items The loaded items
	 */
	protected void onBaseCharacterLoaded(ArrayList<CharacterPlayer> items) {
		this.baseCharacterAdapter.loadData(items);
		
		for (CharacterPlayer item : items) {
			if (item.getId() == this.model.getBaseCharacter().getId()) {
				this.baseCharacterAdapter.selectItem(item);
			}
		}
	}
	/**
	 * Called when baseWeapon have been loaded.
	 * @param items The loaded items
	 */
	protected void onBaseWeaponLoaded(ArrayList<ItemWeapon> items) {
		this.baseWeaponAdapter.loadData(items);
		
		for (ItemWeapon item : items) {
			if (item.getId() == this.model.getBaseWeapon().getId()) {
				this.baseWeaponAdapter.selectItem(item);
			}
		}
	}
	/**
	 * Called when baseSpells have been loaded.
	 * @param items The loaded items
	 */
	protected void onBaseSpellsLoaded(ArrayList<Spell> items) {
		this.baseSpellsAdapter.loadData(items);
		this.baseSpellsAdapter.setCheckedItems(this.model.getBaseSpells());
	}
}
