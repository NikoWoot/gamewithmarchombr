/**************************************************************************
 * TournamentEditFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournament;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.Tournament;
import com.curseblade.entity.TournamentPoolFight;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.widget.MultiEntityWidget;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;
import com.curseblade.provider.TournamentProviderAdapter;
import com.curseblade.provider.utils.TournamentProviderUtils;
import com.curseblade.provider.utils.TournamentPoolFightProviderUtils;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;

/** Tournament create fragment.
 *
 * This fragment gives you an interface to edit a Tournament.
 *
 * @see android.app.Fragment
 */
public class TournamentEditFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected Tournament model = new Tournament();

	/** curr.fields View. */
	/** The poolFights chooser component. */
	protected MultiEntityWidget poolFightsWidget;
	/** The poolFights Adapter. */
	protected MultiEntityWidget.EntityAdapter<TournamentPoolFight>
			poolFightsAdapter;

	/** Initialize view of curr.fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(View view) {
		this.poolFightsAdapter =
				new MultiEntityWidget.EntityAdapter<TournamentPoolFight>() {
			@Override
			public String entityToString(TournamentPoolFight item) {
				return String.valueOf(item.getId());
			}
		};
		this.poolFightsWidget = (MultiEntityWidget) view.findViewById(
						R.id.tournament_poolfights_button);
		this.poolFightsWidget.setAdapter(this.poolFightsAdapter);
	}

	/** Load data from model to curr.fields view. */
	public void loadData() {


		new LoadTask(this).execute();
	}

	/** Save data from curr.fields view to model. */
	public void saveData() {

		this.model.setPoolFights(this.poolFightsAdapter.getCheckedItems());

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}
	@Override
	public View onCreateView(
				LayoutInflater inflater,
				ViewGroup container,
				Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		final View view =
				inflater.inflate(R.layout.fragment_tournament_edit,
						container,
						false);

		final Intent intent =  getActivity().getIntent();
		this.model = (Tournament) intent.getParcelableExtra(
				Tournament.PARCEL);

		this.initializeComponent(view);
		this.loadData();

		return view;
	}

	/**
	 * This class will update the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class EditTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to update. */
		private final Tournament entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public EditTask(final TournamentEditFragment fragment,
					final Tournament entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.tournament_progress_save_title),
					this.ctx.getString(
							R.string.tournament_progress_save_message));
		}

		@Override
		protected Integer doInBackground(Void... params) {
			Integer result = -1;

			try {
				result = new TournamentProviderUtils(this.ctx).update(
					this.entity);
			} catch (SQLiteException e) {
				Log.e("TournamentEditFragment", e.getMessage());
			}

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);

			if (result > 0) {
				final HarmonyFragmentActivity activity =
						(HarmonyFragmentActivity) this.ctx;
				activity.setResult(HarmonyFragmentActivity.RESULT_OK);
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(this.ctx.getString(
						R.string.tournament_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
																int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}


	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private TournamentEditFragment fragment;
		/** poolFights list. */
		private ArrayList<TournamentPoolFight> poolFightsList;
	/** poolFights list. */
		private ArrayList<TournamentPoolFight> associatedPoolFightsList;

		/**
		 * Constructor of the task.
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final TournamentEditFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
				this.ctx.getString(
					R.string.tournament_progress_load_relations_title),
				this.ctx.getString(
					R.string.tournament_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			this.poolFightsList = 
				new TournamentPoolFightProviderUtils(this.ctx).queryAll();
			Uri poolFightsUri = TournamentProviderAdapter.TOURNAMENT_URI;
			poolFightsUri = Uri.withAppendedPath(poolFightsUri, 
									String.valueOf(this.fragment.model.getId()));
			poolFightsUri = Uri.withAppendedPath(poolFightsUri, "poolFights");
			Cursor poolFightsCursor = 
					this.ctx.getContentResolver().query(
							poolFightsUri,
							new String[]{TournamentPoolFightSQLiteAdapter.ALIASED_COL_ID},
							null,
							null, 
							null);
			
			if (poolFightsCursor != null && poolFightsCursor.getCount() > 0) {
				this.associatedPoolFightsList = new ArrayList<TournamentPoolFight>();
				while (poolFightsCursor.moveToNext()) {
					int poolFightsId = poolFightsCursor.getInt(
							poolFightsCursor.getColumnIndex(
									TournamentPoolFightSQLiteAdapter.COL_ID));
					for (TournamentPoolFight poolFights : this.poolFightsList) {
						if (poolFights.getId() == poolFightsId) {
							this.associatedPoolFightsList.add(poolFights);
						}
					}
				}
				poolFightsCursor.close();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			this.fragment.model.setPoolFights(this.associatedPoolFightsList);
			this.fragment.onPoolFightsLoaded(this.poolFightsList);

			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new EditTask(this, this.model).execute();
		}
	}

	/**
	 * Called when poolFights have been loaded.
	 * @param items The loaded items
	 */
	protected void onPoolFightsLoaded(ArrayList<TournamentPoolFight> items) {
		this.poolFightsAdapter.loadData(items);
		this.poolFightsAdapter.setCheckedItems(this.model.getPoolFights());
	}
}
