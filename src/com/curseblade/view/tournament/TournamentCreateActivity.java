/**************************************************************************
 * TournamentCreateActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournament;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;

import android.os.Bundle;

/** 
 * Tournament create Activity.
 *
 * This only contains a TournamentCreateFragment.
 *
 * @see android.app.Activity
 */
public class TournamentCreateActivity extends HarmonyFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_tournament_create);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
