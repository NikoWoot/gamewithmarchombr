/**************************************************************************
 * TournamentCreateFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournament;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.Tournament;
import com.curseblade.entity.TournamentPoolFight;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.widget.MultiEntityWidget;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;
import com.curseblade.provider.utils.TournamentProviderUtils;
import com.curseblade.provider.utils.TournamentPoolFightProviderUtils;

/**
 * Tournament create fragment.
 *
 * This fragment gives you an interface to create a Tournament.
 */
public class TournamentCreateFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected Tournament model = new Tournament();

	/** Fields View. */
	/** The poolFights chooser component. */
	protected MultiEntityWidget poolFightsWidget;
	/** The poolFights Adapter. */
	protected MultiEntityWidget.EntityAdapter<TournamentPoolFight> 
				poolFightsAdapter;

	/** Initialize view of fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(final View view) {
		this.poolFightsAdapter = 
				new MultiEntityWidget.EntityAdapter<TournamentPoolFight>() {
			@Override
			public String entityToString(TournamentPoolFight item) {
				return String.valueOf(item.getId());
			}
		};
		this.poolFightsWidget =
			(MultiEntityWidget) view.findViewById(R.id.tournament_poolfights_button);
		this.poolFightsWidget.setAdapter(this.poolFightsAdapter);
	}

	/** Load data from model to fields view. */
	public void loadData() {


		new LoadTask(this).execute();
	}

	/** Save data from fields view to model. */
	public void saveData() {

		this.model.setPoolFights(this.poolFightsAdapter.getCheckedItems());

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}

	@Override
	public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		final View view = inflater.inflate(
				R.layout.fragment_tournament_create,
				container,
				false);

		this.initializeComponent(view);
		this.loadData();
		return view;
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class CreateTask extends AsyncTask<Void, Void, Uri> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to persist. */
		private final Tournament entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public CreateTask(final TournamentCreateFragment fragment,
				final Tournament entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.tournament_progress_save_title),
					this.ctx.getString(
							R.string.tournament_progress_save_message));
		}

		@Override
		protected Uri doInBackground(Void... params) {
			Uri result = null;

			result = new TournamentProviderUtils(this.ctx).insert(
						this.entity);

			return result;
		}

		@Override
		protected void onPostExecute(Uri result) {
			super.onPostExecute(result);
			if (result != null) {
				final HarmonyFragmentActivity activity =
										 (HarmonyFragmentActivity) this.ctx;
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(
						this.ctx.getString(
								R.string.tournament_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private TournamentCreateFragment fragment;
		/** poolFights list. */
		private ArrayList<TournamentPoolFight> poolFightsList;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final TournamentCreateFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.tournament_progress_load_relations_title),
					this.ctx.getString(
							R.string.tournament_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			this.poolFightsList = 
				new TournamentPoolFightProviderUtils(this.ctx).queryAll();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			this.fragment.poolFightsAdapter.loadData(this.poolFightsList);
			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new CreateTask(this, this.model).execute();
		}
	}
}
