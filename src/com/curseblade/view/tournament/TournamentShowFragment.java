/**************************************************************************
 * TournamentShowFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.tournament;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.curseblade.R;
import com.curseblade.data.TournamentSQLiteAdapter;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;
import com.curseblade.entity.Tournament;
import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.harmony.view.DeleteDialog;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.view.MultiLoader;
import com.curseblade.harmony.view.MultiLoader.UriLoadedCallback;
import com.curseblade.menu.CrudEditDeleteMenuWrapper.CrudEditDeleteMenuInterface;
import com.curseblade.provider.utils.TournamentProviderUtils;
import com.curseblade.provider.TournamentProviderAdapter;

/** Tournament show fragment.
 *
 * This fragment gives you an interface to show a Tournament.
 * 
 * @see android.app.Fragment
 */
public class TournamentShowFragment
		extends HarmonyFragment
		implements CrudEditDeleteMenuInterface,
				DeleteDialog.DeleteDialogCallback {
	/** Model data. */
	protected Tournament model;

	/** DeleteCallback. */
	protected DeleteCallback deleteCallback;

	/* This entity's fields views */
	/** poolFights View. */
	protected TextView poolFightsView;
	/** Data layout. */
	protected RelativeLayout dataLayout;
	/** Text view for no Tournament. */
	protected TextView emptyText;


    /** Initialize view of curr.fields.
     *
     * @param view The layout inflating
     */
    protected void initializeComponent(final View view) {
		this.poolFightsView =
			(TextView) view.findViewById(
					R.id.tournament_poolfights);

		this.dataLayout =
				(RelativeLayout) view.findViewById(
						R.id.tournament_data_layout);
		this.emptyText =
				(TextView) view.findViewById(
						R.id.tournament_empty);
    }

    /** Load data from model to fields view. */
    public void loadData() {
    	if (this.model != null) {

    		this.dataLayout.setVisibility(View.VISIBLE);
    		this.emptyText.setVisibility(View.GONE);


		if (this.model.getPoolFights() != null) {
			String poolFightsValue = "";
			for (TournamentPoolFight item : this.model.getPoolFights()) {
				poolFightsValue += item.getId() + ",";
			}
			this.poolFightsView.setText(poolFightsValue);
		}
		} else {
    		this.dataLayout.setVisibility(View.GONE);
    		this.emptyText.setVisibility(View.VISIBLE);
    	}
    }

    @Override
    public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {

    	// Inflate the layout for this fragment
        final View view =
        		inflater.inflate(
        				R.layout.fragment_tournament_show,
        				container,
        				false);
        
        if (this.getActivity() instanceof DeleteCallback) {
        	this.deleteCallback = (DeleteCallback) this.getActivity();
        }

        this.initializeComponent(view);
        
        final Intent intent =  getActivity().getIntent();
        this.update((Tournament) intent.getParcelableExtra(Tournament.PARCEL));

        return view;
    }

	/**
	 * Updates the view with the given data.
	 *
	 * @param item The Tournament to get the data from.
	 */
	public void update(Tournament item) {
    	this.model = item;
    	
		this.loadData();
		
		if (this.model != null) {
			MultiLoader loader = new MultiLoader(this);
			String baseUri = 
					TournamentProviderAdapter.TOURNAMENT_URI 
					+ "/" 
					+ this.model.getId();

			loader.addUri(Uri.parse(baseUri), new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					TournamentShowFragment.this.onTournamentLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/poolfights"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					TournamentShowFragment.this.onPoolFightsLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.init();
		}
    }

	/**
	 * Called when the entity has been loaded.
	 * 
	 * @param c The cursor of this entity
	 */
	public void onTournamentLoaded(Cursor c) {
		if (c.getCount() > 0) {
			c.moveToFirst();
			new TournamentSQLiteAdapter(getActivity()).cursorToItem(
						c,
						this.model);
			this.loadData();
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onPoolFightsLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				this.model.setPoolFights(
						new TournamentPoolFightSQLiteAdapter(getActivity()).cursorToItems(c));
					this.loadData();
			} else {
				this.model.setPoolFights(null);
					this.loadData();
			}
		}
	}

	/**
	 * Calls the TournamentEditActivity.
	 */
	@Override
	public void onClickEdit() {
		final Intent intent = new Intent(getActivity(),
									TournamentEditActivity.class);
		Bundle extras = new Bundle();
		extras.putParcelable("Tournament", this.model);
		intent.putExtras(extras);

		this.getActivity().startActivity(intent);
	}

	/**
	 * Shows a confirmation dialog.
	 */
	@Override
	public void onClickDelete() {
		new DeleteDialog(this.getActivity(), this).show();
	}

	@Override
	public void onDeleteDialogClose(boolean ok) {
		if (ok) {
			new DeleteTask(this.getActivity(), this.model).execute();
		}
	}
	
	/** 
	 * Called when delete task is done.
	 */	
	public void onPostDelete() {
		if (this.deleteCallback != null) {
			this.deleteCallback.onItemDeleted();
		}
	}

	/**
	 * This class will remove the entity into the DB.
	 * It runs asynchronously.
	 */
	private class DeleteTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private Context ctx;
		/** Entity to delete. */
		private Tournament item;

		/**
		 * Constructor of the task.
		 * @param item The entity to remove from DB
		 * @param ctx A context to build TournamentSQLiteAdapter
		 */
		public DeleteTask(final Context ctx,
					final Tournament item) {
			super();
			this.ctx = ctx;
			this.item = item;
		}

		@Override
		protected Integer doInBackground(Void... params) {
			int result = -1;

			result = new TournamentProviderUtils(this.ctx)
					.delete(this.item);

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result >= 0) {
				TournamentShowFragment.this.onPostDelete();
			}
			super.onPostExecute(result);
		}
		
		

	}
	
	/**
	 * Callback for item deletion.
	 */ 
	public interface DeleteCallback {
		/** Called when current item has been deleted. */
		void onItemDeleted();
	}
}

