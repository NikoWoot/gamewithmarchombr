/**************************************************************************
 * CharacterPlayerEditFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.characterplayer;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemArmury;
import com.curseblade.entity.Spell;
import com.curseblade.entity.ItemWeapon;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.widget.DateWidget;
import com.curseblade.harmony.widget.MultiEntityWidget;
import com.curseblade.harmony.widget.SingleEntityWidget;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;
import com.curseblade.provider.CharacterPlayerProviderAdapter;
import com.curseblade.provider.utils.CharacterPlayerProviderUtils;
import com.curseblade.provider.utils.ItemArmuryProviderUtils;
import com.curseblade.provider.utils.SpellProviderUtils;
import com.curseblade.provider.utils.ItemWeaponProviderUtils;
import com.curseblade.data.ItemArmurySQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;

/** CharacterPlayer create fragment.
 *
 * This fragment gives you an interface to edit a CharacterPlayer.
 *
 * @see android.app.Fragment
 */
public class CharacterPlayerEditFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected CharacterPlayer model = new CharacterPlayer();

	/** curr.fields View. */
	/** pseudo View. */
	protected EditText pseudoView;
	/** life View. */
	protected EditText lifeView;
	/** createdAt Date View. */
	protected DateWidget createdAtView;
	/** level View. */
	protected EditText levelView;
	/** The armuryEquipedItems chooser component. */
	protected MultiEntityWidget armuryEquipedItemsWidget;
	/** The armuryEquipedItems Adapter. */
	protected MultiEntityWidget.EntityAdapter<ItemArmury>
			armuryEquipedItemsAdapter;
	/** The equipedSpells chooser component. */
	protected MultiEntityWidget equipedSpellsWidget;
	/** The equipedSpells Adapter. */
	protected MultiEntityWidget.EntityAdapter<Spell>
			equipedSpellsAdapter;
	/** The weaponUsed chooser component. */
	protected SingleEntityWidget weaponUsedWidget;
	/** The weaponUsed Adapter. */
	protected SingleEntityWidget.EntityAdapter<ItemWeapon>
			weaponUsedAdapter;

	/** Initialize view of curr.fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(View view) {
		this.pseudoView = (EditText) view.findViewById(
				R.id.characterplayer_pseudo);
		this.lifeView = (EditText) view.findViewById(
				R.id.characterplayer_life);
		this.createdAtView = (DateWidget) view.findViewById(
				R.id.characterplayer_createdat);
		this.levelView = (EditText) view.findViewById(
				R.id.characterplayer_level);
		this.armuryEquipedItemsAdapter =
				new MultiEntityWidget.EntityAdapter<ItemArmury>() {
			@Override
			public String entityToString(ItemArmury item) {
				return String.valueOf(item.getId());
			}
		};
		this.armuryEquipedItemsWidget = (MultiEntityWidget) view.findViewById(
						R.id.characterplayer_armuryequipeditems_button);
		this.armuryEquipedItemsWidget.setAdapter(this.armuryEquipedItemsAdapter);
		this.equipedSpellsAdapter =
				new MultiEntityWidget.EntityAdapter<Spell>() {
			@Override
			public String entityToString(Spell item) {
				return String.valueOf(item.getId());
			}
		};
		this.equipedSpellsWidget = (MultiEntityWidget) view.findViewById(
						R.id.characterplayer_equipedspells_button);
		this.equipedSpellsWidget.setAdapter(this.equipedSpellsAdapter);
		this.weaponUsedAdapter =
				new SingleEntityWidget.EntityAdapter<ItemWeapon>() {
			@Override
			public String entityToString(ItemWeapon item) {
				return String.valueOf(item.getId());
			}
		};
		this.weaponUsedWidget =
			(SingleEntityWidget) view.findViewById(R.id.characterplayer_weaponused_button);
		this.weaponUsedWidget.setAdapter(this.weaponUsedAdapter);
	}

	/** Load data from model to curr.fields view. */
	public void loadData() {

		if (this.model.getPseudo() != null) {
			this.pseudoView.setText(this.model.getPseudo());
		}
		if (this.model.getLife() != null) {
			this.lifeView.setText(String.valueOf(this.model.getLife()));
		}
		if (this.model.getCreatedAt() != null) {
			this.createdAtView.setDate(this.model.getCreatedAt());
		}
		if (this.model.getLevel() != null) {
			this.levelView.setText(String.valueOf(this.model.getLevel()));
		}

		new LoadTask(this).execute();
	}

	/** Save data from curr.fields view to model. */
	public void saveData() {

		this.model.setPseudo(this.pseudoView.getEditableText().toString());

		this.model.setLife(Integer.parseInt(
					this.lifeView.getEditableText().toString()));

		this.model.setCreatedAt(this.createdAtView.getDate());

		this.model.setLevel(Integer.parseInt(
					this.levelView.getEditableText().toString()));

		this.model.setArmuryEquipedItems(this.armuryEquipedItemsAdapter.getCheckedItems());

		this.model.setEquipedSpells(this.equipedSpellsAdapter.getCheckedItems());

		this.model.setWeaponUsed(this.weaponUsedAdapter.getSelectedItem());

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

		if (Strings.isNullOrEmpty(
					this.pseudoView.getText().toString().trim())) {
			error = R.string.characterplayer_pseudo_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.lifeView.getText().toString().trim())) {
			error = R.string.characterplayer_life_invalid_field_error;
		}
		if (this.createdAtView.getDate() == null) {
			error = R.string.characterplayer_createdat_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.levelView.getText().toString().trim())) {
			error = R.string.characterplayer_level_invalid_field_error;
		}
		if (this.armuryEquipedItemsAdapter.getCheckedItems().isEmpty()) {
			error = R.string.characterplayer_armuryequipeditems_invalid_field_error;
		}
		if (this.equipedSpellsAdapter.getCheckedItems().isEmpty()) {
			error = R.string.characterplayer_equipedspells_invalid_field_error;
		}
		if (this.weaponUsedAdapter.getSelectedItem() == null) {
			error = R.string.characterplayer_weaponused_invalid_field_error;
		}
	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}
	@Override
	public View onCreateView(
				LayoutInflater inflater,
				ViewGroup container,
				Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		final View view =
				inflater.inflate(R.layout.fragment_characterplayer_edit,
						container,
						false);

		final Intent intent =  getActivity().getIntent();
		this.model = (CharacterPlayer) intent.getParcelableExtra(
				CharacterPlayer.PARCEL);

		this.initializeComponent(view);
		this.loadData();

		return view;
	}

	/**
	 * This class will update the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class EditTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to update. */
		private final CharacterPlayer entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public EditTask(final CharacterPlayerEditFragment fragment,
					final CharacterPlayer entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.characterplayer_progress_save_title),
					this.ctx.getString(
							R.string.characterplayer_progress_save_message));
		}

		@Override
		protected Integer doInBackground(Void... params) {
			Integer result = -1;

			try {
				result = new CharacterPlayerProviderUtils(this.ctx).update(
					this.entity);
			} catch (SQLiteException e) {
				Log.e("CharacterPlayerEditFragment", e.getMessage());
			}

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);

			if (result > 0) {
				final HarmonyFragmentActivity activity =
						(HarmonyFragmentActivity) this.ctx;
				activity.setResult(HarmonyFragmentActivity.RESULT_OK);
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(this.ctx.getString(
						R.string.characterplayer_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
																int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}


	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private CharacterPlayerEditFragment fragment;
		/** armuryEquipedItems list. */
		private ArrayList<ItemArmury> armuryEquipedItemsList;
	/** armuryEquipedItems list. */
		private ArrayList<ItemArmury> associatedArmuryEquipedItemsList;
		/** equipedSpells list. */
		private ArrayList<Spell> equipedSpellsList;
	/** equipedSpells list. */
		private ArrayList<Spell> associatedEquipedSpellsList;
		/** weaponUsed list. */
		private ArrayList<ItemWeapon> weaponUsedList;

		/**
		 * Constructor of the task.
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final CharacterPlayerEditFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
				this.ctx.getString(
					R.string.characterplayer_progress_load_relations_title),
				this.ctx.getString(
					R.string.characterplayer_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			this.armuryEquipedItemsList = 
				new ItemArmuryProviderUtils(this.ctx).queryAll();
			Uri armuryEquipedItemsUri = CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI;
			armuryEquipedItemsUri = Uri.withAppendedPath(armuryEquipedItemsUri, 
									String.valueOf(this.fragment.model.getId()));
			armuryEquipedItemsUri = Uri.withAppendedPath(armuryEquipedItemsUri, "armuryEquipedItems");
			Cursor armuryEquipedItemsCursor = 
					this.ctx.getContentResolver().query(
							armuryEquipedItemsUri,
							new String[]{ItemArmurySQLiteAdapter.ALIASED_COL_ID},
							null,
							null, 
							null);
			
			if (armuryEquipedItemsCursor != null && armuryEquipedItemsCursor.getCount() > 0) {
				this.associatedArmuryEquipedItemsList = new ArrayList<ItemArmury>();
				while (armuryEquipedItemsCursor.moveToNext()) {
					int armuryEquipedItemsId = armuryEquipedItemsCursor.getInt(
							armuryEquipedItemsCursor.getColumnIndex(
									ItemArmurySQLiteAdapter.COL_ID));
					for (ItemArmury armuryEquipedItems : this.armuryEquipedItemsList) {
						if (armuryEquipedItems.getId() == armuryEquipedItemsId) {
							this.associatedArmuryEquipedItemsList.add(armuryEquipedItems);
						}
					}
				}
				armuryEquipedItemsCursor.close();
			}
			this.equipedSpellsList = 
				new SpellProviderUtils(this.ctx).queryAll();
			Uri equipedSpellsUri = CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI;
			equipedSpellsUri = Uri.withAppendedPath(equipedSpellsUri, 
									String.valueOf(this.fragment.model.getId()));
			equipedSpellsUri = Uri.withAppendedPath(equipedSpellsUri, "equipedSpells");
			Cursor equipedSpellsCursor = 
					this.ctx.getContentResolver().query(
							equipedSpellsUri,
							new String[]{SpellSQLiteAdapter.ALIASED_COL_ID},
							null,
							null, 
							null);
			
			if (equipedSpellsCursor != null && equipedSpellsCursor.getCount() > 0) {
				this.associatedEquipedSpellsList = new ArrayList<Spell>();
				while (equipedSpellsCursor.moveToNext()) {
					int equipedSpellsId = equipedSpellsCursor.getInt(
							equipedSpellsCursor.getColumnIndex(
									SpellSQLiteAdapter.COL_ID));
					for (Spell equipedSpells : this.equipedSpellsList) {
						if (equipedSpells.getId() == equipedSpellsId) {
							this.associatedEquipedSpellsList.add(equipedSpells);
						}
					}
				}
				equipedSpellsCursor.close();
			}
			this.weaponUsedList = 
				new ItemWeaponProviderUtils(this.ctx).queryAll();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			this.fragment.model.setArmuryEquipedItems(this.associatedArmuryEquipedItemsList);
			this.fragment.onArmuryEquipedItemsLoaded(this.armuryEquipedItemsList);
			this.fragment.model.setEquipedSpells(this.associatedEquipedSpellsList);
			this.fragment.onEquipedSpellsLoaded(this.equipedSpellsList);
			this.fragment.onWeaponUsedLoaded(this.weaponUsedList);

			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new EditTask(this, this.model).execute();
		}
	}

	/**
	 * Called when armuryEquipedItems have been loaded.
	 * @param items The loaded items
	 */
	protected void onArmuryEquipedItemsLoaded(ArrayList<ItemArmury> items) {
		this.armuryEquipedItemsAdapter.loadData(items);
		this.armuryEquipedItemsAdapter.setCheckedItems(this.model.getArmuryEquipedItems());
	}
	/**
	 * Called when equipedSpells have been loaded.
	 * @param items The loaded items
	 */
	protected void onEquipedSpellsLoaded(ArrayList<Spell> items) {
		this.equipedSpellsAdapter.loadData(items);
		this.equipedSpellsAdapter.setCheckedItems(this.model.getEquipedSpells());
	}
	/**
	 * Called when weaponUsed have been loaded.
	 * @param items The loaded items
	 */
	protected void onWeaponUsedLoaded(ArrayList<ItemWeapon> items) {
		this.weaponUsedAdapter.loadData(items);
		
		for (ItemWeapon item : items) {
			if (item.getId() == this.model.getWeaponUsed().getId()) {
				this.weaponUsedAdapter.selectItem(item);
			}
		}
	}
}
