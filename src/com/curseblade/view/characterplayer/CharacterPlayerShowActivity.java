/**************************************************************************
 * CharacterPlayerShowActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.characterplayer;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.view.characterplayer.CharacterPlayerShowFragment.DeleteCallback;

import android.os.Bundle;

/** CharacterPlayer show Activity.
 *
 * This only contains a CharacterPlayerShowFragment.
 *
 * @see android.app.Activity
 */
public class CharacterPlayerShowActivity 
		extends HarmonyFragmentActivity 
		implements DeleteCallback {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_characterplayer_show);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onItemDeleted() {
		this.finish();
	}
}
