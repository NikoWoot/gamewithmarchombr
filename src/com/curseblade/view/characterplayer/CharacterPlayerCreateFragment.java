/**************************************************************************
 * CharacterPlayerCreateFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.characterplayer;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemArmury;
import com.curseblade.entity.Spell;
import com.curseblade.entity.ItemWeapon;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.widget.DateWidget;
import com.curseblade.harmony.widget.MultiEntityWidget;
import com.curseblade.harmony.widget.SingleEntityWidget;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;
import com.curseblade.provider.utils.CharacterPlayerProviderUtils;
import com.curseblade.provider.utils.ItemArmuryProviderUtils;
import com.curseblade.provider.utils.SpellProviderUtils;
import com.curseblade.provider.utils.ItemWeaponProviderUtils;

/**
 * CharacterPlayer create fragment.
 *
 * This fragment gives you an interface to create a CharacterPlayer.
 */
public class CharacterPlayerCreateFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected CharacterPlayer model = new CharacterPlayer();

	/** Fields View. */
	/** pseudo View. */
	protected EditText pseudoView;
	/** life View. */
	protected EditText lifeView;
	/** createdAt Date View. */
	protected DateWidget createdAtView;
	/** level View. */
	protected EditText levelView;
	/** The armuryEquipedItems chooser component. */
	protected MultiEntityWidget armuryEquipedItemsWidget;
	/** The armuryEquipedItems Adapter. */
	protected MultiEntityWidget.EntityAdapter<ItemArmury> 
				armuryEquipedItemsAdapter;
	/** The equipedSpells chooser component. */
	protected MultiEntityWidget equipedSpellsWidget;
	/** The equipedSpells Adapter. */
	protected MultiEntityWidget.EntityAdapter<Spell> 
				equipedSpellsAdapter;
	/** The weaponUsed chooser component. */
	protected SingleEntityWidget weaponUsedWidget;
	/** The weaponUsed Adapter. */
	protected SingleEntityWidget.EntityAdapter<ItemWeapon> 
				weaponUsedAdapter;

	/** Initialize view of fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(final View view) {
		this.pseudoView =
			(EditText) view.findViewById(R.id.characterplayer_pseudo);
		this.lifeView =
			(EditText) view.findViewById(R.id.characterplayer_life);
		this.createdAtView =
				(DateWidget) view.findViewById(R.id.characterplayer_createdat);
		this.levelView =
			(EditText) view.findViewById(R.id.characterplayer_level);
		this.armuryEquipedItemsAdapter = 
				new MultiEntityWidget.EntityAdapter<ItemArmury>() {
			@Override
			public String entityToString(ItemArmury item) {
				return String.valueOf(item.getId());
			}
		};
		this.armuryEquipedItemsWidget =
			(MultiEntityWidget) view.findViewById(R.id.characterplayer_armuryequipeditems_button);
		this.armuryEquipedItemsWidget.setAdapter(this.armuryEquipedItemsAdapter);
		this.equipedSpellsAdapter = 
				new MultiEntityWidget.EntityAdapter<Spell>() {
			@Override
			public String entityToString(Spell item) {
				return String.valueOf(item.getId());
			}
		};
		this.equipedSpellsWidget =
			(MultiEntityWidget) view.findViewById(R.id.characterplayer_equipedspells_button);
		this.equipedSpellsWidget.setAdapter(this.equipedSpellsAdapter);
		this.weaponUsedAdapter = 
				new SingleEntityWidget.EntityAdapter<ItemWeapon>() {
			@Override
			public String entityToString(ItemWeapon item) {
				return String.valueOf(item.getId());
			}
		};
		this.weaponUsedWidget =
			(SingleEntityWidget) view.findViewById(R.id.characterplayer_weaponused_button);
		this.weaponUsedWidget.setAdapter(this.weaponUsedAdapter);
	}

	/** Load data from model to fields view. */
	public void loadData() {

		if (this.model.getPseudo() != null) {
			this.pseudoView.setText(this.model.getPseudo());
		}
		if (this.model.getLife() != null) {
			this.lifeView.setText(String.valueOf(this.model.getLife()));
		}
		if (this.model.getCreatedAt() != null) {
			this.createdAtView.setDate(this.model.getCreatedAt());
		}
		if (this.model.getLevel() != null) {
			this.levelView.setText(String.valueOf(this.model.getLevel()));
		}

		new LoadTask(this).execute();
	}

	/** Save data from fields view to model. */
	public void saveData() {

		this.model.setPseudo(this.pseudoView.getEditableText().toString());

		this.model.setLife(Integer.parseInt(
					this.lifeView.getEditableText().toString()));

		this.model.setCreatedAt(this.createdAtView.getDate());

		this.model.setLevel(Integer.parseInt(
					this.levelView.getEditableText().toString()));

		this.model.setArmuryEquipedItems(this.armuryEquipedItemsAdapter.getCheckedItems());

		this.model.setEquipedSpells(this.equipedSpellsAdapter.getCheckedItems());

		this.model.setWeaponUsed(this.weaponUsedAdapter.getSelectedItem());

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

		if (Strings.isNullOrEmpty(
					this.pseudoView.getText().toString().trim())) {
			error = R.string.characterplayer_pseudo_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.lifeView.getText().toString().trim())) {
			error = R.string.characterplayer_life_invalid_field_error;
		}
		if (this.createdAtView.getDate() == null) {
			error = R.string.characterplayer_createdat_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.levelView.getText().toString().trim())) {
			error = R.string.characterplayer_level_invalid_field_error;
		}
		if (this.armuryEquipedItemsAdapter.getCheckedItems().isEmpty()) {
			error = R.string.characterplayer_armuryequipeditems_invalid_field_error;
		}
		if (this.equipedSpellsAdapter.getCheckedItems().isEmpty()) {
			error = R.string.characterplayer_equipedspells_invalid_field_error;
		}
		if (this.weaponUsedAdapter.getSelectedItem() == null) {
			error = R.string.characterplayer_weaponused_invalid_field_error;
		}
	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}

	@Override
	public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		final View view = inflater.inflate(
				R.layout.fragment_characterplayer_create,
				container,
				false);

		this.initializeComponent(view);
		this.loadData();
		return view;
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class CreateTask extends AsyncTask<Void, Void, Uri> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to persist. */
		private final CharacterPlayer entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public CreateTask(final CharacterPlayerCreateFragment fragment,
				final CharacterPlayer entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.characterplayer_progress_save_title),
					this.ctx.getString(
							R.string.characterplayer_progress_save_message));
		}

		@Override
		protected Uri doInBackground(Void... params) {
			Uri result = null;

			result = new CharacterPlayerProviderUtils(this.ctx).insert(
						this.entity);

			return result;
		}

		@Override
		protected void onPostExecute(Uri result) {
			super.onPostExecute(result);
			if (result != null) {
				final HarmonyFragmentActivity activity =
										 (HarmonyFragmentActivity) this.ctx;
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(
						this.ctx.getString(
								R.string.characterplayer_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private CharacterPlayerCreateFragment fragment;
		/** armuryEquipedItems list. */
		private ArrayList<ItemArmury> armuryEquipedItemsList;
		/** equipedSpells list. */
		private ArrayList<Spell> equipedSpellsList;
		/** weaponUsed list. */
		private ArrayList<ItemWeapon> weaponUsedList;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final CharacterPlayerCreateFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.characterplayer_progress_load_relations_title),
					this.ctx.getString(
							R.string.characterplayer_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			this.armuryEquipedItemsList = 
				new ItemArmuryProviderUtils(this.ctx).queryAll();
			this.equipedSpellsList = 
				new SpellProviderUtils(this.ctx).queryAll();
			this.weaponUsedList = 
				new ItemWeaponProviderUtils(this.ctx).queryAll();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			this.fragment.armuryEquipedItemsAdapter.loadData(this.armuryEquipedItemsList);
			this.fragment.equipedSpellsAdapter.loadData(this.equipedSpellsList);
			this.fragment.weaponUsedAdapter.loadData(this.weaponUsedList);
			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new CreateTask(this, this.model).execute();
		}
	}
}
