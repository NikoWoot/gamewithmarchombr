/**************************************************************************
 * CharacterPlayerEditActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.characterplayer;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;

import android.os.Bundle;

/** CharacterPlayer edit Activity.
 *
 * This only contains a CharacterPlayerEditFragment.
 *
 * @see android.app.Activity
 */
public class CharacterPlayerEditActivity extends HarmonyFragmentActivity {

	@Override
  	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_characterplayer_edit);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}
