/**************************************************************************
 * CharacterPlayerListAdapter.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.characterplayer;

import java.util.List;

import com.curseblade.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.curseblade.harmony.util.DateUtils;
import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.google.android.pinnedheader.SelectionItemView;
import com.google.android.pinnedheader.headerlist.HeaderAdapter;
import com.google.android.pinnedheader.headerlist.HeaderSectionIndexer;
import com.google.android.pinnedheader.headerlist.PinnedHeaderListView.PinnedHeaderAdapter;
import com.curseblade.entity.CharacterPlayer;

/**
 * List adapter for CharacterPlayer entity.
 */
public class CharacterPlayerListAdapter
		extends HeaderAdapter<CharacterPlayer>
		implements PinnedHeaderAdapter {
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public CharacterPlayerListAdapter(Context ctx) {
		super(ctx);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param resource The resource
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public CharacterPlayerListAdapter(Context context,
			int resource,
			int textViewResourceId,
			List<CharacterPlayer> objects) {
		super(context, resource, textViewResourceId, objects);
	}

	/**
     * Constructor.
     *
	 * @param context The context
	 * @param resource The resource
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public CharacterPlayerListAdapter(Context context,
			int resource,
			int textViewResourceId,
			CharacterPlayer[] objects) {
		super(context, resource, textViewResourceId, objects);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param resource The resource
	 * @param textViewResourceId The resource id of the text view
	 */
	public CharacterPlayerListAdapter(Context context,
			int resource,
			int textViewResourceId) {
		super(context, resource, textViewResourceId);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public CharacterPlayerListAdapter(Context context,
			int textViewResourceId,
			List<CharacterPlayer> objects) {
		super(context, textViewResourceId, objects);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param textViewResourceId The resource id of the text view
	 * @param objects The list of objects of this adapter
	 */
	public CharacterPlayerListAdapter(Context context,
			int textViewResourceId,
			CharacterPlayer[] objects) {
		super(context, textViewResourceId, objects);
	}

	/**
     * Constructor.
     * 
	 * @param context The context
	 * @param textViewResourceId The resource id of the text view
	 */
	public CharacterPlayerListAdapter(Context context,
			int textViewResourceId) {
		super(context, textViewResourceId);
	}

	/** Holder row. */
	private static class ViewHolder extends SelectionItemView {

		/**
		 * Constructor.
		 *
		 * @param context The context
		 */
		public ViewHolder(Context context) {
			this(context, null);
		}
		
		/**
		 * Constructor.
		 *
		 * @param context The context
		 * @param attrs The attribute set
		 */
		public ViewHolder(Context context, AttributeSet attrs) {
			super(context, attrs, R.layout.row_characterplayer);
		}

		/** Populate row with a CharacterPlayer.
		 *
		 * @param model CharacterPlayer data
		 */
		public void populate(final CharacterPlayer model) {
			View convertView = this.getInnerLayout();
			TextView pseudoView =
				(TextView) convertView.findViewById(
						R.id.row_characterplayer_pseudo);
			TextView lifeView =
				(TextView) convertView.findViewById(
						R.id.row_characterplayer_life);
			TextView createdAtView =
				(TextView) convertView.findViewById(
						R.id.row_characterplayer_createdat);
			TextView levelView =
				(TextView) convertView.findViewById(
						R.id.row_characterplayer_level);
			TextView weaponUsedView =
				(TextView) convertView.findViewById(
						R.id.row_characterplayer_weaponused);


			if (model.getPseudo() != null) {
				pseudoView.setText(model.getPseudo());
			}
			if (model.getLife() != null) {
				lifeView.setText(String.valueOf(model.getLife()));
			}
			if (model.getCreatedAt() != null) {
				createdAtView.setText(DateUtils.formatDateToString(model.getCreatedAt()));
			}
			if (model.getLevel() != null) {
				levelView.setText(String.valueOf(model.getLevel()));
			}
			weaponUsedView.setText(
					String.valueOf(model.getWeaponUsed().getId()));
		}
	}

	/** Section indexer for this entity's list. */
	public static class CharacterPlayerSectionIndexer
					extends HeaderSectionIndexer<CharacterPlayer>
					implements SectionIndexer {

		/**
		 * Constructor.
		 * @param items The items of the indexer
		 */
		public CharacterPlayerSectionIndexer(List<CharacterPlayer> items) {
			super(items);
		}
		
		@Override
		protected String getHeaderText(CharacterPlayer item) {
			return "Your entity's header name here";
		}
	}

	@Override
    protected View bindView(View itemView,
				int partition,
				CharacterPlayer item,
				int position) {
    	final ViewHolder view;
    	
    	if (itemView != null) {
    		view = (ViewHolder) itemView;
    	} else {
    		view = new ViewHolder(this.getContext());
		}

    	if (!((HarmonyFragmentActivity) this.getContext()).isDualMode()) {
    		view.setActivatedStateSupported(false);
		}
    	
    	view.populate(item);
        this.bindSectionHeaderAndDivider(view, position);
        
        return view;
    }

	@Override
	public int getPosition(CharacterPlayer item) {
		int result = -1;
		if (item != null) {
			for (int i = 0; i < this.getCount(); i++) {
				if (item.getId() == this.getItem(i).getId()) {
					result = i;
				}
			}
		}
		return result;
	}
}
