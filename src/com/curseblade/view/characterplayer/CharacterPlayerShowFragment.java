/**************************************************************************
 * CharacterPlayerShowFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.characterplayer;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.curseblade.R;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.ItemArmurySQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemArmury;
import com.curseblade.entity.Spell;
import com.curseblade.harmony.util.DateUtils;
import com.curseblade.harmony.view.DeleteDialog;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.view.MultiLoader;
import com.curseblade.harmony.view.MultiLoader.UriLoadedCallback;
import com.curseblade.menu.CrudEditDeleteMenuWrapper.CrudEditDeleteMenuInterface;
import com.curseblade.provider.utils.CharacterPlayerProviderUtils;
import com.curseblade.provider.CharacterPlayerProviderAdapter;

/** CharacterPlayer show fragment.
 *
 * This fragment gives you an interface to show a CharacterPlayer.
 * 
 * @see android.app.Fragment
 */
public class CharacterPlayerShowFragment
		extends HarmonyFragment
		implements CrudEditDeleteMenuInterface,
				DeleteDialog.DeleteDialogCallback {
	/** Model data. */
	protected CharacterPlayer model;

	/** DeleteCallback. */
	protected DeleteCallback deleteCallback;

	/* This entity's fields views */
	/** pseudo View. */
	protected TextView pseudoView;
	/** life View. */
	protected TextView lifeView;
	/** createdAt View. */
	protected TextView createdAtView;
	/** level View. */
	protected TextView levelView;
	/** armuryEquipedItems View. */
	protected TextView armuryEquipedItemsView;
	/** equipedSpells View. */
	protected TextView equipedSpellsView;
	/** weaponUsed View. */
	protected TextView weaponUsedView;
	/** Data layout. */
	protected RelativeLayout dataLayout;
	/** Text view for no CharacterPlayer. */
	protected TextView emptyText;


    /** Initialize view of curr.fields.
     *
     * @param view The layout inflating
     */
    protected void initializeComponent(final View view) {
		this.pseudoView =
			(TextView) view.findViewById(
					R.id.characterplayer_pseudo);
		this.lifeView =
			(TextView) view.findViewById(
					R.id.characterplayer_life);
		this.createdAtView =
			(TextView) view.findViewById(
					R.id.characterplayer_createdat);
		this.levelView =
			(TextView) view.findViewById(
					R.id.characterplayer_level);
		this.armuryEquipedItemsView =
			(TextView) view.findViewById(
					R.id.characterplayer_armuryequipeditems);
		this.equipedSpellsView =
			(TextView) view.findViewById(
					R.id.characterplayer_equipedspells);
		this.weaponUsedView =
			(TextView) view.findViewById(
					R.id.characterplayer_weaponused);

		this.dataLayout =
				(RelativeLayout) view.findViewById(
						R.id.characterplayer_data_layout);
		this.emptyText =
				(TextView) view.findViewById(
						R.id.characterplayer_empty);
    }

    /** Load data from model to fields view. */
    public void loadData() {
    	if (this.model != null) {

    		this.dataLayout.setVisibility(View.VISIBLE);
    		this.emptyText.setVisibility(View.GONE);


		if (this.model.getPseudo() != null) {
			this.pseudoView.setText(this.model.getPseudo());
		}
		if (this.model.getLife() != null) {
			this.lifeView.setText(String.valueOf(this.model.getLife()));
		}
		if (this.model.getCreatedAt() != null) {
			this.createdAtView.setText(
					DateUtils.formatDateToString(
							this.model.getCreatedAt()));
		}
		if (this.model.getLevel() != null) {
			this.levelView.setText(String.valueOf(this.model.getLevel()));
		}
		if (this.model.getArmuryEquipedItems() != null) {
			String armuryEquipedItemsValue = "";
			for (ItemArmury item : this.model.getArmuryEquipedItems()) {
				armuryEquipedItemsValue += item.getId() + ",";
			}
			this.armuryEquipedItemsView.setText(armuryEquipedItemsValue);
		}
		if (this.model.getEquipedSpells() != null) {
			String equipedSpellsValue = "";
			for (Spell item : this.model.getEquipedSpells()) {
				equipedSpellsValue += item.getId() + ",";
			}
			this.equipedSpellsView.setText(equipedSpellsValue);
		}
		if (this.model.getWeaponUsed() != null) {
			this.weaponUsedView.setText(
					String.valueOf(this.model.getWeaponUsed().getId()));
		}
		} else {
    		this.dataLayout.setVisibility(View.GONE);
    		this.emptyText.setVisibility(View.VISIBLE);
    	}
    }

    @Override
    public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {

    	// Inflate the layout for this fragment
        final View view =
        		inflater.inflate(
        				R.layout.fragment_characterplayer_show,
        				container,
        				false);
        
        if (this.getActivity() instanceof DeleteCallback) {
        	this.deleteCallback = (DeleteCallback) this.getActivity();
        }

        this.initializeComponent(view);
        
        final Intent intent =  getActivity().getIntent();
        this.update((CharacterPlayer) intent.getParcelableExtra(CharacterPlayer.PARCEL));

        return view;
    }

	/**
	 * Updates the view with the given data.
	 *
	 * @param item The CharacterPlayer to get the data from.
	 */
	public void update(CharacterPlayer item) {
    	this.model = item;
    	
		this.loadData();
		
		if (this.model != null) {
			MultiLoader loader = new MultiLoader(this);
			String baseUri = 
					CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI 
					+ "/" 
					+ this.model.getId();

			loader.addUri(Uri.parse(baseUri), new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					CharacterPlayerShowFragment.this.onCharacterPlayerLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/armuryequipeditems"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					CharacterPlayerShowFragment.this.onArmuryEquipedItemsLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/equipedspells"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					CharacterPlayerShowFragment.this.onEquipedSpellsLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.addUri(Uri.parse(baseUri + "/weaponused"), 
					new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					CharacterPlayerShowFragment.this.onWeaponUsedLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.init();
		}
    }

	/**
	 * Called when the entity has been loaded.
	 * 
	 * @param c The cursor of this entity
	 */
	public void onCharacterPlayerLoaded(Cursor c) {
		if (c.getCount() > 0) {
			c.moveToFirst();
			new CharacterPlayerSQLiteAdapter(getActivity()).cursorToItem(
						c,
						this.model);
			this.loadData();
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onArmuryEquipedItemsLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				this.model.setArmuryEquipedItems(
						new ItemArmurySQLiteAdapter(getActivity()).cursorToItems(c));
					this.loadData();
			} else {
				this.model.setArmuryEquipedItems(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onEquipedSpellsLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				this.model.setEquipedSpells(
						new SpellSQLiteAdapter(getActivity()).cursorToItems(c));
					this.loadData();
			} else {
				this.model.setEquipedSpells(null);
					this.loadData();
			}
		}
	}
	/**
	 * Called when the relation has been loaded.
	 * 
	 * @param c The cursor of this relation
	 */
	public void onWeaponUsedLoaded(Cursor c) {
		if (this.model != null) {
			if (c != null) {
				if (c.getCount() > 0) {
					c.moveToFirst();
					this.model.setWeaponUsed(
							new ItemWeaponSQLiteAdapter(getActivity()).cursorToItem(c));
					this.loadData();
			}
			} else {
				this.model.setWeaponUsed(null);
					this.loadData();
			}
		}
	}

	/**
	 * Calls the CharacterPlayerEditActivity.
	 */
	@Override
	public void onClickEdit() {
		final Intent intent = new Intent(getActivity(),
									CharacterPlayerEditActivity.class);
		Bundle extras = new Bundle();
		extras.putParcelable("CharacterPlayer", this.model);
		intent.putExtras(extras);

		this.getActivity().startActivity(intent);
	}

	/**
	 * Shows a confirmation dialog.
	 */
	@Override
	public void onClickDelete() {
		new DeleteDialog(this.getActivity(), this).show();
	}

	@Override
	public void onDeleteDialogClose(boolean ok) {
		if (ok) {
			new DeleteTask(this.getActivity(), this.model).execute();
		}
	}
	
	/** 
	 * Called when delete task is done.
	 */	
	public void onPostDelete() {
		if (this.deleteCallback != null) {
			this.deleteCallback.onItemDeleted();
		}
	}

	/**
	 * This class will remove the entity into the DB.
	 * It runs asynchronously.
	 */
	private class DeleteTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private Context ctx;
		/** Entity to delete. */
		private CharacterPlayer item;

		/**
		 * Constructor of the task.
		 * @param item The entity to remove from DB
		 * @param ctx A context to build CharacterPlayerSQLiteAdapter
		 */
		public DeleteTask(final Context ctx,
					final CharacterPlayer item) {
			super();
			this.ctx = ctx;
			this.item = item;
		}

		@Override
		protected Integer doInBackground(Void... params) {
			int result = -1;

			result = new CharacterPlayerProviderUtils(this.ctx)
					.delete(this.item);

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result >= 0) {
				CharacterPlayerShowFragment.this.onPostDelete();
			}
			super.onPostExecute(result);
		}
		
		

	}
	
	/**
	 * Callback for item deletion.
	 */ 
	public interface DeleteCallback {
		/** Called when current item has been deleted. */
		void onItemDeleted();
	}
}

