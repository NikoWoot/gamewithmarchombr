/**************************************************************************
 * SpellEditActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.spell;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;

import android.os.Bundle;

/** Spell edit Activity.
 *
 * This only contains a SpellEditFragment.
 *
 * @see android.app.Activity
 */
public class SpellEditActivity extends HarmonyFragmentActivity {

	@Override
  	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_spell_edit);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}
