/**************************************************************************
 * SpellShowFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.spell;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.curseblade.R;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.entity.Spell;
import com.curseblade.harmony.view.DeleteDialog;
import com.curseblade.harmony.view.HarmonyFragment;
import com.curseblade.harmony.view.MultiLoader;
import com.curseblade.harmony.view.MultiLoader.UriLoadedCallback;
import com.curseblade.menu.CrudEditDeleteMenuWrapper.CrudEditDeleteMenuInterface;
import com.curseblade.provider.utils.SpellProviderUtils;
import com.curseblade.provider.SpellProviderAdapter;

/** Spell show fragment.
 *
 * This fragment gives you an interface to show a Spell.
 * 
 * @see android.app.Fragment
 */
public class SpellShowFragment
		extends HarmonyFragment
		implements CrudEditDeleteMenuInterface,
				DeleteDialog.DeleteDialogCallback {
	/** Model data. */
	protected Spell model;

	/** DeleteCallback. */
	protected DeleteCallback deleteCallback;

	/* This entity's fields views */
	/** name View. */
	protected TextView nameView;
	/** baseAttack View. */
	protected TextView baseAttackView;
	/** criticalAttack View. */
	protected TextView criticalAttackView;
	/** Data layout. */
	protected RelativeLayout dataLayout;
	/** Text view for no Spell. */
	protected TextView emptyText;


    /** Initialize view of curr.fields.
     *
     * @param view The layout inflating
     */
    protected void initializeComponent(final View view) {
		this.nameView =
			(TextView) view.findViewById(
					R.id.spell_name);
		this.baseAttackView =
			(TextView) view.findViewById(
					R.id.spell_baseattack);
		this.criticalAttackView =
			(TextView) view.findViewById(
					R.id.spell_criticalattack);

		this.dataLayout =
				(RelativeLayout) view.findViewById(
						R.id.spell_data_layout);
		this.emptyText =
				(TextView) view.findViewById(
						R.id.spell_empty);
    }

    /** Load data from model to fields view. */
    public void loadData() {
    	if (this.model != null) {

    		this.dataLayout.setVisibility(View.VISIBLE);
    		this.emptyText.setVisibility(View.GONE);


		if (this.model.getName() != null) {
			this.nameView.setText(this.model.getName());
		}
		if (this.model.getBaseAttack() != null) {
			this.baseAttackView.setText(String.valueOf(this.model.getBaseAttack()));
		}
		if (this.model.getCriticalAttack() != null) {
			this.criticalAttackView.setText(String.valueOf(this.model.getCriticalAttack()));
		}
		} else {
    		this.dataLayout.setVisibility(View.GONE);
    		this.emptyText.setVisibility(View.VISIBLE);
    	}
    }

    @Override
    public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {

    	// Inflate the layout for this fragment
        final View view =
        		inflater.inflate(
        				R.layout.fragment_spell_show,
        				container,
        				false);
        
        if (this.getActivity() instanceof DeleteCallback) {
        	this.deleteCallback = (DeleteCallback) this.getActivity();
        }

        this.initializeComponent(view);
        
        final Intent intent =  getActivity().getIntent();
        this.update((Spell) intent.getParcelableExtra(Spell.PARCEL));

        return view;
    }

	/**
	 * Updates the view with the given data.
	 *
	 * @param item The Spell to get the data from.
	 */
	public void update(Spell item) {
    	this.model = item;
    	
		this.loadData();
		
		if (this.model != null) {
			MultiLoader loader = new MultiLoader(this);
			String baseUri = 
					SpellProviderAdapter.SPELL_URI 
					+ "/" 
					+ this.model.getId();

			loader.addUri(Uri.parse(baseUri), new UriLoadedCallback() {

				@Override
				public void onLoadComplete(Cursor c) {
					SpellShowFragment.this.onSpellLoaded(c);
				}

				@Override
				public void onLoaderReset() {

				}
			});
			loader.init();
		}
    }

	/**
	 * Called when the entity has been loaded.
	 * 
	 * @param c The cursor of this entity
	 */
	public void onSpellLoaded(Cursor c) {
		if (c.getCount() > 0) {
			c.moveToFirst();
			new SpellSQLiteAdapter(getActivity()).cursorToItem(
						c,
						this.model);
			this.loadData();
		}
	}

	/**
	 * Calls the SpellEditActivity.
	 */
	@Override
	public void onClickEdit() {
		final Intent intent = new Intent(getActivity(),
									SpellEditActivity.class);
		Bundle extras = new Bundle();
		extras.putParcelable("Spell", this.model);
		intent.putExtras(extras);

		this.getActivity().startActivity(intent);
	}

	/**
	 * Shows a confirmation dialog.
	 */
	@Override
	public void onClickDelete() {
		new DeleteDialog(this.getActivity(), this).show();
	}

	@Override
	public void onDeleteDialogClose(boolean ok) {
		if (ok) {
			new DeleteTask(this.getActivity(), this.model).execute();
		}
	}
	
	/** 
	 * Called when delete task is done.
	 */	
	public void onPostDelete() {
		if (this.deleteCallback != null) {
			this.deleteCallback.onItemDeleted();
		}
	}

	/**
	 * This class will remove the entity into the DB.
	 * It runs asynchronously.
	 */
	private class DeleteTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private Context ctx;
		/** Entity to delete. */
		private Spell item;

		/**
		 * Constructor of the task.
		 * @param item The entity to remove from DB
		 * @param ctx A context to build SpellSQLiteAdapter
		 */
		public DeleteTask(final Context ctx,
					final Spell item) {
			super();
			this.ctx = ctx;
			this.item = item;
		}

		@Override
		protected Integer doInBackground(Void... params) {
			int result = -1;

			result = new SpellProviderUtils(this.ctx)
					.delete(this.item);

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result >= 0) {
				SpellShowFragment.this.onPostDelete();
			}
			super.onPostExecute(result);
		}
		
		

	}
	
	/**
	 * Callback for item deletion.
	 */ 
	public interface DeleteCallback {
		/** Called when current item has been deleted. */
		void onItemDeleted();
	}
}

