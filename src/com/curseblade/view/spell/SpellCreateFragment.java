/**************************************************************************
 * SpellCreateFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.spell;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.Spell;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;

import com.curseblade.harmony.widget.SingleEntityWidget;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;
import com.curseblade.provider.utils.SpellProviderUtils;

/**
 * Spell create fragment.
 *
 * This fragment gives you an interface to create a Spell.
 */
public class SpellCreateFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected Spell model = new Spell();

	/** Fields View. */
	/** name View. */
	protected EditText nameView;
	/** baseAttack View. */
	protected EditText baseAttackView;
	/** criticalAttack View. */
	protected EditText criticalAttackView;

	/** Initialize view of fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(final View view) {
		this.nameView =
			(EditText) view.findViewById(R.id.spell_name);
		this.baseAttackView =
			(EditText) view.findViewById(R.id.spell_baseattack);
		this.criticalAttackView =
			(EditText) view.findViewById(R.id.spell_criticalattack);
	}

	/** Load data from model to fields view. */
	public void loadData() {

		if (this.model.getName() != null) {
			this.nameView.setText(this.model.getName());
		}
		if (this.model.getBaseAttack() != null) {
			this.baseAttackView.setText(String.valueOf(this.model.getBaseAttack()));
		}
		if (this.model.getCriticalAttack() != null) {
			this.criticalAttackView.setText(String.valueOf(this.model.getCriticalAttack()));
		}

		new LoadTask(this).execute();
	}

	/** Save data from fields view to model. */
	public void saveData() {

		this.model.setName(this.nameView.getEditableText().toString());

		this.model.setBaseAttack(Double.parseDouble(
					this.baseAttackView.getEditableText().toString()));

		this.model.setCriticalAttack(Double.parseDouble(
					this.criticalAttackView.getEditableText().toString()));

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

		if (Strings.isNullOrEmpty(
					this.nameView.getText().toString().trim())) {
			error = R.string.spell_name_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.baseAttackView.getText().toString().trim())) {
			error = R.string.spell_baseattack_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.criticalAttackView.getText().toString().trim())) {
			error = R.string.spell_criticalattack_invalid_field_error;
		}
	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}

	@Override
	public View onCreateView(
			LayoutInflater inflater,
			ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		final View view = inflater.inflate(
				R.layout.fragment_spell_create,
				container,
				false);

		this.initializeComponent(view);
		this.loadData();
		return view;
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class CreateTask extends AsyncTask<Void, Void, Uri> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to persist. */
		private final Spell entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public CreateTask(final SpellCreateFragment fragment,
				final Spell entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.spell_progress_save_title),
					this.ctx.getString(
							R.string.spell_progress_save_message));
		}

		@Override
		protected Uri doInBackground(Void... params) {
			Uri result = null;

			result = new SpellProviderUtils(this.ctx).insert(
						this.entity);

			return result;
		}

		@Override
		protected void onPostExecute(Uri result) {
			super.onPostExecute(result);
			if (result != null) {
				final HarmonyFragmentActivity activity =
										 (HarmonyFragmentActivity) this.ctx;
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(
						this.ctx.getString(
								R.string.spell_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}

	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private SpellCreateFragment fragment;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final SpellCreateFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.spell_progress_load_relations_title),
					this.ctx.getString(
							R.string.spell_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new CreateTask(this, this.model).execute();
		}
	}
}
