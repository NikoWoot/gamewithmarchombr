/**************************************************************************
 * ItemArmuryShowActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.itemarmury;

import com.curseblade.R;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.view.itemarmury.ItemArmuryShowFragment.DeleteCallback;

import android.os.Bundle;

/** ItemArmury show Activity.
 *
 * This only contains a ItemArmuryShowFragment.
 *
 * @see android.app.Activity
 */
public class ItemArmuryShowActivity 
		extends HarmonyFragmentActivity 
		implements DeleteCallback {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_itemarmury_show);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onItemDeleted() {
		this.finish();
	}
}
