/**************************************************************************
 * ItemArmuryListLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.itemarmury;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.CursorLoader;

import com.curseblade.criterias.ItemArmuryCriterias;

/**
 * ItemArmury Loader.
 */
public class ItemArmuryListLoader
				extends CursorLoader {

	/**
	 * Constructor.
	 * @param ctx context
	 * @param crit ItemArmuryCriterias
	 */
	public ItemArmuryListLoader(
			final Context ctx,
			final ItemArmuryCriterias crit) {
		super(ctx);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param uri The URI associated with this loader
	 * @param projection The projection to use
	 * @param selection The selection
	 * @param selectionArgs The selection Args
	 * @param sortOrder The sort order
	 */
	public ItemArmuryListLoader(
					Context ctx,
					Uri uri,
					String[] projection,
					String selection,
					String[] selectionArgs,
					String sortOrder) {
		super(ctx,
				uri,
				projection,
				selection,
				selectionArgs,
				sortOrder);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param uri The URI associated with this loader
	 * @param projection The projection to use
	 * @param criterias ItemArmuryCriterias
	 * @param sortOrder The sort order
	 */
	public ItemArmuryListLoader(
					Context ctx,
					Uri uri,
					String[] projection,
					ItemArmuryCriterias criterias,
					String sortOrder) {
		super(ctx,
				uri,
				projection,
				criterias.toSQLiteSelection(),
				criterias.toSQLiteSelectionArgs(),
				sortOrder);
	}
}
