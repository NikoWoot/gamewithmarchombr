/**************************************************************************
 * ItemArmuryEditFragment.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.view.itemarmury;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.database.sqlite.SQLiteException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.common.base.Strings;
import com.curseblade.R;
import com.curseblade.entity.ItemArmury;
import com.curseblade.entity.ItemArmuryType;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.harmony.view.HarmonyFragment;

import com.curseblade.harmony.widget.SingleEntityWidget;
import com.curseblade.harmony.widget.EnumSpinner;
import com.curseblade.menu.SaveMenuWrapper.SaveMenuInterface;

import com.curseblade.provider.utils.ItemArmuryProviderUtils;

/** ItemArmury create fragment.
 *
 * This fragment gives you an interface to edit a ItemArmury.
 *
 * @see android.app.Fragment
 */
public class ItemArmuryEditFragment extends HarmonyFragment
			implements SaveMenuInterface {
	/** Model data. */
	protected ItemArmury model = new ItemArmury();

	/** curr.fields View. */
	/** name View. */
	protected EditText nameView;
	/** defValue View. */
	protected EditText defValueView;
	/** armuryType View. */
	protected EnumSpinner armuryTypeView;

	/** Initialize view of curr.fields.
	 *
	 * @param view The layout inflating
	 */
	protected void initializeComponent(View view) {
		this.nameView = (EditText) view.findViewById(
				R.id.itemarmury_name);
		this.defValueView = (EditText) view.findViewById(
				R.id.itemarmury_defvalue);
		this.armuryTypeView = (EnumSpinner) view.findViewById(
				R.id.itemarmury_armurytype);
		this.armuryTypeView.setEnum(ItemArmuryType.class);
	}

	/** Load data from model to curr.fields view. */
	public void loadData() {

		if (this.model.getName() != null) {
			this.nameView.setText(this.model.getName());
		}
		if (this.model.getDefValue() != null) {
			this.defValueView.setText(String.valueOf(this.model.getDefValue()));
		}
		if (this.model.getArmuryType() != null) {
			this.armuryTypeView.setSelectedItem(this.model.getArmuryType());
		}

		new LoadTask(this).execute();
	}

	/** Save data from curr.fields view to model. */
	public void saveData() {

		this.model.setName(this.nameView.getEditableText().toString());

		this.model.setDefValue(Integer.parseInt(
					this.defValueView.getEditableText().toString()));

		this.model.setArmuryType((ItemArmuryType) this.armuryTypeView.getSelectedItem());

	}

	/** Check data is valid.
	 *
	 * @return true if valid
	 */
	public boolean validateData() {
		int error = 0;

		if (Strings.isNullOrEmpty(
					this.nameView.getText().toString().trim())) {
			error = R.string.itemarmury_name_invalid_field_error;
		}
		if (Strings.isNullOrEmpty(
					this.defValueView.getText().toString().trim())) {
			error = R.string.itemarmury_defvalue_invalid_field_error;
		}
	
		if (error > 0) {
			Toast.makeText(this.getActivity(),
				this.getActivity().getString(error),
				Toast.LENGTH_SHORT).show();
		}
		return error == 0;
	}
	@Override
	public View onCreateView(
				LayoutInflater inflater,
				ViewGroup container,
				Bundle savedInstanceState) {

		// Inflate the layout for this fragment
		final View view =
				inflater.inflate(R.layout.fragment_itemarmury_edit,
						container,
						false);

		final Intent intent =  getActivity().getIntent();
		this.model = (ItemArmury) intent.getParcelableExtra(
				ItemArmury.PARCEL);

		this.initializeComponent(view);
		this.loadData();

		return view;
	}

	/**
	 * This class will update the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class EditTask extends AsyncTask<Void, Void, Integer> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Entity to update. */
		private final ItemArmury entity;
		/** Progress Dialog. */
		private ProgressDialog progress;

		/**
		 * Constructor of the task.
		 * @param entity The entity to insert in the DB
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public EditTask(final ItemArmuryEditFragment fragment,
					final ItemArmury entity) {
			super();
			this.ctx = fragment.getActivity();
			this.entity = entity;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
					this.ctx.getString(
							R.string.itemarmury_progress_save_title),
					this.ctx.getString(
							R.string.itemarmury_progress_save_message));
		}

		@Override
		protected Integer doInBackground(Void... params) {
			Integer result = -1;

			try {
				result = new ItemArmuryProviderUtils(this.ctx).update(
					this.entity);
			} catch (SQLiteException e) {
				Log.e("ItemArmuryEditFragment", e.getMessage());
			}

			return result;
		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);

			if (result > 0) {
				final HarmonyFragmentActivity activity =
						(HarmonyFragmentActivity) this.ctx;
				activity.setResult(HarmonyFragmentActivity.RESULT_OK);
				activity.finish();
			} else {
				final AlertDialog.Builder builder =
						new AlertDialog.Builder(this.ctx);
				builder.setIcon(0);
				builder.setMessage(this.ctx.getString(
						R.string.itemarmury_error_create));
				builder.setPositiveButton(
						this.ctx.getString(android.R.string.yes),
						new Dialog.OnClickListener() {
							public void onClick(DialogInterface dialog,
																int which) {

							}
						});
				builder.show();
			}

			this.progress.dismiss();
		}
	}


	/**
	 * This class will save the entity into the DB.
	 * It runs asynchronously and shows a progressDialog
	 */
	public static class LoadTask extends AsyncTask<Void, Void, Void> {
		/** AsyncTask's context. */
		private final Context ctx;
		/** Progress Dialog. */
		private ProgressDialog progress;
		/** Fragment. */
		private ItemArmuryEditFragment fragment;

		/**
		 * Constructor of the task.
		 * @param fragment The parent fragment from where the aSyncTask is
		 * called
		 */
		public LoadTask(final ItemArmuryEditFragment fragment) {
			super();
			this.ctx = fragment.getActivity();
			this.fragment = fragment;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			this.progress = ProgressDialog.show(this.ctx,
				this.ctx.getString(
					R.string.itemarmury_progress_load_relations_title),
				this.ctx.getString(
					R.string.itemarmury_progress_load_relations_message));
		}

		@Override
		protected Void doInBackground(Void... params) {
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			this.progress.dismiss();
		}
	}

	@Override
	public void onClickSave() {
		if (this.validateData()) {
			this.saveData();
			new EditTask(this, this.model).execute();
		}
	}

}
