/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.notification;

import com.curseblade.R;
import com.curseblade.start.SplashScreenActivity;

/**
 * Class to contains all Constants for the Notification System
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class NotificationConstants {

	/**
	 * Class for nextStep after click on Notification
	 */
	public static Class<?> NOTIFICATION_ACTIVITY = SplashScreenActivity.class;

	/**
	 * Icon for Notification
	 */
	public static Integer NOTIFICATION_ICON = R.drawable.ic_launcher;

}
