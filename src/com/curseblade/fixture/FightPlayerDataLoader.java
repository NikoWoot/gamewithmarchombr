/**************************************************************************
 * FightPlayerDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.Fight;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.entity.Spell;



/**
 * FightPlayerDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class FightPlayerDataLoader
						extends FixtureBase<FightPlayer> {
	/** FightPlayerDataLoader name. */
	private static final String FILE_NAME = "FightPlayer";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for teamId. */
	private static final String TEAMID = "teamId";
	/** Constant field for fight. */
	private static final String FIGHT = "fight";
	/** Constant field for pseudo. */
	private static final String PSEUDO = "pseudo";
	/** Constant field for life. */
	private static final String LIFE = "life";
	/** Constant field for dead. */
	private static final String DEAD = "dead";
	/** Constant field for baseCharacter. */
	private static final String BASECHARACTER = "baseCharacter";
	/** Constant field for baseWeapon. */
	private static final String BASEWEAPON = "baseWeapon";
	/** Constant field for baseSpells. */
	private static final String BASESPELLS = "baseSpells";


	/** FightPlayerDataLoader instance (Singleton). */
	private static FightPlayerDataLoader instance;

	/**
	 * Get the FightPlayerDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static FightPlayerDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new FightPlayerDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private FightPlayerDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected FightPlayer extractItem(final Map<?, ?> columns) {
		final FightPlayer fightPlayer =
				new FightPlayer();

		return this.extractItem(columns, fightPlayer);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param fightPlayer Entity to extract
	 * @return A FightPlayer entity
	 */
	protected FightPlayer extractItem(final Map<?, ?> columns,
				FightPlayer fightPlayer) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			fightPlayer.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = TEAMID;
		if (columns.get(TEAMID) != null) {
			fightPlayer.setTeamId(
				(Integer) columns.get(TEAMID));
		}

		this.currentFieldName = FIGHT;
		if (columns.get(FIGHT) != null) {
			final Fight fight =
				FightDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(FIGHT));
			if (fight != null) {
				fightPlayer.setFight(fight);
			}
		}

		this.currentFieldName = PSEUDO;
		if (columns.get(PSEUDO) != null) {
			fightPlayer.setPseudo(
				(String) columns.get(PSEUDO));
		}

		this.currentFieldName = LIFE;
		if (columns.get(LIFE) != null) {
			fightPlayer.setLife(
				(Integer) columns.get(LIFE));
		}

		this.currentFieldName = DEAD;
		if (columns.get(DEAD) != null) {
			fightPlayer.setDead(
				(Boolean) columns.get(DEAD));
		}

		this.currentFieldName = BASECHARACTER;
		if (columns.get(BASECHARACTER) != null) {
			final CharacterPlayer characterPlayer =
				CharacterPlayerDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(BASECHARACTER));
			if (characterPlayer != null) {
				fightPlayer.setBaseCharacter(characterPlayer);
			}
		}

		this.currentFieldName = BASEWEAPON;
		if (columns.get(BASEWEAPON) != null) {
			final ItemWeapon itemWeapon =
				ItemWeaponDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(BASEWEAPON));
			if (itemWeapon != null) {
				fightPlayer.setBaseWeapon(itemWeapon);
			}
		}

		this.currentFieldName = BASESPELLS;
		if (columns.get(BASESPELLS) != null) {
			ArrayList<Spell> baseSpellssList =
				new ArrayList<Spell>();
			final Map<?, ?> spellsMap =
				(Map<?, ?>) columns.get(BASESPELLS);
			for (final Object spellName : spellsMap.values()) {
				Spell baseSpells = SpellDataLoader.getInstance(this.ctx).get((String) spellName);
				if (baseSpells != null) {
					baseSpellssList.add(baseSpells);
				}
			}
			fightPlayer.setBaseSpells(baseSpellssList);
		}


		return fightPlayer;
	}
	/**
	 * Loads FightPlayers into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final FightPlayer fightPlayer : this.items.values()) {
			fightPlayer.setId(
					manager.persist(fightPlayer));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected FightPlayer get(final String key) {
		final FightPlayer result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
