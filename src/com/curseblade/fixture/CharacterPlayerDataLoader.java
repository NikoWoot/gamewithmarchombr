/**************************************************************************
 * CharacterPlayerDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;

import android.content.Context;

import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemArmury;
import com.curseblade.entity.Spell;
import com.curseblade.entity.ItemWeapon;

import com.curseblade.harmony.util.DateUtils;

/**
 * CharacterPlayerDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class CharacterPlayerDataLoader
						extends FixtureBase<CharacterPlayer> {
	/** CharacterPlayerDataLoader name. */
	private static final String FILE_NAME = "CharacterPlayer";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for pseudo. */
	private static final String PSEUDO = "pseudo";
	/** Constant field for life. */
	private static final String LIFE = "life";
	/** Constant field for createdAt. */
	private static final String CREATEDAT = "createdAt";
	/** Constant field for level. */
	private static final String LEVEL = "level";
	/** Constant field for armuryEquipedItems. */
	private static final String ARMURYEQUIPEDITEMS = "armuryEquipedItems";
	/** Constant field for equipedSpells. */
	private static final String EQUIPEDSPELLS = "equipedSpells";
	/** Constant field for weaponUsed. */
	private static final String WEAPONUSED = "weaponUsed";


	/** CharacterPlayerDataLoader instance (Singleton). */
	private static CharacterPlayerDataLoader instance;

	/**
	 * Get the CharacterPlayerDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static CharacterPlayerDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new CharacterPlayerDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private CharacterPlayerDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected CharacterPlayer extractItem(final Map<?, ?> columns) {
		final CharacterPlayer characterPlayer =
				new CharacterPlayer();

		return this.extractItem(columns, characterPlayer);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param characterPlayer Entity to extract
	 * @return A CharacterPlayer entity
	 */
	protected CharacterPlayer extractItem(final Map<?, ?> columns,
				CharacterPlayer characterPlayer) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			characterPlayer.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = PSEUDO;
		if (columns.get(PSEUDO) != null) {
			characterPlayer.setPseudo(
				(String) columns.get(PSEUDO));
		}

		this.currentFieldName = LIFE;
		if (columns.get(LIFE) != null) {
			characterPlayer.setLife(
				(Integer) columns.get(LIFE));
		}

		this.currentFieldName = CREATEDAT;
		if (columns.get(CREATEDAT) != null) {
			characterPlayer.setCreatedAt(
				DateUtils.formatYAMLStringToDateTime(
					(String) columns.get(CREATEDAT)));
		}

		this.currentFieldName = LEVEL;
		if (columns.get(LEVEL) != null) {
			characterPlayer.setLevel(
				(Integer) columns.get(LEVEL));
		}

		// FIXME : Cast to Map (Yaml return ArrayList)
		this.currentFieldName = ARMURYEQUIPEDITEMS;
		if (columns.get(ARMURYEQUIPEDITEMS) != null) {
			ArrayList<ItemArmury> armuryEquipedItemssList =
				new ArrayList<ItemArmury>();
			final ArrayList itemArmurysMap =
				(ArrayList) columns.get(ARMURYEQUIPEDITEMS);
			for (final Object itemArmuryName : itemArmurysMap) {
				ItemArmury armuryEquipedItems = ItemArmuryDataLoader.getInstance(this.ctx).get((String) itemArmuryName);
				if (armuryEquipedItems != null) {
					armuryEquipedItemssList.add(armuryEquipedItems);
				}
			}
			characterPlayer.setArmuryEquipedItems(armuryEquipedItemssList);
		}

		// FIXME : Cast to Map (Yaml return ArrayList)
		this.currentFieldName = EQUIPEDSPELLS;
		if (columns.get(EQUIPEDSPELLS) != null) {
			ArrayList<Spell> equipedSpellssList =
				new ArrayList<Spell>();
			final ArrayList spellsMap =
				(ArrayList) columns.get(EQUIPEDSPELLS);
			for (final Object spellName : spellsMap) {
				Spell equipedSpells = SpellDataLoader.getInstance(this.ctx).get((String) spellName);
				if (equipedSpells != null) {
					equipedSpellssList.add(equipedSpells);
				}
			}
			characterPlayer.setEquipedSpells(equipedSpellssList);
		}

		this.currentFieldName = WEAPONUSED;
		if (columns.get(WEAPONUSED) != null) {
			final ItemWeapon itemWeapon =
				ItemWeaponDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(WEAPONUSED));
			if (itemWeapon != null) {
				characterPlayer.setWeaponUsed(itemWeapon);
			}
		}


		return characterPlayer;
	}
	/**
	 * Loads CharacterPlayers into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final CharacterPlayer characterPlayer : this.items.values()) {
			characterPlayer.setId(
					manager.persist(characterPlayer));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected CharacterPlayer get(final String key) {
		final CharacterPlayer result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
