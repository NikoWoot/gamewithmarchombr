/**************************************************************************
 * ItemArmuryDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.curseblade.entity.ItemArmury;
import com.curseblade.entity.ItemArmuryType;



/**
 * ItemArmuryDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class ItemArmuryDataLoader
						extends FixtureBase<ItemArmury> {
	/** ItemArmuryDataLoader name. */
	private static final String FILE_NAME = "ItemArmury";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for name. */
	private static final String NAME = "name";
	/** Constant field for defValue. */
	private static final String DEFVALUE = "defValue";
	/** Constant field for armuryType. */
	private static final String ARMURYTYPE = "armuryType";


	/** ItemArmuryDataLoader instance (Singleton). */
	private static ItemArmuryDataLoader instance;

	/**
	 * Get the ItemArmuryDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static ItemArmuryDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new ItemArmuryDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private ItemArmuryDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected ItemArmury extractItem(final Map<?, ?> columns) {
		final ItemArmury itemArmury =
				new ItemArmury();

		return this.extractItem(columns, itemArmury);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param itemArmury Entity to extract
	 * @return A ItemArmury entity
	 */
	protected ItemArmury extractItem(final Map<?, ?> columns,
				ItemArmury itemArmury) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			itemArmury.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = NAME;
		if (columns.get(NAME) != null) {
			itemArmury.setName(
				(String) columns.get(NAME));
		}

		this.currentFieldName = DEFVALUE;
		if (columns.get(DEFVALUE) != null) {
			itemArmury.setDefValue(
				(Integer) columns.get(DEFVALUE));
		}

		this.currentFieldName = ARMURYTYPE;
		if (columns.get(ARMURYTYPE) != null) {
			itemArmury.setArmuryType(ItemArmuryType.valueOf(
				(String) columns.get(ARMURYTYPE)));
		}


		return itemArmury;
	}
	/**
	 * Loads ItemArmurys into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final ItemArmury itemArmury : this.items.values()) {
			itemArmury.setId(
					manager.persist(itemArmury));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected ItemArmury get(final String key) {
		final ItemArmury result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
