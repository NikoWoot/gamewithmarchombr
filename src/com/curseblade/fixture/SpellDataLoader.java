/**************************************************************************
 * SpellDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.curseblade.entity.Spell;



/**
 * SpellDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class SpellDataLoader
						extends FixtureBase<Spell> {
	/** SpellDataLoader name. */
	private static final String FILE_NAME = "Spell";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for name. */
	private static final String NAME = "name";
	/** Constant field for baseAttack. */
	private static final String BASEATTACK = "baseAttack";
	/** Constant field for criticalAttack. */
	private static final String CRITICALATTACK = "criticalAttack";


	/** SpellDataLoader instance (Singleton). */
	private static SpellDataLoader instance;

	/**
	 * Get the SpellDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static SpellDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new SpellDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private SpellDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected Spell extractItem(final Map<?, ?> columns) {
		final Spell spell =
				new Spell();

		return this.extractItem(columns, spell);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param spell Entity to extract
	 * @return A Spell entity
	 */
	protected Spell extractItem(final Map<?, ?> columns,
				Spell spell) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			spell.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = NAME;
		if (columns.get(NAME) != null) {
			spell.setName(
				(String) columns.get(NAME));
		}

		this.currentFieldName = BASEATTACK;
		if (columns.get(BASEATTACK) != null) {
			spell.setBaseAttack(
				(Double) columns.get(BASEATTACK));
		}

		this.currentFieldName = CRITICALATTACK;
		if (columns.get(CRITICALATTACK) != null) {
			spell.setCriticalAttack(
				(Double) columns.get(CRITICALATTACK));
		}


		return spell;
	}
	/**
	 * Loads Spells into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final Spell spell : this.items.values()) {
			spell.setId(
					manager.persist(spell));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected Spell get(final String key) {
		final Spell result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
