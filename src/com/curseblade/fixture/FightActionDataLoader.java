/**************************************************************************
 * FightActionDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.curseblade.entity.FightAction;
import com.curseblade.entity.Fight;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.FightActionType;



/**
 * FightActionDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class FightActionDataLoader
						extends FixtureBase<FightAction> {
	/** FightActionDataLoader name. */
	private static final String FILE_NAME = "FightAction";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for fight. */
	private static final String FIGHT = "fight";
	/** Constant field for sender. */
	private static final String SENDER = "sender";
	/** Constant field for receiver. */
	private static final String RECEIVER = "receiver";
	/** Constant field for actionType. */
	private static final String ACTIONTYPE = "actionType";
	/** Constant field for beforeValue. */
	private static final String BEFOREVALUE = "beforeValue";
	/** Constant field for afterValue. */
	private static final String AFTERVALUE = "afterValue";


	/** FightActionDataLoader instance (Singleton). */
	private static FightActionDataLoader instance;

	/**
	 * Get the FightActionDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static FightActionDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new FightActionDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private FightActionDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected FightAction extractItem(final Map<?, ?> columns) {
		final FightAction fightAction =
				new FightAction();

		return this.extractItem(columns, fightAction);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param fightAction Entity to extract
	 * @return A FightAction entity
	 */
	protected FightAction extractItem(final Map<?, ?> columns,
				FightAction fightAction) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			fightAction.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = FIGHT;
		if (columns.get(FIGHT) != null) {
			final Fight fight =
				FightDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(FIGHT));
			if (fight != null) {
				fightAction.setFight(fight);
			}
		}

		this.currentFieldName = SENDER;
		if (columns.get(SENDER) != null) {
			final FightPlayer fightPlayer =
				FightPlayerDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(SENDER));
			if (fightPlayer != null) {
				fightAction.setSender(fightPlayer);
			}
		}

		this.currentFieldName = RECEIVER;
		if (columns.get(RECEIVER) != null) {
			final FightPlayer fightPlayer =
				FightPlayerDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(RECEIVER));
			if (fightPlayer != null) {
				fightAction.setReceiver(fightPlayer);
			}
		}

		this.currentFieldName = ACTIONTYPE;
		if (columns.get(ACTIONTYPE) != null) {
			fightAction.setActionType(FightActionType.valueOf(
				(String) columns.get(ACTIONTYPE)));
		}

		this.currentFieldName = BEFOREVALUE;
		if (columns.get(BEFOREVALUE) != null) {
			fightAction.setBeforeValue(
				(Integer) columns.get(BEFOREVALUE));
		}

		this.currentFieldName = AFTERVALUE;
		if (columns.get(AFTERVALUE) != null) {
			fightAction.setAfterValue(
				(Integer) columns.get(AFTERVALUE));
		}


		return fightAction;
	}
	/**
	 * Loads FightActions into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final FightAction fightAction : this.items.values()) {
			fightAction.setId(
					manager.persist(fightAction));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected FightAction get(final String key) {
		final FightAction result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
