/**************************************************************************
 * HarmonyRssItemDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.Date;
import java.util.Map;

import org.joda.time.DateTime;

import android.content.Context;

import com.curseblade.entity.HarmonyRssItem;

import com.curseblade.harmony.util.DateUtils;

/**
 * HarmonyRssItemDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class HarmonyRssItemDataLoader
						extends FixtureBase<HarmonyRssItem> {
	/** HarmonyRssItemDataLoader name. */
	private static final String FILE_NAME = "HarmonyRssItem";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for hash. */
	private static final String HASH = "hash";
	/** Constant field for guid. */
	private static final String GUID = "guid";
	/** Constant field for title. */
	private static final String TITLE = "title";
	/** Constant field for link. */
	private static final String LINK = "link";
	/** Constant field for description. */
	private static final String DESCRIPTION = "description";
	/** Constant field for enclosure. */
	private static final String ENCLOSURE = "enclosure";
	/** Constant field for author. */
	private static final String AUTHOR = "author";
	/** Constant field for pubDate. */
	private static final String PUBDATE = "pubDate";
	/** Constant field for categories. */
	private static final String CATEGORIES = "categories";


	/** HarmonyRssItemDataLoader instance (Singleton). */
	private static HarmonyRssItemDataLoader instance;

	/**
	 * Get the HarmonyRssItemDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static HarmonyRssItemDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new HarmonyRssItemDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private HarmonyRssItemDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected HarmonyRssItem extractItem(final Map<?, ?> columns) {
		final HarmonyRssItem harmonyRssItem =
				new HarmonyRssItem();

		return this.extractItem(columns, harmonyRssItem);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param harmonyRssItem Entity to extract
	 * @return A HarmonyRssItem entity
	 */
	protected HarmonyRssItem extractItem(final Map<?, ?> columns,
				HarmonyRssItem harmonyRssItem) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			harmonyRssItem.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = HASH;
		if (columns.get(HASH) != null) {
			harmonyRssItem.setHash(
				(Integer) columns.get(HASH));
		}

		this.currentFieldName = GUID;
		if (columns.get(GUID) != null) {
			harmonyRssItem.setGuid(
				(String) columns.get(GUID));
		}

		this.currentFieldName = TITLE;
		if (columns.get(TITLE) != null) {
			harmonyRssItem.setTitle(
				(String) columns.get(TITLE));
		}

		this.currentFieldName = LINK;
		if (columns.get(LINK) != null) {
			harmonyRssItem.setLink(
				(String) columns.get(LINK));
		}

		this.currentFieldName = DESCRIPTION;
		if (columns.get(DESCRIPTION) != null) {
			harmonyRssItem.setDescription(
				(String) columns.get(DESCRIPTION));
		}

		this.currentFieldName = ENCLOSURE;
		if (columns.get(ENCLOSURE) != null) {
			harmonyRssItem.setEnclosure(
				(String) columns.get(ENCLOSURE));
		}

		this.currentFieldName = AUTHOR;
		if (columns.get(AUTHOR) != null) {
			harmonyRssItem.setAuthor(
				(String) columns.get(AUTHOR));
		}

		this.currentFieldName = PUBDATE;
		if (columns.get(PUBDATE) != null) {
			harmonyRssItem.setPubDate(
				DateUtils.formatYAMLStringToDateTime(
					(String) columns.get(PUBDATE)));
		}

		this.currentFieldName = CATEGORIES;
		if (columns.get(CATEGORIES) != null) {
			harmonyRssItem.setCategories(
				(String) columns.get(CATEGORIES));
		}


		return harmonyRssItem;
	}
	/**
	 * Loads HarmonyRssItems into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final HarmonyRssItem harmonyRssItem : this.items.values()) {
			harmonyRssItem.setId(
					manager.persist(harmonyRssItem));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected HarmonyRssItem get(final String key) {
		final HarmonyRssItem result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else if (NewsDataLoader.getInstance(this.ctx).items.containsKey(key)) {
			result = NewsDataLoader.getInstance(this.ctx).items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
