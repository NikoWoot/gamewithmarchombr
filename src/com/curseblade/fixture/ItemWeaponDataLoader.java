/**************************************************************************
 * ItemWeaponDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.Map;

import android.content.Context;

import com.curseblade.entity.ItemWeapon;
import com.curseblade.entity.ItemWeaponType;



/**
 * ItemWeaponDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class ItemWeaponDataLoader
						extends FixtureBase<ItemWeapon> {
	/** ItemWeaponDataLoader name. */
	private static final String FILE_NAME = "ItemWeapon";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for name. */
	private static final String NAME = "name";
	/** Constant field for baseAttack. */
	private static final String BASEATTACK = "baseAttack";
	/** Constant field for weaponType. */
	private static final String WEAPONTYPE = "weaponType";


	/** ItemWeaponDataLoader instance (Singleton). */
	private static ItemWeaponDataLoader instance;

	/**
	 * Get the ItemWeaponDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static ItemWeaponDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new ItemWeaponDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private ItemWeaponDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected ItemWeapon extractItem(final Map<?, ?> columns) {
		final ItemWeapon itemWeapon =
				new ItemWeapon();

		return this.extractItem(columns, itemWeapon);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param itemWeapon Entity to extract
	 * @return A ItemWeapon entity
	 */
	protected ItemWeapon extractItem(final Map<?, ?> columns,
				ItemWeapon itemWeapon) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			itemWeapon.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = NAME;
		if (columns.get(NAME) != null) {
			itemWeapon.setName(
				(String) columns.get(NAME));
		}

		this.currentFieldName = BASEATTACK;
		if (columns.get(BASEATTACK) != null) {
			itemWeapon.setBaseAttack(
				(Double) columns.get(BASEATTACK));
		}

		this.currentFieldName = WEAPONTYPE;
		if (columns.get(WEAPONTYPE) != null) {
			itemWeapon.setWeaponType(ItemWeaponType.valueOf(
				(String) columns.get(WEAPONTYPE)));
		}


		return itemWeapon;
	}
	/**
	 * Loads ItemWeapons into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final ItemWeapon itemWeapon : this.items.values()) {
			itemWeapon.setId(
					manager.persist(itemWeapon));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected ItemWeapon get(final String key) {
		final ItemWeapon result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
