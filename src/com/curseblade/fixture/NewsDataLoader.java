/**************************************************************************
 * NewsDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.Map;

import android.content.Context;

import com.curseblade.entity.News;



/**
 * NewsDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class NewsDataLoader
						extends FixtureBase<News> {
	/** NewsDataLoader name. */
	private static final String FILE_NAME = "News";

	/** Constant field for id. */
	private static final String ID = "id";


	/** NewsDataLoader instance (Singleton). */
	private static NewsDataLoader instance;

	/**
	 * Get the NewsDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static NewsDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new NewsDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private NewsDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected News extractItem(final Map<?, ?> columns) {
		final News news =
				new News();

		return this.extractItem(columns, news);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param news Entity to extract
	 * @return A News entity
	 */
	protected News extractItem(final Map<?, ?> columns,
				News news) {
		HarmonyRssItemDataLoader.getInstance(this.ctx).extractItem(columns, news);


		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			news.setId(
				(Integer) columns.get(ID));
		}


		return news;
	}
	/**
	 * Loads Newss into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final News news : this.items.values()) {
			news.setId(
					manager.persist(news));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected News get(final String key) {
		final News result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
