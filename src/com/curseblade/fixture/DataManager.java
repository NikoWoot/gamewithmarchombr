/**************************************************************************
 * DataManager.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.HashMap;
import java.util.Map;

import com.curseblade.data.base.SQLiteAdapterBase;
import com.curseblade.data.ItemArmurySQLiteAdapter;
import com.curseblade.entity.ItemArmury;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.entity.Fight;
import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.data.FightActionSQLiteAdapter;
import com.curseblade.entity.FightAction;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;
import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.data.TournamentSQLiteAdapter;
import com.curseblade.entity.Tournament;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;
import com.curseblade.entity.HarmonyRssItem;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.entity.Spell;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.entity.FightPlayer;

/**
 * DataManager.
 * 
 * This class is an "orm-like" manager which simplifies insertion in database
 * with sqlite adapters.
 */
public class DataManager {
	/** HashMap to join Entity Name and its SQLiteAdapterBase. */
	protected Map<String, SQLiteAdapterBase<?>> adapters =
			new HashMap<String, SQLiteAdapterBase<?>>();
	/** is successfull. */
	protected boolean isSuccessfull = true;
	/** is in internal transaction. */
	protected boolean isInInternalTransaction = false;
	/** database. */
	protected SQLiteDatabase db;
	/** ItemArmury name constant. */
	private static final String ITEMARMURY = "ItemArmury";
	/** Fight name constant. */
	private static final String FIGHT = "Fight";
	/** ItemWeapon name constant. */
	private static final String ITEMWEAPON = "ItemWeapon";
	/** FightAction name constant. */
	private static final String FIGHTACTION = "FightAction";
	/** TournamentPoolFight name constant. */
	private static final String TOURNAMENTPOOLFIGHT = "TournamentPoolFight";
	/** Tournament name constant. */
	private static final String TOURNAMENT = "Tournament";
	/** HarmonyRssItem name constant. */
	private static final String HARMONYRSSITEM = "HarmonyRssItem";
	/** Spell name constant. */
	private static final String SPELL = "Spell";
	/** CharacterPlayer name constant. */
	private static final String CHARACTERPLAYER = "CharacterPlayer";
	/** FightPlayer name constant. */
	private static final String FIGHTPLAYER = "FightPlayer";
	/**
	 * Constructor.
	 * @param ctx The context
	 * @param db The DB to work in
	 */
	public DataManager(final Context ctx, final SQLiteDatabase db) {
		this.db = db;
		this.adapters.put(ITEMARMURY,
				new ItemArmurySQLiteAdapter(ctx));
		this.adapters.get(ITEMARMURY).open(this.db);
		this.adapters.put(FIGHT,
				new FightSQLiteAdapter(ctx));
		this.adapters.get(FIGHT).open(this.db);
		this.adapters.put(ITEMWEAPON,
				new ItemWeaponSQLiteAdapter(ctx));
		this.adapters.get(ITEMWEAPON).open(this.db);
		this.adapters.put(FIGHTACTION,
				new FightActionSQLiteAdapter(ctx));
		this.adapters.get(FIGHTACTION).open(this.db);
		this.adapters.put(TOURNAMENTPOOLFIGHT,
				new TournamentPoolFightSQLiteAdapter(ctx));
		this.adapters.get(TOURNAMENTPOOLFIGHT).open(this.db);
		this.adapters.put(TOURNAMENT,
				new TournamentSQLiteAdapter(ctx));
		this.adapters.get(TOURNAMENT).open(this.db);
		this.adapters.put(HARMONYRSSITEM,
				new HarmonyRssItemSQLiteAdapter(ctx));
		this.adapters.get(HARMONYRSSITEM).open(this.db);
		this.adapters.put(SPELL,
				new SpellSQLiteAdapter(ctx));
		this.adapters.get(SPELL).open(this.db);
		this.adapters.put(CHARACTERPLAYER,
				new CharacterPlayerSQLiteAdapter(ctx));
		this.adapters.get(CHARACTERPLAYER).open(this.db);
		this.adapters.put(FIGHTPLAYER,
				new FightPlayerSQLiteAdapter(ctx));
		this.adapters.get(FIGHTPLAYER).open(this.db);
	}
	/**
     * Finds a object by its identifier.
     *
     * This is just a convenient shortcut for getRepository($className)
     * ->find($id).
     *
     * @param nameClass The class of the object's name
     * @param id The id of the object
     * @return The found object
     */
    public Object find(final String nameClass, final int id) {
    	Object ret = null;
    	this.beginTransaction();

    	if (nameClass.equals(ITEMARMURY)) {
        	ret = ((ItemArmurySQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}
    	if (nameClass.equals(FIGHT)) {
        	ret = ((FightSQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}
    	if (nameClass.equals(ITEMWEAPON)) {
        	ret = ((ItemWeaponSQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}
    	if (nameClass.equals(FIGHTACTION)) {
        	ret = ((FightActionSQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}
    	if (nameClass.equals(TOURNAMENTPOOLFIGHT)) {
        	ret = ((TournamentPoolFightSQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}
    	if (nameClass.equals(TOURNAMENT)) {
        	ret = ((TournamentSQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}
    	if (nameClass.equals(HARMONYRSSITEM)) {
        	ret = ((HarmonyRssItemSQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}
    	if (nameClass.equals(SPELL)) {
        	ret = ((SpellSQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}
    	if (nameClass.equals(CHARACTERPLAYER)) {
        	ret = ((CharacterPlayerSQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}
    	if (nameClass.equals(FIGHTPLAYER)) {
        	ret = ((FightPlayerSQLiteAdapter)
        							   this.adapters.get(nameClass)).query(id);
    	}

    	return ret;
    }

    /**
     * Tells the ObjectManager to make an instance managed and persistent.
     *
     * The object will be entered into the database as a result of the <br />
     * flush operation.
     *
     * NOTE: The persist operation always considers objects that are not<br />
     * yet known to this ObjectManager as NEW. Do not pass detached <br />
     * objects to the persist operation.
     *
     * @param object $object The instance to make managed and persistent.
     * @return Count of objects entered into the DB
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public int persist(final Object object) {
    	int result;

    	this.beginTransaction();
    	try {
    		final SQLiteAdapterBase adapter = this.getRepository(object);

    		result = (int) adapter.insert(object);
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		this.isSuccessfull = false;
    		result = 0;
    	}

    	return result;
    }

    /**
     * Removes an object instance.
     *
     * A removed object will be removed from the database as a result of <br />
     * the flush operation.
     *
     * @param object $object The object instance to remove.
     */
    public void remove(final Object object) {
    	this.beginTransaction();
    	try {
    		if (object instanceof ItemArmury) {
    			((ItemArmurySQLiteAdapter)
    					this.adapters.get(ITEMARMURY))
    						.remove(((ItemArmury) object).getId());
    		}
    		if (object instanceof Fight) {
    			((FightSQLiteAdapter)
    					this.adapters.get(FIGHT))
    						.remove(((Fight) object).getId());
    		}
    		if (object instanceof ItemWeapon) {
    			((ItemWeaponSQLiteAdapter)
    					this.adapters.get(ITEMWEAPON))
    						.remove(((ItemWeapon) object).getId());
    		}
    		if (object instanceof FightAction) {
    			((FightActionSQLiteAdapter)
    					this.adapters.get(FIGHTACTION))
    						.remove(((FightAction) object).getId());
    		}
    		if (object instanceof TournamentPoolFight) {
    			((TournamentPoolFightSQLiteAdapter)
    					this.adapters.get(TOURNAMENTPOOLFIGHT))
    						.remove(((TournamentPoolFight) object).getId());
    		}
    		if (object instanceof Tournament) {
    			((TournamentSQLiteAdapter)
    					this.adapters.get(TOURNAMENT))
    						.remove(((Tournament) object).getId());
    		}
    		if (object instanceof HarmonyRssItem) {
    			((HarmonyRssItemSQLiteAdapter)
    					this.adapters.get(HARMONYRSSITEM))
    						.remove(((HarmonyRssItem) object).getId());
    		}
    		if (object instanceof Spell) {
    			((SpellSQLiteAdapter)
    					this.adapters.get(SPELL))
    						.remove(((Spell) object).getId());
    		}
    		if (object instanceof CharacterPlayer) {
    			((CharacterPlayerSQLiteAdapter)
    					this.adapters.get(CHARACTERPLAYER))
    						.remove(((CharacterPlayer) object).getId());
    		}
    		if (object instanceof FightPlayer) {
    			((FightPlayerSQLiteAdapter)
    					this.adapters.get(FIGHTPLAYER))
    						.remove(((FightPlayer) object).getId());
    		}
    	} catch (Exception ex) {
    		this.isSuccessfull = false;
    	}
    }

//    /**
//     * Merges the state of a detached object into the persistence context
//     * of this ObjectManager and returns the managed copy of the object.
//     * The object passed to merge will not become associated/managed with
//	   * this ObjectManager.
//     *
//     * @param object $object
//     */
//    public void merge(Object object) {
//
//    }
//
//    /**
//     * Clears the ObjectManager. All objects that are currently managed
//     * by this ObjectManager become detached.
//     *
//     * @param objectName $objectName if given, only objects of this type will
//     * get detached
//     */
//    public void clear(String objectName) {
//
//    }
//
//    /**
//     * Detaches an object from the ObjectManager, causing a managed object to
//     * become detached. Unflushed changes made to the object if any
//     * (including removal of the object), will not be synchronized to the
//     * database.
//     * Objects which previously referenced the detached object will continue
//     * to reference it.
//     *
//     * @param object $object The object to detach.
//     */
//    public void detach(Object object) {
//
//    }
//
//    /**
//     * Refreshes the persistent state of an object from the database,
//     * overriding any local changes that have not yet been persisted.
//     *
//     * @param object $object The object to refresh.
//     */
//    public void refresh(Object object) {
//
//    }

    /**
     * Flushes all changes to objects that have been queued up to now to <br />
     * the database. This effectively synchronizes the in-memory state of<br />
     * managed objects with the database.
     */
    public void flush() {
    	if (this.isInInternalTransaction) {
    		if (this.isSuccessfull) {
    			this.db.setTransactionSuccessful();
    		}
    		this.db.endTransaction();
    		this.isInInternalTransaction = false;
    	}
    }

    /**
     * Gets the repository for a class.
     *
     * @param className $className
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public SQLiteAdapterBase<?> getRepository(final String className) {
    	return this.adapters.get(className);
    }


    /**
     * Gets the repository for a given object.
     *
     * @param o object
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
	private SQLiteAdapterBase<?> getRepository(final Object o) {
		final String className = o.getClass().getSimpleName();

		return this.getRepository(className);
	}

//    /**
//     * Returns the ClassMetadata descriptor for a class.
//     *
//     * The class name must be the fully-qualified class name without a <br />
//     * leading backslash (as it is returned by get_class($obj)).
//     *
//     * @param className $className
//     * @return \Doctrine\Common\Persistence\Mapping\ClassMetadata
//     */
//    public ClassMetadata getClassMetadata(final String className) {
//    	return null;
//    }

    /**
     * Check if the object is part of the current UnitOfWork and therefore
     * managed.
     *
     * @param object $object
     * @return bool
     */
    public boolean contains(final Object object) {
    	return false;
    }

    /**
     * Called before any transaction to open the DB.
     */
    private void beginTransaction() {
    	// If we are not already in a transaction, begin it
    	if (!this.isInInternalTransaction) {
    		this.db.beginTransaction();
    		this.isSuccessfull = true;
    		this.isInInternalTransaction = true;
    	}
    }

}
