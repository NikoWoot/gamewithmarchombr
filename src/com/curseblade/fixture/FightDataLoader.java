/**************************************************************************
 * FightDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.curseblade.entity.Fight;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.FightAction;



/**
 * FightDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class FightDataLoader
						extends FixtureBase<Fight> {
	/** FightDataLoader name. */
	private static final String FILE_NAME = "Fight";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for starterFighters. */
	private static final String STARTERFIGHTERS = "starterFighters";
	/** Constant field for alternatedFighters. */
	private static final String ALTERNATEDFIGHTERS = "alternatedFighters";
	/** Constant field for survivorFighters. */
	private static final String SURVIVORFIGHTERS = "survivorFighters";
	/** Constant field for actions. */
	private static final String ACTIONS = "actions";
	/** Constant field for senderPlayer. */
	private static final String SENDERPLAYER = "senderPlayer";
	/** Constant field for receiverPlayer. */
	private static final String RECEIVERPLAYER = "receiverPlayer";
	/** Constant field for currentElement. */
	private static final String CURRENTELEMENT = "currentElement";


	/** FightDataLoader instance (Singleton). */
	private static FightDataLoader instance;

	/**
	 * Get the FightDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static FightDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new FightDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private FightDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected Fight extractItem(final Map<?, ?> columns) {
		final Fight fight =
				new Fight();

		return this.extractItem(columns, fight);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param fight Entity to extract
	 * @return A Fight entity
	 */
	protected Fight extractItem(final Map<?, ?> columns,
				Fight fight) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			fight.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = STARTERFIGHTERS;
		if (columns.get(STARTERFIGHTERS) != null) {
			ArrayList<FightPlayer> starterFighterssList =
				new ArrayList<FightPlayer>();
			final Map<?, ?> fightPlayersMap =
				(Map<?, ?>) columns.get(STARTERFIGHTERS);
			for (final Object fightPlayerName : fightPlayersMap.values()) {
				FightPlayer starterFighters = FightPlayerDataLoader.getInstance(this.ctx).get((String) fightPlayerName);
				if (starterFighters != null) {
					starterFighterssList.add(starterFighters);
				}
			}
			fight.setStarterFighters(starterFighterssList);
		}

		this.currentFieldName = ALTERNATEDFIGHTERS;
		if (columns.get(ALTERNATEDFIGHTERS) != null) {
			ArrayList<FightPlayer> alternatedFighterssList =
				new ArrayList<FightPlayer>();
			final Map<?, ?> fightPlayersMap =
				(Map<?, ?>) columns.get(ALTERNATEDFIGHTERS);
			for (final Object fightPlayerName : fightPlayersMap.values()) {
				FightPlayer alternatedFighters = FightPlayerDataLoader.getInstance(this.ctx).get((String) fightPlayerName);
				if (alternatedFighters != null) {
					alternatedFighterssList.add(alternatedFighters);
				}
			}
			fight.setAlternatedFighters(alternatedFighterssList);
		}

		this.currentFieldName = SURVIVORFIGHTERS;
		if (columns.get(SURVIVORFIGHTERS) != null) {
			ArrayList<FightPlayer> survivorFighterssList =
				new ArrayList<FightPlayer>();
			final Map<?, ?> fightPlayersMap =
				(Map<?, ?>) columns.get(SURVIVORFIGHTERS);
			for (final Object fightPlayerName : fightPlayersMap.values()) {
				FightPlayer survivorFighters = FightPlayerDataLoader.getInstance(this.ctx).get((String) fightPlayerName);
				if (survivorFighters != null) {
					survivorFighterssList.add(survivorFighters);
				}
			}
			fight.setSurvivorFighters(survivorFighterssList);
		}

		this.currentFieldName = ACTIONS;
		if (columns.get(ACTIONS) != null) {
			ArrayList<FightAction> actionssList =
				new ArrayList<FightAction>();
			final Map<?, ?> fightActionsMap =
				(Map<?, ?>) columns.get(ACTIONS);
			for (final Object fightActionName : fightActionsMap.values()) {
				FightAction actions = FightActionDataLoader.getInstance(this.ctx).get((String) fightActionName);
				if (actions != null) {
					actionssList.add(actions);
				}
			}
			fight.setActions(actionssList);
		}

		this.currentFieldName = SENDERPLAYER;
		if (columns.get(SENDERPLAYER) != null) {
			final FightPlayer fightPlayer =
				FightPlayerDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(SENDERPLAYER));
			if (fightPlayer != null) {
				fight.setSenderPlayer(fightPlayer);
			}
		}

		this.currentFieldName = RECEIVERPLAYER;
		if (columns.get(RECEIVERPLAYER) != null) {
			final FightPlayer fightPlayer =
				FightPlayerDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(RECEIVERPLAYER));
			if (fightPlayer != null) {
				fight.setReceiverPlayer(fightPlayer);
			}
		}

		this.currentFieldName = CURRENTELEMENT;
		if (columns.get(CURRENTELEMENT) != null) {
			fight.setCurrentElement(
				(Integer) columns.get(CURRENTELEMENT));
		}


		return fight;
	}
	/**
	 * Loads Fights into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final Fight fight : this.items.values()) {
			fight.setId(
					manager.persist(fight));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected Fight get(final String key) {
		final Fight result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
