/**************************************************************************
 * TournamentDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.curseblade.entity.Tournament;
import com.curseblade.entity.TournamentPoolFight;



/**
 * TournamentDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class TournamentDataLoader
						extends FixtureBase<Tournament> {
	/** TournamentDataLoader name. */
	private static final String FILE_NAME = "Tournament";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for maxElements. */
	private static final String MAXELEMENTS = "maxElements";
	/** Constant field for poolFights. */
	private static final String POOLFIGHTS = "poolFights";


	/** TournamentDataLoader instance (Singleton). */
	private static TournamentDataLoader instance;

	/**
	 * Get the TournamentDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static TournamentDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new TournamentDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private TournamentDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected Tournament extractItem(final Map<?, ?> columns) {
		final Tournament tournament =
				new Tournament();

		return this.extractItem(columns, tournament);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param tournament Entity to extract
	 * @return A Tournament entity
	 */
	protected Tournament extractItem(final Map<?, ?> columns,
				Tournament tournament) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			tournament.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = MAXELEMENTS;
		if (columns.get(MAXELEMENTS) != null) {
			tournament.setMaxElements(
				(Integer) columns.get(MAXELEMENTS));
		}

		this.currentFieldName = POOLFIGHTS;
		if (columns.get(POOLFIGHTS) != null) {
			ArrayList<TournamentPoolFight> poolFightssList =
				new ArrayList<TournamentPoolFight>();
			final Map<?, ?> tournamentPoolFightsMap =
				(Map<?, ?>) columns.get(POOLFIGHTS);
			for (final Object tournamentPoolFightName : tournamentPoolFightsMap.values()) {
				TournamentPoolFight poolFights = TournamentPoolFightDataLoader.getInstance(this.ctx).get((String) tournamentPoolFightName);
				if (poolFights != null) {
					poolFightssList.add(poolFights);
				}
			}
			tournament.setPoolFights(poolFightssList);
		}


		return tournament;
	}
	/**
	 * Loads Tournaments into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final Tournament tournament : this.items.values()) {
			tournament.setId(
					manager.persist(tournament));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected Tournament get(final String key) {
		final Tournament result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
