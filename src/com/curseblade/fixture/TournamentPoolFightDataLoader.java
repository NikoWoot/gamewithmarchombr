/**************************************************************************
 * TournamentPoolFightDataLoader.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.fixture;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.entity.FightPlayer;



/**
 * TournamentPoolFightDataLoader.
 *
 * This dataloader implements the parsing method needed while reading
 * the fixtures files.
 */
public final class TournamentPoolFightDataLoader
						extends FixtureBase<TournamentPoolFight> {
	/** TournamentPoolFightDataLoader name. */
	private static final String FILE_NAME = "TournamentPoolFight";

	/** Constant field for id. */
	private static final String ID = "id";
	/** Constant field for nodeLevel. */
	private static final String NODELEVEL = "nodeLevel";
	/** Constant field for winner. */
	private static final String WINNER = "winner";
	/** Constant field for parentPool. */
	private static final String PARENTPOOL = "parentPool";
	/** Constant field for leftPool. */
	private static final String LEFTPOOL = "leftPool";
	/** Constant field for rightPool. */
	private static final String RIGHTPOOL = "rightPool";


	/** TournamentPoolFightDataLoader instance (Singleton). */
	private static TournamentPoolFightDataLoader instance;

	/**
	 * Get the TournamentPoolFightDataLoader singleton.
	 * @param ctx The context
	 * @return The dataloader instance
	 */
	public static TournamentPoolFightDataLoader getInstance(
											final Context ctx) {
		if (instance == null) {
			instance = new TournamentPoolFightDataLoader(ctx);
		}
		return instance;
	}

	/**
	 * Constructor.
	 * @param ctx The context
	 */
	private TournamentPoolFightDataLoader(final Context ctx) {
		super(ctx);
	}


	@Override
	protected TournamentPoolFight extractItem(final Map<?, ?> columns) {
		final TournamentPoolFight tournamentPoolFight =
				new TournamentPoolFight();

		return this.extractItem(columns, tournamentPoolFight);
	}
	/**
	 * Extract an entity from a fixture element (YML).
	 * @param columns Columns to extract
	 * @param tournamentPoolFight Entity to extract
	 * @return A TournamentPoolFight entity
	 */
	protected TournamentPoolFight extractItem(final Map<?, ?> columns,
				TournamentPoolFight tournamentPoolFight) {

		this.currentFieldName = ID;
		if (columns.get(ID) != null) {
			tournamentPoolFight.setId(
				(Integer) columns.get(ID));
		}

		this.currentFieldName = NODELEVEL;
		if (columns.get(NODELEVEL) != null) {
			tournamentPoolFight.setNodeLevel(
				(Integer) columns.get(NODELEVEL));
		}

		this.currentFieldName = WINNER;
		if (columns.get(WINNER) != null) {
			final FightPlayer fightPlayer =
				FightPlayerDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(WINNER));
			if (fightPlayer != null) {
				tournamentPoolFight.setWinner(fightPlayer);
			}
		}

		this.currentFieldName = PARENTPOOL;
		if (columns.get(PARENTPOOL) != null) {
			final TournamentPoolFight tournamentPoolFight1 =
				TournamentPoolFightDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(PARENTPOOL));
			if (tournamentPoolFight1 != null) {
				tournamentPoolFight.setParentPool(tournamentPoolFight1);
			}
		}

		this.currentFieldName = LEFTPOOL;
		if (columns.get(LEFTPOOL) != null) {
			final TournamentPoolFight tournamentPoolFight2 =
				TournamentPoolFightDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(LEFTPOOL));
			if (tournamentPoolFight2 != null) {
				tournamentPoolFight.setLeftPool(tournamentPoolFight2);
			}
		}

		this.currentFieldName = RIGHTPOOL;
		if (columns.get(RIGHTPOOL) != null) {
			final TournamentPoolFight tournamentPoolFight3 =
				TournamentPoolFightDataLoader.getInstance(
						this.ctx).get(
								(String) columns.get(RIGHTPOOL));
			if (tournamentPoolFight3 != null) {
				tournamentPoolFight.setRightPool(tournamentPoolFight3);
			}
		}


		return tournamentPoolFight;
	}
	/**
	 * Loads TournamentPoolFights into the DataManager.
	 * @param manager The DataManager
	 */
	@Override
	public void load(final DataManager manager) {
		for (final TournamentPoolFight tournamentPoolFight : this.items.values()) {
			tournamentPoolFight.setId(
					manager.persist(tournamentPoolFight));
		}
		manager.flush();
	}

	/**
	 * Give priority for fixtures insertion in database.
	 * 0 is the first.
	 * @return The order
	 */
	@Override
	public int getOrder() {
		return 0;
	}

	/**
	 * Get the fixture file name.
	 * @return A String representing the file name
	 */
	@Override
	public String getFixtureFileName() {
		return FILE_NAME;
	}

	@Override
	protected TournamentPoolFight get(final String key) {
		final TournamentPoolFight result;
		if (this.items.containsKey(key)) {
			result = this.items.get(key);
		}
		else {
			result = null;
		}
		return result;
	}
}
