/**************************************************************************
 * HomeActivity.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade;

import com.curseblade.harmony.view.HarmonyFragmentActivity;
import com.curseblade.start.CharRepresentationActivity;
import com.curseblade.start.FightGameActivity;
import com.curseblade.view.itemarmury.ItemArmuryListActivity;
import com.curseblade.view.fight.FightListActivity;
import com.curseblade.view.itemweapon.ItemWeaponListActivity;
import com.curseblade.view.tournamentpoolfight.TournamentPoolFightListActivity;
import com.curseblade.view.tournament.TournamentListActivity;
import com.curseblade.view.spell.SpellListActivity;
import com.curseblade.view.characterplayer.CharacterPlayerListActivity;
import com.curseblade.view.fightplayer.FightPlayerListActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * Home Activity. This is from where you can access to your entities activities
 * by default. BEWARE : This class is regenerated with orm:generate:crud. Don't
 * modify it.
 * 
 * @see android.app.Activity
 */
public class HarmonyHomeActivity extends HarmonyFragmentActivity implements
		OnClickListener {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.main);

		this.initButtons();
	}

	/**
	 * Initialize the buttons click listeners.
	 */
	private void initButtons() {
		this.findViewById(R.id.itemarmury_list_button).setOnClickListener(this);
		this.findViewById(R.id.fight_list_button).setOnClickListener(this);
		this.findViewById(R.id.itemweapon_list_button).setOnClickListener(this);
		this.findViewById(R.id.tournamentpoolfight_list_button)
				.setOnClickListener(this);
		this.findViewById(R.id.tournament_list_button).setOnClickListener(this);
		this.findViewById(R.id.spell_list_button).setOnClickListener(this);
		this.findViewById(R.id.characterplayer_list_button).setOnClickListener(
				this);
		this.findViewById(R.id.fightplayer_list_button)
				.setOnClickListener(this);
		this.findViewById(R.id.myFight_list_button).setOnClickListener(this);
		this.findViewById(R.id.myAvatar_list_btn).setOnClickListener(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
		case R.id.itemarmury_list_button:
			intent = new Intent(this, ItemArmuryListActivity.class);
			break;

		case R.id.fight_list_button:
			intent = new Intent(this, FightListActivity.class);
			break;

		case R.id.itemweapon_list_button:
			intent = new Intent(this, ItemWeaponListActivity.class);
			break;

		case R.id.tournamentpoolfight_list_button:
			intent = new Intent(this, TournamentPoolFightListActivity.class);
			break;

		case R.id.tournament_list_button:
			intent = new Intent(this, TournamentListActivity.class);
			break;

		case R.id.spell_list_button:
			intent = new Intent(this, SpellListActivity.class);
			break;

		case R.id.characterplayer_list_button:
			intent = new Intent(this, CharacterPlayerListActivity.class);
			break;

		case R.id.fightplayer_list_button:
			intent = new Intent(this, FightPlayerListActivity.class);
			break;

		case R.id.myFight_list_button:
			intent = new Intent(this, FightGameActivity.class);
			break;

		case R.id.myAvatar_list_btn:
			intent = new Intent(this, CharRepresentationActivity.class);
			break;

		default:
			intent = null;
			break;
		}

		if (intent != null) {
			this.startActivity(intent);
		}
	}

}
