/**************************************************************************
 * ItemWeaponSQLiteAdapter.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data;

import com.curseblade.data.base.ItemWeaponSQLiteAdapterBase;
import android.content.Context;

/**
 * ItemWeapon adapter database class. 
 * This class will help you access your database to do any basic operation you
 * need. 
 * Feel free to modify it, override, add more methods etc.
 */
public class ItemWeaponSQLiteAdapter extends ItemWeaponSQLiteAdapterBase {

	/**
	 * Constructor.
	 * @param ctx context
	 */
	public ItemWeaponSQLiteAdapter(final Context ctx) {
		super(ctx);
	}
}
