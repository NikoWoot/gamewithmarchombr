/**************************************************************************
 * ItemArmurySQLiteAdapter.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data;

import com.curseblade.data.base.ItemArmurySQLiteAdapterBase;
import android.content.Context;

/**
 * ItemArmury adapter database class. 
 * This class will help you access your database to do any basic operation you
 * need. 
 * Feel free to modify it, override, add more methods etc.
 */
public class ItemArmurySQLiteAdapter extends ItemArmurySQLiteAdapterBase {

	/**
	 * Constructor.
	 * @param ctx context
	 */
	public ItemArmurySQLiteAdapter(final Context ctx) {
		super(ctx);
	}
}
