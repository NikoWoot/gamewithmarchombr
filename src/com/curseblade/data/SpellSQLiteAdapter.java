/**************************************************************************
 * SpellSQLiteAdapter.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data;

import com.curseblade.data.base.SpellSQLiteAdapterBase;
import android.content.Context;

/**
 * Spell adapter database class. 
 * This class will help you access your database to do any basic operation you
 * need. 
 * Feel free to modify it, override, add more methods etc.
 */
public class SpellSQLiteAdapter extends SpellSQLiteAdapterBase {

	/**
	 * Constructor.
	 * @param ctx context
	 */
	public SpellSQLiteAdapter(final Context ctx) {
		super(ctx);
	}
}
