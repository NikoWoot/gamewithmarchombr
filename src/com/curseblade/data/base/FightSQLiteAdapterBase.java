/**************************************************************************
 * FightSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.ObjectArrays;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.data.FightActionSQLiteAdapter;
import com.curseblade.entity.Fight;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.FightAction;


import com.curseblade.CursebladeApplication;


/** Fight adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit FightAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class FightSQLiteAdapterBase
						extends SQLiteAdapterBase<Fight> {

	/** TAG for debug purpose. */
	protected static final String TAG = "FightDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "Fight";

	/**
	 *  Columns constants fields mapping.
	 */
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** senderPlayer. */
	public static final String COL_SENDERPLAYER =
			"senderPlayer";
	/** Alias. */
	public static final String ALIASED_COL_SENDERPLAYER =
			TABLE_NAME + "." + COL_SENDERPLAYER;
	/** receiverPlayer. */
	public static final String COL_RECEIVERPLAYER =
			"receiverPlayer";
	/** Alias. */
	public static final String ALIASED_COL_RECEIVERPLAYER =
			TABLE_NAME + "." + COL_RECEIVERPLAYER;
	/** currentElement. */
	public static final String COL_CURRENTELEMENT =
			"currentElement";
	/** Alias. */
	public static final String ALIASED_COL_CURRENTELEMENT =
			TABLE_NAME + "." + COL_CURRENTELEMENT;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		FightSQLiteAdapter.COL_ID,
		FightSQLiteAdapter.COL_SENDERPLAYER,
		FightSQLiteAdapter.COL_RECEIVERPLAYER,
		FightSQLiteAdapter.COL_CURRENTELEMENT
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		FightSQLiteAdapter.ALIASED_COL_ID,
		FightSQLiteAdapter.ALIASED_COL_SENDERPLAYER,
		FightSQLiteAdapter.ALIASED_COL_RECEIVERPLAYER,
		FightSQLiteAdapter.ALIASED_COL_CURRENTELEMENT
	};

	/**
	 * Get the table name used in DB for your Fight entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your Fight entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the Fight entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_SENDERPLAYER	+ " INTEGER,"
		 + COL_RECEIVERPLAYER	+ " INTEGER,"
		 + COL_CURRENTELEMENT	+ " INTEGER NOT NULL,"
		
		
		 + "FOREIGN KEY(" + COL_SENDERPLAYER + ") REFERENCES " 
			 + FightPlayerSQLiteAdapter.TABLE_NAME 
				+ " (" + FightPlayerSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_RECEIVERPLAYER + ") REFERENCES " 
			 + FightPlayerSQLiteAdapter.TABLE_NAME 
				+ " (" + FightPlayerSQLiteAdapter.COL_ID + ")"
		+ ", UNIQUE(" + COL_ID + ")"
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public FightSQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters

	/**
	 * Convert Fight entity to Content Values for database.
	 * @param item Fight entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final Fight item) {
		final ContentValues result = new ContentValues();

		if (item.getId() != null) {
			result.put(COL_ID,
				String.valueOf(item.getId()));
		}

		if (item.getSenderPlayer() != null) {
			result.put(COL_SENDERPLAYER,
				item.getSenderPlayer().getId());
		} else {
			result.put(COL_SENDERPLAYER, (String) null);
		}

		if (item.getReceiverPlayer() != null) {
			result.put(COL_RECEIVERPLAYER,
				item.getReceiverPlayer().getId());
		} else {
			result.put(COL_RECEIVERPLAYER, (String) null);
		}

		if (item.getCurrentElement() != null) {
			result.put(COL_CURRENTELEMENT,
				String.valueOf(item.getCurrentElement()));
		}


		return result;
	}

	/**
	 * Convert Cursor of database to Fight entity.
	 * @param cursor Cursor object
	 * @return Fight entity
	 */
	public Fight cursorToItem(final Cursor cursor) {
		Fight result = new Fight();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to Fight entity.
	 * @param cursor Cursor object
	 * @param result Fight entity
	 */
	public void cursorToItem(final Cursor cursor, final Fight result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_SENDERPLAYER);
			if (!cursor.isNull(index)) {
				final FightPlayer senderPlayer = new FightPlayer();
				senderPlayer.setId(cursor.getInt(index));
				result.setSenderPlayer(senderPlayer);
			}

			index = cursor.getColumnIndexOrThrow(COL_RECEIVERPLAYER);
			if (!cursor.isNull(index)) {
				final FightPlayer receiverPlayer = new FightPlayer();
				receiverPlayer.setId(cursor.getInt(index));
				result.setReceiverPlayer(receiverPlayer);
			}

			index = cursor.getColumnIndexOrThrow(COL_CURRENTELEMENT);
			result.setCurrentElement(
					cursor.getInt(index));


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read Fight by id in database.
	 *
	 * @param id Identify of Fight
	 * @return Fight entity
	 */
	public Fight getByID(final Integer id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final Fight result = this.cursorToItem(cursor);
		cursor.close();

		final FightPlayerSQLiteAdapter starterFightersAdapter =
				new FightPlayerSQLiteAdapter(this.ctx);
		starterFightersAdapter.open(this.mDatabase);
		Cursor starterfightersCursor = starterFightersAdapter
					.getByFight(result.getId(), FightPlayerSQLiteAdapter.ALIASED_COLS, null, null, null);
		result.setStarterFighters(starterFightersAdapter.cursorToItems(starterfightersCursor));
		final FightPlayerSQLiteAdapter alternatedFightersAdapter =
				new FightPlayerSQLiteAdapter(this.ctx);
		alternatedFightersAdapter.open(this.mDatabase);
		Cursor alternatedfightersCursor = alternatedFightersAdapter
					.getByFight(result.getId(), FightPlayerSQLiteAdapter.ALIASED_COLS, null, null, null);
		result.setAlternatedFighters(alternatedFightersAdapter.cursorToItems(alternatedfightersCursor));
		final FightPlayerSQLiteAdapter survivorFightersAdapter =
				new FightPlayerSQLiteAdapter(this.ctx);
		survivorFightersAdapter.open(this.mDatabase);
		Cursor survivorfightersCursor = survivorFightersAdapter
					.getByFight(result.getId(), FightPlayerSQLiteAdapter.ALIASED_COLS, null, null, null);
		result.setSurvivorFighters(survivorFightersAdapter.cursorToItems(survivorfightersCursor));
		final FightActionSQLiteAdapter actionsAdapter =
				new FightActionSQLiteAdapter(this.ctx);
		actionsAdapter.open(this.mDatabase);
		Cursor actionsCursor = actionsAdapter
					.getByFightactionsInternal(result.getId(), FightActionSQLiteAdapter.ALIASED_COLS, null, null, null);
		result.setActions(actionsAdapter.cursorToItems(actionsCursor));
		if (result.getSenderPlayer() != null) {
			final FightPlayerSQLiteAdapter senderPlayerAdapter =
					new FightPlayerSQLiteAdapter(this.ctx);
			senderPlayerAdapter.open(this.mDatabase);
			
			result.setSenderPlayer(senderPlayerAdapter.getByID(
							result.getSenderPlayer().getId()));
		}
		if (result.getReceiverPlayer() != null) {
			final FightPlayerSQLiteAdapter receiverPlayerAdapter =
					new FightPlayerSQLiteAdapter(this.ctx);
			receiverPlayerAdapter.open(this.mDatabase);
			
			result.setReceiverPlayer(receiverPlayerAdapter.getByID(
							result.getReceiverPlayer().getId()));
		}
		return result;
	}

	/**
	 * Find & read Fight by senderPlayer.
	 * @param senderplayerId senderplayerId
	 * @param orderBy Order by string (can be null)
	 * @return List of Fight entities
	 */
	 public Cursor getBySenderPlayer(final int senderplayerId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = FightSQLiteAdapter.COL_SENDERPLAYER + "=?";
		String idSelectionArgs = String.valueOf(senderplayerId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read Fight by receiverPlayer.
	 * @param receiverplayerId receiverplayerId
	 * @param orderBy Order by string (can be null)
	 * @return List of Fight entities
	 */
	 public Cursor getByReceiverPlayer(final int receiverplayerId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = FightSQLiteAdapter.COL_RECEIVERPLAYER + "=?";
		String idSelectionArgs = String.valueOf(receiverplayerId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }

	/**
	 * Read All Fights entities.
	 *
	 * @return List of Fight entities
	 */
	public ArrayList<Fight> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<Fight> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a Fight entity into database.
	 *
	 * @param item The Fight entity to persist
	 * @return Id of the Fight entity
	 */
	public long insert(final Fight item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		values.remove(FightSQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					FightSQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		if (item.getStarterFighters() != null) {
			FightPlayerSQLiteAdapterBase starterFightersAdapter =
					new FightPlayerSQLiteAdapter(this.ctx);
			starterFightersAdapter.open(this.mDatabase);
			for (FightPlayer fightplayer
						: item.getStarterFighters()) {
				fightplayer.setFight(item);
				starterFightersAdapter.insertOrUpdate(fightplayer);
			}
		}
		if (item.getAlternatedFighters() != null) {
			FightPlayerSQLiteAdapterBase alternatedFightersAdapter =
					new FightPlayerSQLiteAdapter(this.ctx);
			alternatedFightersAdapter.open(this.mDatabase);
			for (FightPlayer fightplayer
						: item.getAlternatedFighters()) {
				fightplayer.setFight(item);
				alternatedFightersAdapter.insertOrUpdate(fightplayer);
			}
		}
		if (item.getSurvivorFighters() != null) {
			FightPlayerSQLiteAdapterBase survivorFightersAdapter =
					new FightPlayerSQLiteAdapter(this.ctx);
			survivorFightersAdapter.open(this.mDatabase);
			for (FightPlayer fightplayer
						: item.getSurvivorFighters()) {
				fightplayer.setFight(item);
				survivorFightersAdapter.insertOrUpdate(fightplayer);
			}
		}
		if (item.getActions() != null) {
			FightActionSQLiteAdapterBase actionsAdapter =
					new FightActionSQLiteAdapter(this.ctx);
			actionsAdapter.open(this.mDatabase);
			for (FightAction fightaction
						: item.getActions()) {
				actionsAdapter.insertOrUpdateWithFightActions(
									fightaction,
									newid);
			}
		}
		return newid;
	}

	/**
	 * Either insert or update a Fight entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The Fight entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final Fight item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a Fight entity into database.
	 *
	 * @param item The Fight entity to persist
	 * @return count of updated entities
	 */
	public int update(final Fight item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		final String whereClause =
				 FightSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Delete a Fight entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  FightSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param fight The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final Fight fight) {
		return this.delete(fight.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the Fight corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  FightSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a Fight entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
