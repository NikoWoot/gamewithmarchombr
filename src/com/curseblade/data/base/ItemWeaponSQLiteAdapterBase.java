/**************************************************************************
 * ItemWeaponSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.entity.ItemWeaponType;


import com.curseblade.CursebladeApplication;


/** ItemWeapon adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit ItemWeaponAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class ItemWeaponSQLiteAdapterBase
						extends SQLiteAdapterBase<ItemWeapon> {

	/** TAG for debug purpose. */
	protected static final String TAG = "ItemWeaponDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "ItemWeapon";

	/**
	 *  Columns constants fields mapping.
	 */
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** name. */
	public static final String COL_NAME =
			"name";
	/** Alias. */
	public static final String ALIASED_COL_NAME =
			TABLE_NAME + "." + COL_NAME;
	/** baseAttack. */
	public static final String COL_BASEATTACK =
			"baseAttack";
	/** Alias. */
	public static final String ALIASED_COL_BASEATTACK =
			TABLE_NAME + "." + COL_BASEATTACK;
	/** weaponType. */
	public static final String COL_WEAPONTYPE =
			"weaponType";
	/** Alias. */
	public static final String ALIASED_COL_WEAPONTYPE =
			TABLE_NAME + "." + COL_WEAPONTYPE;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		ItemWeaponSQLiteAdapter.COL_ID,
		ItemWeaponSQLiteAdapter.COL_NAME,
		ItemWeaponSQLiteAdapter.COL_BASEATTACK,
		ItemWeaponSQLiteAdapter.COL_WEAPONTYPE
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		ItemWeaponSQLiteAdapter.ALIASED_COL_ID,
		ItemWeaponSQLiteAdapter.ALIASED_COL_NAME,
		ItemWeaponSQLiteAdapter.ALIASED_COL_BASEATTACK,
		ItemWeaponSQLiteAdapter.ALIASED_COL_WEAPONTYPE
	};

	/**
	 * Get the table name used in DB for your ItemWeapon entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your ItemWeapon entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the ItemWeapon entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_NAME	+ " VARCHAR NOT NULL,"
		 + COL_BASEATTACK	+ " DOUBLE NOT NULL,"
		 + COL_WEAPONTYPE	+ " VARCHAR NOT NULL"
		
		
		+ ", UNIQUE(" + COL_ID + ")"
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public ItemWeaponSQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters

	/**
	 * Convert ItemWeapon entity to Content Values for database.
	 * @param item ItemWeapon entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final ItemWeapon item) {
		final ContentValues result = new ContentValues();

		if (item.getId() != null) {
			result.put(COL_ID,
				String.valueOf(item.getId()));
		}

		if (item.getName() != null) {
			result.put(COL_NAME,
				item.getName());
		}

		if (item.getBaseAttack() != null) {
			result.put(COL_BASEATTACK,
				String.valueOf(item.getBaseAttack()));
		}

		if (item.getWeaponType() != null) {
			result.put(COL_WEAPONTYPE,
				item.getWeaponType().name());
		}


		return result;
	}

	/**
	 * Convert Cursor of database to ItemWeapon entity.
	 * @param cursor Cursor object
	 * @return ItemWeapon entity
	 */
	public ItemWeapon cursorToItem(final Cursor cursor) {
		ItemWeapon result = new ItemWeapon();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to ItemWeapon entity.
	 * @param cursor Cursor object
	 * @param result ItemWeapon entity
	 */
	public void cursorToItem(final Cursor cursor, final ItemWeapon result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_NAME);
			result.setName(
					cursor.getString(index));

			index = cursor.getColumnIndexOrThrow(COL_BASEATTACK);
			result.setBaseAttack(
					cursor.getDouble(index));

			index = cursor.getColumnIndexOrThrow(COL_WEAPONTYPE);
			result.setWeaponType(
				ItemWeaponType.valueOf(cursor.getString(index)));


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read ItemWeapon by id in database.
	 *
	 * @param id Identify of ItemWeapon
	 * @return ItemWeapon entity
	 */
	public ItemWeapon getByID(final Integer id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final ItemWeapon result = this.cursorToItem(cursor);
		cursor.close();

		return result;
	}


	/**
	 * Read All ItemWeapons entities.
	 *
	 * @return List of ItemWeapon entities
	 */
	public ArrayList<ItemWeapon> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<ItemWeapon> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a ItemWeapon entity into database.
	 *
	 * @param item The ItemWeapon entity to persist
	 * @return Id of the ItemWeapon entity
	 */
	public long insert(final ItemWeapon item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		values.remove(ItemWeaponSQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					ItemWeaponSQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		return newid;
	}

	/**
	 * Either insert or update a ItemWeapon entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The ItemWeapon entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final ItemWeapon item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a ItemWeapon entity into database.
	 *
	 * @param item The ItemWeapon entity to persist
	 * @return count of updated entities
	 */
	public int update(final ItemWeapon item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		final String whereClause =
				 ItemWeaponSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Delete a ItemWeapon entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  ItemWeaponSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param itemWeapon The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final ItemWeapon itemWeapon) {
		return this.delete(itemWeapon.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the ItemWeapon corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  ItemWeaponSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a ItemWeapon entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
