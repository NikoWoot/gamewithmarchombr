/**************************************************************************
 * FightActionSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.ObjectArrays;
import com.curseblade.data.FightActionSQLiteAdapter;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.entity.FightAction;
import com.curseblade.entity.Fight;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.FightActionType;


import com.curseblade.CursebladeApplication;


/** FightAction adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit FightActionAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class FightActionSQLiteAdapterBase
						extends SQLiteAdapterBase<FightAction> {

	/** TAG for debug purpose. */
	protected static final String TAG = "FightActionDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "FightAction";

	/**
	 *  Columns constants fields mapping.
	 */
	/** Fight_actions_internal. */
	public static final String COL_FIGHTACTIONSINTERNAL =
			"Fight_actions_internal";
	/** Alias. */
	public static final String ALIASED_COL_FIGHTACTIONSINTERNAL =
			TABLE_NAME + "." + COL_FIGHTACTIONSINTERNAL;
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** fight. */
	public static final String COL_FIGHT =
			"fight";
	/** Alias. */
	public static final String ALIASED_COL_FIGHT =
			TABLE_NAME + "." + COL_FIGHT;
	/** sender. */
	public static final String COL_SENDER =
			"sender";
	/** Alias. */
	public static final String ALIASED_COL_SENDER =
			TABLE_NAME + "." + COL_SENDER;
	/** receiver. */
	public static final String COL_RECEIVER =
			"receiver";
	/** Alias. */
	public static final String ALIASED_COL_RECEIVER =
			TABLE_NAME + "." + COL_RECEIVER;
	/** actionType. */
	public static final String COL_ACTIONTYPE =
			"actionType";
	/** Alias. */
	public static final String ALIASED_COL_ACTIONTYPE =
			TABLE_NAME + "." + COL_ACTIONTYPE;
	/** beforeValue. */
	public static final String COL_BEFOREVALUE =
			"beforeValue";
	/** Alias. */
	public static final String ALIASED_COL_BEFOREVALUE =
			TABLE_NAME + "." + COL_BEFOREVALUE;
	/** afterValue. */
	public static final String COL_AFTERVALUE =
			"afterValue";
	/** Alias. */
	public static final String ALIASED_COL_AFTERVALUE =
			TABLE_NAME + "." + COL_AFTERVALUE;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		FightActionSQLiteAdapter.COL_FIGHTACTIONSINTERNAL,
		FightActionSQLiteAdapter.COL_ID,
		FightActionSQLiteAdapter.COL_FIGHT,
		FightActionSQLiteAdapter.COL_SENDER,
		FightActionSQLiteAdapter.COL_RECEIVER,
		FightActionSQLiteAdapter.COL_ACTIONTYPE,
		FightActionSQLiteAdapter.COL_BEFOREVALUE,
		FightActionSQLiteAdapter.COL_AFTERVALUE
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		FightActionSQLiteAdapter.ALIASED_COL_FIGHTACTIONSINTERNAL,
		FightActionSQLiteAdapter.ALIASED_COL_ID,
		FightActionSQLiteAdapter.ALIASED_COL_FIGHT,
		FightActionSQLiteAdapter.ALIASED_COL_SENDER,
		FightActionSQLiteAdapter.ALIASED_COL_RECEIVER,
		FightActionSQLiteAdapter.ALIASED_COL_ACTIONTYPE,
		FightActionSQLiteAdapter.ALIASED_COL_BEFOREVALUE,
		FightActionSQLiteAdapter.ALIASED_COL_AFTERVALUE
	};

	/**
	 * Get the table name used in DB for your FightAction entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your FightAction entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the FightAction entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_FIGHTACTIONSINTERNAL	+ " INTEGER,"
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_FIGHT	+ " INTEGER NOT NULL,"
		 + COL_SENDER	+ " INTEGER NOT NULL,"
		 + COL_RECEIVER	+ " INTEGER NOT NULL,"
		 + COL_ACTIONTYPE	+ " VARCHAR NOT NULL,"
		 + COL_BEFOREVALUE	+ " INTEGER NOT NULL,"
		 + COL_AFTERVALUE	+ " INTEGER NOT NULL,"
		
		
		 + "FOREIGN KEY(" + COL_FIGHTACTIONSINTERNAL + ") REFERENCES " 
			 + FightSQLiteAdapter.TABLE_NAME 
				+ " (" + FightSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_FIGHT + ") REFERENCES " 
			 + FightSQLiteAdapter.TABLE_NAME 
				+ " (" + FightSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_SENDER + ") REFERENCES " 
			 + FightPlayerSQLiteAdapter.TABLE_NAME 
				+ " (" + FightPlayerSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_RECEIVER + ") REFERENCES " 
			 + FightPlayerSQLiteAdapter.TABLE_NAME 
				+ " (" + FightPlayerSQLiteAdapter.COL_ID + ")"
		+ ", UNIQUE(" + COL_ID + ")"
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public FightActionSQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters
	/** Convert FightAction entity to Content Values for database.
	 *
	 * @param item FightAction entity object
	 * @param fightId fight id
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final FightAction item,
				int fightId) {
		final ContentValues result = this.itemToContentValues(item);
		result.put(COL_FIGHTACTIONSINTERNAL,
				String.valueOf(fightId));
		return result;
	}

	/**
	 * Convert FightAction entity to Content Values for database.
	 * @param item FightAction entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final FightAction item) {
		final ContentValues result = new ContentValues();

		if (item.getId() != null) {
			result.put(COL_ID,
				String.valueOf(item.getId()));
		}

		if (item.getFight() != null) {
			result.put(COL_FIGHT,
				item.getFight().getId());
		}

		if (item.getSender() != null) {
			result.put(COL_SENDER,
				item.getSender().getId());
		}

		if (item.getReceiver() != null) {
			result.put(COL_RECEIVER,
				item.getReceiver().getId());
		}

		if (item.getActionType() != null) {
			result.put(COL_ACTIONTYPE,
				item.getActionType().name());
		}

		if (item.getBeforeValue() != null) {
			result.put(COL_BEFOREVALUE,
				String.valueOf(item.getBeforeValue()));
		}

		if (item.getAfterValue() != null) {
			result.put(COL_AFTERVALUE,
				String.valueOf(item.getAfterValue()));
		}


		return result;
	}

	/**
	 * Convert Cursor of database to FightAction entity.
	 * @param cursor Cursor object
	 * @return FightAction entity
	 */
	public FightAction cursorToItem(final Cursor cursor) {
		FightAction result = new FightAction();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to FightAction entity.
	 * @param cursor Cursor object
	 * @param result FightAction entity
	 */
	public void cursorToItem(final Cursor cursor, final FightAction result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_FIGHT);
			final Fight fight = new Fight();
			fight.setId(cursor.getInt(index));
			result.setFight(fight);

			index = cursor.getColumnIndexOrThrow(COL_SENDER);
			final FightPlayer sender = new FightPlayer();
			sender.setId(cursor.getInt(index));
			result.setSender(sender);

			index = cursor.getColumnIndexOrThrow(COL_RECEIVER);
			final FightPlayer receiver = new FightPlayer();
			receiver.setId(cursor.getInt(index));
			result.setReceiver(receiver);

			index = cursor.getColumnIndexOrThrow(COL_ACTIONTYPE);
			result.setActionType(
				FightActionType.valueOf(cursor.getString(index)));

			index = cursor.getColumnIndexOrThrow(COL_BEFOREVALUE);
			result.setBeforeValue(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_AFTERVALUE);
			result.setAfterValue(
					cursor.getInt(index));


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read FightAction by id in database.
	 *
	 * @param id Identify of FightAction
	 * @return FightAction entity
	 */
	public FightAction getByID(final Integer id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final FightAction result = this.cursorToItem(cursor);
		cursor.close();

		if (result.getFight() != null) {
			final FightSQLiteAdapter fightAdapter =
					new FightSQLiteAdapter(this.ctx);
			fightAdapter.open(this.mDatabase);
			
			result.setFight(fightAdapter.getByID(
							result.getFight().getId()));
		}
		if (result.getSender() != null) {
			final FightPlayerSQLiteAdapter senderAdapter =
					new FightPlayerSQLiteAdapter(this.ctx);
			senderAdapter.open(this.mDatabase);
			
			result.setSender(senderAdapter.getByID(
							result.getSender().getId()));
		}
		if (result.getReceiver() != null) {
			final FightPlayerSQLiteAdapter receiverAdapter =
					new FightPlayerSQLiteAdapter(this.ctx);
			receiverAdapter.open(this.mDatabase);
			
			result.setReceiver(receiverAdapter.getByID(
							result.getReceiver().getId()));
		}
		return result;
	}

	/**
	 * Find & read FightAction by FightactionsInternal.
	 * @param fightactionsinternalId fightactionsinternalId
	 * @param orderBy Order by string (can be null)
	 * @return List of FightAction entities
	 */
	 public Cursor getByFightactionsInternal(final int fightactionsinternalId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = FightActionSQLiteAdapter.COL_FIGHTACTIONSINTERNAL + "=?";
		String idSelectionArgs = String.valueOf(fightactionsinternalId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read FightAction by fight.
	 * @param fightId fightId
	 * @param orderBy Order by string (can be null)
	 * @return List of FightAction entities
	 */
	 public Cursor getByFight(final int fightId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = FightActionSQLiteAdapter.COL_FIGHT + "=?";
		String idSelectionArgs = String.valueOf(fightId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read FightAction by sender.
	 * @param senderId senderId
	 * @param orderBy Order by string (can be null)
	 * @return List of FightAction entities
	 */
	 public Cursor getBySender(final int senderId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = FightActionSQLiteAdapter.COL_SENDER + "=?";
		String idSelectionArgs = String.valueOf(senderId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read FightAction by receiver.
	 * @param receiverId receiverId
	 * @param orderBy Order by string (can be null)
	 * @return List of FightAction entities
	 */
	 public Cursor getByReceiver(final int receiverId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = FightActionSQLiteAdapter.COL_RECEIVER + "=?";
		String idSelectionArgs = String.valueOf(receiverId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }

	/**
	 * Read All FightActions entities.
	 *
	 * @return List of FightAction entities
	 */
	public ArrayList<FightAction> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<FightAction> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a FightAction entity into database.
	 *
	 * @param item The FightAction entity to persist
	 * @return Id of the FightAction entity
	 */
	public long insert(final FightAction item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item, 0);
		values.remove(FightActionSQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					FightActionSQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		return newid;
	}

	/**
	 * Either insert or update a FightAction entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The FightAction entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final FightAction item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a FightAction entity into database.
	 *
	 * @param item The FightAction entity to persist
	 * @return count of updated entities
	 */
	public int update(final FightAction item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item, 0);
		final String whereClause =
				 FightActionSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Update a FightAction entity into database.
	 *
	 * @param item The FightAction entity to persist
	 * @param fightId The fight id
	 * @return count of updated entities
	 */
	public int updateWithFightActions(
					FightAction item, int fightId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		ContentValues values =
				this.itemToContentValues(item,
							fightId);
		String whereClause =
				 FightActionSQLiteAdapter.COL_ID
				 + "=? ";
		String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Either insert or update a FightAction entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The FightAction entity to persist
	 * @param fightId The fight id
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdateWithFightActions(
			FightAction item, int fightId) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.updateWithFightActions(item,
					fightId);
		} else {
			// Item doesn't exist => create it
			long id = this.insertWithFightActions(item,
					fightId);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}


	/**
	 * Insert a FightAction entity into database.
	 *
	 * @param item The FightAction entity to persist
	 * @param fightId The fight id
	 * @return Id of the FightAction entity
	 */
	public long insertWithFightActions(
			FightAction item, int fightId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		ContentValues values = this.itemToContentValues(item,
				fightId);
		values.remove(FightActionSQLiteAdapter.COL_ID);
		int newid = (int) this.insert(
			null,
			values);


		return newid;
	}


	/**
	 * Delete a FightAction entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  FightActionSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param fightAction The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final FightAction fightAction) {
		return this.delete(fightAction.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the FightAction corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  FightActionSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a FightAction entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
