/**************************************************************************
 * TournamentPoolFightSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.ObjectArrays;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;
import com.curseblade.data.TournamentSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.entity.FightPlayer;


import com.curseblade.CursebladeApplication;


/** TournamentPoolFight adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit TournamentPoolFightAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class TournamentPoolFightSQLiteAdapterBase
						extends SQLiteAdapterBase<TournamentPoolFight> {

	/** TAG for debug purpose. */
	protected static final String TAG = "TournamentPoolFightDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "TournamentPoolFight";

	/**
	 *  Columns constants fields mapping.
	 */
	/** Tournament_poolFights_internal. */
	public static final String COL_TOURNAMENTPOOLFIGHTSINTERNAL =
			"Tournament_poolFights_internal";
	/** Alias. */
	public static final String ALIASED_COL_TOURNAMENTPOOLFIGHTSINTERNAL =
			TABLE_NAME + "." + COL_TOURNAMENTPOOLFIGHTSINTERNAL;
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** nodeLevel. */
	public static final String COL_NODELEVEL =
			"nodeLevel";
	/** Alias. */
	public static final String ALIASED_COL_NODELEVEL =
			TABLE_NAME + "." + COL_NODELEVEL;
	/** winner. */
	public static final String COL_WINNER =
			"winner";
	/** Alias. */
	public static final String ALIASED_COL_WINNER =
			TABLE_NAME + "." + COL_WINNER;
	/** parentPool. */
	public static final String COL_PARENTPOOL =
			"parentPool";
	/** Alias. */
	public static final String ALIASED_COL_PARENTPOOL =
			TABLE_NAME + "." + COL_PARENTPOOL;
	/** leftPool. */
	public static final String COL_LEFTPOOL =
			"leftPool";
	/** Alias. */
	public static final String ALIASED_COL_LEFTPOOL =
			TABLE_NAME + "." + COL_LEFTPOOL;
	/** rightPool. */
	public static final String COL_RIGHTPOOL =
			"rightPool";
	/** Alias. */
	public static final String ALIASED_COL_RIGHTPOOL =
			TABLE_NAME + "." + COL_RIGHTPOOL;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		TournamentPoolFightSQLiteAdapter.COL_TOURNAMENTPOOLFIGHTSINTERNAL,
		TournamentPoolFightSQLiteAdapter.COL_ID,
		TournamentPoolFightSQLiteAdapter.COL_NODELEVEL,
		TournamentPoolFightSQLiteAdapter.COL_WINNER,
		TournamentPoolFightSQLiteAdapter.COL_PARENTPOOL,
		TournamentPoolFightSQLiteAdapter.COL_LEFTPOOL,
		TournamentPoolFightSQLiteAdapter.COL_RIGHTPOOL
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		TournamentPoolFightSQLiteAdapter.ALIASED_COL_TOURNAMENTPOOLFIGHTSINTERNAL,
		TournamentPoolFightSQLiteAdapter.ALIASED_COL_ID,
		TournamentPoolFightSQLiteAdapter.ALIASED_COL_NODELEVEL,
		TournamentPoolFightSQLiteAdapter.ALIASED_COL_WINNER,
		TournamentPoolFightSQLiteAdapter.ALIASED_COL_PARENTPOOL,
		TournamentPoolFightSQLiteAdapter.ALIASED_COL_LEFTPOOL,
		TournamentPoolFightSQLiteAdapter.ALIASED_COL_RIGHTPOOL
	};

	/**
	 * Get the table name used in DB for your TournamentPoolFight entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your TournamentPoolFight entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the TournamentPoolFight entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_TOURNAMENTPOOLFIGHTSINTERNAL	+ " INTEGER,"
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_NODELEVEL	+ " INTEGER NOT NULL,"
		 + COL_WINNER	+ " INTEGER,"
		 + COL_PARENTPOOL	+ " INTEGER,"
		 + COL_LEFTPOOL	+ " INTEGER,"
		 + COL_RIGHTPOOL	+ " INTEGER,"
		
		
		 + "FOREIGN KEY(" + COL_TOURNAMENTPOOLFIGHTSINTERNAL + ") REFERENCES " 
			 + TournamentSQLiteAdapter.TABLE_NAME 
				+ " (" + TournamentSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_WINNER + ") REFERENCES " 
			 + FightPlayerSQLiteAdapter.TABLE_NAME 
				+ " (" + FightPlayerSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_PARENTPOOL + ") REFERENCES " 
			 + TournamentPoolFightSQLiteAdapter.TABLE_NAME 
				+ " (" + TournamentPoolFightSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_LEFTPOOL + ") REFERENCES " 
			 + TournamentPoolFightSQLiteAdapter.TABLE_NAME 
				+ " (" + TournamentPoolFightSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_RIGHTPOOL + ") REFERENCES " 
			 + TournamentPoolFightSQLiteAdapter.TABLE_NAME 
				+ " (" + TournamentPoolFightSQLiteAdapter.COL_ID + ")"
		+ ", UNIQUE(" + COL_ID + ")"
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public TournamentPoolFightSQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters
	/** Convert TournamentPoolFight entity to Content Values for database.
	 *
	 * @param item TournamentPoolFight entity object
	 * @param tournamentId tournament id
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final TournamentPoolFight item,
				int tournamentId) {
		final ContentValues result = this.itemToContentValues(item);
		result.put(COL_TOURNAMENTPOOLFIGHTSINTERNAL,
				String.valueOf(tournamentId));
		return result;
	}

	/**
	 * Convert TournamentPoolFight entity to Content Values for database.
	 * @param item TournamentPoolFight entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final TournamentPoolFight item) {
		final ContentValues result = new ContentValues();

		if (item.getId() != null) {
			result.put(COL_ID,
				String.valueOf(item.getId()));
		}

		if (item.getNodeLevel() != null) {
			result.put(COL_NODELEVEL,
				String.valueOf(item.getNodeLevel()));
		}

		if (item.getWinner() != null) {
			result.put(COL_WINNER,
				item.getWinner().getId());
		} else {
			result.put(COL_WINNER, (String) null);
		}

		if (item.getParentPool() != null) {
			result.put(COL_PARENTPOOL,
				item.getParentPool().getId());
		} else {
			result.put(COL_PARENTPOOL, (String) null);
		}

		if (item.getLeftPool() != null) {
			result.put(COL_LEFTPOOL,
				item.getLeftPool().getId());
		} else {
			result.put(COL_LEFTPOOL, (String) null);
		}

		if (item.getRightPool() != null) {
			result.put(COL_RIGHTPOOL,
				item.getRightPool().getId());
		} else {
			result.put(COL_RIGHTPOOL, (String) null);
		}


		return result;
	}

	/**
	 * Convert Cursor of database to TournamentPoolFight entity.
	 * @param cursor Cursor object
	 * @return TournamentPoolFight entity
	 */
	public TournamentPoolFight cursorToItem(final Cursor cursor) {
		TournamentPoolFight result = new TournamentPoolFight();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to TournamentPoolFight entity.
	 * @param cursor Cursor object
	 * @param result TournamentPoolFight entity
	 */
	public void cursorToItem(final Cursor cursor, final TournamentPoolFight result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_NODELEVEL);
			result.setNodeLevel(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_WINNER);
			if (!cursor.isNull(index)) {
				final FightPlayer winner = new FightPlayer();
				winner.setId(cursor.getInt(index));
				result.setWinner(winner);
			}

			index = cursor.getColumnIndexOrThrow(COL_PARENTPOOL);
			if (!cursor.isNull(index)) {
				final TournamentPoolFight parentPool = new TournamentPoolFight();
				parentPool.setId(cursor.getInt(index));
				result.setParentPool(parentPool);
			}

			index = cursor.getColumnIndexOrThrow(COL_LEFTPOOL);
			if (!cursor.isNull(index)) {
				final TournamentPoolFight leftPool = new TournamentPoolFight();
				leftPool.setId(cursor.getInt(index));
				result.setLeftPool(leftPool);
			}

			index = cursor.getColumnIndexOrThrow(COL_RIGHTPOOL);
			if (!cursor.isNull(index)) {
				final TournamentPoolFight rightPool = new TournamentPoolFight();
				rightPool.setId(cursor.getInt(index));
				result.setRightPool(rightPool);
			}


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read TournamentPoolFight by id in database.
	 *
	 * @param id Identify of TournamentPoolFight
	 * @return TournamentPoolFight entity
	 */
	public TournamentPoolFight getByID(final Integer id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final TournamentPoolFight result = this.cursorToItem(cursor);
		cursor.close();

		if (result.getWinner() != null) {
			final FightPlayerSQLiteAdapter winnerAdapter =
					new FightPlayerSQLiteAdapter(this.ctx);
			winnerAdapter.open(this.mDatabase);
			
			result.setWinner(winnerAdapter.getByID(
							result.getWinner().getId()));
		}
		if (result.getParentPool() != null) {
			final TournamentPoolFightSQLiteAdapter parentPoolAdapter =
					new TournamentPoolFightSQLiteAdapter(this.ctx);
			parentPoolAdapter.open(this.mDatabase);
			
			result.setParentPool(parentPoolAdapter.getByID(
							result.getParentPool().getId()));
		}
		if (result.getLeftPool() != null) {
			final TournamentPoolFightSQLiteAdapter leftPoolAdapter =
					new TournamentPoolFightSQLiteAdapter(this.ctx);
			leftPoolAdapter.open(this.mDatabase);
			
			result.setLeftPool(leftPoolAdapter.getByID(
							result.getLeftPool().getId()));
		}
		if (result.getRightPool() != null) {
			final TournamentPoolFightSQLiteAdapter rightPoolAdapter =
					new TournamentPoolFightSQLiteAdapter(this.ctx);
			rightPoolAdapter.open(this.mDatabase);
			
			result.setRightPool(rightPoolAdapter.getByID(
							result.getRightPool().getId()));
		}
		return result;
	}

	/**
	 * Find & read TournamentPoolFight by TournamentpoolFightsInternal.
	 * @param tournamentpoolfightsinternalId tournamentpoolfightsinternalId
	 * @param orderBy Order by string (can be null)
	 * @return List of TournamentPoolFight entities
	 */
	 public Cursor getByTournamentpoolFightsInternal(final int tournamentpoolfightsinternalId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = TournamentPoolFightSQLiteAdapter.COL_TOURNAMENTPOOLFIGHTSINTERNAL + "=?";
		String idSelectionArgs = String.valueOf(tournamentpoolfightsinternalId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read TournamentPoolFight by winner.
	 * @param winnerId winnerId
	 * @param orderBy Order by string (can be null)
	 * @return List of TournamentPoolFight entities
	 */
	 public Cursor getByWinner(final int winnerId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = TournamentPoolFightSQLiteAdapter.COL_WINNER + "=?";
		String idSelectionArgs = String.valueOf(winnerId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read TournamentPoolFight by parentPool.
	 * @param parentpoolId parentpoolId
	 * @param orderBy Order by string (can be null)
	 * @return List of TournamentPoolFight entities
	 */
	 public Cursor getByParentPool(final int parentpoolId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = TournamentPoolFightSQLiteAdapter.COL_PARENTPOOL + "=?";
		String idSelectionArgs = String.valueOf(parentpoolId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read TournamentPoolFight by leftPool.
	 * @param leftpoolId leftpoolId
	 * @param orderBy Order by string (can be null)
	 * @return List of TournamentPoolFight entities
	 */
	 public Cursor getByLeftPool(final int leftpoolId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = TournamentPoolFightSQLiteAdapter.COL_LEFTPOOL + "=?";
		String idSelectionArgs = String.valueOf(leftpoolId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read TournamentPoolFight by rightPool.
	 * @param rightpoolId rightpoolId
	 * @param orderBy Order by string (can be null)
	 * @return List of TournamentPoolFight entities
	 */
	 public Cursor getByRightPool(final int rightpoolId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = TournamentPoolFightSQLiteAdapter.COL_RIGHTPOOL + "=?";
		String idSelectionArgs = String.valueOf(rightpoolId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }

	/**
	 * Read All TournamentPoolFights entities.
	 *
	 * @return List of TournamentPoolFight entities
	 */
	public ArrayList<TournamentPoolFight> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<TournamentPoolFight> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a TournamentPoolFight entity into database.
	 *
	 * @param item The TournamentPoolFight entity to persist
	 * @return Id of the TournamentPoolFight entity
	 */
	public long insert(final TournamentPoolFight item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item, 0);
		values.remove(TournamentPoolFightSQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					TournamentPoolFightSQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		return newid;
	}

	/**
	 * Either insert or update a TournamentPoolFight entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The TournamentPoolFight entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final TournamentPoolFight item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a TournamentPoolFight entity into database.
	 *
	 * @param item The TournamentPoolFight entity to persist
	 * @return count of updated entities
	 */
	public int update(final TournamentPoolFight item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item, 0);
		final String whereClause =
				 TournamentPoolFightSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Update a TournamentPoolFight entity into database.
	 *
	 * @param item The TournamentPoolFight entity to persist
	 * @param tournamentId The tournament id
	 * @return count of updated entities
	 */
	public int updateWithTournamentPoolFights(
					TournamentPoolFight item, int tournamentId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		ContentValues values =
				this.itemToContentValues(item,
							tournamentId);
		String whereClause =
				 TournamentPoolFightSQLiteAdapter.COL_ID
				 + "=? ";
		String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Either insert or update a TournamentPoolFight entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The TournamentPoolFight entity to persist
	 * @param tournamentId The tournament id
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdateWithTournamentPoolFights(
			TournamentPoolFight item, int tournamentId) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.updateWithTournamentPoolFights(item,
					tournamentId);
		} else {
			// Item doesn't exist => create it
			long id = this.insertWithTournamentPoolFights(item,
					tournamentId);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}


	/**
	 * Insert a TournamentPoolFight entity into database.
	 *
	 * @param item The TournamentPoolFight entity to persist
	 * @param tournamentId The tournament id
	 * @return Id of the TournamentPoolFight entity
	 */
	public long insertWithTournamentPoolFights(
			TournamentPoolFight item, int tournamentId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		ContentValues values = this.itemToContentValues(item,
				tournamentId);
		values.remove(TournamentPoolFightSQLiteAdapter.COL_ID);
		int newid = (int) this.insert(
			null,
			values);


		return newid;
	}


	/**
	 * Delete a TournamentPoolFight entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  TournamentPoolFightSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param tournamentPoolFight The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final TournamentPoolFight tournamentPoolFight) {
		return this.delete(tournamentPoolFight.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the TournamentPoolFight corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  TournamentPoolFightSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a TournamentPoolFight entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
