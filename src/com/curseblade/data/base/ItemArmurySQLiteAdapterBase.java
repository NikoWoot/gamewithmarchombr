/**************************************************************************
 * ItemArmurySQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.ObjectArrays;
import com.curseblade.data.ItemArmurySQLiteAdapter;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.entity.ItemArmury;
import com.curseblade.entity.ItemArmuryType;


import com.curseblade.CursebladeApplication;


/** ItemArmury adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit ItemArmuryAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class ItemArmurySQLiteAdapterBase
						extends SQLiteAdapterBase<ItemArmury> {

	/** TAG for debug purpose. */
	protected static final String TAG = "ItemArmuryDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "ItemArmury";

	/**
	 *  Columns constants fields mapping.
	 */
	/** CharacterPlayer_armuryEquipedItems_internal. */
	public static final String COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL =
			"CharacterPlayer_armuryEquipedItems_internal";
	/** Alias. */
	public static final String ALIASED_COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL =
			TABLE_NAME + "." + COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL;
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** name. */
	public static final String COL_NAME =
			"name";
	/** Alias. */
	public static final String ALIASED_COL_NAME =
			TABLE_NAME + "." + COL_NAME;
	/** defValue. */
	public static final String COL_DEFVALUE =
			"defValue";
	/** Alias. */
	public static final String ALIASED_COL_DEFVALUE =
			TABLE_NAME + "." + COL_DEFVALUE;
	/** armuryType. */
	public static final String COL_ARMURYTYPE =
			"armuryType";
	/** Alias. */
	public static final String ALIASED_COL_ARMURYTYPE =
			TABLE_NAME + "." + COL_ARMURYTYPE;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		ItemArmurySQLiteAdapter.COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL,
		ItemArmurySQLiteAdapter.COL_ID,
		ItemArmurySQLiteAdapter.COL_NAME,
		ItemArmurySQLiteAdapter.COL_DEFVALUE,
		ItemArmurySQLiteAdapter.COL_ARMURYTYPE
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		ItemArmurySQLiteAdapter.ALIASED_COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL,
		ItemArmurySQLiteAdapter.ALIASED_COL_ID,
		ItemArmurySQLiteAdapter.ALIASED_COL_NAME,
		ItemArmurySQLiteAdapter.ALIASED_COL_DEFVALUE,
		ItemArmurySQLiteAdapter.ALIASED_COL_ARMURYTYPE
	};

	/**
	 * Get the table name used in DB for your ItemArmury entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your ItemArmury entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the ItemArmury entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL	+ " INTEGER,"
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_NAME	+ " VARCHAR NOT NULL,"
		 + COL_DEFVALUE	+ " INTEGER NOT NULL,"
		 + COL_ARMURYTYPE	+ " VARCHAR NOT NULL,"
		
		
		 + "FOREIGN KEY(" + COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL + ") REFERENCES " 
			 + CharacterPlayerSQLiteAdapter.TABLE_NAME 
				+ " (" + CharacterPlayerSQLiteAdapter.COL_ID + ")"
		+ ", UNIQUE(" + COL_ID + ")"
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public ItemArmurySQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters
	/** Convert ItemArmury entity to Content Values for database.
	 *
	 * @param item ItemArmury entity object
	 * @param characterplayerId characterplayer id
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final ItemArmury item,
				int characterplayerId) {
		final ContentValues result = this.itemToContentValues(item);
		result.put(COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL,
				String.valueOf(characterplayerId));
		return result;
	}

	/**
	 * Convert ItemArmury entity to Content Values for database.
	 * @param item ItemArmury entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final ItemArmury item) {
		final ContentValues result = new ContentValues();

		if (item.getId() != null) {
			result.put(COL_ID,
				String.valueOf(item.getId()));
		}

		if (item.getName() != null) {
			result.put(COL_NAME,
				item.getName());
		}

		if (item.getDefValue() != null) {
			result.put(COL_DEFVALUE,
				String.valueOf(item.getDefValue()));
		}

		if (item.getArmuryType() != null) {
			result.put(COL_ARMURYTYPE,
				item.getArmuryType().name());
		}


		return result;
	}

	/**
	 * Convert Cursor of database to ItemArmury entity.
	 * @param cursor Cursor object
	 * @return ItemArmury entity
	 */
	public ItemArmury cursorToItem(final Cursor cursor) {
		ItemArmury result = new ItemArmury();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to ItemArmury entity.
	 * @param cursor Cursor object
	 * @param result ItemArmury entity
	 */
	public void cursorToItem(final Cursor cursor, final ItemArmury result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_NAME);
			result.setName(
					cursor.getString(index));

			index = cursor.getColumnIndexOrThrow(COL_DEFVALUE);
			result.setDefValue(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_ARMURYTYPE);
			result.setArmuryType(
				ItemArmuryType.valueOf(cursor.getString(index)));


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read ItemArmury by id in database.
	 *
	 * @param id Identify of ItemArmury
	 * @return ItemArmury entity
	 */
	public ItemArmury getByID(final Integer id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final ItemArmury result = this.cursorToItem(cursor);
		cursor.close();

		return result;
	}

	/**
	 * Find & read ItemArmury by CharacterPlayerarmuryEquipedItemsInternal.
	 * @param characterplayerarmuryequipeditemsinternalId characterplayerarmuryequipeditemsinternalId
	 * @param orderBy Order by string (can be null)
	 * @return List of ItemArmury entities
	 */
	 public Cursor getByCharacterPlayerarmuryEquipedItemsInternal(final int characterplayerarmuryequipeditemsinternalId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = ItemArmurySQLiteAdapter.COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL + "=?";
		String idSelectionArgs = String.valueOf(characterplayerarmuryequipeditemsinternalId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }

	/**
	 * Read All ItemArmurys entities.
	 *
	 * @return List of ItemArmury entities
	 */
	public ArrayList<ItemArmury> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<ItemArmury> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a ItemArmury entity into database.
	 *
	 * @param item The ItemArmury entity to persist
	 * @return Id of the ItemArmury entity
	 */
	public long insert(final ItemArmury item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item, 0);
		values.remove(ItemArmurySQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					ItemArmurySQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		return newid;
	}

	/**
	 * Either insert or update a ItemArmury entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The ItemArmury entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final ItemArmury item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a ItemArmury entity into database.
	 *
	 * @param item The ItemArmury entity to persist
	 * @return count of updated entities
	 */
	public int update(final ItemArmury item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item, 0);
		final String whereClause =
				 ItemArmurySQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Update a ItemArmury entity into database.
	 *
	 * @param item The ItemArmury entity to persist
	 * @param characterplayerId The characterplayer id
	 * @return count of updated entities
	 */
	public int updateWithCharacterPlayerArmuryEquipedItems(
					ItemArmury item, int characterplayerId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		ContentValues values =
				this.itemToContentValues(item,
							characterplayerId);
		String whereClause =
				 ItemArmurySQLiteAdapter.COL_ID
				 + "=? ";
		String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Either insert or update a ItemArmury entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The ItemArmury entity to persist
	 * @param characterplayerId The characterplayer id
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdateWithCharacterPlayerArmuryEquipedItems(
			ItemArmury item, int characterplayerId) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.updateWithCharacterPlayerArmuryEquipedItems(item,
					characterplayerId);
		} else {
			// Item doesn't exist => create it
			long id = this.insertWithCharacterPlayerArmuryEquipedItems(item,
					characterplayerId);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}


	/**
	 * Insert a ItemArmury entity into database.
	 *
	 * @param item The ItemArmury entity to persist
	 * @param characterplayerId The characterplayer id
	 * @return Id of the ItemArmury entity
	 */
	public long insertWithCharacterPlayerArmuryEquipedItems(
			ItemArmury item, int characterplayerId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		ContentValues values = this.itemToContentValues(item,
				characterplayerId);
		values.remove(ItemArmurySQLiteAdapter.COL_ID);
		int newid = (int) this.insert(
			null,
			values);


		return newid;
	}


	/**
	 * Delete a ItemArmury entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  ItemArmurySQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param itemArmury The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final ItemArmury itemArmury) {
		return this.delete(itemArmury.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the ItemArmury corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  ItemArmurySQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a ItemArmury entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
