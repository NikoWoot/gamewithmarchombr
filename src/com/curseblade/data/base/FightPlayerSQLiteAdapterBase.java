/**************************************************************************
 * FightPlayerSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.ObjectArrays;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.Fight;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.entity.Spell;


import com.curseblade.CursebladeApplication;


/** FightPlayer adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit FightPlayerAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class FightPlayerSQLiteAdapterBase
						extends SQLiteAdapterBase<FightPlayer> {

	/** TAG for debug purpose. */
	protected static final String TAG = "FightPlayerDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "FightPlayer";

	/**
	 *  Columns constants fields mapping.
	 */
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** teamId. */
	public static final String COL_TEAMID =
			"teamId";
	/** Alias. */
	public static final String ALIASED_COL_TEAMID =
			TABLE_NAME + "." + COL_TEAMID;
	/** fight. */
	public static final String COL_FIGHT =
			"fight";
	/** Alias. */
	public static final String ALIASED_COL_FIGHT =
			TABLE_NAME + "." + COL_FIGHT;
	/** pseudo. */
	public static final String COL_PSEUDO =
			"pseudo";
	/** Alias. */
	public static final String ALIASED_COL_PSEUDO =
			TABLE_NAME + "." + COL_PSEUDO;
	/** life. */
	public static final String COL_LIFE =
			"life";
	/** Alias. */
	public static final String ALIASED_COL_LIFE =
			TABLE_NAME + "." + COL_LIFE;
	/** dead. */
	public static final String COL_DEAD =
			"dead";
	/** Alias. */
	public static final String ALIASED_COL_DEAD =
			TABLE_NAME + "." + COL_DEAD;
	/** baseCharacter. */
	public static final String COL_BASECHARACTER =
			"baseCharacter";
	/** Alias. */
	public static final String ALIASED_COL_BASECHARACTER =
			TABLE_NAME + "." + COL_BASECHARACTER;
	/** baseWeapon. */
	public static final String COL_BASEWEAPON =
			"baseWeapon";
	/** Alias. */
	public static final String ALIASED_COL_BASEWEAPON =
			TABLE_NAME + "." + COL_BASEWEAPON;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		FightPlayerSQLiteAdapter.COL_ID,
		FightPlayerSQLiteAdapter.COL_TEAMID,
		FightPlayerSQLiteAdapter.COL_FIGHT,
		FightPlayerSQLiteAdapter.COL_PSEUDO,
		FightPlayerSQLiteAdapter.COL_LIFE,
		FightPlayerSQLiteAdapter.COL_DEAD,
		FightPlayerSQLiteAdapter.COL_BASECHARACTER,
		FightPlayerSQLiteAdapter.COL_BASEWEAPON
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		FightPlayerSQLiteAdapter.ALIASED_COL_ID,
		FightPlayerSQLiteAdapter.ALIASED_COL_TEAMID,
		FightPlayerSQLiteAdapter.ALIASED_COL_FIGHT,
		FightPlayerSQLiteAdapter.ALIASED_COL_PSEUDO,
		FightPlayerSQLiteAdapter.ALIASED_COL_LIFE,
		FightPlayerSQLiteAdapter.ALIASED_COL_DEAD,
		FightPlayerSQLiteAdapter.ALIASED_COL_BASECHARACTER,
		FightPlayerSQLiteAdapter.ALIASED_COL_BASEWEAPON
	};

	/**
	 * Get the table name used in DB for your FightPlayer entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your FightPlayer entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the FightPlayer entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_TEAMID	+ " INTEGER NOT NULL,"
		 + COL_FIGHT	+ " INTEGER NOT NULL,"
		 + COL_PSEUDO	+ " VARCHAR NOT NULL,"
		 + COL_LIFE	+ " INTEGER NOT NULL,"
		 + COL_DEAD	+ " BOOLEAN NOT NULL,"
		 + COL_BASECHARACTER	+ " INTEGER NOT NULL,"
		 + COL_BASEWEAPON	+ " INTEGER NOT NULL,"
		
		
		 + "FOREIGN KEY(" + COL_FIGHT + ") REFERENCES " 
			 + FightSQLiteAdapter.TABLE_NAME 
				+ " (" + FightSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_BASECHARACTER + ") REFERENCES " 
			 + CharacterPlayerSQLiteAdapter.TABLE_NAME 
				+ " (" + CharacterPlayerSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_BASEWEAPON + ") REFERENCES " 
			 + ItemWeaponSQLiteAdapter.TABLE_NAME 
				+ " (" + ItemWeaponSQLiteAdapter.COL_ID + ")"
		+ ", UNIQUE(" + COL_ID + ")"
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public FightPlayerSQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters

	/**
	 * Convert FightPlayer entity to Content Values for database.
	 * @param item FightPlayer entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final FightPlayer item) {
		final ContentValues result = new ContentValues();

		if (item.getId() != null) {
			result.put(COL_ID,
				String.valueOf(item.getId()));
		}

		if (item.getTeamId() != null) {
			result.put(COL_TEAMID,
				String.valueOf(item.getTeamId()));
		}

		if (item.getFight() != null) {
			result.put(COL_FIGHT,
				item.getFight().getId());
		}

		if (item.getPseudo() != null) {
			result.put(COL_PSEUDO,
				item.getPseudo());
		}

		if (item.getLife() != null) {
			result.put(COL_LIFE,
				String.valueOf(item.getLife()));
		}

		result.put(COL_DEAD,
			item.isDead());

		if (item.getBaseCharacter() != null) {
			result.put(COL_BASECHARACTER,
				item.getBaseCharacter().getId());
		}

		if (item.getBaseWeapon() != null) {
			result.put(COL_BASEWEAPON,
				item.getBaseWeapon().getId());
		}


		return result;
	}

	/**
	 * Convert Cursor of database to FightPlayer entity.
	 * @param cursor Cursor object
	 * @return FightPlayer entity
	 */
	public FightPlayer cursorToItem(final Cursor cursor) {
		FightPlayer result = new FightPlayer();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to FightPlayer entity.
	 * @param cursor Cursor object
	 * @param result FightPlayer entity
	 */
	public void cursorToItem(final Cursor cursor, final FightPlayer result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_TEAMID);
			result.setTeamId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_FIGHT);
			final Fight fight = new Fight();
			fight.setId(cursor.getInt(index));
			result.setFight(fight);

			index = cursor.getColumnIndexOrThrow(COL_PSEUDO);
			result.setPseudo(
					cursor.getString(index));

			index = cursor.getColumnIndexOrThrow(COL_LIFE);
			result.setLife(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_DEAD);
			result.setDead(
					cursor.getInt(index) == 1);

			index = cursor.getColumnIndexOrThrow(COL_BASECHARACTER);
			final CharacterPlayer baseCharacter = new CharacterPlayer();
			baseCharacter.setId(cursor.getInt(index));
			result.setBaseCharacter(baseCharacter);

			index = cursor.getColumnIndexOrThrow(COL_BASEWEAPON);
			final ItemWeapon baseWeapon = new ItemWeapon();
			baseWeapon.setId(cursor.getInt(index));
			result.setBaseWeapon(baseWeapon);


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read FightPlayer by id in database.
	 *
	 * @param id Identify of FightPlayer
	 * @return FightPlayer entity
	 */
	public FightPlayer getByID(final Integer id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final FightPlayer result = this.cursorToItem(cursor);
		cursor.close();

		if (result.getFight() != null) {
			final FightSQLiteAdapter fightAdapter =
					new FightSQLiteAdapter(this.ctx);
			fightAdapter.open(this.mDatabase);
			
			result.setFight(fightAdapter.getByID(
							result.getFight().getId()));
		}
		if (result.getBaseCharacter() != null) {
			final CharacterPlayerSQLiteAdapter baseCharacterAdapter =
					new CharacterPlayerSQLiteAdapter(this.ctx);
			baseCharacterAdapter.open(this.mDatabase);
			
			result.setBaseCharacter(baseCharacterAdapter.getByID(
							result.getBaseCharacter().getId()));
		}
		if (result.getBaseWeapon() != null) {
			final ItemWeaponSQLiteAdapter baseWeaponAdapter =
					new ItemWeaponSQLiteAdapter(this.ctx);
			baseWeaponAdapter.open(this.mDatabase);
			
			result.setBaseWeapon(baseWeaponAdapter.getByID(
							result.getBaseWeapon().getId()));
		}
		final SpellSQLiteAdapter baseSpellsAdapter =
				new SpellSQLiteAdapter(this.ctx);
		baseSpellsAdapter.open(this.mDatabase);
		Cursor basespellsCursor = baseSpellsAdapter
					.getByFightPlayerbaseSpellsInternal(result.getId(), SpellSQLiteAdapter.ALIASED_COLS, null, null, null);
		result.setBaseSpells(baseSpellsAdapter.cursorToItems(basespellsCursor));
		return result;
	}

	/**
	 * Find & read FightPlayer by fight.
	 * @param fightId fightId
	 * @param orderBy Order by string (can be null)
	 * @return List of FightPlayer entities
	 */
	 public Cursor getByFight(final int fightId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = FightPlayerSQLiteAdapter.COL_FIGHT + "=?";
		String idSelectionArgs = String.valueOf(fightId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read FightPlayer by baseCharacter.
	 * @param basecharacterId basecharacterId
	 * @param orderBy Order by string (can be null)
	 * @return List of FightPlayer entities
	 */
	 public Cursor getByBaseCharacter(final int basecharacterId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = FightPlayerSQLiteAdapter.COL_BASECHARACTER + "=?";
		String idSelectionArgs = String.valueOf(basecharacterId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read FightPlayer by baseWeapon.
	 * @param baseweaponId baseweaponId
	 * @param orderBy Order by string (can be null)
	 * @return List of FightPlayer entities
	 */
	 public Cursor getByBaseWeapon(final int baseweaponId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = FightPlayerSQLiteAdapter.COL_BASEWEAPON + "=?";
		String idSelectionArgs = String.valueOf(baseweaponId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }

	/**
	 * Read All FightPlayers entities.
	 *
	 * @return List of FightPlayer entities
	 */
	public ArrayList<FightPlayer> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<FightPlayer> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a FightPlayer entity into database.
	 *
	 * @param item The FightPlayer entity to persist
	 * @return Id of the FightPlayer entity
	 */
	public long insert(final FightPlayer item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		values.remove(FightPlayerSQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					FightPlayerSQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		if (item.getBaseSpells() != null) {
			SpellSQLiteAdapterBase baseSpellsAdapter =
					new SpellSQLiteAdapter(this.ctx);
			baseSpellsAdapter.open(this.mDatabase);
			for (Spell spell
						: item.getBaseSpells()) {
				baseSpellsAdapter.insertOrUpdateWithFightPlayerBaseSpells(
									spell,
									newid);
			}
		}
		return newid;
	}

	/**
	 * Either insert or update a FightPlayer entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The FightPlayer entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final FightPlayer item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a FightPlayer entity into database.
	 *
	 * @param item The FightPlayer entity to persist
	 * @return count of updated entities
	 */
	public int update(final FightPlayer item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		final String whereClause =
				 FightPlayerSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Delete a FightPlayer entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  FightPlayerSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param fightPlayer The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final FightPlayer fightPlayer) {
		return this.delete(fightPlayer.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the FightPlayer corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  FightPlayerSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a FightPlayer entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
