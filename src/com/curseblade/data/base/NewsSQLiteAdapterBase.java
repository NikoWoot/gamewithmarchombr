/**************************************************************************
 * NewsSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.curseblade.data.NewsSQLiteAdapter;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;
import com.curseblade.entity.News;


import com.curseblade.CursebladeApplication;

import com.curseblade.harmony.util.DatabaseUtil;

/** News adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit NewsAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class NewsSQLiteAdapterBase
						extends SQLiteAdapterBase<News> {
	/** Mother Adapter. */
	private final HarmonyRssItemSQLiteAdapter motherAdapter;

	/** TAG for debug purpose. */
	protected static final String TAG = "NewsDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "News";

	/**
	 *  Columns constants fields mapping.
	 */
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		HarmonyRssItemSQLiteAdapter.COL_ID
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_HASH,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_GUID,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_TITLE,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_LINK,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_DESCRIPTION,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_ENCLOSURE,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_AUTHOR,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_PUBDATE,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_CATEGORIES
	};

	/**
	 * Get the table name used in DB for your News entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your News entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		result += " INNER JOIN ";
		result += this.motherAdapter.getJoinedTableName();
		result += " ON ";
		result += ALIASED_COL_ID + " = " + HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID;
		return result;
	}

	/**
	 * Get the column names from the News entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT"
		
		
		+ ", FOREIGN KEY (" + COL_ID + ") REFERENCES " + HarmonyRssItemSQLiteAdapter.TABLE_NAME + "(" + HarmonyRssItemSQLiteAdapter.COL_ID + ") ON DELETE CASCADE"
		+ ");"
;
	}
	@Override
	public SQLiteDatabase open() {
		SQLiteDatabase db = super.open();
		this.motherAdapter.open(db);
		return db;
	}

	@Override
	public SQLiteDatabase open(SQLiteDatabase db) {
		this.motherAdapter.open(db);
		return super.open(db);
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public NewsSQLiteAdapterBase(final Context ctx) {
		super(ctx);
		this.motherAdapter = new HarmonyRssItemSQLiteAdapter(ctx);
	}

	// Converters

	/**
	 * Convert News entity to Content Values for database.
	 * @param item News entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final News item) {
		final ContentValues result = new ContentValues();
		result.putAll(this.motherAdapter.itemToContentValues(item));

		result.put(COL_ID,
			String.valueOf(item.getId()));


		return result;
	}

	/**
	 * Convert Cursor of database to News entity.
	 * @param cursor Cursor object
	 * @return News entity
	 */
	public News cursorToItem(final Cursor cursor) {
		News result = new News();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to News entity.
	 * @param cursor Cursor object
	 * @param result News entity
	 */
	public void cursorToItem(final Cursor cursor, final News result) {
		if (cursor.getCount() != 0) {
			this.motherAdapter.cursorToItem(cursor, result);

			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read News by id in database.
	 *
	 * @param id Identify of News
	 * @return News entity
	 */
	public News getByID(final int id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final News result = this.cursorToItem(cursor);
		cursor.close();

		return result;
	}


	/**
	 * Read All Newss entities.
	 *
	 * @return List of News entities
	 */
	public ArrayList<News> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<News> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a News entity into database.
	 *
	 * @param item The News entity to persist
	 * @return Id of the News entity
	 */
	public long insert(final News item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		values.remove(HarmonyRssItemSQLiteAdapter.COL_ID);
		this.motherAdapter.open(this.mDatabase);
		final ContentValues currentValues =
				DatabaseUtil.extractContentValues(values, COLS);
		int newid = (int) this.motherAdapter.insert(null, values);
		currentValues.put(COL_ID, newid);
		if (values.size() != 0) {
			this.insert(
					null,
					currentValues);
		} else {
			this.insert(
					HarmonyRssItemSQLiteAdapter.COL_ID,
					currentValues);
		}
		item.setId((int) newid);
		return newid;
	}

	/**
	 * Either insert or update a News entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The News entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final News item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a News entity into database.
	 *
	 * @param item The News entity to persist
	 * @return count of updated entities
	 */
	public int update(final News item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		final String whereClause =
				 HarmonyRssItemSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		final ContentValues currentValues =
				DatabaseUtil.extractContentValues(values, COLS);
		this.motherAdapter.update(values, whereClause, whereArgs);

		return this.update(
				currentValues,
				whereClause,
				whereArgs);
	}


	/**
	 * Delete a News entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final int id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  HarmonyRssItemSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param news The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final News news) {
		return this.delete(news.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the News corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final int id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a News entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
