/**************************************************************************
 * CharacterPlayerSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.ObjectArrays;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.ItemArmurySQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemArmury;
import com.curseblade.entity.Spell;
import com.curseblade.entity.ItemWeapon;

import com.curseblade.harmony.util.DateUtils;
import com.curseblade.CursebladeApplication;


/** CharacterPlayer adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit CharacterPlayerAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class CharacterPlayerSQLiteAdapterBase
						extends SQLiteAdapterBase<CharacterPlayer> {

	/** TAG for debug purpose. */
	protected static final String TAG = "CharacterPlayerDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "CharacterPlayer";

	/**
	 *  Columns constants fields mapping.
	 */
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** pseudo. */
	public static final String COL_PSEUDO =
			"pseudo";
	/** Alias. */
	public static final String ALIASED_COL_PSEUDO =
			TABLE_NAME + "." + COL_PSEUDO;
	/** life. */
	public static final String COL_LIFE =
			"life";
	/** Alias. */
	public static final String ALIASED_COL_LIFE =
			TABLE_NAME + "." + COL_LIFE;
	/** createdAt. */
	public static final String COL_CREATEDAT =
			"createdAt";
	/** Alias. */
	public static final String ALIASED_COL_CREATEDAT =
			TABLE_NAME + "." + COL_CREATEDAT;
	/** level. */
	public static final String COL_LEVEL =
			"level";
	/** Alias. */
	public static final String ALIASED_COL_LEVEL =
			TABLE_NAME + "." + COL_LEVEL;
	/** weaponUsed. */
	public static final String COL_WEAPONUSED =
			"weaponUsed";
	/** Alias. */
	public static final String ALIASED_COL_WEAPONUSED =
			TABLE_NAME + "." + COL_WEAPONUSED;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		CharacterPlayerSQLiteAdapter.COL_ID,
		CharacterPlayerSQLiteAdapter.COL_PSEUDO,
		CharacterPlayerSQLiteAdapter.COL_LIFE,
		CharacterPlayerSQLiteAdapter.COL_CREATEDAT,
		CharacterPlayerSQLiteAdapter.COL_LEVEL,
		CharacterPlayerSQLiteAdapter.COL_WEAPONUSED
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		CharacterPlayerSQLiteAdapter.ALIASED_COL_ID,
		CharacterPlayerSQLiteAdapter.ALIASED_COL_PSEUDO,
		CharacterPlayerSQLiteAdapter.ALIASED_COL_LIFE,
		CharacterPlayerSQLiteAdapter.ALIASED_COL_CREATEDAT,
		CharacterPlayerSQLiteAdapter.ALIASED_COL_LEVEL,
		CharacterPlayerSQLiteAdapter.ALIASED_COL_WEAPONUSED
	};

	/**
	 * Get the table name used in DB for your CharacterPlayer entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your CharacterPlayer entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the CharacterPlayer entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_PSEUDO	+ " VARCHAR NOT NULL,"
		 + COL_LIFE	+ " INTEGER NOT NULL DEFAULT '50',"
		 + COL_CREATEDAT	+ " DATE NOT NULL,"
		 + COL_LEVEL	+ " INTEGER NOT NULL DEFAULT '1',"
		 + COL_WEAPONUSED	+ " INTEGER NOT NULL,"
		
		
		 + "FOREIGN KEY(" + COL_WEAPONUSED + ") REFERENCES " 
			 + ItemWeaponSQLiteAdapter.TABLE_NAME 
				+ " (" + ItemWeaponSQLiteAdapter.COL_ID + ")"
		+ ", UNIQUE(" + COL_ID + ")"
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public CharacterPlayerSQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters

	/**
	 * Convert CharacterPlayer entity to Content Values for database.
	 * @param item CharacterPlayer entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final CharacterPlayer item) {
		final ContentValues result = new ContentValues();

		if (item.getId() != null) {
			result.put(COL_ID,
				String.valueOf(item.getId()));
		}

		if (item.getPseudo() != null) {
			result.put(COL_PSEUDO,
				item.getPseudo());
		}

		if (item.getLife() != null) {
			result.put(COL_LIFE,
				String.valueOf(item.getLife()));
		}

		if (item.getCreatedAt() != null) {
			result.put(COL_CREATEDAT,
				item.getCreatedAt().toString(ISODateTimeFormat.dateTime()));
		}

		if (item.getLevel() != null) {
			result.put(COL_LEVEL,
				String.valueOf(item.getLevel()));
		}

		if (item.getWeaponUsed() != null) {
			result.put(COL_WEAPONUSED,
				item.getWeaponUsed().getId());
		}


		return result;
	}

	/**
	 * Convert Cursor of database to CharacterPlayer entity.
	 * @param cursor Cursor object
	 * @return CharacterPlayer entity
	 */
	public CharacterPlayer cursorToItem(final Cursor cursor) {
		CharacterPlayer result = new CharacterPlayer();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to CharacterPlayer entity.
	 * @param cursor Cursor object
	 * @param result CharacterPlayer entity
	 */
	public void cursorToItem(final Cursor cursor, final CharacterPlayer result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_PSEUDO);
			result.setPseudo(
					cursor.getString(index));

			index = cursor.getColumnIndexOrThrow(COL_LIFE);
			result.setLife(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_CREATEDAT);
			final DateTime dtCreatedAt =
					DateUtils.formatISOStringToDateTime(
							cursor.getString(index));
			if (dtCreatedAt != null) {
					result.setCreatedAt(
							dtCreatedAt);
			} else {
				result.setCreatedAt(new DateTime());
			}

			index = cursor.getColumnIndexOrThrow(COL_LEVEL);
			result.setLevel(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_WEAPONUSED);
			final ItemWeapon weaponUsed = new ItemWeapon();
			weaponUsed.setId(cursor.getInt(index));
			result.setWeaponUsed(weaponUsed);


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read CharacterPlayer by id in database.
	 *
	 * @param id Identify of CharacterPlayer
	 * @return CharacterPlayer entity
	 */
	public CharacterPlayer getByID(final Integer id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final CharacterPlayer result = this.cursorToItem(cursor);
		cursor.close();

		final ItemArmurySQLiteAdapter armuryEquipedItemsAdapter =
				new ItemArmurySQLiteAdapter(this.ctx);
		armuryEquipedItemsAdapter.open(this.mDatabase);
		Cursor armuryequipeditemsCursor = armuryEquipedItemsAdapter
					.getByCharacterPlayerarmuryEquipedItemsInternal(result.getId(), ItemArmurySQLiteAdapter.ALIASED_COLS, null, null, null);
		result.setArmuryEquipedItems(armuryEquipedItemsAdapter.cursorToItems(armuryequipeditemsCursor));
		final SpellSQLiteAdapter equipedSpellsAdapter =
				new SpellSQLiteAdapter(this.ctx);
		equipedSpellsAdapter.open(this.mDatabase);
		Cursor equipedspellsCursor = equipedSpellsAdapter
					.getByCharacterPlayerequipedSpellsInternal(result.getId(), SpellSQLiteAdapter.ALIASED_COLS, null, null, null);
		result.setEquipedSpells(equipedSpellsAdapter.cursorToItems(equipedspellsCursor));
		if (result.getWeaponUsed() != null) {
			final ItemWeaponSQLiteAdapter weaponUsedAdapter =
					new ItemWeaponSQLiteAdapter(this.ctx);
			weaponUsedAdapter.open(this.mDatabase);
			
			result.setWeaponUsed(weaponUsedAdapter.getByID(
							result.getWeaponUsed().getId()));
		}
		return result;
	}

	/**
	 * Find & read CharacterPlayer by weaponUsed.
	 * @param weaponusedId weaponusedId
	 * @param orderBy Order by string (can be null)
	 * @return List of CharacterPlayer entities
	 */
	 public Cursor getByWeaponUsed(final int weaponusedId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = CharacterPlayerSQLiteAdapter.COL_WEAPONUSED + "=?";
		String idSelectionArgs = String.valueOf(weaponusedId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }

	/**
	 * Read All CharacterPlayers entities.
	 *
	 * @return List of CharacterPlayer entities
	 */
	public ArrayList<CharacterPlayer> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<CharacterPlayer> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a CharacterPlayer entity into database.
	 *
	 * @param item The CharacterPlayer entity to persist
	 * @return Id of the CharacterPlayer entity
	 */
	public long insert(final CharacterPlayer item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		values.remove(CharacterPlayerSQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					CharacterPlayerSQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		if (item.getArmuryEquipedItems() != null) {
			ItemArmurySQLiteAdapterBase armuryEquipedItemsAdapter =
					new ItemArmurySQLiteAdapter(this.ctx);
			armuryEquipedItemsAdapter.open(this.mDatabase);
			for (ItemArmury itemarmury
						: item.getArmuryEquipedItems()) {
				armuryEquipedItemsAdapter.insertOrUpdateWithCharacterPlayerArmuryEquipedItems(
									itemarmury,
									newid);
			}
		}
		if (item.getEquipedSpells() != null) {
			SpellSQLiteAdapterBase equipedSpellsAdapter =
					new SpellSQLiteAdapter(this.ctx);
			equipedSpellsAdapter.open(this.mDatabase);
			for (Spell spell
						: item.getEquipedSpells()) {
				equipedSpellsAdapter.insertOrUpdateWithCharacterPlayerEquipedSpells(
									spell,
									newid);
			}
		}
		return newid;
	}

	/**
	 * Either insert or update a CharacterPlayer entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The CharacterPlayer entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final CharacterPlayer item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a CharacterPlayer entity into database.
	 *
	 * @param item The CharacterPlayer entity to persist
	 * @return count of updated entities
	 */
	public int update(final CharacterPlayer item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		final String whereClause =
				 CharacterPlayerSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Delete a CharacterPlayer entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  CharacterPlayerSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param characterPlayer The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final CharacterPlayer characterPlayer) {
		return this.delete(characterPlayer.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the CharacterPlayer corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  CharacterPlayerSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a CharacterPlayer entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
