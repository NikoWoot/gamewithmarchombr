/**************************************************************************
 * SpellSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.common.base.Strings;
import com.google.common.collect.ObjectArrays;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.entity.Spell;


import com.curseblade.CursebladeApplication;


/** Spell adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit SpellAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class SpellSQLiteAdapterBase
						extends SQLiteAdapterBase<Spell> {

	/** TAG for debug purpose. */
	protected static final String TAG = "SpellDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "Spell";

	/**
	 *  Columns constants fields mapping.
	 */
	/** CharacterPlayer_equipedSpells_internal. */
	public static final String COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL =
			"CharacterPlayer_equipedSpells_internal";
	/** Alias. */
	public static final String ALIASED_COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL =
			TABLE_NAME + "." + COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL;
	/** FightPlayer_baseSpells_internal. */
	public static final String COL_FIGHTPLAYERBASESPELLSINTERNAL =
			"FightPlayer_baseSpells_internal";
	/** Alias. */
	public static final String ALIASED_COL_FIGHTPLAYERBASESPELLSINTERNAL =
			TABLE_NAME + "." + COL_FIGHTPLAYERBASESPELLSINTERNAL;
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** name. */
	public static final String COL_NAME =
			"name";
	/** Alias. */
	public static final String ALIASED_COL_NAME =
			TABLE_NAME + "." + COL_NAME;
	/** baseAttack. */
	public static final String COL_BASEATTACK =
			"baseAttack";
	/** Alias. */
	public static final String ALIASED_COL_BASEATTACK =
			TABLE_NAME + "." + COL_BASEATTACK;
	/** criticalAttack. */
	public static final String COL_CRITICALATTACK =
			"criticalAttack";
	/** Alias. */
	public static final String ALIASED_COL_CRITICALATTACK =
			TABLE_NAME + "." + COL_CRITICALATTACK;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		SpellSQLiteAdapter.COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL,
		SpellSQLiteAdapter.COL_FIGHTPLAYERBASESPELLSINTERNAL,
		SpellSQLiteAdapter.COL_ID,
		SpellSQLiteAdapter.COL_NAME,
		SpellSQLiteAdapter.COL_BASEATTACK,
		SpellSQLiteAdapter.COL_CRITICALATTACK
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		SpellSQLiteAdapter.ALIASED_COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL,
		SpellSQLiteAdapter.ALIASED_COL_FIGHTPLAYERBASESPELLSINTERNAL,
		SpellSQLiteAdapter.ALIASED_COL_ID,
		SpellSQLiteAdapter.ALIASED_COL_NAME,
		SpellSQLiteAdapter.ALIASED_COL_BASEATTACK,
		SpellSQLiteAdapter.ALIASED_COL_CRITICALATTACK
	};

	/**
	 * Get the table name used in DB for your Spell entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your Spell entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the Spell entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL	+ " INTEGER,"
		 + COL_FIGHTPLAYERBASESPELLSINTERNAL	+ " INTEGER,"
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_NAME	+ " VARCHAR NOT NULL,"
		 + COL_BASEATTACK	+ " DOUBLE NOT NULL,"
		 + COL_CRITICALATTACK	+ " DOUBLE NOT NULL,"
		
		
		 + "FOREIGN KEY(" + COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL + ") REFERENCES " 
			 + CharacterPlayerSQLiteAdapter.TABLE_NAME 
				+ " (" + CharacterPlayerSQLiteAdapter.COL_ID + "),"
		 + "FOREIGN KEY(" + COL_FIGHTPLAYERBASESPELLSINTERNAL + ") REFERENCES " 
			 + FightPlayerSQLiteAdapter.TABLE_NAME 
				+ " (" + FightPlayerSQLiteAdapter.COL_ID + ")"
		+ ", UNIQUE(" + COL_ID + ")"
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public SpellSQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters
	/** Convert Spell entity to Content Values for database.
	 *
	 * @param item Spell entity object
	 * @param characterplayerId characterplayer id
	 * @param fightplayerId fightplayer id
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final Spell item,
				int characterplayerId,
				int fightplayerId) {
		final ContentValues result = this.itemToContentValues(item);
		result.put(COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL,
				String.valueOf(characterplayerId));
		result.put(COL_FIGHTPLAYERBASESPELLSINTERNAL,
				String.valueOf(fightplayerId));
		return result;
	}

	/**
	 * Convert Spell entity to Content Values for database.
	 * @param item Spell entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final Spell item) {
		final ContentValues result = new ContentValues();

		if (item.getId() != null) {
			result.put(COL_ID,
				String.valueOf(item.getId()));
		}

		if (item.getName() != null) {
			result.put(COL_NAME,
				item.getName());
		}

		if (item.getBaseAttack() != null) {
			result.put(COL_BASEATTACK,
				String.valueOf(item.getBaseAttack()));
		}

		if (item.getCriticalAttack() != null) {
			result.put(COL_CRITICALATTACK,
				String.valueOf(item.getCriticalAttack()));
		}


		return result;
	}

	/**
	 * Convert Cursor of database to Spell entity.
	 * @param cursor Cursor object
	 * @return Spell entity
	 */
	public Spell cursorToItem(final Cursor cursor) {
		Spell result = new Spell();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to Spell entity.
	 * @param cursor Cursor object
	 * @param result Spell entity
	 */
	public void cursorToItem(final Cursor cursor, final Spell result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_NAME);
			result.setName(
					cursor.getString(index));

			index = cursor.getColumnIndexOrThrow(COL_BASEATTACK);
			result.setBaseAttack(
					cursor.getDouble(index));

			index = cursor.getColumnIndexOrThrow(COL_CRITICALATTACK);
			result.setCriticalAttack(
					cursor.getDouble(index));


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read Spell by id in database.
	 *
	 * @param id Identify of Spell
	 * @return Spell entity
	 */
	public Spell getByID(final Integer id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final Spell result = this.cursorToItem(cursor);
		cursor.close();

		return result;
	}

	/**
	 * Find & read Spell by CharacterPlayerequipedSpellsInternal.
	 * @param characterplayerequipedspellsinternalId characterplayerequipedspellsinternalId
	 * @param orderBy Order by string (can be null)
	 * @return List of Spell entities
	 */
	 public Cursor getByCharacterPlayerequipedSpellsInternal(final int characterplayerequipedspellsinternalId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = SpellSQLiteAdapter.COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL + "=?";
		String idSelectionArgs = String.valueOf(characterplayerequipedspellsinternalId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }
	/**
	 * Find & read Spell by FightPlayerbaseSpellsInternal.
	 * @param fightplayerbasespellsinternalId fightplayerbasespellsinternalId
	 * @param orderBy Order by string (can be null)
	 * @return List of Spell entities
	 */
	 public Cursor getByFightPlayerbaseSpellsInternal(final int fightplayerbasespellsinternalId, String[] projection, String selection, String[] selectionArgs, String orderBy) {
		String idSelection = SpellSQLiteAdapter.COL_FIGHTPLAYERBASESPELLSINTERNAL + "=?";
		String idSelectionArgs = String.valueOf(fightplayerbasespellsinternalId);
		if (!Strings.isNullOrEmpty(selection)) {
			selection += " AND " + idSelection;
			selectionArgs = ObjectArrays.concat(selectionArgs, idSelectionArgs);
		} else {
			selection = idSelection;
			selectionArgs = new String[]{idSelectionArgs};
		}
		final Cursor cursor = this.query(
				projection,
				selection,
				selectionArgs,
				null,
				null,
				orderBy);

		return cursor;
	 }

	/**
	 * Read All Spells entities.
	 *
	 * @return List of Spell entities
	 */
	public ArrayList<Spell> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<Spell> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a Spell entity into database.
	 *
	 * @param item The Spell entity to persist
	 * @return Id of the Spell entity
	 */
	public long insert(final Spell item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item, 0, 0);
		values.remove(SpellSQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					SpellSQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		return newid;
	}

	/**
	 * Either insert or update a Spell entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The Spell entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final Spell item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a Spell entity into database.
	 *
	 * @param item The Spell entity to persist
	 * @return count of updated entities
	 */
	public int update(final Spell item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item, 0, 0);
		final String whereClause =
				 SpellSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Update a Spell entity into database.
	 *
	 * @param item The Spell entity to persist
	 * @param characterplayerId The characterplayer id
	 * @return count of updated entities
	 */
	public int updateWithCharacterPlayerEquipedSpells(
					Spell item, int characterplayerId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		ContentValues values =
				this.itemToContentValues(item,
							characterplayerId, 0);
		String whereClause =
				 SpellSQLiteAdapter.COL_ID
				 + "=? ";
		String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Either insert or update a Spell entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The Spell entity to persist
	 * @param characterplayerId The characterplayer id
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdateWithCharacterPlayerEquipedSpells(
			Spell item, int characterplayerId) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.updateWithCharacterPlayerEquipedSpells(item,
					characterplayerId);
		} else {
			// Item doesn't exist => create it
			long id = this.insertWithCharacterPlayerEquipedSpells(item,
					characterplayerId);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}


	/**
	 * Insert a Spell entity into database.
	 *
	 * @param item The Spell entity to persist
	 * @param characterplayerId The characterplayer id
	 * @return Id of the Spell entity
	 */
	public long insertWithCharacterPlayerEquipedSpells(
			Spell item, int characterplayerId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		ContentValues values = this.itemToContentValues(item,
				characterplayerId,
				0);
		values.remove(SpellSQLiteAdapter.COL_ID);
		int newid = (int) this.insert(
			null,
			values);


		return newid;
	}


	/**
	 * Update a Spell entity into database.
	 *
	 * @param item The Spell entity to persist
	 * @param fightplayerId The fightplayer id
	 * @return count of updated entities
	 */
	public int updateWithFightPlayerBaseSpells(
					Spell item, int fightplayerId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		ContentValues values =
				this.itemToContentValues(item, 0,
							fightplayerId);
		String whereClause =
				 SpellSQLiteAdapter.COL_ID
				 + "=? ";
		String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Either insert or update a Spell entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The Spell entity to persist
	 * @param fightplayerId The fightplayer id
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdateWithFightPlayerBaseSpells(
			Spell item, int fightplayerId) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.updateWithFightPlayerBaseSpells(item,
					fightplayerId);
		} else {
			// Item doesn't exist => create it
			long id = this.insertWithFightPlayerBaseSpells(item,
					fightplayerId);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}


	/**
	 * Insert a Spell entity into database.
	 *
	 * @param item The Spell entity to persist
	 * @param fightplayerId The fightplayer id
	 * @return Id of the Spell entity
	 */
	public long insertWithFightPlayerBaseSpells(
			Spell item, int fightplayerId) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		ContentValues values = this.itemToContentValues(item,
				0,
				fightplayerId);
		values.remove(SpellSQLiteAdapter.COL_ID);
		int newid = (int) this.insert(
			null,
			values);


		return newid;
	}


	/**
	 * Delete a Spell entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  SpellSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param spell The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final Spell spell) {
		return this.delete(spell.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the Spell corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  SpellSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a Spell entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
