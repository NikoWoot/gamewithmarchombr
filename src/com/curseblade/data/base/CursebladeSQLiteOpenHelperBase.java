/**************************************************************************
 * CursebladeSQLiteOpenHelperBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.curseblade.data.CursebladeSQLiteOpenHelper;
import com.curseblade.data.ItemArmurySQLiteAdapter;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.data.FightActionSQLiteAdapter;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;
import com.curseblade.data.TournamentSQLiteAdapter;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;
import com.curseblade.data.NewsSQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.CursebladeApplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.curseblade.fixture.DataLoader;


/**
 * This class makes it easy for ContentProvider implementations to defer <br />
 * opening and upgrading the database until first use, to avoid blocking <br />
 * application startup with long-running database upgrades.
 * @see android.database.sqlite.SQLiteOpenHelper
 */
public class CursebladeSQLiteOpenHelperBase
						extends SQLiteOpenHelper {
	/** TAG for debug purpose. */
	protected static final String TAG = "DatabaseHelper";
	/** Context. */
	protected Context ctx;

	/** Android's default system path of the database.
	 *
	 */
	private static String DB_PATH;
	/** database name. */
	private static String DB_NAME;
	/** is assets exist.*/
	private static boolean assetsExist;
	/** Are we in a JUnit context ?*/
	public static boolean isJUnit = false;

	/**
	 * Constructor.
	 * @param ctx Context
	 * @param name name
	 * @param factory factory
	 * @param version version
	 */
	public CursebladeSQLiteOpenHelperBase(final Context ctx,
		   final String name, final CursorFactory factory, final int version) {
		super(ctx, name, factory, version);
		this.ctx = ctx;
		DB_NAME = name;
		DB_PATH = ctx.getDatabasePath(DB_NAME).getAbsolutePath();

		try {
			this.ctx.getAssets().open(DB_NAME);
			assetsExist = true;
		} catch (IOException e) {
			assetsExist = false;
		}
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		// Activation of SQLiteConstraints
		//db.execSQL("PRAGMA foreign_keys = ON;");
	}

	@Override
	public void onCreate(final SQLiteDatabase db) {
		Log.i(TAG, "Create database..");

		if (!assetsExist) {
			/// Create Schema
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : ItemArmury");
			}

			db.execSQL(ItemArmurySQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : Fight");
			}

			db.execSQL(FightSQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : ItemWeapon");
			}

			db.execSQL(ItemWeaponSQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : FightAction");
			}

			db.execSQL(FightActionSQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : TournamentPoolFight");
			}

			db.execSQL(TournamentPoolFightSQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : Tournament");
			}

			db.execSQL(TournamentSQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : HarmonyRssItem");
			}

			db.execSQL(HarmonyRssItemSQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : News");
			}

			db.execSQL(NewsSQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : Spell");
			}

			db.execSQL(SpellSQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : CharacterPlayer");
			}

			db.execSQL(CharacterPlayerSQLiteAdapter.getSchema());
			if (CursebladeApplication.DEBUG) {
				Log.d(TAG, "Creating schema : FightPlayer");
			}

			db.execSQL(FightPlayerSQLiteAdapter.getSchema());
			db.execSQL("PRAGMA foreign_keys = ON;");
			if (!CursebladeSQLiteOpenHelper.isJUnit) {
				this.loadData(db);
			}
		}

	}

	/**
	 * Clear the database given in parameters.
	 * @param db The database to clear
	 */
	public static void clearDatabase(final SQLiteDatabase db) {
		Log.i(TAG, "Clearing database...");

		db.delete(ItemArmurySQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(FightSQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(ItemWeaponSQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(FightActionSQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(TournamentPoolFightSQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(TournamentSQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(HarmonyRssItemSQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(NewsSQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(SpellSQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(CharacterPlayerSQLiteAdapter.TABLE_NAME,
				null,
				null);
		db.delete(FightPlayerSQLiteAdapter.TABLE_NAME,
				null,
				null);
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, final int oldVersion,
			final int newVersion) {
		Log.i(TAG, "Update database..");

		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Upgrading database from version " + oldVersion
					   + " to " + newVersion);
		}

		// TODO : Upgrade your tables !
	}

	/**
	 * Loads data from the fixture files.
	 * @param db The database to populate with fixtures
	 */
	private void loadData(final SQLiteDatabase db) {
		final DataLoader dataLoader = new DataLoader(this.ctx);
		dataLoader.clean();
		int mode = DataLoader.MODE_APP;
		if (CursebladeApplication.DEBUG) {
			mode = DataLoader.MODE_APP | DataLoader.MODE_DEBUG;
		}
		dataLoader.loadData(db, mode);
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * @throws IOException if error has occured while copying files
	 */
	public void createDataBase() throws IOException {
		if (assetsExist && !checkDataBase()) {
			// By calling this method and empty database will be created into
			// the default system path
			// so we're gonna be able to overwrite that database with ours
			this.getReadableDatabase();

			try {
				copyDataBase();

			} catch (IOException e) {
				throw new Error("Error copying database");
			}
		}
	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 *
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase() {
		boolean result;

		SQLiteDatabase checkDB = null;
		try {
			final String myPath = DB_PATH + DB_NAME;
			// NOTE : the system throw error message : "Database is locked"
			// when the Database is not found (incorrect path)
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READONLY);
			result = true;
		} catch (SQLiteException e) {
			// database doesn't exist yet.
			result = false;
		}

		if (checkDB != null) {
			checkDB.close();
		}

		return result;
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * @throws IOException if error has occured while copying files
	 * */
	private void copyDataBase() throws IOException {

		// Open your local db as the input stream
		final InputStream myInput = this.ctx.getAssets().open(DB_NAME);

		// Path to the just created empty db
		final String outFileName = DB_PATH + DB_NAME;

		// Open the empty db as the output stream
		final OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		final byte[] buffer = new byte[1024];
		int length = myInput.read(buffer);
		while (length > 0) {
			myOutput.write(buffer, 0, length);
			length = myInput.read(buffer);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}
}
