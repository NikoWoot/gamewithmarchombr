/**************************************************************************
 * HarmonyRssItemSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.curseblade.data.HarmonyRssItemSQLiteAdapter;
import com.curseblade.data.NewsSQLiteAdapter;
import com.curseblade.entity.HarmonyRssItem;

import com.curseblade.harmony.util.DateUtils;
import com.curseblade.CursebladeApplication;


/** HarmonyRssItem adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit HarmonyRssItemAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class HarmonyRssItemSQLiteAdapterBase
						extends SQLiteAdapterBase<HarmonyRssItem> {

	/** TAG for debug purpose. */
	protected static final String TAG = "HarmonyRssItemDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "HarmonyRssItem";

	/**
	 *  Columns constants fields mapping.
	 */
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** hash. */
	public static final String COL_HASH =
			"hash";
	/** Alias. */
	public static final String ALIASED_COL_HASH =
			TABLE_NAME + "." + COL_HASH;
	/** guid. */
	public static final String COL_GUID =
			"guid";
	/** Alias. */
	public static final String ALIASED_COL_GUID =
			TABLE_NAME + "." + COL_GUID;
	/** title. */
	public static final String COL_TITLE =
			"title";
	/** Alias. */
	public static final String ALIASED_COL_TITLE =
			TABLE_NAME + "." + COL_TITLE;
	/** link. */
	public static final String COL_LINK =
			"link";
	/** Alias. */
	public static final String ALIASED_COL_LINK =
			TABLE_NAME + "." + COL_LINK;
	/** description. */
	public static final String COL_DESCRIPTION =
			"description";
	/** Alias. */
	public static final String ALIASED_COL_DESCRIPTION =
			TABLE_NAME + "." + COL_DESCRIPTION;
	/** enclosure. */
	public static final String COL_ENCLOSURE =
			"enclosure";
	/** Alias. */
	public static final String ALIASED_COL_ENCLOSURE =
			TABLE_NAME + "." + COL_ENCLOSURE;
	/** author. */
	public static final String COL_AUTHOR =
			"author";
	/** Alias. */
	public static final String ALIASED_COL_AUTHOR =
			TABLE_NAME + "." + COL_AUTHOR;
	/** pubDate. */
	public static final String COL_PUBDATE =
			"pubDate";
	/** Alias. */
	public static final String ALIASED_COL_PUBDATE =
			TABLE_NAME + "." + COL_PUBDATE;
	/** categories. */
	public static final String COL_CATEGORIES =
			"categories";
	/** Alias. */
	public static final String ALIASED_COL_CATEGORIES =
			TABLE_NAME + "." + COL_CATEGORIES;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		HarmonyRssItemSQLiteAdapter.COL_ID,
		HarmonyRssItemSQLiteAdapter.COL_HASH,
		HarmonyRssItemSQLiteAdapter.COL_GUID,
		HarmonyRssItemSQLiteAdapter.COL_TITLE,
		HarmonyRssItemSQLiteAdapter.COL_LINK,
		HarmonyRssItemSQLiteAdapter.COL_DESCRIPTION,
		HarmonyRssItemSQLiteAdapter.COL_ENCLOSURE,
		HarmonyRssItemSQLiteAdapter.COL_AUTHOR,
		HarmonyRssItemSQLiteAdapter.COL_PUBDATE,
		HarmonyRssItemSQLiteAdapter.COL_CATEGORIES
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_HASH,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_GUID,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_TITLE,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_LINK,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_DESCRIPTION,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_ENCLOSURE,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_AUTHOR,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_PUBDATE,
		HarmonyRssItemSQLiteAdapter.ALIASED_COL_CATEGORIES
	};

	/**
	 * Get the table name used in DB for your HarmonyRssItem entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your HarmonyRssItem entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the HarmonyRssItem entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_HASH	+ " INTEGER NOT NULL,"
		 + COL_GUID	+ " VARCHAR,"
		 + COL_TITLE	+ " VARCHAR NOT NULL,"
		 + COL_LINK	+ " VARCHAR NOT NULL,"
		 + COL_DESCRIPTION	+ " VARCHAR NOT NULL,"
		 + COL_ENCLOSURE	+ " VARCHAR,"
		 + COL_AUTHOR	+ " VARCHAR,"
		 + COL_PUBDATE	+ " DATETIME,"
		 + COL_CATEGORIES	+ " VARCHAR"
		
		
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public HarmonyRssItemSQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters

	/**
	 * Convert HarmonyRssItem entity to Content Values for database.
	 * @param item HarmonyRssItem entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final HarmonyRssItem item) {
		final ContentValues result = new ContentValues();

		result.put(COL_ID,
			String.valueOf(item.getId()));

		result.put(COL_HASH,
			String.valueOf(item.getHash()));

		if (item.getGuid() != null) {
			result.put(COL_GUID,
				item.getGuid());
		} else {
			result.put(COL_GUID, (String) null);
		}

		if (item.getTitle() != null) {
			result.put(COL_TITLE,
				item.getTitle());
		}

		if (item.getLink() != null) {
			result.put(COL_LINK,
				item.getLink());
		}

		if (item.getDescription() != null) {
			result.put(COL_DESCRIPTION,
				item.getDescription());
		}

		if (item.getEnclosure() != null) {
			result.put(COL_ENCLOSURE,
				item.getEnclosure());
		} else {
			result.put(COL_ENCLOSURE, (String) null);
		}

		if (item.getAuthor() != null) {
			result.put(COL_AUTHOR,
				item.getAuthor());
		} else {
			result.put(COL_AUTHOR, (String) null);
		}

		if (item.getPubDate() != null) {
			result.put(COL_PUBDATE,
				item.getPubDate().toString(ISODateTimeFormat.dateTime()));
		} else {
			result.put(COL_PUBDATE, (String) null);
		}

		if (item.getCategories() != null) {
			result.put(COL_CATEGORIES,
				item.getCategories());
		} else {
			result.put(COL_CATEGORIES, (String) null);
		}


		return result;
	}

	/**
	 * Convert Cursor of database to HarmonyRssItem entity.
	 * @param cursor Cursor object
	 * @return HarmonyRssItem entity
	 */
	public HarmonyRssItem cursorToItem(final Cursor cursor) {
		HarmonyRssItem result = new HarmonyRssItem();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to HarmonyRssItem entity.
	 * @param cursor Cursor object
	 * @param result HarmonyRssItem entity
	 */
	public void cursorToItem(final Cursor cursor, final HarmonyRssItem result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_HASH);
			result.setHash(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_GUID);
			if (!cursor.isNull(index)) {
				result.setGuid(
					cursor.getString(index));
			}

			index = cursor.getColumnIndexOrThrow(COL_TITLE);
			result.setTitle(
					cursor.getString(index));

			index = cursor.getColumnIndexOrThrow(COL_LINK);
			result.setLink(
					cursor.getString(index));

			index = cursor.getColumnIndexOrThrow(COL_DESCRIPTION);
			result.setDescription(
					cursor.getString(index));

			index = cursor.getColumnIndexOrThrow(COL_ENCLOSURE);
			if (!cursor.isNull(index)) {
				result.setEnclosure(
					cursor.getString(index));
			}

			index = cursor.getColumnIndexOrThrow(COL_AUTHOR);
			if (!cursor.isNull(index)) {
				result.setAuthor(
					cursor.getString(index));
			}

			index = cursor.getColumnIndexOrThrow(COL_PUBDATE);
			if (!cursor.isNull(index)) {
				final DateTime dtPubDate =
					DateUtils.formatISOStringToDateTime(
							cursor.getString(index));
				if (dtPubDate != null) {
						result.setPubDate(
							dtPubDate);
				} else {
					result.setPubDate(new DateTime());
				}
			}

			index = cursor.getColumnIndexOrThrow(COL_CATEGORIES);
			if (!cursor.isNull(index)) {
				result.setCategories(
					cursor.getString(index));
			}


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read HarmonyRssItem by id in database.
	 *
	 * @param id Identify of HarmonyRssItem
	 * @return HarmonyRssItem entity
	 */
	public HarmonyRssItem getByID(final int id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final HarmonyRssItem result = this.cursorToItem(cursor);
		cursor.close();

		return result;
	}


	/**
	 * Read All HarmonyRssItems entities.
	 *
	 * @return List of HarmonyRssItem entities
	 */
	public ArrayList<HarmonyRssItem> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<HarmonyRssItem> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a HarmonyRssItem entity into database.
	 *
	 * @param item The HarmonyRssItem entity to persist
	 * @return Id of the HarmonyRssItem entity
	 */
	public long insert(final HarmonyRssItem item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		values.remove(HarmonyRssItemSQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					HarmonyRssItemSQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		return newid;
	}

	/**
	 * Either insert or update a HarmonyRssItem entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The HarmonyRssItem entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final HarmonyRssItem item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a HarmonyRssItem entity into database.
	 *
	 * @param item The HarmonyRssItem entity to persist
	 * @return count of updated entities
	 */
	public int update(final HarmonyRssItem item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		final String whereClause =
				 HarmonyRssItemSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Delete a HarmonyRssItem entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final int id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  HarmonyRssItemSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param harmonyRssItem The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final HarmonyRssItem harmonyRssItem) {
		return this.delete(harmonyRssItem.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the HarmonyRssItem corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final int id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a HarmonyRssItem entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
