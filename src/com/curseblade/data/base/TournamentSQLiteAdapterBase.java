/**************************************************************************
 * TournamentSQLiteAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data.base;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.curseblade.data.TournamentSQLiteAdapter;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;
import com.curseblade.entity.Tournament;
import com.curseblade.entity.TournamentPoolFight;


import com.curseblade.CursebladeApplication;


/** Tournament adapter database abstract class. <br/>
 * <b><i>This class will be overwrited whenever you regenerate the project<br/>
 * with Harmony.<br />
 * You should edit TournamentAdapter class instead of this<br/>
 * one or you will lose all your modifications.</i></b>
 */
public abstract class TournamentSQLiteAdapterBase
						extends SQLiteAdapterBase<Tournament> {

	/** TAG for debug purpose. */
	protected static final String TAG = "TournamentDBAdapter";

	/** Table name of SQLite database. */
	public static final String TABLE_NAME = "Tournament";

	/**
	 *  Columns constants fields mapping.
	 */
	/** id. */
	public static final String COL_ID =
			"id";
	/** Alias. */
	public static final String ALIASED_COL_ID =
			TABLE_NAME + "." + COL_ID;
	/** maxElements. */
	public static final String COL_MAXELEMENTS =
			"maxElements";
	/** Alias. */
	public static final String ALIASED_COL_MAXELEMENTS =
			TABLE_NAME + "." + COL_MAXELEMENTS;

	/** Global Fields. */
	public static final String[] COLS = new String[] {

		TournamentSQLiteAdapter.COL_ID,
		TournamentSQLiteAdapter.COL_MAXELEMENTS
	};

	/** Global Fields. */
	public static final String[] ALIASED_COLS = new String[] {

		TournamentSQLiteAdapter.ALIASED_COL_ID,
		TournamentSQLiteAdapter.ALIASED_COL_MAXELEMENTS
	};

	/**
	 * Get the table name used in DB for your Tournament entity.
	 * @return A String showing the table name
	 */
	public String getTableName() {
		return TABLE_NAME;
	}

	/**
	 * Get the joined table name used in DB for your Tournament entity
	 * and its parents.
	 * @return A String showing the joined table name
	 */
	public String getJoinedTableName() {
		String result = TABLE_NAME;
		return result;
	}

	/**
	 * Get the column names from the Tournament entity table.
	 * @return An array of String representing the columns
	 */
	public String[] getCols() {
		return ALIASED_COLS;
	}

	/**
	 * Generate Entity Table Schema.
	 * @return "SQL query : CREATE TABLE..."
	 */
	public static String getSchema() {
		return "CREATE TABLE "
		+ TABLE_NAME	+ " ("
		
		 + COL_ID	+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
		 + COL_MAXELEMENTS	+ " INTEGER NOT NULL DEFAULT '10'"
		
		
		+ ", UNIQUE(" + COL_ID + ")"
		+ ");"
;
	}
	/**
	 * Constructor.
	 * @param ctx context
	 */
	public TournamentSQLiteAdapterBase(final Context ctx) {
		super(ctx);
	}

	// Converters

	/**
	 * Convert Tournament entity to Content Values for database.
	 * @param item Tournament entity object
	 * @return ContentValues object
	 */
	public ContentValues itemToContentValues(final Tournament item) {
		final ContentValues result = new ContentValues();

		if (item.getId() != null) {
			result.put(COL_ID,
				String.valueOf(item.getId()));
		}

		if (item.getMaxElements() != null) {
			result.put(COL_MAXELEMENTS,
				String.valueOf(item.getMaxElements()));
		}


		return result;
	}

	/**
	 * Convert Cursor of database to Tournament entity.
	 * @param cursor Cursor object
	 * @return Tournament entity
	 */
	public Tournament cursorToItem(final Cursor cursor) {
		Tournament result = new Tournament();
		this.cursorToItem(cursor, result);
		return result;
	}

	/**
	 * Convert Cursor of database to Tournament entity.
	 * @param cursor Cursor object
	 * @param result Tournament entity
	 */
	public void cursorToItem(final Cursor cursor, final Tournament result) {
		if (cursor.getCount() != 0) {
			int index;

			index = cursor.getColumnIndexOrThrow(COL_ID);
			result.setId(
					cursor.getInt(index));

			index = cursor.getColumnIndexOrThrow(COL_MAXELEMENTS);
			result.setMaxElements(
					cursor.getInt(index));


		}
	}

	//// CRUD Entity ////
	/**
	 * Find & read Tournament by id in database.
	 *
	 * @param id Identify of Tournament
	 * @return Tournament entity
	 */
	public Tournament getByID(final Integer id) {
		final Cursor cursor = this.getSingleCursor(id);
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
		}

		final Tournament result = this.cursorToItem(cursor);
		cursor.close();

		final TournamentPoolFightSQLiteAdapter poolFightsAdapter =
				new TournamentPoolFightSQLiteAdapter(this.ctx);
		poolFightsAdapter.open(this.mDatabase);
		Cursor poolfightsCursor = poolFightsAdapter
					.getByTournamentpoolFightsInternal(result.getId(), TournamentPoolFightSQLiteAdapter.ALIASED_COLS, null, null, null);
		result.setPoolFights(poolFightsAdapter.cursorToItems(poolfightsCursor));
		return result;
	}


	/**
	 * Read All Tournaments entities.
	 *
	 * @return List of Tournament entities
	 */
	public ArrayList<Tournament> getAll() {
		final Cursor cursor = this.getAllCursor();
		final ArrayList<Tournament> result = this.cursorToItems(cursor);
		cursor.close();

		return result;
	}



	/**
	 * Insert a Tournament entity into database.
	 *
	 * @param item The Tournament entity to persist
	 * @return Id of the Tournament entity
	 */
	public long insert(final Tournament item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Insert DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		values.remove(TournamentSQLiteAdapter.COL_ID);
		int newid;
		if (values.size() != 0) {
			newid = (int) this.insert(
					null,
					values);
		} else {
			newid = (int) this.insert(
					TournamentSQLiteAdapter.COL_ID,
					values);
		}
		item.setId((int) newid);
		if (item.getPoolFights() != null) {
			TournamentPoolFightSQLiteAdapterBase poolFightsAdapter =
					new TournamentPoolFightSQLiteAdapter(this.ctx);
			poolFightsAdapter.open(this.mDatabase);
			for (TournamentPoolFight tournamentpoolfight
						: item.getPoolFights()) {
				poolFightsAdapter.insertOrUpdateWithTournamentPoolFights(
									tournamentpoolfight,
									newid);
			}
		}
		return newid;
	}

	/**
	 * Either insert or update a Tournament entity into database whether.
	 * it already exists or not.
	 *
	 * @param item The Tournament entity to persist
	 * @return 1 if everything went well, 0 otherwise
	 */
	public int insertOrUpdate(final Tournament item) {
		int result = 0;
		if (this.getByID(item.getId()) != null) {
			// Item already exists => update it
			result = this.update(item);
		} else {
			// Item doesn't exist => create it
			final long id = this.insert(item);
			if (id != 0) {
				result = 1;
			}
		}

		return result;
	}

	/**
	 * Update a Tournament entity into database.
	 *
	 * @param item The Tournament entity to persist
	 * @return count of updated entities
	 */
	public int update(final Tournament item) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Update DB(" + TABLE_NAME + ")");
		}

		final ContentValues values =
				this.itemToContentValues(item);
		final String whereClause =
				 TournamentSQLiteAdapter.COL_ID
				 + "=? ";
		final String[] whereArgs =
				new String[] {String.valueOf(item.getId()) };

		return this.update(
				values,
				whereClause,
				whereArgs);
	}


	/**
	 * Delete a Tournament entity of database.
	 *
	 * @param id id
	 * @return count of updated entities
	 */
	public int remove(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Delete DB(" + TABLE_NAME
					+ ") id : " + id);
		}

		
		final String whereClause =  TournamentSQLiteAdapter.COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.delete(
				whereClause,
				whereArgs);
	}

	/**
	 * Deletes the given entity.
	 * @param tournament The entity to delete
	 * @return count of updated entities
	 */
	public int delete(final Tournament tournament) {
		return this.delete(tournament.getId());
	}

	/**
	 *  Internal Cursor.
	 * @param id id
	 *  @return A Cursor pointing to the Tournament corresponding
	 *		to the given id.
	 */
	protected Cursor getSingleCursor(final Integer id) {
		if (CursebladeApplication.DEBUG) {
			Log.d(TAG, "Get entities id : " + id);
		}

		final String whereClause =  TournamentSQLiteAdapter.ALIASED_COL_ID
					 + "=? ";
		final String[] whereArgs = new String[] {String.valueOf(id) };

		return this.query(ALIASED_COLS,
				whereClause,
				whereArgs,
				null,
				null,
				null);
	}


	/**
	 * Query the DB to find a Tournament entity.
	 * @param id The id of the entity to get from the DB
	 * @return The cursor pointing to the query's result
	 */
	public Cursor query(final int id) {
		return this.query(
				ALIASED_COLS,
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)},
				null,
				null,
				null);
	}

	/**
	 * Deletes the given entity.
	 * @param id The ID of the entity to delete
	 * @return the number of token deleted
	 */
	public int delete(final int id) {
		return this.delete(
				ALIASED_COL_ID + " = ?",
				new String[]{String.valueOf(id)});
	}

}
