/**************************************************************************
 * FightSQLiteAdapter.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data;

import com.curseblade.data.base.FightSQLiteAdapterBase;
import android.content.Context;

/**
 * Fight adapter database class. 
 * This class will help you access your database to do any basic operation you
 * need. 
 * Feel free to modify it, override, add more methods etc.
 */
public class FightSQLiteAdapter extends FightSQLiteAdapterBase {

	/**
	 * Constructor.
	 * @param ctx context
	 */
	public FightSQLiteAdapter(final Context ctx) {
		super(ctx);
	}
}
