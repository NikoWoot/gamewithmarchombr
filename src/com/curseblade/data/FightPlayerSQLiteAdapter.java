/**************************************************************************
 * FightPlayerSQLiteAdapter.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data;

import com.curseblade.data.base.FightPlayerSQLiteAdapterBase;
import android.content.Context;

/**
 * FightPlayer adapter database class. 
 * This class will help you access your database to do any basic operation you
 * need. 
 * Feel free to modify it, override, add more methods etc.
 */
public class FightPlayerSQLiteAdapter extends FightPlayerSQLiteAdapterBase {

	/**
	 * Constructor.
	 * @param ctx context
	 */
	public FightPlayerSQLiteAdapter(final Context ctx) {
		super(ctx);
	}
}
