/**************************************************************************
 * CursebladeSQLiteOpenHelper.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.data;

import com.curseblade.data.base.CursebladeSQLiteOpenHelperBase;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

/**
 * This class makes it easy for ContentProvider implementations to defer <br />
 * opening and upgrading the database until first use, to avoid blocking <br />
 * application startup with long-running database upgrades.
 * @see android.database.sqlite.SQLiteOpenHelper
 */
public class CursebladeSQLiteOpenHelper
					extends CursebladeSQLiteOpenHelperBase {

	/**
	 * Constructor.
	 * @param ctx context
	 * @param name name
	 * @param factory factory
	 * @param version version
	 */
	public CursebladeSQLiteOpenHelper(final Context ctx,
		   final String name, final CursorFactory factory, final int version) {
		super(ctx, name, factory, version);
	}

}
