/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.job;

import com.curseblade.entity.*;

/**
 * Class for realising automaticly a fight between First and SecondTeam
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class FightAutomatic {

	/**
	 * Method to realised Fight Automaticly (fully useful for computer IA)
	 * 
	 * @param f
	 *            Fight to realised
	 * @throws Exception
	 *             Exception if fight is not available
	 */
	public static void launchAutomaticFight(Fight f) throws Exception {

		// Count by team
		int firstTeamCount = 0;
		int secondTeamCount = 0;

		// Prepare the fight
		f.prepare();

		// It's a death fight !
		do {
			// Start turn
			f.startTurn();

			// SenderPlayer attack ReceiverPlayer with her weapon
			f.attack(f.getSenderPlayer(), f.getReceiverPlayer(), f
					.getSenderPlayer().getBaseWeapon());

			// End Turn
			f.endTurn();

			// Actualize the teamCount
			firstTeamCount = f.countTeam(1, f.getAlternatedFighters());
			secondTeamCount = f.countTeam(2, f.getAlternatedFighters());

		} while (firstTeamCount > 0 && secondTeamCount > 0);

		// End of fight
		f.endFight();

	}
}
