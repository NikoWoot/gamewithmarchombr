/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.job;

import java.util.Random;

import com.curseblade.entity.Fight;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.Spell;

/**
 * FightSimpleIA is a class to manage an computerCharacterPlayer during a Fight
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class FightSimpleIA {

	private Fight fight;
	private FightPlayer computerCharacter;
	private Random random;

	/**
	 * Default constructor
	 * @param fight actualy fight
	 * @param computerCharacter Bot controled by IA
	 */
	public FightSimpleIA(Fight fight, FightPlayer computerCharacter) {
		this.fight = fight;
		this.computerCharacter = computerCharacter;
		this.random = new Random();
	}

	/**
	 * Method to execute turn actions during a fight
	 * 
	 * @throws Exception
	 *             Transmit exception on invocator
	 */
	public void playTurn() throws Exception {

		if (this.fight.getSenderPlayer() == computerCharacter) {

			if (this.random.nextInt(100) > 50) {
				
				// Random spell
				int randomPos = this.random.nextInt(this.computerCharacter
						.getBaseSpells().size() - 1);
				
				// Get this spell
				Spell spell = this.computerCharacter.getBaseSpells().get(
						randomPos);
				
				// launch spell
				this.fight.attack(this.computerCharacter,
						this.fight.getReceiverPlayer(), spell);

			} else {

				// use weapon
				this.fight.attack(this.computerCharacter,
						this.fight.getReceiverPlayer(),
						this.computerCharacter.getBaseWeapon());
			}
		}

	}
}
