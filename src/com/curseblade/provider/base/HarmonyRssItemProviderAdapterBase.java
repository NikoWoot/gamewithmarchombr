/**************************************************************************
 * HarmonyRssItemProviderAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.base;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.google.common.base.Strings;

import com.curseblade.entity.HarmonyRssItem;
import com.curseblade.provider.CursebladeProvider;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;

/**
 * HarmonyRssItemProviderAdapterBase.
 */
public abstract class HarmonyRssItemProviderAdapterBase
				extends ProviderAdapterBase<HarmonyRssItem> {

	/** TAG for debug purpose. */
	protected static final String TAG = "HarmonyRssItemProviderAdapter";

	/** HARMONYRSSITEM_URI. */
	public	  static Uri HARMONYRSSITEM_URI;

	/** harmonyRssItem type. */
	protected static final String harmonyRssItemType =
			"harmonyrssitem";

	/** HARMONYRSSITEM_ALL. */
	protected static final int HARMONYRSSITEM_ALL =
			799254719;
	/** HARMONYRSSITEM_ONE. */
	protected static final int HARMONYRSSITEM_ONE =
			799254720;


	/**
	 * Static constructor.
	 */
	static {
		HARMONYRSSITEM_URI =
				CursebladeProvider.generateUri(
						harmonyRssItemType);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				harmonyRssItemType,
				HARMONYRSSITEM_ALL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				harmonyRssItemType + "/#",
				HARMONYRSSITEM_ONE);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public HarmonyRssItemProviderAdapterBase(
				final Context ctx,
				final SQLiteDatabase db) {
		super(ctx);
		this.adapter = new HarmonyRssItemSQLiteAdapter(ctx);
		if (db != null) {
			this.db = this.adapter.open(db);
		} else {
			this.db = this.adapter.open();
		}

		this.uriIds.add(HARMONYRSSITEM_ALL);
		this.uriIds.add(HARMONYRSSITEM_ONE);
	}

	@Override
	public String getType(final Uri uri) {
		String result;
		final String single =
				"vnc.android.cursor.item/"
					+ CursebladeProvider.authority + ".";
		final String collection =
				"vnc.android.cursor.collection/"
					+ CursebladeProvider.authority + ".";

		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);

		switch (matchedUri) {
			case HARMONYRSSITEM_ALL:
				result = collection + "harmonyrssitem";
				break;
			case HARMONYRSSITEM_ONE:
				result = single + "harmonyrssitem";
				break;
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int delete(
			final Uri uri,
			String selection,
			String[] selectionArgs) {
		int matchedUri = CursebladeProviderBase
					.getUriMatcher().match(uri);
		int result = -1;
		switch (matchedUri) {
			case HARMONYRSSITEM_ONE:
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				selection = HarmonyRssItemSQLiteAdapter.COL_ID
						+ " = ?";
				selectionArgs = new String[1];
				selectionArgs[0] = String.valueOf(id);
				result = this.adapter.delete(
						selection,
						selectionArgs);
				break;
			case HARMONYRSSITEM_ALL:
				result = this.adapter.delete(
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}
	
	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);
		
		Uri result = null;
		int id = 0;
		switch (matchedUri) {
			case HARMONYRSSITEM_ALL:
				if (values.size() > 0) {
					id = (int) this.adapter.insert(null, values);
				} else {
					id = (int) this.adapter.insert(HarmonyRssItemSQLiteAdapter.COL_ID, values);
				}
				if (id > 0) {
					result = ContentUris.withAppendedId(
							HARMONYRSSITEM_URI,
							id);
				}
				break;
			default:
				result = null;
				break;
		}
		return result;
	}

	@Override
	public Cursor query(final Uri uri,
						String[] projection,
						String selection,
						String[] selectionArgs,
						final String sortOrder) {

		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		Cursor result = null;
		

		switch (matchedUri) {

			case HARMONYRSSITEM_ALL:
				result = this.adapter.query(
							projection,
							selection,
							selectionArgs,
							null,
							null,
							sortOrder);
				break;
			case HARMONYRSSITEM_ONE:
				result = this.queryById(uri.getPathSegments().get(1));
				break;
			
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues values,
			String selection,
			String[] selectionArgs) {
		
		
		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		int result = -1;
		switch (matchedUri) {
			case HARMONYRSSITEM_ONE:
				String id = uri.getPathSegments().get(1);
				result = this.adapter.update(
						values,
						HarmonyRssItemSQLiteAdapter.COL_ID + " = "
						+ id,
						selectionArgs);
				break;
			case HARMONYRSSITEM_ALL:
				result = this.adapter.update(
							values,
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}



	/**
	 * Get the entity URI.
	 * @return The URI
	 */
	@Override
	public Uri getUri() {
		return HARMONYRSSITEM_URI;
	}

	/**
	 * Query by ID.
	 *
	 * @param id The id of the entity to retrieve
	 * @return The cursor
	 */
	private Cursor queryById(String id) {
		Cursor result = null;
		String selection = HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID
						+ " = ?";

		String[] selectionArgs = new String[]{id};

		result = this.adapter.query(
					HarmonyRssItemSQLiteAdapter.ALIASED_COLS,
					selection,
					selectionArgs,
					null,
					null,
					null);
		return result;
	}
}

