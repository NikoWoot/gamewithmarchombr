/**************************************************************************
 * TournamentPoolFightProviderAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.base;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.google.common.base.Strings;

import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.provider.CursebladeProvider;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;

/**
 * TournamentPoolFightProviderAdapterBase.
 */
public abstract class TournamentPoolFightProviderAdapterBase
				extends ProviderAdapterBase<TournamentPoolFight> {

	/** TAG for debug purpose. */
	protected static final String TAG = "TournamentPoolFightProviderAdapter";

	/** TOURNAMENTPOOLFIGHT_URI. */
	public	  static Uri TOURNAMENTPOOLFIGHT_URI;

	/** tournamentPoolFight type. */
	protected static final String tournamentPoolFightType =
			"tournamentpoolfight";

	/** TOURNAMENTPOOLFIGHT_ALL. */
	protected static final int TOURNAMENTPOOLFIGHT_ALL =
			2018675381;
	/** TOURNAMENTPOOLFIGHT_ONE. */
	protected static final int TOURNAMENTPOOLFIGHT_ONE =
			2018675382;

	/** TOURNAMENTPOOLFIGHT_WINNER. */
	protected static final int TOURNAMENTPOOLFIGHT_WINNER =
			2018675383;
	/** TOURNAMENTPOOLFIGHT_PARENTPOOL. */
	protected static final int TOURNAMENTPOOLFIGHT_PARENTPOOL =
			2018675384;
	/** TOURNAMENTPOOLFIGHT_LEFTPOOL. */
	protected static final int TOURNAMENTPOOLFIGHT_LEFTPOOL =
			2018675385;
	/** TOURNAMENTPOOLFIGHT_RIGHTPOOL. */
	protected static final int TOURNAMENTPOOLFIGHT_RIGHTPOOL =
			2018675386;

	/**
	 * Static constructor.
	 */
	static {
		TOURNAMENTPOOLFIGHT_URI =
				CursebladeProvider.generateUri(
						tournamentPoolFightType);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				tournamentPoolFightType,
				TOURNAMENTPOOLFIGHT_ALL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				tournamentPoolFightType + "/#",
				TOURNAMENTPOOLFIGHT_ONE);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				tournamentPoolFightType + "/#/winner",
				TOURNAMENTPOOLFIGHT_WINNER);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				tournamentPoolFightType + "/#/parentpool",
				TOURNAMENTPOOLFIGHT_PARENTPOOL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				tournamentPoolFightType + "/#/leftpool",
				TOURNAMENTPOOLFIGHT_LEFTPOOL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				tournamentPoolFightType + "/#/rightpool",
				TOURNAMENTPOOLFIGHT_RIGHTPOOL);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public TournamentPoolFightProviderAdapterBase(
				final Context ctx,
				final SQLiteDatabase db) {
		super(ctx);
		this.adapter = new TournamentPoolFightSQLiteAdapter(ctx);
		if (db != null) {
			this.db = this.adapter.open(db);
		} else {
			this.db = this.adapter.open();
		}

		this.uriIds.add(TOURNAMENTPOOLFIGHT_ALL);
		this.uriIds.add(TOURNAMENTPOOLFIGHT_ONE);
		this.uriIds.add(TOURNAMENTPOOLFIGHT_WINNER);
		this.uriIds.add(TOURNAMENTPOOLFIGHT_PARENTPOOL);
		this.uriIds.add(TOURNAMENTPOOLFIGHT_LEFTPOOL);
		this.uriIds.add(TOURNAMENTPOOLFIGHT_RIGHTPOOL);
	}

	@Override
	public String getType(final Uri uri) {
		String result;
		final String single =
				"vnc.android.cursor.item/"
					+ CursebladeProvider.authority + ".";
		final String collection =
				"vnc.android.cursor.collection/"
					+ CursebladeProvider.authority + ".";

		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);

		switch (matchedUri) {
			case TOURNAMENTPOOLFIGHT_ALL:
				result = collection + "tournamentpoolfight";
				break;
			case TOURNAMENTPOOLFIGHT_ONE:
				result = single + "tournamentpoolfight";
				break;
			case TOURNAMENTPOOLFIGHT_WINNER:
				result = single + "tournamentpoolfight";
				break;
			case TOURNAMENTPOOLFIGHT_PARENTPOOL:
				result = single + "tournamentpoolfight";
				break;
			case TOURNAMENTPOOLFIGHT_LEFTPOOL:
				result = single + "tournamentpoolfight";
				break;
			case TOURNAMENTPOOLFIGHT_RIGHTPOOL:
				result = single + "tournamentpoolfight";
				break;
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int delete(
			final Uri uri,
			String selection,
			String[] selectionArgs) {
		int matchedUri = CursebladeProviderBase
					.getUriMatcher().match(uri);
		int result = -1;
		switch (matchedUri) {
			case TOURNAMENTPOOLFIGHT_ONE:
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				selection = TournamentPoolFightSQLiteAdapter.COL_ID
						+ " = ?";
				selectionArgs = new String[1];
				selectionArgs[0] = String.valueOf(id);
				result = this.adapter.delete(
						selection,
						selectionArgs);
				break;
			case TOURNAMENTPOOLFIGHT_ALL:
				result = this.adapter.delete(
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}
	
	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);
		
		Uri result = null;
		int id = 0;
		switch (matchedUri) {
			case TOURNAMENTPOOLFIGHT_ALL:
				if (values.size() > 0) {
					id = (int) this.adapter.insert(null, values);
				} else {
					id = (int) this.adapter.insert(TournamentPoolFightSQLiteAdapter.COL_ID, values);
				}
				if (id > 0) {
					result = ContentUris.withAppendedId(
							TOURNAMENTPOOLFIGHT_URI,
							id);
				}
				break;
			default:
				result = null;
				break;
		}
		return result;
	}

	@Override
	public Cursor query(final Uri uri,
						String[] projection,
						String selection,
						String[] selectionArgs,
						final String sortOrder) {

		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		Cursor result = null;
		Cursor tournamentPoolFightCursor;
		

		switch (matchedUri) {

			case TOURNAMENTPOOLFIGHT_ALL:
				result = this.adapter.query(
							projection,
							selection,
							selectionArgs,
							null,
							null,
							sortOrder);
				break;
			case TOURNAMENTPOOLFIGHT_ONE:
				result = this.queryById(uri.getPathSegments().get(1));
				break;
			
			case TOURNAMENTPOOLFIGHT_WINNER:
				tournamentPoolFightCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (tournamentPoolFightCursor.getCount() > 0) {
					tournamentPoolFightCursor.moveToFirst();
					int winnerId = tournamentPoolFightCursor.getInt(tournamentPoolFightCursor.getColumnIndex(
									TournamentPoolFightSQLiteAdapter.COL_WINNER));
					
					FightPlayerSQLiteAdapter fightPlayerAdapter = new FightPlayerSQLiteAdapter(this.ctx);
					fightPlayerAdapter.open(this.getDb());
					result = fightPlayerAdapter.query(winnerId);
				}
				break;

			case TOURNAMENTPOOLFIGHT_PARENTPOOL:
				tournamentPoolFightCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (tournamentPoolFightCursor.getCount() > 0) {
					tournamentPoolFightCursor.moveToFirst();
					int parentPoolId = tournamentPoolFightCursor.getInt(tournamentPoolFightCursor.getColumnIndex(
									TournamentPoolFightSQLiteAdapter.COL_PARENTPOOL));
					
					TournamentPoolFightSQLiteAdapter tournamentPoolFightAdapter = new TournamentPoolFightSQLiteAdapter(this.ctx);
					tournamentPoolFightAdapter.open(this.getDb());
					result = tournamentPoolFightAdapter.query(parentPoolId);
				}
				break;

			case TOURNAMENTPOOLFIGHT_LEFTPOOL:
				tournamentPoolFightCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (tournamentPoolFightCursor.getCount() > 0) {
					tournamentPoolFightCursor.moveToFirst();
					int leftPoolId = tournamentPoolFightCursor.getInt(tournamentPoolFightCursor.getColumnIndex(
									TournamentPoolFightSQLiteAdapter.COL_LEFTPOOL));
					
					TournamentPoolFightSQLiteAdapter tournamentPoolFightAdapter = new TournamentPoolFightSQLiteAdapter(this.ctx);
					tournamentPoolFightAdapter.open(this.getDb());
					result = tournamentPoolFightAdapter.query(leftPoolId);
				}
				break;

			case TOURNAMENTPOOLFIGHT_RIGHTPOOL:
				tournamentPoolFightCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (tournamentPoolFightCursor.getCount() > 0) {
					tournamentPoolFightCursor.moveToFirst();
					int rightPoolId = tournamentPoolFightCursor.getInt(tournamentPoolFightCursor.getColumnIndex(
									TournamentPoolFightSQLiteAdapter.COL_RIGHTPOOL));
					
					TournamentPoolFightSQLiteAdapter tournamentPoolFightAdapter = new TournamentPoolFightSQLiteAdapter(this.ctx);
					tournamentPoolFightAdapter.open(this.getDb());
					result = tournamentPoolFightAdapter.query(rightPoolId);
				}
				break;

			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues values,
			String selection,
			String[] selectionArgs) {
		
		
		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		int result = -1;
		switch (matchedUri) {
			case TOURNAMENTPOOLFIGHT_ONE:
				String id = uri.getPathSegments().get(1);
				result = this.adapter.update(
						values,
						TournamentPoolFightSQLiteAdapter.COL_ID + " = "
						+ id,
						selectionArgs);
				break;
			case TOURNAMENTPOOLFIGHT_ALL:
				result = this.adapter.update(
							values,
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}



	/**
	 * Get the entity URI.
	 * @return The URI
	 */
	@Override
	public Uri getUri() {
		return TOURNAMENTPOOLFIGHT_URI;
	}

	/**
	 * Query by ID.
	 *
	 * @param id The id of the entity to retrieve
	 * @return The cursor
	 */
	private Cursor queryById(String id) {
		Cursor result = null;
		String selection = TournamentPoolFightSQLiteAdapter.ALIASED_COL_ID
						+ " = ?";

		String[] selectionArgs = new String[]{id};

		result = this.adapter.query(
					TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
					selection,
					selectionArgs,
					null,
					null,
					null);
		return result;
	}
}

