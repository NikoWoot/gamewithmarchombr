/**************************************************************************
 * CharacterPlayerProviderAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.base;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.google.common.base.Strings;

import com.curseblade.entity.CharacterPlayer;
import com.curseblade.provider.CursebladeProvider;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.ItemArmurySQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.data.ItemWeaponSQLiteAdapter;

/**
 * CharacterPlayerProviderAdapterBase.
 */
public abstract class CharacterPlayerProviderAdapterBase
				extends ProviderAdapterBase<CharacterPlayer> {

	/** TAG for debug purpose. */
	protected static final String TAG = "CharacterPlayerProviderAdapter";

	/** CHARACTERPLAYER_URI. */
	public	  static Uri CHARACTERPLAYER_URI;

	/** characterPlayer type. */
	protected static final String characterPlayerType =
			"characterplayer";

	/** CHARACTERPLAYER_ALL. */
	protected static final int CHARACTERPLAYER_ALL =
			1046034250;
	/** CHARACTERPLAYER_ONE. */
	protected static final int CHARACTERPLAYER_ONE =
			1046034251;

	/** CHARACTERPLAYER_ARMURYEQUIPEDITEMS. */
	protected static final int CHARACTERPLAYER_ARMURYEQUIPEDITEMS =
			1046034252;
	/** CHARACTERPLAYER_EQUIPEDSPELLS. */
	protected static final int CHARACTERPLAYER_EQUIPEDSPELLS =
			1046034253;
	/** CHARACTERPLAYER_WEAPONUSED. */
	protected static final int CHARACTERPLAYER_WEAPONUSED =
			1046034254;

	/**
	 * Static constructor.
	 */
	static {
		CHARACTERPLAYER_URI =
				CursebladeProvider.generateUri(
						characterPlayerType);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				characterPlayerType,
				CHARACTERPLAYER_ALL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				characterPlayerType + "/#",
				CHARACTERPLAYER_ONE);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				characterPlayerType + "/#/armuryequipeditems",
				CHARACTERPLAYER_ARMURYEQUIPEDITEMS);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				characterPlayerType + "/#/equipedspells",
				CHARACTERPLAYER_EQUIPEDSPELLS);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				characterPlayerType + "/#/weaponused",
				CHARACTERPLAYER_WEAPONUSED);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public CharacterPlayerProviderAdapterBase(
				final Context ctx,
				final SQLiteDatabase db) {
		super(ctx);
		this.adapter = new CharacterPlayerSQLiteAdapter(ctx);
		if (db != null) {
			this.db = this.adapter.open(db);
		} else {
			this.db = this.adapter.open();
		}

		this.uriIds.add(CHARACTERPLAYER_ALL);
		this.uriIds.add(CHARACTERPLAYER_ONE);
		this.uriIds.add(CHARACTERPLAYER_ARMURYEQUIPEDITEMS);
		this.uriIds.add(CHARACTERPLAYER_EQUIPEDSPELLS);
		this.uriIds.add(CHARACTERPLAYER_WEAPONUSED);
	}

	@Override
	public String getType(final Uri uri) {
		String result;
		final String single =
				"vnc.android.cursor.item/"
					+ CursebladeProvider.authority + ".";
		final String collection =
				"vnc.android.cursor.collection/"
					+ CursebladeProvider.authority + ".";

		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);

		switch (matchedUri) {
			case CHARACTERPLAYER_ALL:
				result = collection + "characterplayer";
				break;
			case CHARACTERPLAYER_ONE:
				result = single + "characterplayer";
				break;
			case CHARACTERPLAYER_ARMURYEQUIPEDITEMS:
				result = collection + "characterplayer";
				break;
			case CHARACTERPLAYER_EQUIPEDSPELLS:
				result = collection + "characterplayer";
				break;
			case CHARACTERPLAYER_WEAPONUSED:
				result = single + "characterplayer";
				break;
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int delete(
			final Uri uri,
			String selection,
			String[] selectionArgs) {
		int matchedUri = CursebladeProviderBase
					.getUriMatcher().match(uri);
		int result = -1;
		switch (matchedUri) {
			case CHARACTERPLAYER_ONE:
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				selection = CharacterPlayerSQLiteAdapter.COL_ID
						+ " = ?";
				selectionArgs = new String[1];
				selectionArgs[0] = String.valueOf(id);
				result = this.adapter.delete(
						selection,
						selectionArgs);
				break;
			case CHARACTERPLAYER_ALL:
				result = this.adapter.delete(
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}
	
	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);
		
		Uri result = null;
		int id = 0;
		switch (matchedUri) {
			case CHARACTERPLAYER_ALL:
				if (values.size() > 0) {
					id = (int) this.adapter.insert(null, values);
				} else {
					id = (int) this.adapter.insert(CharacterPlayerSQLiteAdapter.COL_ID, values);
				}
				if (id > 0) {
					result = ContentUris.withAppendedId(
							CHARACTERPLAYER_URI,
							id);
				}
				break;
			default:
				result = null;
				break;
		}
		return result;
	}

	@Override
	public Cursor query(final Uri uri,
						String[] projection,
						String selection,
						String[] selectionArgs,
						final String sortOrder) {

		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		Cursor result = null;
		Cursor characterPlayerCursor;
		int id = 0;

		switch (matchedUri) {

			case CHARACTERPLAYER_ALL:
				result = this.adapter.query(
							projection,
							selection,
							selectionArgs,
							null,
							null,
							sortOrder);
				break;
			case CHARACTERPLAYER_ONE:
				result = this.queryById(uri.getPathSegments().get(1));
				break;
			
			case CHARACTERPLAYER_ARMURYEQUIPEDITEMS:
				id = Integer.parseInt(uri.getPathSegments().get(1));
				ItemArmurySQLiteAdapter armuryEquipedItemsAdapter = new ItemArmurySQLiteAdapter(this.ctx);
				armuryEquipedItemsAdapter.open(this.getDb());
				result = armuryEquipedItemsAdapter.getByCharacterPlayerarmuryEquipedItemsInternal(id, ItemArmurySQLiteAdapter.ALIASED_COLS, selection, selectionArgs, null);
				break;

			case CHARACTERPLAYER_EQUIPEDSPELLS:
				id = Integer.parseInt(uri.getPathSegments().get(1));
				SpellSQLiteAdapter equipedSpellsAdapter = new SpellSQLiteAdapter(this.ctx);
				equipedSpellsAdapter.open(this.getDb());
				result = equipedSpellsAdapter.getByCharacterPlayerequipedSpellsInternal(id, SpellSQLiteAdapter.ALIASED_COLS, selection, selectionArgs, null);
				break;

			case CHARACTERPLAYER_WEAPONUSED:
				characterPlayerCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (characterPlayerCursor.getCount() > 0) {
					characterPlayerCursor.moveToFirst();
					int weaponUsedId = characterPlayerCursor.getInt(characterPlayerCursor.getColumnIndex(
									CharacterPlayerSQLiteAdapter.COL_WEAPONUSED));
					
					ItemWeaponSQLiteAdapter itemWeaponAdapter = new ItemWeaponSQLiteAdapter(this.ctx);
					itemWeaponAdapter.open(this.getDb());
					result = itemWeaponAdapter.query(weaponUsedId);
				}
				break;

			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues values,
			String selection,
			String[] selectionArgs) {
		
		
		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		int result = -1;
		switch (matchedUri) {
			case CHARACTERPLAYER_ONE:
				String id = uri.getPathSegments().get(1);
				result = this.adapter.update(
						values,
						CharacterPlayerSQLiteAdapter.COL_ID + " = "
						+ id,
						selectionArgs);
				break;
			case CHARACTERPLAYER_ALL:
				result = this.adapter.update(
							values,
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}



	/**
	 * Get the entity URI.
	 * @return The URI
	 */
	@Override
	public Uri getUri() {
		return CHARACTERPLAYER_URI;
	}

	/**
	 * Query by ID.
	 *
	 * @param id The id of the entity to retrieve
	 * @return The cursor
	 */
	private Cursor queryById(String id) {
		Cursor result = null;
		String selection = CharacterPlayerSQLiteAdapter.ALIASED_COL_ID
						+ " = ?";

		String[] selectionArgs = new String[]{id};

		result = this.adapter.query(
					CharacterPlayerSQLiteAdapter.ALIASED_COLS,
					selection,
					selectionArgs,
					null,
					null,
					null);
		return result;
	}
}

