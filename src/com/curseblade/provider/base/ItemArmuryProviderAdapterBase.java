/**************************************************************************
 * ItemArmuryProviderAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.base;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.google.common.base.Strings;

import com.curseblade.entity.ItemArmury;
import com.curseblade.provider.CursebladeProvider;
import com.curseblade.data.ItemArmurySQLiteAdapter;

/**
 * ItemArmuryProviderAdapterBase.
 */
public abstract class ItemArmuryProviderAdapterBase
				extends ProviderAdapterBase<ItemArmury> {

	/** TAG for debug purpose. */
	protected static final String TAG = "ItemArmuryProviderAdapter";

	/** ITEMARMURY_URI. */
	public	  static Uri ITEMARMURY_URI;

	/** itemArmury type. */
	protected static final String itemArmuryType =
			"itemarmury";

	/** ITEMARMURY_ALL. */
	protected static final int ITEMARMURY_ALL =
			818526541;
	/** ITEMARMURY_ONE. */
	protected static final int ITEMARMURY_ONE =
			818526542;


	/**
	 * Static constructor.
	 */
	static {
		ITEMARMURY_URI =
				CursebladeProvider.generateUri(
						itemArmuryType);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				itemArmuryType,
				ITEMARMURY_ALL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				itemArmuryType + "/#",
				ITEMARMURY_ONE);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public ItemArmuryProviderAdapterBase(
				final Context ctx,
				final SQLiteDatabase db) {
		super(ctx);
		this.adapter = new ItemArmurySQLiteAdapter(ctx);
		if (db != null) {
			this.db = this.adapter.open(db);
		} else {
			this.db = this.adapter.open();
		}

		this.uriIds.add(ITEMARMURY_ALL);
		this.uriIds.add(ITEMARMURY_ONE);
	}

	@Override
	public String getType(final Uri uri) {
		String result;
		final String single =
				"vnc.android.cursor.item/"
					+ CursebladeProvider.authority + ".";
		final String collection =
				"vnc.android.cursor.collection/"
					+ CursebladeProvider.authority + ".";

		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);

		switch (matchedUri) {
			case ITEMARMURY_ALL:
				result = collection + "itemarmury";
				break;
			case ITEMARMURY_ONE:
				result = single + "itemarmury";
				break;
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int delete(
			final Uri uri,
			String selection,
			String[] selectionArgs) {
		int matchedUri = CursebladeProviderBase
					.getUriMatcher().match(uri);
		int result = -1;
		switch (matchedUri) {
			case ITEMARMURY_ONE:
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				selection = ItemArmurySQLiteAdapter.COL_ID
						+ " = ?";
				selectionArgs = new String[1];
				selectionArgs[0] = String.valueOf(id);
				result = this.adapter.delete(
						selection,
						selectionArgs);
				break;
			case ITEMARMURY_ALL:
				result = this.adapter.delete(
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}
	
	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);
		
		Uri result = null;
		int id = 0;
		switch (matchedUri) {
			case ITEMARMURY_ALL:
				if (values.size() > 0) {
					id = (int) this.adapter.insert(null, values);
				} else {
					id = (int) this.adapter.insert(ItemArmurySQLiteAdapter.COL_ID, values);
				}
				if (id > 0) {
					result = ContentUris.withAppendedId(
							ITEMARMURY_URI,
							id);
				}
				break;
			default:
				result = null;
				break;
		}
		return result;
	}

	@Override
	public Cursor query(final Uri uri,
						String[] projection,
						String selection,
						String[] selectionArgs,
						final String sortOrder) {

		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		Cursor result = null;
		Cursor itemArmuryCursor;
		

		switch (matchedUri) {

			case ITEMARMURY_ALL:
				result = this.adapter.query(
							projection,
							selection,
							selectionArgs,
							null,
							null,
							sortOrder);
				break;
			case ITEMARMURY_ONE:
				result = this.queryById(uri.getPathSegments().get(1));
				break;
			
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues values,
			String selection,
			String[] selectionArgs) {
		
		
		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		int result = -1;
		switch (matchedUri) {
			case ITEMARMURY_ONE:
				String id = uri.getPathSegments().get(1);
				result = this.adapter.update(
						values,
						ItemArmurySQLiteAdapter.COL_ID + " = "
						+ id,
						selectionArgs);
				break;
			case ITEMARMURY_ALL:
				result = this.adapter.update(
							values,
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}



	/**
	 * Get the entity URI.
	 * @return The URI
	 */
	@Override
	public Uri getUri() {
		return ITEMARMURY_URI;
	}

	/**
	 * Query by ID.
	 *
	 * @param id The id of the entity to retrieve
	 * @return The cursor
	 */
	private Cursor queryById(String id) {
		Cursor result = null;
		String selection = ItemArmurySQLiteAdapter.ALIASED_COL_ID
						+ " = ?";

		String[] selectionArgs = new String[]{id};

		result = this.adapter.query(
					ItemArmurySQLiteAdapter.ALIASED_COLS,
					selection,
					selectionArgs,
					null,
					null,
					null);
		return result;
	}
}

