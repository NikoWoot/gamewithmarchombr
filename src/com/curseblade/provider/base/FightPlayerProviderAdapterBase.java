/**************************************************************************
 * FightPlayerProviderAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.base;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.google.common.base.Strings;

import com.curseblade.entity.FightPlayer;
import com.curseblade.provider.CursebladeProvider;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;

/**
 * FightPlayerProviderAdapterBase.
 */
public abstract class FightPlayerProviderAdapterBase
				extends ProviderAdapterBase<FightPlayer> {

	/** TAG for debug purpose. */
	protected static final String TAG = "FightPlayerProviderAdapter";

	/** FIGHTPLAYER_URI. */
	public	  static Uri FIGHTPLAYER_URI;

	/** fightPlayer type. */
	protected static final String fightPlayerType =
			"fightplayer";

	/** FIGHTPLAYER_ALL. */
	protected static final int FIGHTPLAYER_ALL =
			1252100849;
	/** FIGHTPLAYER_ONE. */
	protected static final int FIGHTPLAYER_ONE =
			1252100850;

	/** FIGHTPLAYER_FIGHT. */
	protected static final int FIGHTPLAYER_FIGHT =
			1252100851;
	/** FIGHTPLAYER_BASECHARACTER. */
	protected static final int FIGHTPLAYER_BASECHARACTER =
			1252100852;
	/** FIGHTPLAYER_BASEWEAPON. */
	protected static final int FIGHTPLAYER_BASEWEAPON =
			1252100853;
	/** FIGHTPLAYER_BASESPELLS. */
	protected static final int FIGHTPLAYER_BASESPELLS =
			1252100854;

	/**
	 * Static constructor.
	 */
	static {
		FIGHTPLAYER_URI =
				CursebladeProvider.generateUri(
						fightPlayerType);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightPlayerType,
				FIGHTPLAYER_ALL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightPlayerType + "/#",
				FIGHTPLAYER_ONE);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightPlayerType + "/#/fight",
				FIGHTPLAYER_FIGHT);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightPlayerType + "/#/basecharacter",
				FIGHTPLAYER_BASECHARACTER);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightPlayerType + "/#/baseweapon",
				FIGHTPLAYER_BASEWEAPON);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightPlayerType + "/#/basespells",
				FIGHTPLAYER_BASESPELLS);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public FightPlayerProviderAdapterBase(
				final Context ctx,
				final SQLiteDatabase db) {
		super(ctx);
		this.adapter = new FightPlayerSQLiteAdapter(ctx);
		if (db != null) {
			this.db = this.adapter.open(db);
		} else {
			this.db = this.adapter.open();
		}

		this.uriIds.add(FIGHTPLAYER_ALL);
		this.uriIds.add(FIGHTPLAYER_ONE);
		this.uriIds.add(FIGHTPLAYER_FIGHT);
		this.uriIds.add(FIGHTPLAYER_BASECHARACTER);
		this.uriIds.add(FIGHTPLAYER_BASEWEAPON);
		this.uriIds.add(FIGHTPLAYER_BASESPELLS);
	}

	@Override
	public String getType(final Uri uri) {
		String result;
		final String single =
				"vnc.android.cursor.item/"
					+ CursebladeProvider.authority + ".";
		final String collection =
				"vnc.android.cursor.collection/"
					+ CursebladeProvider.authority + ".";

		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);

		switch (matchedUri) {
			case FIGHTPLAYER_ALL:
				result = collection + "fightplayer";
				break;
			case FIGHTPLAYER_ONE:
				result = single + "fightplayer";
				break;
			case FIGHTPLAYER_FIGHT:
				result = single + "fightplayer";
				break;
			case FIGHTPLAYER_BASECHARACTER:
				result = single + "fightplayer";
				break;
			case FIGHTPLAYER_BASEWEAPON:
				result = single + "fightplayer";
				break;
			case FIGHTPLAYER_BASESPELLS:
				result = collection + "fightplayer";
				break;
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int delete(
			final Uri uri,
			String selection,
			String[] selectionArgs) {
		int matchedUri = CursebladeProviderBase
					.getUriMatcher().match(uri);
		int result = -1;
		switch (matchedUri) {
			case FIGHTPLAYER_ONE:
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				selection = FightPlayerSQLiteAdapter.COL_ID
						+ " = ?";
				selectionArgs = new String[1];
				selectionArgs[0] = String.valueOf(id);
				result = this.adapter.delete(
						selection,
						selectionArgs);
				break;
			case FIGHTPLAYER_ALL:
				result = this.adapter.delete(
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}
	
	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);
		
		Uri result = null;
		int id = 0;
		switch (matchedUri) {
			case FIGHTPLAYER_ALL:
				if (values.size() > 0) {
					id = (int) this.adapter.insert(null, values);
				} else {
					id = (int) this.adapter.insert(FightPlayerSQLiteAdapter.COL_ID, values);
				}
				if (id > 0) {
					result = ContentUris.withAppendedId(
							FIGHTPLAYER_URI,
							id);
				}
				break;
			default:
				result = null;
				break;
		}
		return result;
	}

	@Override
	public Cursor query(final Uri uri,
						String[] projection,
						String selection,
						String[] selectionArgs,
						final String sortOrder) {

		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		Cursor result = null;
		Cursor fightPlayerCursor;
		int id = 0;

		switch (matchedUri) {

			case FIGHTPLAYER_ALL:
				result = this.adapter.query(
							projection,
							selection,
							selectionArgs,
							null,
							null,
							sortOrder);
				break;
			case FIGHTPLAYER_ONE:
				result = this.queryById(uri.getPathSegments().get(1));
				break;
			
			case FIGHTPLAYER_FIGHT:
				fightPlayerCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (fightPlayerCursor.getCount() > 0) {
					fightPlayerCursor.moveToFirst();
					int fightId = fightPlayerCursor.getInt(fightPlayerCursor.getColumnIndex(
									FightPlayerSQLiteAdapter.COL_FIGHT));
					
					FightSQLiteAdapter fightAdapter = new FightSQLiteAdapter(this.ctx);
					fightAdapter.open(this.getDb());
					result = fightAdapter.query(fightId);
				}
				break;

			case FIGHTPLAYER_BASECHARACTER:
				fightPlayerCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (fightPlayerCursor.getCount() > 0) {
					fightPlayerCursor.moveToFirst();
					int baseCharacterId = fightPlayerCursor.getInt(fightPlayerCursor.getColumnIndex(
									FightPlayerSQLiteAdapter.COL_BASECHARACTER));
					
					CharacterPlayerSQLiteAdapter characterPlayerAdapter = new CharacterPlayerSQLiteAdapter(this.ctx);
					characterPlayerAdapter.open(this.getDb());
					result = characterPlayerAdapter.query(baseCharacterId);
				}
				break;

			case FIGHTPLAYER_BASEWEAPON:
				fightPlayerCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (fightPlayerCursor.getCount() > 0) {
					fightPlayerCursor.moveToFirst();
					int baseWeaponId = fightPlayerCursor.getInt(fightPlayerCursor.getColumnIndex(
									FightPlayerSQLiteAdapter.COL_BASEWEAPON));
					
					ItemWeaponSQLiteAdapter itemWeaponAdapter = new ItemWeaponSQLiteAdapter(this.ctx);
					itemWeaponAdapter.open(this.getDb());
					result = itemWeaponAdapter.query(baseWeaponId);
				}
				break;

			case FIGHTPLAYER_BASESPELLS:
				id = Integer.parseInt(uri.getPathSegments().get(1));
				SpellSQLiteAdapter baseSpellsAdapter = new SpellSQLiteAdapter(this.ctx);
				baseSpellsAdapter.open(this.getDb());
				result = baseSpellsAdapter.getByFightPlayerbaseSpellsInternal(id, SpellSQLiteAdapter.ALIASED_COLS, selection, selectionArgs, null);
				break;

			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues values,
			String selection,
			String[] selectionArgs) {
		
		
		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		int result = -1;
		switch (matchedUri) {
			case FIGHTPLAYER_ONE:
				String id = uri.getPathSegments().get(1);
				result = this.adapter.update(
						values,
						FightPlayerSQLiteAdapter.COL_ID + " = "
						+ id,
						selectionArgs);
				break;
			case FIGHTPLAYER_ALL:
				result = this.adapter.update(
							values,
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}



	/**
	 * Get the entity URI.
	 * @return The URI
	 */
	@Override
	public Uri getUri() {
		return FIGHTPLAYER_URI;
	}

	/**
	 * Query by ID.
	 *
	 * @param id The id of the entity to retrieve
	 * @return The cursor
	 */
	private Cursor queryById(String id) {
		Cursor result = null;
		String selection = FightPlayerSQLiteAdapter.ALIASED_COL_ID
						+ " = ?";

		String[] selectionArgs = new String[]{id};

		result = this.adapter.query(
					FightPlayerSQLiteAdapter.ALIASED_COLS,
					selection,
					selectionArgs,
					null,
					null,
					null);
		return result;
	}
}

