/**************************************************************************
 * FightActionProviderAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.base;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.google.common.base.Strings;

import com.curseblade.entity.FightAction;
import com.curseblade.provider.CursebladeProvider;
import com.curseblade.data.FightActionSQLiteAdapter;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;

/**
 * FightActionProviderAdapterBase.
 */
public abstract class FightActionProviderAdapterBase
				extends ProviderAdapterBase<FightAction> {

	/** TAG for debug purpose. */
	protected static final String TAG = "FightActionProviderAdapter";

	/** FIGHTACTION_URI. */
	public	  static Uri FIGHTACTION_URI;

	/** fightAction type. */
	protected static final String fightActionType =
			"fightaction";

	/** FIGHTACTION_ALL. */
	protected static final int FIGHTACTION_ALL =
			814902854;
	/** FIGHTACTION_ONE. */
	protected static final int FIGHTACTION_ONE =
			814902855;

	/** FIGHTACTION_FIGHT. */
	protected static final int FIGHTACTION_FIGHT =
			814902856;
	/** FIGHTACTION_SENDER. */
	protected static final int FIGHTACTION_SENDER =
			814902857;
	/** FIGHTACTION_RECEIVER. */
	protected static final int FIGHTACTION_RECEIVER =
			814902858;

	/**
	 * Static constructor.
	 */
	static {
		FIGHTACTION_URI =
				CursebladeProvider.generateUri(
						fightActionType);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightActionType,
				FIGHTACTION_ALL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightActionType + "/#",
				FIGHTACTION_ONE);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightActionType + "/#/fight",
				FIGHTACTION_FIGHT);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightActionType + "/#/sender",
				FIGHTACTION_SENDER);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightActionType + "/#/receiver",
				FIGHTACTION_RECEIVER);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public FightActionProviderAdapterBase(
				final Context ctx,
				final SQLiteDatabase db) {
		super(ctx);
		this.adapter = new FightActionSQLiteAdapter(ctx);
		if (db != null) {
			this.db = this.adapter.open(db);
		} else {
			this.db = this.adapter.open();
		}

		this.uriIds.add(FIGHTACTION_ALL);
		this.uriIds.add(FIGHTACTION_ONE);
		this.uriIds.add(FIGHTACTION_FIGHT);
		this.uriIds.add(FIGHTACTION_SENDER);
		this.uriIds.add(FIGHTACTION_RECEIVER);
	}

	@Override
	public String getType(final Uri uri) {
		String result;
		final String single =
				"vnc.android.cursor.item/"
					+ CursebladeProvider.authority + ".";
		final String collection =
				"vnc.android.cursor.collection/"
					+ CursebladeProvider.authority + ".";

		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);

		switch (matchedUri) {
			case FIGHTACTION_ALL:
				result = collection + "fightaction";
				break;
			case FIGHTACTION_ONE:
				result = single + "fightaction";
				break;
			case FIGHTACTION_FIGHT:
				result = single + "fightaction";
				break;
			case FIGHTACTION_SENDER:
				result = single + "fightaction";
				break;
			case FIGHTACTION_RECEIVER:
				result = single + "fightaction";
				break;
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int delete(
			final Uri uri,
			String selection,
			String[] selectionArgs) {
		int matchedUri = CursebladeProviderBase
					.getUriMatcher().match(uri);
		int result = -1;
		switch (matchedUri) {
			case FIGHTACTION_ONE:
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				selection = FightActionSQLiteAdapter.COL_ID
						+ " = ?";
				selectionArgs = new String[1];
				selectionArgs[0] = String.valueOf(id);
				result = this.adapter.delete(
						selection,
						selectionArgs);
				break;
			case FIGHTACTION_ALL:
				result = this.adapter.delete(
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}
	
	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);
		
		Uri result = null;
		int id = 0;
		switch (matchedUri) {
			case FIGHTACTION_ALL:
				if (values.size() > 0) {
					id = (int) this.adapter.insert(null, values);
				} else {
					id = (int) this.adapter.insert(FightActionSQLiteAdapter.COL_ID, values);
				}
				if (id > 0) {
					result = ContentUris.withAppendedId(
							FIGHTACTION_URI,
							id);
				}
				break;
			default:
				result = null;
				break;
		}
		return result;
	}

	@Override
	public Cursor query(final Uri uri,
						String[] projection,
						String selection,
						String[] selectionArgs,
						final String sortOrder) {

		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		Cursor result = null;
		Cursor fightActionCursor;
		

		switch (matchedUri) {

			case FIGHTACTION_ALL:
				result = this.adapter.query(
							projection,
							selection,
							selectionArgs,
							null,
							null,
							sortOrder);
				break;
			case FIGHTACTION_ONE:
				result = this.queryById(uri.getPathSegments().get(1));
				break;
			
			case FIGHTACTION_FIGHT:
				fightActionCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (fightActionCursor.getCount() > 0) {
					fightActionCursor.moveToFirst();
					int fightId = fightActionCursor.getInt(fightActionCursor.getColumnIndex(
									FightActionSQLiteAdapter.COL_FIGHT));
					
					FightSQLiteAdapter fightAdapter = new FightSQLiteAdapter(this.ctx);
					fightAdapter.open(this.getDb());
					result = fightAdapter.query(fightId);
				}
				break;

			case FIGHTACTION_SENDER:
				fightActionCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (fightActionCursor.getCount() > 0) {
					fightActionCursor.moveToFirst();
					int senderId = fightActionCursor.getInt(fightActionCursor.getColumnIndex(
									FightActionSQLiteAdapter.COL_SENDER));
					
					FightPlayerSQLiteAdapter fightPlayerAdapter = new FightPlayerSQLiteAdapter(this.ctx);
					fightPlayerAdapter.open(this.getDb());
					result = fightPlayerAdapter.query(senderId);
				}
				break;

			case FIGHTACTION_RECEIVER:
				fightActionCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (fightActionCursor.getCount() > 0) {
					fightActionCursor.moveToFirst();
					int receiverId = fightActionCursor.getInt(fightActionCursor.getColumnIndex(
									FightActionSQLiteAdapter.COL_RECEIVER));
					
					FightPlayerSQLiteAdapter fightPlayerAdapter = new FightPlayerSQLiteAdapter(this.ctx);
					fightPlayerAdapter.open(this.getDb());
					result = fightPlayerAdapter.query(receiverId);
				}
				break;

			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues values,
			String selection,
			String[] selectionArgs) {
		
		
		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		int result = -1;
		switch (matchedUri) {
			case FIGHTACTION_ONE:
				String id = uri.getPathSegments().get(1);
				result = this.adapter.update(
						values,
						FightActionSQLiteAdapter.COL_ID + " = "
						+ id,
						selectionArgs);
				break;
			case FIGHTACTION_ALL:
				result = this.adapter.update(
							values,
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}



	/**
	 * Get the entity URI.
	 * @return The URI
	 */
	@Override
	public Uri getUri() {
		return FIGHTACTION_URI;
	}

	/**
	 * Query by ID.
	 *
	 * @param id The id of the entity to retrieve
	 * @return The cursor
	 */
	private Cursor queryById(String id) {
		Cursor result = null;
		String selection = FightActionSQLiteAdapter.ALIASED_COL_ID
						+ " = ?";

		String[] selectionArgs = new String[]{id};

		result = this.adapter.query(
					FightActionSQLiteAdapter.ALIASED_COLS,
					selection,
					selectionArgs,
					null,
					null,
					null);
		return result;
	}
}

