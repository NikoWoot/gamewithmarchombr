/**************************************************************************
 * TournamentProviderAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.base;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.google.common.base.Strings;

import com.curseblade.entity.Tournament;
import com.curseblade.provider.CursebladeProvider;
import com.curseblade.data.TournamentSQLiteAdapter;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;

/**
 * TournamentProviderAdapterBase.
 */
public abstract class TournamentProviderAdapterBase
				extends ProviderAdapterBase<Tournament> {

	/** TAG for debug purpose. */
	protected static final String TAG = "TournamentProviderAdapter";

	/** TOURNAMENT_URI. */
	public	  static Uri TOURNAMENT_URI;

	/** tournament type. */
	protected static final String tournamentType =
			"tournament";

	/** TOURNAMENT_ALL. */
	protected static final int TOURNAMENT_ALL =
			997471753;
	/** TOURNAMENT_ONE. */
	protected static final int TOURNAMENT_ONE =
			997471754;

	/** TOURNAMENT_POOLFIGHTS. */
	protected static final int TOURNAMENT_POOLFIGHTS =
			997471755;

	/**
	 * Static constructor.
	 */
	static {
		TOURNAMENT_URI =
				CursebladeProvider.generateUri(
						tournamentType);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				tournamentType,
				TOURNAMENT_ALL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				tournamentType + "/#",
				TOURNAMENT_ONE);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				tournamentType + "/#/poolfights",
				TOURNAMENT_POOLFIGHTS);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public TournamentProviderAdapterBase(
				final Context ctx,
				final SQLiteDatabase db) {
		super(ctx);
		this.adapter = new TournamentSQLiteAdapter(ctx);
		if (db != null) {
			this.db = this.adapter.open(db);
		} else {
			this.db = this.adapter.open();
		}

		this.uriIds.add(TOURNAMENT_ALL);
		this.uriIds.add(TOURNAMENT_ONE);
		this.uriIds.add(TOURNAMENT_POOLFIGHTS);
	}

	@Override
	public String getType(final Uri uri) {
		String result;
		final String single =
				"vnc.android.cursor.item/"
					+ CursebladeProvider.authority + ".";
		final String collection =
				"vnc.android.cursor.collection/"
					+ CursebladeProvider.authority + ".";

		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);

		switch (matchedUri) {
			case TOURNAMENT_ALL:
				result = collection + "tournament";
				break;
			case TOURNAMENT_ONE:
				result = single + "tournament";
				break;
			case TOURNAMENT_POOLFIGHTS:
				result = collection + "tournament";
				break;
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int delete(
			final Uri uri,
			String selection,
			String[] selectionArgs) {
		int matchedUri = CursebladeProviderBase
					.getUriMatcher().match(uri);
		int result = -1;
		switch (matchedUri) {
			case TOURNAMENT_ONE:
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				selection = TournamentSQLiteAdapter.COL_ID
						+ " = ?";
				selectionArgs = new String[1];
				selectionArgs[0] = String.valueOf(id);
				result = this.adapter.delete(
						selection,
						selectionArgs);
				break;
			case TOURNAMENT_ALL:
				result = this.adapter.delete(
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}
	
	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);
		
		Uri result = null;
		int id = 0;
		switch (matchedUri) {
			case TOURNAMENT_ALL:
				if (values.size() > 0) {
					id = (int) this.adapter.insert(null, values);
				} else {
					id = (int) this.adapter.insert(TournamentSQLiteAdapter.COL_ID, values);
				}
				if (id > 0) {
					result = ContentUris.withAppendedId(
							TOURNAMENT_URI,
							id);
				}
				break;
			default:
				result = null;
				break;
		}
		return result;
	}

	@Override
	public Cursor query(final Uri uri,
						String[] projection,
						String selection,
						String[] selectionArgs,
						final String sortOrder) {

		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		Cursor result = null;
		int id = 0;

		switch (matchedUri) {

			case TOURNAMENT_ALL:
				result = this.adapter.query(
							projection,
							selection,
							selectionArgs,
							null,
							null,
							sortOrder);
				break;
			case TOURNAMENT_ONE:
				result = this.queryById(uri.getPathSegments().get(1));
				break;
			
			case TOURNAMENT_POOLFIGHTS:
				id = Integer.parseInt(uri.getPathSegments().get(1));
				TournamentPoolFightSQLiteAdapter poolFightsAdapter = new TournamentPoolFightSQLiteAdapter(this.ctx);
				poolFightsAdapter.open(this.getDb());
				result = poolFightsAdapter.getByTournamentpoolFightsInternal(id, TournamentPoolFightSQLiteAdapter.ALIASED_COLS, selection, selectionArgs, null);
				break;

			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues values,
			String selection,
			String[] selectionArgs) {
		
		
		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		int result = -1;
		switch (matchedUri) {
			case TOURNAMENT_ONE:
				String id = uri.getPathSegments().get(1);
				result = this.adapter.update(
						values,
						TournamentSQLiteAdapter.COL_ID + " = "
						+ id,
						selectionArgs);
				break;
			case TOURNAMENT_ALL:
				result = this.adapter.update(
							values,
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}



	/**
	 * Get the entity URI.
	 * @return The URI
	 */
	@Override
	public Uri getUri() {
		return TOURNAMENT_URI;
	}

	/**
	 * Query by ID.
	 *
	 * @param id The id of the entity to retrieve
	 * @return The cursor
	 */
	private Cursor queryById(String id) {
		Cursor result = null;
		String selection = TournamentSQLiteAdapter.ALIASED_COL_ID
						+ " = ?";

		String[] selectionArgs = new String[]{id};

		result = this.adapter.query(
					TournamentSQLiteAdapter.ALIASED_COLS,
					selection,
					selectionArgs,
					null,
					null,
					null);
		return result;
	}
}

