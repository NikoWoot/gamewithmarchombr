/**************************************************************************
 * FightProviderAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.base;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.google.common.base.Strings;

import com.curseblade.entity.Fight;
import com.curseblade.provider.CursebladeProvider;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.data.FightActionSQLiteAdapter;

/**
 * FightProviderAdapterBase.
 */
public abstract class FightProviderAdapterBase
				extends ProviderAdapterBase<Fight> {

	/** TAG for debug purpose. */
	protected static final String TAG = "FightProviderAdapter";

	/** FIGHT_URI. */
	public	  static Uri FIGHT_URI;

	/** fight type. */
	protected static final String fightType =
			"fight";

	/** FIGHT_ALL. */
	protected static final int FIGHT_ALL =
			67876848;
	/** FIGHT_ONE. */
	protected static final int FIGHT_ONE =
			67876849;

	/** FIGHT_STARTERFIGHTERS. */
	protected static final int FIGHT_STARTERFIGHTERS =
			67876850;
	/** FIGHT_ALTERNATEDFIGHTERS. */
	protected static final int FIGHT_ALTERNATEDFIGHTERS =
			67876851;
	/** FIGHT_SURVIVORFIGHTERS. */
	protected static final int FIGHT_SURVIVORFIGHTERS =
			67876852;
	/** FIGHT_ACTIONS. */
	protected static final int FIGHT_ACTIONS =
			67876853;
	/** FIGHT_SENDERPLAYER. */
	protected static final int FIGHT_SENDERPLAYER =
			67876854;
	/** FIGHT_RECEIVERPLAYER. */
	protected static final int FIGHT_RECEIVERPLAYER =
			67876855;

	/**
	 * Static constructor.
	 */
	static {
		FIGHT_URI =
				CursebladeProvider.generateUri(
						fightType);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightType,
				FIGHT_ALL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightType + "/#",
				FIGHT_ONE);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightType + "/#/starterfighters",
				FIGHT_STARTERFIGHTERS);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightType + "/#/alternatedfighters",
				FIGHT_ALTERNATEDFIGHTERS);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightType + "/#/survivorfighters",
				FIGHT_SURVIVORFIGHTERS);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightType + "/#/actions",
				FIGHT_ACTIONS);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightType + "/#/senderplayer",
				FIGHT_SENDERPLAYER);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				fightType + "/#/receiverplayer",
				FIGHT_RECEIVERPLAYER);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public FightProviderAdapterBase(
				final Context ctx,
				final SQLiteDatabase db) {
		super(ctx);
		this.adapter = new FightSQLiteAdapter(ctx);
		if (db != null) {
			this.db = this.adapter.open(db);
		} else {
			this.db = this.adapter.open();
		}

		this.uriIds.add(FIGHT_ALL);
		this.uriIds.add(FIGHT_ONE);
		this.uriIds.add(FIGHT_STARTERFIGHTERS);
		this.uriIds.add(FIGHT_ALTERNATEDFIGHTERS);
		this.uriIds.add(FIGHT_SURVIVORFIGHTERS);
		this.uriIds.add(FIGHT_ACTIONS);
		this.uriIds.add(FIGHT_SENDERPLAYER);
		this.uriIds.add(FIGHT_RECEIVERPLAYER);
	}

	@Override
	public String getType(final Uri uri) {
		String result;
		final String single =
				"vnc.android.cursor.item/"
					+ CursebladeProvider.authority + ".";
		final String collection =
				"vnc.android.cursor.collection/"
					+ CursebladeProvider.authority + ".";

		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);

		switch (matchedUri) {
			case FIGHT_ALL:
				result = collection + "fight";
				break;
			case FIGHT_ONE:
				result = single + "fight";
				break;
			case FIGHT_STARTERFIGHTERS:
				result = collection + "fight";
				break;
			case FIGHT_ALTERNATEDFIGHTERS:
				result = collection + "fight";
				break;
			case FIGHT_SURVIVORFIGHTERS:
				result = collection + "fight";
				break;
			case FIGHT_ACTIONS:
				result = collection + "fight";
				break;
			case FIGHT_SENDERPLAYER:
				result = single + "fight";
				break;
			case FIGHT_RECEIVERPLAYER:
				result = single + "fight";
				break;
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int delete(
			final Uri uri,
			String selection,
			String[] selectionArgs) {
		int matchedUri = CursebladeProviderBase
					.getUriMatcher().match(uri);
		int result = -1;
		switch (matchedUri) {
			case FIGHT_ONE:
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				selection = FightSQLiteAdapter.COL_ID
						+ " = ?";
				selectionArgs = new String[1];
				selectionArgs[0] = String.valueOf(id);
				result = this.adapter.delete(
						selection,
						selectionArgs);
				break;
			case FIGHT_ALL:
				result = this.adapter.delete(
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}
	
	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);
		
		Uri result = null;
		int id = 0;
		switch (matchedUri) {
			case FIGHT_ALL:
				if (values.size() > 0) {
					id = (int) this.adapter.insert(null, values);
				} else {
					id = (int) this.adapter.insert(FightSQLiteAdapter.COL_ID, values);
				}
				if (id > 0) {
					result = ContentUris.withAppendedId(
							FIGHT_URI,
							id);
				}
				break;
			default:
				result = null;
				break;
		}
		return result;
	}

	@Override
	public Cursor query(final Uri uri,
						String[] projection,
						String selection,
						String[] selectionArgs,
						final String sortOrder) {

		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		Cursor result = null;
		Cursor fightCursor;
		int id = 0;

		switch (matchedUri) {

			case FIGHT_ALL:
				result = this.adapter.query(
							projection,
							selection,
							selectionArgs,
							null,
							null,
							sortOrder);
				break;
			case FIGHT_ONE:
				result = this.queryById(uri.getPathSegments().get(1));
				break;
			
			case FIGHT_STARTERFIGHTERS:
				id = Integer.parseInt(uri.getPathSegments().get(1));
				FightPlayerSQLiteAdapter starterFightersAdapter = new FightPlayerSQLiteAdapter(this.ctx);
				starterFightersAdapter.open(this.getDb());
				result = starterFightersAdapter.getByFight(id, FightPlayerSQLiteAdapter.ALIASED_COLS, selection, selectionArgs, null);
				break;

			case FIGHT_ALTERNATEDFIGHTERS:
				id = Integer.parseInt(uri.getPathSegments().get(1));
				FightPlayerSQLiteAdapter alternatedFightersAdapter = new FightPlayerSQLiteAdapter(this.ctx);
				alternatedFightersAdapter.open(this.getDb());
				result = alternatedFightersAdapter.getByFight(id, FightPlayerSQLiteAdapter.ALIASED_COLS, selection, selectionArgs, null);
				break;

			case FIGHT_SURVIVORFIGHTERS:
				id = Integer.parseInt(uri.getPathSegments().get(1));
				FightPlayerSQLiteAdapter survivorFightersAdapter = new FightPlayerSQLiteAdapter(this.ctx);
				survivorFightersAdapter.open(this.getDb());
				result = survivorFightersAdapter.getByFight(id, FightPlayerSQLiteAdapter.ALIASED_COLS, selection, selectionArgs, null);
				break;

			case FIGHT_ACTIONS:
				id = Integer.parseInt(uri.getPathSegments().get(1));
				FightActionSQLiteAdapter actionsAdapter = new FightActionSQLiteAdapter(this.ctx);
				actionsAdapter.open(this.getDb());
				result = actionsAdapter.getByFightactionsInternal(id, FightActionSQLiteAdapter.ALIASED_COLS, selection, selectionArgs, null);
				break;

			case FIGHT_SENDERPLAYER:
				fightCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (fightCursor.getCount() > 0) {
					fightCursor.moveToFirst();
					int senderPlayerId = fightCursor.getInt(fightCursor.getColumnIndex(
									FightSQLiteAdapter.COL_SENDERPLAYER));
					
					FightPlayerSQLiteAdapter fightPlayerAdapter = new FightPlayerSQLiteAdapter(this.ctx);
					fightPlayerAdapter.open(this.getDb());
					result = fightPlayerAdapter.query(senderPlayerId);
				}
				break;

			case FIGHT_RECEIVERPLAYER:
				fightCursor = this.queryById(uri.getPathSegments().get(1));
				
				if (fightCursor.getCount() > 0) {
					fightCursor.moveToFirst();
					int receiverPlayerId = fightCursor.getInt(fightCursor.getColumnIndex(
									FightSQLiteAdapter.COL_RECEIVERPLAYER));
					
					FightPlayerSQLiteAdapter fightPlayerAdapter = new FightPlayerSQLiteAdapter(this.ctx);
					fightPlayerAdapter.open(this.getDb());
					result = fightPlayerAdapter.query(receiverPlayerId);
				}
				break;

			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues values,
			String selection,
			String[] selectionArgs) {
		
		
		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		int result = -1;
		switch (matchedUri) {
			case FIGHT_ONE:
				String id = uri.getPathSegments().get(1);
				result = this.adapter.update(
						values,
						FightSQLiteAdapter.COL_ID + " = "
						+ id,
						selectionArgs);
				break;
			case FIGHT_ALL:
				result = this.adapter.update(
							values,
							selection,
							selectionArgs);
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}



	/**
	 * Get the entity URI.
	 * @return The URI
	 */
	@Override
	public Uri getUri() {
		return FIGHT_URI;
	}

	/**
	 * Query by ID.
	 *
	 * @param id The id of the entity to retrieve
	 * @return The cursor
	 */
	private Cursor queryById(String id) {
		Cursor result = null;
		String selection = FightSQLiteAdapter.ALIASED_COL_ID
						+ " = ?";

		String[] selectionArgs = new String[]{id};

		result = this.adapter.query(
					FightSQLiteAdapter.ALIASED_COLS,
					selection,
					selectionArgs,
					null,
					null,
					null);
		return result;
	}
}

