/**************************************************************************
 * NewsProviderAdapterBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.base;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.google.common.base.Strings;

import com.curseblade.entity.News;
import com.curseblade.provider.CursebladeProvider;
import com.curseblade.provider.HarmonyRssItemProviderAdapter;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;

import com.curseblade.criterias.NewsCriterias;
import com.curseblade.criterias.base.Criteria;
import com.curseblade.criterias.base.Criteria.Type;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.criterias.base.value.ArrayValue;
import com.curseblade.data.NewsSQLiteAdapter;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;
import com.curseblade.harmony.util.DatabaseUtil;

/**
 * NewsProviderAdapterBase.
 */
public abstract class NewsProviderAdapterBase
				extends ProviderAdapterBase<News> {

	/** TAG for debug purpose. */
	protected static final String TAG = "NewsProviderAdapter";

	/** NEWS_URI. */
	public	  static Uri NEWS_URI;

	/** news type. */
	protected static final String newsType =
			"news";

	/** NEWS_ALL. */
	protected static final int NEWS_ALL =
			2424563;
	/** NEWS_ONE. */
	protected static final int NEWS_ONE =
			2424564;


	/**
	 * Static constructor.
	 */
	static {
		NEWS_URI =
				CursebladeProvider.generateUri(
						newsType);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				newsType,
				NEWS_ALL);
		CursebladeProvider.getUriMatcher().addURI(
				CursebladeProvider.authority,
				newsType + "/#",
				NEWS_ONE);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public NewsProviderAdapterBase(
				final Context ctx,
				final SQLiteDatabase db) {
		super(ctx);
		this.adapter = new NewsSQLiteAdapter(ctx);
		if (db != null) {
			this.db = this.adapter.open(db);
		} else {
			this.db = this.adapter.open();
		}

		this.uriIds.add(NEWS_ALL);
		this.uriIds.add(NEWS_ONE);
	}

	@Override
	public String getType(final Uri uri) {
		String result;
		final String single =
				"vnc.android.cursor.item/"
					+ CursebladeProvider.authority + ".";
		final String collection =
				"vnc.android.cursor.collection/"
					+ CursebladeProvider.authority + ".";

		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);

		switch (matchedUri) {
			case NEWS_ALL:
				result = collection + "news";
				break;
			case NEWS_ONE:
				result = single + "news";
				break;
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int delete(
			final Uri uri,
			String selection,
			String[] selectionArgs) {
		int matchedUri = CursebladeProviderBase
					.getUriMatcher().match(uri);
		int result = -1;
		switch (matchedUri) {
			case NEWS_ONE:
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				Uri motherUri = Uri.withAppendedPath(
						HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI, String.valueOf(id));
				result = this.ctx.getContentResolver().delete(motherUri,
						selection, selectionArgs);
				break;
			case NEWS_ALL:
				// Query the ids of the changing fields.
				Cursor idsCursor = this.adapter.query(
						new String[]{HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID},
						selection,
						selectionArgs,
						null,
						null,
						null);
				// If there are ids
				if (idsCursor.getCount() > 0) {
					CriteriasBase parentCrit = this.cursorToIDSelection(idsCursor, HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID);
					String parentSelection = parentCrit.toSQLiteSelection();
					String[] parentSelectionArgs = parentCrit.toSQLiteSelectionArgs();
					result = this.ctx.getContentResolver().delete(
							HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI,
							parentSelection,
							parentSelectionArgs);
				}
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}
	
	@Override
	public Uri insert(final Uri uri, final ContentValues values) {
		int matchedUri = CursebladeProviderBase
				.getUriMatcher().match(uri);
		ContentValues newsValues =
			DatabaseUtil.extractContentValues(values, NewsSQLiteAdapter.COLS);
		Uri result = null;
		int id = 0;
		switch (matchedUri) {
			case NEWS_ALL:
				Uri newUri = this.ctx.getContentResolver().insert(
						HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI,
						values);
				int newId = Integer.parseInt(newUri.getPathSegments().get(1));
				newsValues.put(HarmonyRssItemSQLiteAdapter.COL_ID, newId);
				if (newsValues.size() > 0) {
					id = (int) this.adapter.insert(null, newsValues);
				} else {
					id = (int) this.adapter.insert(HarmonyRssItemSQLiteAdapter.COL_ID, newsValues);
				}
				if (id > 0) {
					result = ContentUris.withAppendedId(
							NEWS_URI,
							id);
				}
				break;
			default:
				result = null;
				break;
		}
		return result;
	}

	@Override
	public Cursor query(final Uri uri,
						String[] projection,
						String selection,
						String[] selectionArgs,
						final String sortOrder) {

		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		Cursor result = null;
		

		switch (matchedUri) {

			case NEWS_ALL:
				result = this.adapter.query(
							projection,
							selection,
							selectionArgs,
							null,
							null,
							sortOrder);
				break;
			case NEWS_ONE:
				result = this.queryById(uri.getPathSegments().get(1));
				break;
			
			default:
				result = null;
				break;
		}

		return result;
	}

	@Override
	public int update(
			final Uri uri,
			final ContentValues values,
			String selection,
			String[] selectionArgs) {
		
		ContentValues newsValues = DatabaseUtil.extractContentValues(values, NewsSQLiteAdapter.COLS);
		int matchedUri = CursebladeProviderBase.getUriMatcher()
				.match(uri);
		int result = -1;
		switch (matchedUri) {
			case NEWS_ONE:
				String id = uri.getPathSegments().get(1);
				Uri parentUri = Uri.withAppendedPath(HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI,
						String.valueOf(id));
				result = this.ctx.getContentResolver().update(
						parentUri,
						values,
						null,
						null);
				result += this.adapter.update(
						newsValues,
						HarmonyRssItemSQLiteAdapter.COL_ID + " = ?",
						new String[]{String.valueOf(id)});
				break;
			case NEWS_ALL:
				// Query the ids of the changing fields.
				Cursor idsCursor = this.adapter.query(
						new String[]{HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID},
						selection,
						selectionArgs,
						null,
						null,
						null);
				// If there are ids
				if (idsCursor.getCount() > 0) {
					// If there are values in this table
					if (newsValues.size() > 0) {
						CriteriasBase currentCrit = this.cursorToIDSelection(
								idsCursor,
								HarmonyRssItemSQLiteAdapter.COL_ID);

						String currentSelection = currentCrit.toSQLiteSelection();
						String[] currentSelectionArgs = currentCrit
								.toSQLiteSelectionArgs();
						// Update the current table
						result += this.adapter.update(
								newsValues,
								currentSelection,
								currentSelectionArgs);
					}
					// If there are still values to be updated in parents
					if (values.size() > 0) {
						CriteriasBase parentCrit = this.cursorToIDSelection(
								idsCursor,
								HarmonyRssItemSQLiteAdapter.COL_ID);

						String parentSelection = parentCrit.toSQLiteSelection();
						String[] parentSelectionArgs = parentCrit
								.toSQLiteSelectionArgs();
						// Update the parents tables
						result = this.ctx.getContentResolver().update(
								HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI,
								values,
								parentSelection,
								parentSelectionArgs);
					}
				}
				break;
			default:
				result = -1;
				break;
		}
		return result;
	}

	/**
	 * Transform a cursor of ids into a Criteria.
	 *
	 * @param cursor The cursor
	 *Â @param key The key to map the ids to
	 *
	 * @return The criteria
	 */
	protected CriteriasBase cursorToIDSelection(Cursor cursor, String key) {
		NewsCriterias crit = new NewsCriterias(GroupType.AND);
		Criteria inCrit = new Criteria();
		inCrit.setKey(key);
		inCrit.setType(Type.IN);
		ArrayValue inArray = new ArrayValue();
		cursor.moveToFirst();
		do {
			inArray.addValue(cursor.getString(
				cursor.getColumnIndex(HarmonyRssItemSQLiteAdapter.COL_ID)));
		} while (cursor.moveToNext());
		inCrit.addValue(inArray);
		crit.add(inCrit);
		return crit;
	}


	/**
	 * Get the entity URI.
	 * @return The URI
	 */
	@Override
	public Uri getUri() {
		return NEWS_URI;
	}

	/**
	 * Query by ID.
	 *
	 * @param id The id of the entity to retrieve
	 * @return The cursor
	 */
	private Cursor queryById(String id) {
		Cursor result = null;
		String selection = HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID
						+ " = ?";

		String[] selectionArgs = new String[]{id};

		result = this.adapter.query(
					NewsSQLiteAdapter.ALIASED_COLS,
					selection,
					selectionArgs,
					null,
					null,
					null);
		return result;
	}
}

