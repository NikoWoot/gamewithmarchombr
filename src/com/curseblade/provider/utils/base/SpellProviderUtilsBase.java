/**************************************************************************
 * SpellProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.SpellCriterias;
import com.curseblade.criterias.CharacterPlayerCriterias;
import com.curseblade.criterias.FightPlayerCriterias;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.SpellSQLiteAdapter;

import com.curseblade.entity.Spell;

import com.curseblade.provider.SpellProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * Spell Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class SpellProviderUtilsBase
			extends ProviderUtilsBase<Spell> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "SpellProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public SpellProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final Spell item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		SpellSQLiteAdapter adapt =
				new SpellSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(SpellSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				SpellProviderAdapter.SPELL_URI)
						.withValues(itemValues)
						.build());


		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Insert into DB.
	 * @param item Spell to insert
	 * @param characterPlayerequipedSpellsInternalId characterPlayerequipedSpellsInternal Id* @param fightPlayerbaseSpellsInternalId fightPlayerbaseSpellsInternal Id
	 * @return number of rows affected
	 */
	public Uri insert(final Spell item,
							 final int characterPlayerequipedSpellsInternalId,
							 final int fightPlayerbaseSpellsInternalId) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();


		SpellSQLiteAdapter adapt =
				new SpellSQLiteAdapter(this.getContext());
		ContentValues itemValues = adapt.itemToContentValues(item,
					characterPlayerequipedSpellsInternalId,
					fightPlayerbaseSpellsInternalId);
		itemValues.remove(SpellSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				SpellProviderAdapter.SPELL_URI)
			    	.withValues(itemValues)
			    	.build());



		try {
			ContentProviderResult[] results =
				prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Delete from DB.
	 * @param item Spell
	 * @return number of row affected
	 */
	public int delete(final Spell item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				SpellProviderAdapter.SPELL_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return Spell
	 */
	public Spell query(final int id) {
		Spell result = null;
		SpellSQLiteAdapter adapt =
					new SpellSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		SpellCriterias crits =
				new SpellCriterias(GroupType.AND);
		crits.add(SpellSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			SpellProviderAdapter.SPELL_URI,
			SpellSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<Spell>
	 */
	public ArrayList<Spell> queryAll() {
		ArrayList<Spell> result =
					new ArrayList<Spell>();
		SpellSQLiteAdapter adapt =
					new SpellSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				SpellProviderAdapter.SPELL_URI,
				SpellSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<Spell>
	 */
	public ArrayList<Spell> query(
				CriteriasBase<Spell> criteria) {
		ArrayList<Spell> result =
					new ArrayList<Spell>();
		SpellSQLiteAdapter adapt =
					new SpellSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				SpellProviderAdapter.SPELL_URI,
				SpellSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item Spell
	 * @return number of rows updated
	 */
	public int update(final Spell item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		SpellSQLiteAdapter adapt =
				new SpellSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(item);

		Uri uri = Uri.withAppendedPath(
				SpellProviderAdapter.SPELL_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());


		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item Spell
	 * @param characterPlayerequipedSpellsInternalId characterPlayerequipedSpellsInternal Id* @param fightPlayerbaseSpellsInternalId fightPlayerbaseSpellsInternal Id
	 * @return number of rows updated
	 */
	public int update(final Spell item,
							 final int characterPlayerequipedSpellsInternalId,
							 final int fightPlayerbaseSpellsInternalId) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		SpellSQLiteAdapter adapt =
				new SpellSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item,
				characterPlayerequipedSpellsInternalId,
				fightPlayerbaseSpellsInternalId);

		Uri uri = Uri.withAppendedPath(
				SpellProviderAdapter.SPELL_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());



		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/** Relations operations. */
}
