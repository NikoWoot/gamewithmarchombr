/**************************************************************************
 * FightActionProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.FightActionCriterias;
import com.curseblade.criterias.FightCriterias;
import com.curseblade.criterias.FightPlayerCriterias;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.FightActionSQLiteAdapter;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;

import com.curseblade.entity.FightAction;
import com.curseblade.entity.Fight;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.FightActionType;

import com.curseblade.provider.FightActionProviderAdapter;
import com.curseblade.provider.FightProviderAdapter;
import com.curseblade.provider.FightPlayerProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * FightAction Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class FightActionProviderUtilsBase
			extends ProviderUtilsBase<FightAction> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "FightActionProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public FightActionProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final FightAction item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		FightActionSQLiteAdapter adapt =
				new FightActionSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(FightActionSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				FightActionProviderAdapter.FIGHTACTION_URI)
						.withValues(itemValues)
						.build());


		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Insert into DB.
	 * @param item FightAction to insert
	 * @param fightactionsInternalId fightactionsInternal Id
	 * @return number of rows affected
	 */
	public Uri insert(final FightAction item,
							 final int fightactionsInternalId) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();


		FightActionSQLiteAdapter adapt =
				new FightActionSQLiteAdapter(this.getContext());
		ContentValues itemValues = adapt.itemToContentValues(item,
					fightactionsInternalId);
		itemValues.remove(FightActionSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				FightActionProviderAdapter.FIGHTACTION_URI)
			    	.withValues(itemValues)
			    	.build());



		try {
			ContentProviderResult[] results =
				prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Delete from DB.
	 * @param item FightAction
	 * @return number of row affected
	 */
	public int delete(final FightAction item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				FightActionProviderAdapter.FIGHTACTION_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return FightAction
	 */
	public FightAction query(final int id) {
		FightAction result = null;
		FightActionSQLiteAdapter adapt =
					new FightActionSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		FightActionCriterias crits =
				new FightActionCriterias(GroupType.AND);
		crits.add(FightActionSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			FightActionProviderAdapter.FIGHTACTION_URI,
			FightActionSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

			if (result.getFight() != null) {
				result.setFight(
					this.getAssociateFight(result));
			}
			if (result.getSender() != null) {
				result.setSender(
					this.getAssociateSender(result));
			}
			if (result.getReceiver() != null) {
				result.setReceiver(
					this.getAssociateReceiver(result));
			}
		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<FightAction>
	 */
	public ArrayList<FightAction> queryAll() {
		ArrayList<FightAction> result =
					new ArrayList<FightAction>();
		FightActionSQLiteAdapter adapt =
					new FightActionSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				FightActionProviderAdapter.FIGHTACTION_URI,
				FightActionSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<FightAction>
	 */
	public ArrayList<FightAction> query(
				CriteriasBase<FightAction> criteria) {
		ArrayList<FightAction> result =
					new ArrayList<FightAction>();
		FightActionSQLiteAdapter adapt =
					new FightActionSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				FightActionProviderAdapter.FIGHTACTION_URI,
				FightActionSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item FightAction
	 * @return number of rows updated
	 */
	public int update(final FightAction item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		FightActionSQLiteAdapter adapt =
				new FightActionSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(item);

		Uri uri = Uri.withAppendedPath(
				FightActionProviderAdapter.FIGHTACTION_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());


		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item FightAction
	 * @param fightactionsInternalId fightactionsInternal Id
	 * @return number of rows updated
	 */
	public int update(final FightAction item,
							 final int fightactionsInternalId) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		FightActionSQLiteAdapter adapt =
				new FightActionSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item,
				fightactionsInternalId);

		Uri uri = Uri.withAppendedPath(
				FightActionProviderAdapter.FIGHTACTION_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());



		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/** Relations operations. */
	/**
	 * Get associate Fight.
	 * @param item FightAction
	 * @return Fight
	 */
	public Fight getAssociateFight(
			final FightAction item) {
		Fight result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightCursor = prov.query(
				FightProviderAdapter.FIGHT_URI,
				FightSQLiteAdapter.ALIASED_COLS,
				FightSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getFight().getId())},
				null);

		if (fightCursor.getCount() > 0) {
			fightCursor.moveToFirst();
			FightSQLiteAdapter fightAdapt =
					new FightSQLiteAdapter(this.getContext());
			result = fightAdapt.cursorToItem(fightCursor);
		} else {
			result = null;
		}
		fightCursor.close();

		return result;
	}

	/**
	 * Get associate Sender.
	 * @param item FightAction
	 * @return FightPlayer
	 */
	public FightPlayer getAssociateSender(
			final FightAction item) {
		FightPlayer result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightPlayerCursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				FightPlayerSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getSender().getId())},
				null);

		if (fightPlayerCursor.getCount() > 0) {
			fightPlayerCursor.moveToFirst();
			FightPlayerSQLiteAdapter fightPlayerAdapt =
					new FightPlayerSQLiteAdapter(this.getContext());
			result = fightPlayerAdapt.cursorToItem(fightPlayerCursor);
		} else {
			result = null;
		}
		fightPlayerCursor.close();

		return result;
	}

	/**
	 * Get associate Receiver.
	 * @param item FightAction
	 * @return FightPlayer
	 */
	public FightPlayer getAssociateReceiver(
			final FightAction item) {
		FightPlayer result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightPlayerCursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				FightPlayerSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getReceiver().getId())},
				null);

		if (fightPlayerCursor.getCount() > 0) {
			fightPlayerCursor.moveToFirst();
			FightPlayerSQLiteAdapter fightPlayerAdapt =
					new FightPlayerSQLiteAdapter(this.getContext());
			result = fightPlayerAdapt.cursorToItem(fightPlayerCursor);
		} else {
			result = null;
		}
		fightPlayerCursor.close();

		return result;
	}

}
