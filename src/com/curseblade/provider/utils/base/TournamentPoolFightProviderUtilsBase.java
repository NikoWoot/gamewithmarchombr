/**************************************************************************
 * TournamentPoolFightProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.TournamentPoolFightCriterias;
import com.curseblade.criterias.TournamentCriterias;
import com.curseblade.criterias.FightPlayerCriterias;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;

import com.curseblade.entity.TournamentPoolFight;
import com.curseblade.entity.FightPlayer;

import com.curseblade.provider.TournamentPoolFightProviderAdapter;
import com.curseblade.provider.FightPlayerProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * TournamentPoolFight Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class TournamentPoolFightProviderUtilsBase
			extends ProviderUtilsBase<TournamentPoolFight> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "TournamentPoolFightProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public TournamentPoolFightProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final TournamentPoolFight item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		TournamentPoolFightSQLiteAdapter adapt =
				new TournamentPoolFightSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(TournamentPoolFightSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI)
						.withValues(itemValues)
						.build());


		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Insert into DB.
	 * @param item TournamentPoolFight to insert
	 * @param tournamentpoolFightsInternalId tournamentpoolFightsInternal Id
	 * @return number of rows affected
	 */
	public Uri insert(final TournamentPoolFight item,
							 final int tournamentpoolFightsInternalId) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();


		TournamentPoolFightSQLiteAdapter adapt =
				new TournamentPoolFightSQLiteAdapter(this.getContext());
		ContentValues itemValues = adapt.itemToContentValues(item,
					tournamentpoolFightsInternalId);
		itemValues.remove(TournamentPoolFightSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI)
			    	.withValues(itemValues)
			    	.build());



		try {
			ContentProviderResult[] results =
				prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Delete from DB.
	 * @param item TournamentPoolFight
	 * @return number of row affected
	 */
	public int delete(final TournamentPoolFight item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return TournamentPoolFight
	 */
	public TournamentPoolFight query(final int id) {
		TournamentPoolFight result = null;
		TournamentPoolFightSQLiteAdapter adapt =
					new TournamentPoolFightSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		TournamentPoolFightCriterias crits =
				new TournamentPoolFightCriterias(GroupType.AND);
		crits.add(TournamentPoolFightSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
			TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

			if (result.getWinner() != null) {
				result.setWinner(
					this.getAssociateWinner(result));
			}
			if (result.getParentPool() != null) {
				result.setParentPool(
					this.getAssociateParentPool(result));
			}
			if (result.getLeftPool() != null) {
				result.setLeftPool(
					this.getAssociateLeftPool(result));
			}
			if (result.getRightPool() != null) {
				result.setRightPool(
					this.getAssociateRightPool(result));
			}
		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<TournamentPoolFight>
	 */
	public ArrayList<TournamentPoolFight> queryAll() {
		ArrayList<TournamentPoolFight> result =
					new ArrayList<TournamentPoolFight>();
		TournamentPoolFightSQLiteAdapter adapt =
					new TournamentPoolFightSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<TournamentPoolFight>
	 */
	public ArrayList<TournamentPoolFight> query(
				CriteriasBase<TournamentPoolFight> criteria) {
		ArrayList<TournamentPoolFight> result =
					new ArrayList<TournamentPoolFight>();
		TournamentPoolFightSQLiteAdapter adapt =
					new TournamentPoolFightSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item TournamentPoolFight
	 * @return number of rows updated
	 */
	public int update(final TournamentPoolFight item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		TournamentPoolFightSQLiteAdapter adapt =
				new TournamentPoolFightSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(item);

		Uri uri = Uri.withAppendedPath(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());


		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item TournamentPoolFight
	 * @param tournamentpoolFightsInternalId tournamentpoolFightsInternal Id
	 * @return number of rows updated
	 */
	public int update(final TournamentPoolFight item,
							 final int tournamentpoolFightsInternalId) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		TournamentPoolFightSQLiteAdapter adapt =
				new TournamentPoolFightSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item,
				tournamentpoolFightsInternalId);

		Uri uri = Uri.withAppendedPath(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());



		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/** Relations operations. */
	/**
	 * Get associate Winner.
	 * @param item TournamentPoolFight
	 * @return FightPlayer
	 */
	public FightPlayer getAssociateWinner(
			final TournamentPoolFight item) {
		FightPlayer result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightPlayerCursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				FightPlayerSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getWinner().getId())},
				null);

		if (fightPlayerCursor.getCount() > 0) {
			fightPlayerCursor.moveToFirst();
			FightPlayerSQLiteAdapter fightPlayerAdapt =
					new FightPlayerSQLiteAdapter(this.getContext());
			result = fightPlayerAdapt.cursorToItem(fightPlayerCursor);
		} else {
			result = null;
		}
		fightPlayerCursor.close();

		return result;
	}

	/**
	 * Get associate ParentPool.
	 * @param item TournamentPoolFight
	 * @return TournamentPoolFight
	 */
	public TournamentPoolFight getAssociateParentPool(
			final TournamentPoolFight item) {
		TournamentPoolFight result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor tournamentPoolFightCursor = prov.query(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
				TournamentPoolFightSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getParentPool().getId())},
				null);

		if (tournamentPoolFightCursor.getCount() > 0) {
			tournamentPoolFightCursor.moveToFirst();
			TournamentPoolFightSQLiteAdapter tournamentPoolFightAdapt =
					new TournamentPoolFightSQLiteAdapter(this.getContext());
			result = tournamentPoolFightAdapt.cursorToItem(tournamentPoolFightCursor);
		} else {
			result = null;
		}
		tournamentPoolFightCursor.close();

		return result;
	}

	/**
	 * Get associate LeftPool.
	 * @param item TournamentPoolFight
	 * @return TournamentPoolFight
	 */
	public TournamentPoolFight getAssociateLeftPool(
			final TournamentPoolFight item) {
		TournamentPoolFight result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor tournamentPoolFightCursor = prov.query(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
				TournamentPoolFightSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getLeftPool().getId())},
				null);

		if (tournamentPoolFightCursor.getCount() > 0) {
			tournamentPoolFightCursor.moveToFirst();
			TournamentPoolFightSQLiteAdapter tournamentPoolFightAdapt =
					new TournamentPoolFightSQLiteAdapter(this.getContext());
			result = tournamentPoolFightAdapt.cursorToItem(tournamentPoolFightCursor);
		} else {
			result = null;
		}
		tournamentPoolFightCursor.close();

		return result;
	}

	/**
	 * Get associate RightPool.
	 * @param item TournamentPoolFight
	 * @return TournamentPoolFight
	 */
	public TournamentPoolFight getAssociateRightPool(
			final TournamentPoolFight item) {
		TournamentPoolFight result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor tournamentPoolFightCursor = prov.query(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
				TournamentPoolFightSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getRightPool().getId())},
				null);

		if (tournamentPoolFightCursor.getCount() > 0) {
			tournamentPoolFightCursor.moveToFirst();
			TournamentPoolFightSQLiteAdapter tournamentPoolFightAdapt =
					new TournamentPoolFightSQLiteAdapter(this.getContext());
			result = tournamentPoolFightAdapt.cursorToItem(tournamentPoolFightCursor);
		} else {
			result = null;
		}
		tournamentPoolFightCursor.close();

		return result;
	}

}
