/**************************************************************************
 * NewsProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.NewsCriterias;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.NewsSQLiteAdapter;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;

import com.curseblade.entity.News;

import com.curseblade.provider.NewsProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * News Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class NewsProviderUtilsBase
			extends ProviderUtilsBase<News> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "NewsProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public NewsProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final News item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		NewsSQLiteAdapter adapt =
				new NewsSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(HarmonyRssItemSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				NewsProviderAdapter.NEWS_URI)
						.withValues(itemValues)
						.build());


		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}


	/**
	 * Delete from DB.
	 * @param item News
	 * @return number of row affected
	 */
	public int delete(final News item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				NewsProviderAdapter.NEWS_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return News
	 */
	public News query(final int id) {
		News result = null;
		NewsSQLiteAdapter adapt =
					new NewsSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		NewsCriterias crits =
				new NewsCriterias(GroupType.AND);
		crits.add(HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			NewsProviderAdapter.NEWS_URI,
			NewsSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<News>
	 */
	public ArrayList<News> queryAll() {
		ArrayList<News> result =
					new ArrayList<News>();
		NewsSQLiteAdapter adapt =
					new NewsSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				NewsProviderAdapter.NEWS_URI,
				NewsSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<News>
	 */
	public ArrayList<News> query(
				CriteriasBase<News> criteria) {
		ArrayList<News> result =
					new ArrayList<News>();
		NewsSQLiteAdapter adapt =
					new NewsSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				NewsProviderAdapter.NEWS_URI,
				NewsSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item News
	 
	 * @return number of rows updated
	 */
	public int update(final News item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		NewsSQLiteAdapter adapt =
				new NewsSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item);

		Uri uri = Uri.withAppendedPath(
				NewsProviderAdapter.NEWS_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());



		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	
}
