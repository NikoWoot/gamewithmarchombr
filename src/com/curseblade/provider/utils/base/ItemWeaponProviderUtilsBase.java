/**************************************************************************
 * ItemWeaponProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.ItemWeaponCriterias;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.ItemWeaponSQLiteAdapter;

import com.curseblade.entity.ItemWeapon;
import com.curseblade.entity.ItemWeaponType;

import com.curseblade.provider.ItemWeaponProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * ItemWeapon Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class ItemWeaponProviderUtilsBase
			extends ProviderUtilsBase<ItemWeapon> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "ItemWeaponProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public ItemWeaponProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final ItemWeapon item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		ItemWeaponSQLiteAdapter adapt =
				new ItemWeaponSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(ItemWeaponSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				ItemWeaponProviderAdapter.ITEMWEAPON_URI)
						.withValues(itemValues)
						.build());


		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}


	/**
	 * Delete from DB.
	 * @param item ItemWeapon
	 * @return number of row affected
	 */
	public int delete(final ItemWeapon item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				ItemWeaponProviderAdapter.ITEMWEAPON_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return ItemWeapon
	 */
	public ItemWeapon query(final int id) {
		ItemWeapon result = null;
		ItemWeaponSQLiteAdapter adapt =
					new ItemWeaponSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		ItemWeaponCriterias crits =
				new ItemWeaponCriterias(GroupType.AND);
		crits.add(ItemWeaponSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			ItemWeaponProviderAdapter.ITEMWEAPON_URI,
			ItemWeaponSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<ItemWeapon>
	 */
	public ArrayList<ItemWeapon> queryAll() {
		ArrayList<ItemWeapon> result =
					new ArrayList<ItemWeapon>();
		ItemWeaponSQLiteAdapter adapt =
					new ItemWeaponSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				ItemWeaponProviderAdapter.ITEMWEAPON_URI,
				ItemWeaponSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<ItemWeapon>
	 */
	public ArrayList<ItemWeapon> query(
				CriteriasBase<ItemWeapon> criteria) {
		ArrayList<ItemWeapon> result =
					new ArrayList<ItemWeapon>();
		ItemWeaponSQLiteAdapter adapt =
					new ItemWeaponSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				ItemWeaponProviderAdapter.ITEMWEAPON_URI,
				ItemWeaponSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item ItemWeapon
	 
	 * @return number of rows updated
	 */
	public int update(final ItemWeapon item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ItemWeaponSQLiteAdapter adapt =
				new ItemWeaponSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item);

		Uri uri = Uri.withAppendedPath(
				ItemWeaponProviderAdapter.ITEMWEAPON_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());



		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	
}
