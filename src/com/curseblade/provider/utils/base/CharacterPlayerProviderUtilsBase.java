/**************************************************************************
 * CharacterPlayerProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.CharacterPlayerCriterias;
import com.curseblade.criterias.ItemArmuryCriterias;
import com.curseblade.criterias.SpellCriterias;
import com.curseblade.criterias.ItemWeaponCriterias;
import com.curseblade.criterias.base.Criteria;
import com.curseblade.criterias.base.Criteria.Type;
import com.curseblade.criterias.base.value.ArrayValue;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.ItemArmurySQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;
import com.curseblade.data.ItemWeaponSQLiteAdapter;

import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemArmury;
import com.curseblade.entity.Spell;
import com.curseblade.entity.ItemWeapon;

import com.curseblade.provider.CharacterPlayerProviderAdapter;
import com.curseblade.provider.ItemArmuryProviderAdapter;
import com.curseblade.provider.SpellProviderAdapter;
import com.curseblade.provider.ItemWeaponProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * CharacterPlayer Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class CharacterPlayerProviderUtilsBase
			extends ProviderUtilsBase<CharacterPlayer> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "CharacterPlayerProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public CharacterPlayerProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final CharacterPlayer item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		CharacterPlayerSQLiteAdapter adapt =
				new CharacterPlayerSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(CharacterPlayerSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI)
						.withValues(itemValues)
						.build());

		if (item.getArmuryEquipedItems() != null && item.getArmuryEquipedItems().size() > 0) {
			String armuryEquipedItemsSelection = ItemArmurySQLiteAdapter.COL_ID + " IN (";
			String[] armuryEquipedItemsSelectionArgs = new String[item.getArmuryEquipedItems().size()];
			for (int i = 0; i < item.getArmuryEquipedItems().size(); i++) {
				armuryEquipedItemsSelectionArgs[i] = String.valueOf(item.getArmuryEquipedItems().get(i).getId());
				armuryEquipedItemsSelection += "? ";
				if (i != item.getArmuryEquipedItems().size() - 1) {
					 armuryEquipedItemsSelection += ", ";
				}
			}
			armuryEquipedItemsSelection += ")";

			operations.add(ContentProviderOperation.newUpdate(ItemArmuryProviderAdapter.ITEMARMURY_URI)
					.withValueBackReference(
							ItemArmurySQLiteAdapter
									.COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL,
							0)
					.withSelection(armuryEquipedItemsSelection, armuryEquipedItemsSelectionArgs)
					.build());
		}
		if (item.getEquipedSpells() != null && item.getEquipedSpells().size() > 0) {
			String equipedSpellsSelection = SpellSQLiteAdapter.COL_ID + " IN (";
			String[] equipedSpellsSelectionArgs = new String[item.getEquipedSpells().size()];
			for (int i = 0; i < item.getEquipedSpells().size(); i++) {
				equipedSpellsSelectionArgs[i] = String.valueOf(item.getEquipedSpells().get(i).getId());
				equipedSpellsSelection += "? ";
				if (i != item.getEquipedSpells().size() - 1) {
					 equipedSpellsSelection += ", ";
				}
			}
			equipedSpellsSelection += ")";

			operations.add(ContentProviderOperation.newUpdate(SpellProviderAdapter.SPELL_URI)
					.withValueBackReference(
							SpellSQLiteAdapter
									.COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL,
							0)
					.withSelection(equipedSpellsSelection, equipedSpellsSelectionArgs)
					.build());
		}

		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}


	/**
	 * Delete from DB.
	 * @param item CharacterPlayer
	 * @return number of row affected
	 */
	public int delete(final CharacterPlayer item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return CharacterPlayer
	 */
	public CharacterPlayer query(final int id) {
		CharacterPlayer result = null;
		CharacterPlayerSQLiteAdapter adapt =
					new CharacterPlayerSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		CharacterPlayerCriterias crits =
				new CharacterPlayerCriterias(GroupType.AND);
		crits.add(CharacterPlayerSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI,
			CharacterPlayerSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

			result.setArmuryEquipedItems(
				this.getAssociateArmuryEquipedItems(result));
			result.setEquipedSpells(
				this.getAssociateEquipedSpells(result));
			if (result.getWeaponUsed() != null) {
				result.setWeaponUsed(
					this.getAssociateWeaponUsed(result));
			}
		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<CharacterPlayer>
	 */
	public ArrayList<CharacterPlayer> queryAll() {
		ArrayList<CharacterPlayer> result =
					new ArrayList<CharacterPlayer>();
		CharacterPlayerSQLiteAdapter adapt =
					new CharacterPlayerSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI,
				CharacterPlayerSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<CharacterPlayer>
	 */
	public ArrayList<CharacterPlayer> query(
				CriteriasBase<CharacterPlayer> criteria) {
		ArrayList<CharacterPlayer> result =
					new ArrayList<CharacterPlayer>();
		CharacterPlayerSQLiteAdapter adapt =
					new CharacterPlayerSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI,
				CharacterPlayerSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item CharacterPlayer
	 
	 * @return number of rows updated
	 */
	public int update(final CharacterPlayer item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		CharacterPlayerSQLiteAdapter adapt =
				new CharacterPlayerSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item);

		Uri uri = Uri.withAppendedPath(
				CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());


		if (item.getArmuryEquipedItems() != null && item.getArmuryEquipedItems().size() > 0) {
			// Set new armuryEquipedItems for CharacterPlayer
			ItemArmuryCriterias armuryEquipedItemsCrit =
						new ItemArmuryCriterias(GroupType.AND);
			Criteria crit = new Criteria();
			ArrayValue values = new ArrayValue();
			crit.setType(Type.IN);
			crit.setKey(ItemArmurySQLiteAdapter.COL_ID);
			crit.addValue(values);
			armuryEquipedItemsCrit.add(crit);


			for (int i = 0; i < item.getArmuryEquipedItems().size(); i++) {
				values.addValue(String.valueOf(
						item.getArmuryEquipedItems().get(i).getId()));
			}

			operations.add(ContentProviderOperation.newUpdate(
					ItemArmuryProviderAdapter.ITEMARMURY_URI)
						.withValue(
								ItemArmurySQLiteAdapter
										.COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL,
								item.getId())
					.withSelection(
							armuryEquipedItemsCrit.toSQLiteSelection(),
							armuryEquipedItemsCrit.toSQLiteSelectionArgs())
					.build());

			// Remove old associated armuryEquipedItems
			crit.setType(Type.NOT_IN);
			armuryEquipedItemsCrit.add(ItemArmurySQLiteAdapter.COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL,
					String.valueOf(item.getId()),
					Type.EQUALS);
			

			operations.add(ContentProviderOperation.newUpdate(
					ItemArmuryProviderAdapter.ITEMARMURY_URI)
						.withValue(
								ItemArmurySQLiteAdapter
										.COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL,
								null)
					.withSelection(
							armuryEquipedItemsCrit.toSQLiteSelection(),
							armuryEquipedItemsCrit.toSQLiteSelectionArgs())
					.build());
		}

		if (item.getEquipedSpells() != null && item.getEquipedSpells().size() > 0) {
			// Set new equipedSpells for CharacterPlayer
			SpellCriterias equipedSpellsCrit =
						new SpellCriterias(GroupType.AND);
			Criteria crit = new Criteria();
			ArrayValue values = new ArrayValue();
			crit.setType(Type.IN);
			crit.setKey(SpellSQLiteAdapter.COL_ID);
			crit.addValue(values);
			equipedSpellsCrit.add(crit);


			for (int i = 0; i < item.getEquipedSpells().size(); i++) {
				values.addValue(String.valueOf(
						item.getEquipedSpells().get(i).getId()));
			}

			operations.add(ContentProviderOperation.newUpdate(
					SpellProviderAdapter.SPELL_URI)
						.withValue(
								SpellSQLiteAdapter
										.COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL,
								item.getId())
					.withSelection(
							equipedSpellsCrit.toSQLiteSelection(),
							equipedSpellsCrit.toSQLiteSelectionArgs())
					.build());

			// Remove old associated equipedSpells
			crit.setType(Type.NOT_IN);
			equipedSpellsCrit.add(SpellSQLiteAdapter.COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL,
					String.valueOf(item.getId()),
					Type.EQUALS);
			

			operations.add(ContentProviderOperation.newUpdate(
					SpellProviderAdapter.SPELL_URI)
						.withValue(
								SpellSQLiteAdapter
										.COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL,
								null)
					.withSelection(
							equipedSpellsCrit.toSQLiteSelection(),
							equipedSpellsCrit.toSQLiteSelectionArgs())
					.build());
		}


		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/** Relations operations. */
	/**
	 * Get associate ArmuryEquipedItems.
	 * @param item CharacterPlayer
	 * @return ItemArmury
	 */
	public ArrayList<ItemArmury> getAssociateArmuryEquipedItems(
			final CharacterPlayer item) {
		ArrayList<ItemArmury> result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor itemArmuryCursor = prov.query(
				ItemArmuryProviderAdapter.ITEMARMURY_URI,
				ItemArmurySQLiteAdapter.ALIASED_COLS,
				ItemArmurySQLiteAdapter.COL_CHARACTERPLAYERARMURYEQUIPEDITEMSINTERNAL
						+ "= ?",
				new String[]{String.valueOf(item.getId())},
				null);

		ItemArmurySQLiteAdapter itemArmuryAdapt =
				new ItemArmurySQLiteAdapter(this.getContext());
		result = itemArmuryAdapt.cursorToItems(
						itemArmuryCursor);
		itemArmuryCursor.close();

		return result;
	}

	/**
	 * Get associate EquipedSpells.
	 * @param item CharacterPlayer
	 * @return Spell
	 */
	public ArrayList<Spell> getAssociateEquipedSpells(
			final CharacterPlayer item) {
		ArrayList<Spell> result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor spellCursor = prov.query(
				SpellProviderAdapter.SPELL_URI,
				SpellSQLiteAdapter.ALIASED_COLS,
				SpellSQLiteAdapter.COL_CHARACTERPLAYEREQUIPEDSPELLSINTERNAL
						+ "= ?",
				new String[]{String.valueOf(item.getId())},
				null);

		SpellSQLiteAdapter spellAdapt =
				new SpellSQLiteAdapter(this.getContext());
		result = spellAdapt.cursorToItems(
						spellCursor);
		spellCursor.close();

		return result;
	}

	/**
	 * Get associate WeaponUsed.
	 * @param item CharacterPlayer
	 * @return ItemWeapon
	 */
	public ItemWeapon getAssociateWeaponUsed(
			final CharacterPlayer item) {
		ItemWeapon result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor itemWeaponCursor = prov.query(
				ItemWeaponProviderAdapter.ITEMWEAPON_URI,
				ItemWeaponSQLiteAdapter.ALIASED_COLS,
				ItemWeaponSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getWeaponUsed().getId())},
				null);

		if (itemWeaponCursor.getCount() > 0) {
			itemWeaponCursor.moveToFirst();
			ItemWeaponSQLiteAdapter itemWeaponAdapt =
					new ItemWeaponSQLiteAdapter(this.getContext());
			result = itemWeaponAdapt.cursorToItem(itemWeaponCursor);
		} else {
			result = null;
		}
		itemWeaponCursor.close();

		return result;
	}

}
