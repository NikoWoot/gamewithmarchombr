/**************************************************************************
 * FightProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.FightCriterias;
import com.curseblade.criterias.FightPlayerCriterias;
import com.curseblade.criterias.FightActionCriterias;
import com.curseblade.criterias.base.Criteria;
import com.curseblade.criterias.base.Criteria.Type;
import com.curseblade.criterias.base.value.ArrayValue;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.data.FightActionSQLiteAdapter;

import com.curseblade.entity.Fight;
import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.FightAction;

import com.curseblade.provider.FightProviderAdapter;
import com.curseblade.provider.FightPlayerProviderAdapter;
import com.curseblade.provider.FightActionProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * Fight Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class FightProviderUtilsBase
			extends ProviderUtilsBase<Fight> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "FightProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public FightProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final Fight item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		FightSQLiteAdapter adapt =
				new FightSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(FightSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				FightProviderAdapter.FIGHT_URI)
						.withValues(itemValues)
						.build());

		if (item.getStarterFighters() != null && item.getStarterFighters().size() > 0) {
			String starterFightersSelection = FightPlayerSQLiteAdapter.COL_ID + " IN (";
			String[] starterFightersSelectionArgs = new String[item.getStarterFighters().size()];
			for (int i = 0; i < item.getStarterFighters().size(); i++) {
				starterFightersSelectionArgs[i] = String.valueOf(item.getStarterFighters().get(i).getId());
				starterFightersSelection += "? ";
				if (i != item.getStarterFighters().size() - 1) {
					 starterFightersSelection += ", ";
				}
			}
			starterFightersSelection += ")";

			operations.add(ContentProviderOperation.newUpdate(FightPlayerProviderAdapter.FIGHTPLAYER_URI)
					.withValueBackReference(
							FightPlayerSQLiteAdapter
									.COL_FIGHT,
							0)
					.withSelection(starterFightersSelection, starterFightersSelectionArgs)
					.build());
		}
		if (item.getAlternatedFighters() != null && item.getAlternatedFighters().size() > 0) {
			String alternatedFightersSelection = FightPlayerSQLiteAdapter.COL_ID + " IN (";
			String[] alternatedFightersSelectionArgs = new String[item.getAlternatedFighters().size()];
			for (int i = 0; i < item.getAlternatedFighters().size(); i++) {
				alternatedFightersSelectionArgs[i] = String.valueOf(item.getAlternatedFighters().get(i).getId());
				alternatedFightersSelection += "? ";
				if (i != item.getAlternatedFighters().size() - 1) {
					 alternatedFightersSelection += ", ";
				}
			}
			alternatedFightersSelection += ")";

			operations.add(ContentProviderOperation.newUpdate(FightPlayerProviderAdapter.FIGHTPLAYER_URI)
					.withValueBackReference(
							FightPlayerSQLiteAdapter
									.COL_FIGHT,
							0)
					.withSelection(alternatedFightersSelection, alternatedFightersSelectionArgs)
					.build());
		}
		if (item.getSurvivorFighters() != null && item.getSurvivorFighters().size() > 0) {
			String survivorFightersSelection = FightPlayerSQLiteAdapter.COL_ID + " IN (";
			String[] survivorFightersSelectionArgs = new String[item.getSurvivorFighters().size()];
			for (int i = 0; i < item.getSurvivorFighters().size(); i++) {
				survivorFightersSelectionArgs[i] = String.valueOf(item.getSurvivorFighters().get(i).getId());
				survivorFightersSelection += "? ";
				if (i != item.getSurvivorFighters().size() - 1) {
					 survivorFightersSelection += ", ";
				}
			}
			survivorFightersSelection += ")";

			operations.add(ContentProviderOperation.newUpdate(FightPlayerProviderAdapter.FIGHTPLAYER_URI)
					.withValueBackReference(
							FightPlayerSQLiteAdapter
									.COL_FIGHT,
							0)
					.withSelection(survivorFightersSelection, survivorFightersSelectionArgs)
					.build());
		}
		if (item.getActions() != null && item.getActions().size() > 0) {
			String actionsSelection = FightActionSQLiteAdapter.COL_ID + " IN (";
			String[] actionsSelectionArgs = new String[item.getActions().size()];
			for (int i = 0; i < item.getActions().size(); i++) {
				actionsSelectionArgs[i] = String.valueOf(item.getActions().get(i).getId());
				actionsSelection += "? ";
				if (i != item.getActions().size() - 1) {
					 actionsSelection += ", ";
				}
			}
			actionsSelection += ")";

			operations.add(ContentProviderOperation.newUpdate(FightActionProviderAdapter.FIGHTACTION_URI)
					.withValueBackReference(
							FightActionSQLiteAdapter
									.COL_FIGHTACTIONSINTERNAL,
							0)
					.withSelection(actionsSelection, actionsSelectionArgs)
					.build());
		}

		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}


	/**
	 * Delete from DB.
	 * @param item Fight
	 * @return number of row affected
	 */
	public int delete(final Fight item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				FightProviderAdapter.FIGHT_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return Fight
	 */
	public Fight query(final int id) {
		Fight result = null;
		FightSQLiteAdapter adapt =
					new FightSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		FightCriterias crits =
				new FightCriterias(GroupType.AND);
		crits.add(FightSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			FightProviderAdapter.FIGHT_URI,
			FightSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

			result.setStarterFighters(
				this.getAssociateStarterFighters(result));
			result.setAlternatedFighters(
				this.getAssociateAlternatedFighters(result));
			result.setSurvivorFighters(
				this.getAssociateSurvivorFighters(result));
			result.setActions(
				this.getAssociateActions(result));
			if (result.getSenderPlayer() != null) {
				result.setSenderPlayer(
					this.getAssociateSenderPlayer(result));
			}
			if (result.getReceiverPlayer() != null) {
				result.setReceiverPlayer(
					this.getAssociateReceiverPlayer(result));
			}
		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<Fight>
	 */
	public ArrayList<Fight> queryAll() {
		ArrayList<Fight> result =
					new ArrayList<Fight>();
		FightSQLiteAdapter adapt =
					new FightSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				FightProviderAdapter.FIGHT_URI,
				FightSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<Fight>
	 */
	public ArrayList<Fight> query(
				CriteriasBase<Fight> criteria) {
		ArrayList<Fight> result =
					new ArrayList<Fight>();
		FightSQLiteAdapter adapt =
					new FightSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				FightProviderAdapter.FIGHT_URI,
				FightSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item Fight
	 
	 * @return number of rows updated
	 */
	public int update(final Fight item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		FightSQLiteAdapter adapt =
				new FightSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item);

		Uri uri = Uri.withAppendedPath(
				FightProviderAdapter.FIGHT_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());


		if (item.getStarterFighters() != null && item.getStarterFighters().size() > 0) {
			// Set new starterFighters for Fight
			FightPlayerCriterias starterFightersCrit =
						new FightPlayerCriterias(GroupType.AND);
			Criteria crit = new Criteria();
			ArrayValue values = new ArrayValue();
			crit.setType(Type.IN);
			crit.setKey(FightPlayerSQLiteAdapter.COL_ID);
			crit.addValue(values);
			starterFightersCrit.add(crit);


			for (int i = 0; i < item.getStarterFighters().size(); i++) {
				values.addValue(String.valueOf(
						item.getStarterFighters().get(i).getId()));
			}

			operations.add(ContentProviderOperation.newUpdate(
					FightPlayerProviderAdapter.FIGHTPLAYER_URI)
						.withValue(
								FightPlayerSQLiteAdapter
										.COL_FIGHT,
								item.getId())
					.withSelection(
							starterFightersCrit.toSQLiteSelection(),
							starterFightersCrit.toSQLiteSelectionArgs())
					.build());

			// Remove old associated starterFighters
			crit.setType(Type.NOT_IN);
			starterFightersCrit.add(FightPlayerSQLiteAdapter.COL_FIGHT,
					String.valueOf(item.getId()),
					Type.EQUALS);
			

			operations.add(ContentProviderOperation.newUpdate(
					FightPlayerProviderAdapter.FIGHTPLAYER_URI)
						.withValue(
								FightPlayerSQLiteAdapter
										.COL_FIGHT,
								null)
					.withSelection(
							starterFightersCrit.toSQLiteSelection(),
							starterFightersCrit.toSQLiteSelectionArgs())
					.build());
		}

		if (item.getAlternatedFighters() != null && item.getAlternatedFighters().size() > 0) {
			// Set new alternatedFighters for Fight
			FightPlayerCriterias alternatedFightersCrit =
						new FightPlayerCriterias(GroupType.AND);
			Criteria crit = new Criteria();
			ArrayValue values = new ArrayValue();
			crit.setType(Type.IN);
			crit.setKey(FightPlayerSQLiteAdapter.COL_ID);
			crit.addValue(values);
			alternatedFightersCrit.add(crit);


			for (int i = 0; i < item.getAlternatedFighters().size(); i++) {
				values.addValue(String.valueOf(
						item.getAlternatedFighters().get(i).getId()));
			}

			operations.add(ContentProviderOperation.newUpdate(
					FightPlayerProviderAdapter.FIGHTPLAYER_URI)
						.withValue(
								FightPlayerSQLiteAdapter
										.COL_FIGHT,
								item.getId())
					.withSelection(
							alternatedFightersCrit.toSQLiteSelection(),
							alternatedFightersCrit.toSQLiteSelectionArgs())
					.build());

			// Remove old associated alternatedFighters
			crit.setType(Type.NOT_IN);
			alternatedFightersCrit.add(FightPlayerSQLiteAdapter.COL_FIGHT,
					String.valueOf(item.getId()),
					Type.EQUALS);
			

			operations.add(ContentProviderOperation.newUpdate(
					FightPlayerProviderAdapter.FIGHTPLAYER_URI)
						.withValue(
								FightPlayerSQLiteAdapter
										.COL_FIGHT,
								null)
					.withSelection(
							alternatedFightersCrit.toSQLiteSelection(),
							alternatedFightersCrit.toSQLiteSelectionArgs())
					.build());
		}

		if (item.getSurvivorFighters() != null && item.getSurvivorFighters().size() > 0) {
			// Set new survivorFighters for Fight
			FightPlayerCriterias survivorFightersCrit =
						new FightPlayerCriterias(GroupType.AND);
			Criteria crit = new Criteria();
			ArrayValue values = new ArrayValue();
			crit.setType(Type.IN);
			crit.setKey(FightPlayerSQLiteAdapter.COL_ID);
			crit.addValue(values);
			survivorFightersCrit.add(crit);


			for (int i = 0; i < item.getSurvivorFighters().size(); i++) {
				values.addValue(String.valueOf(
						item.getSurvivorFighters().get(i).getId()));
			}

			operations.add(ContentProviderOperation.newUpdate(
					FightPlayerProviderAdapter.FIGHTPLAYER_URI)
						.withValue(
								FightPlayerSQLiteAdapter
										.COL_FIGHT,
								item.getId())
					.withSelection(
							survivorFightersCrit.toSQLiteSelection(),
							survivorFightersCrit.toSQLiteSelectionArgs())
					.build());

			// Remove old associated survivorFighters
			crit.setType(Type.NOT_IN);
			survivorFightersCrit.add(FightPlayerSQLiteAdapter.COL_FIGHT,
					String.valueOf(item.getId()),
					Type.EQUALS);
			

			operations.add(ContentProviderOperation.newUpdate(
					FightPlayerProviderAdapter.FIGHTPLAYER_URI)
						.withValue(
								FightPlayerSQLiteAdapter
										.COL_FIGHT,
								null)
					.withSelection(
							survivorFightersCrit.toSQLiteSelection(),
							survivorFightersCrit.toSQLiteSelectionArgs())
					.build());
		}

		if (item.getActions() != null && item.getActions().size() > 0) {
			// Set new actions for Fight
			FightActionCriterias actionsCrit =
						new FightActionCriterias(GroupType.AND);
			Criteria crit = new Criteria();
			ArrayValue values = new ArrayValue();
			crit.setType(Type.IN);
			crit.setKey(FightActionSQLiteAdapter.COL_ID);
			crit.addValue(values);
			actionsCrit.add(crit);


			for (int i = 0; i < item.getActions().size(); i++) {
				values.addValue(String.valueOf(
						item.getActions().get(i).getId()));
			}

			operations.add(ContentProviderOperation.newUpdate(
					FightActionProviderAdapter.FIGHTACTION_URI)
						.withValue(
								FightActionSQLiteAdapter
										.COL_FIGHTACTIONSINTERNAL,
								item.getId())
					.withSelection(
							actionsCrit.toSQLiteSelection(),
							actionsCrit.toSQLiteSelectionArgs())
					.build());

			// Remove old associated actions
			crit.setType(Type.NOT_IN);
			actionsCrit.add(FightActionSQLiteAdapter.COL_FIGHTACTIONSINTERNAL,
					String.valueOf(item.getId()),
					Type.EQUALS);
			

			operations.add(ContentProviderOperation.newUpdate(
					FightActionProviderAdapter.FIGHTACTION_URI)
						.withValue(
								FightActionSQLiteAdapter
										.COL_FIGHTACTIONSINTERNAL,
								null)
					.withSelection(
							actionsCrit.toSQLiteSelection(),
							actionsCrit.toSQLiteSelectionArgs())
					.build());
		}


		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/** Relations operations. */
	/**
	 * Get associate StarterFighters.
	 * @param item Fight
	 * @return FightPlayer
	 */
	public ArrayList<FightPlayer> getAssociateStarterFighters(
			final Fight item) {
		ArrayList<FightPlayer> result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightPlayerCursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				FightPlayerSQLiteAdapter.COL_FIGHT
						+ "= ?",
				new String[]{String.valueOf(item.getId())},
				null);

		FightPlayerSQLiteAdapter fightPlayerAdapt =
				new FightPlayerSQLiteAdapter(this.getContext());
		result = fightPlayerAdapt.cursorToItems(
						fightPlayerCursor);
		fightPlayerCursor.close();

		return result;
	}

	/**
	 * Get associate AlternatedFighters.
	 * @param item Fight
	 * @return FightPlayer
	 */
	public ArrayList<FightPlayer> getAssociateAlternatedFighters(
			final Fight item) {
		ArrayList<FightPlayer> result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightPlayerCursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				FightPlayerSQLiteAdapter.COL_FIGHT
						+ "= ?",
				new String[]{String.valueOf(item.getId())},
				null);

		FightPlayerSQLiteAdapter fightPlayerAdapt =
				new FightPlayerSQLiteAdapter(this.getContext());
		result = fightPlayerAdapt.cursorToItems(
						fightPlayerCursor);
		fightPlayerCursor.close();

		return result;
	}

	/**
	 * Get associate SurvivorFighters.
	 * @param item Fight
	 * @return FightPlayer
	 */
	public ArrayList<FightPlayer> getAssociateSurvivorFighters(
			final Fight item) {
		ArrayList<FightPlayer> result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightPlayerCursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				FightPlayerSQLiteAdapter.COL_FIGHT
						+ "= ?",
				new String[]{String.valueOf(item.getId())},
				null);

		FightPlayerSQLiteAdapter fightPlayerAdapt =
				new FightPlayerSQLiteAdapter(this.getContext());
		result = fightPlayerAdapt.cursorToItems(
						fightPlayerCursor);
		fightPlayerCursor.close();

		return result;
	}

	/**
	 * Get associate Actions.
	 * @param item Fight
	 * @return FightAction
	 */
	public ArrayList<FightAction> getAssociateActions(
			final Fight item) {
		ArrayList<FightAction> result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightActionCursor = prov.query(
				FightActionProviderAdapter.FIGHTACTION_URI,
				FightActionSQLiteAdapter.ALIASED_COLS,
				FightActionSQLiteAdapter.COL_FIGHTACTIONSINTERNAL
						+ "= ?",
				new String[]{String.valueOf(item.getId())},
				null);

		FightActionSQLiteAdapter fightActionAdapt =
				new FightActionSQLiteAdapter(this.getContext());
		result = fightActionAdapt.cursorToItems(
						fightActionCursor);
		fightActionCursor.close();

		return result;
	}

	/**
	 * Get associate SenderPlayer.
	 * @param item Fight
	 * @return FightPlayer
	 */
	public FightPlayer getAssociateSenderPlayer(
			final Fight item) {
		FightPlayer result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightPlayerCursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				FightPlayerSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getSenderPlayer().getId())},
				null);

		if (fightPlayerCursor.getCount() > 0) {
			fightPlayerCursor.moveToFirst();
			FightPlayerSQLiteAdapter fightPlayerAdapt =
					new FightPlayerSQLiteAdapter(this.getContext());
			result = fightPlayerAdapt.cursorToItem(fightPlayerCursor);
		} else {
			result = null;
		}
		fightPlayerCursor.close();

		return result;
	}

	/**
	 * Get associate ReceiverPlayer.
	 * @param item Fight
	 * @return FightPlayer
	 */
	public FightPlayer getAssociateReceiverPlayer(
			final Fight item) {
		FightPlayer result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightPlayerCursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				FightPlayerSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getReceiverPlayer().getId())},
				null);

		if (fightPlayerCursor.getCount() > 0) {
			fightPlayerCursor.moveToFirst();
			FightPlayerSQLiteAdapter fightPlayerAdapt =
					new FightPlayerSQLiteAdapter(this.getContext());
			result = fightPlayerAdapt.cursorToItem(fightPlayerCursor);
		} else {
			result = null;
		}
		fightPlayerCursor.close();

		return result;
	}

}
