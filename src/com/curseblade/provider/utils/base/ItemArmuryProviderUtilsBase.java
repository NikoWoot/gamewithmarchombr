/**************************************************************************
 * ItemArmuryProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.ItemArmuryCriterias;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.ItemArmurySQLiteAdapter;

import com.curseblade.entity.ItemArmury;

import com.curseblade.provider.ItemArmuryProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * ItemArmury Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class ItemArmuryProviderUtilsBase
			extends ProviderUtilsBase<ItemArmury> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "ItemArmuryProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public ItemArmuryProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final ItemArmury item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		ItemArmurySQLiteAdapter adapt =
				new ItemArmurySQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(ItemArmurySQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				ItemArmuryProviderAdapter.ITEMARMURY_URI)
						.withValues(itemValues)
						.build());


		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Insert into DB.
	 * @param item ItemArmury to insert
	 * @param characterPlayerarmuryEquipedItemsInternalId characterPlayerarmuryEquipedItemsInternal Id
	 * @return number of rows affected
	 */
	public Uri insert(final ItemArmury item,
							 final int characterPlayerarmuryEquipedItemsInternalId) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();


		ItemArmurySQLiteAdapter adapt =
				new ItemArmurySQLiteAdapter(this.getContext());
		ContentValues itemValues = adapt.itemToContentValues(item,
					characterPlayerarmuryEquipedItemsInternalId);
		itemValues.remove(ItemArmurySQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				ItemArmuryProviderAdapter.ITEMARMURY_URI)
			    	.withValues(itemValues)
			    	.build());



		try {
			ContentProviderResult[] results =
				prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Delete from DB.
	 * @param item ItemArmury
	 * @return number of row affected
	 */
	public int delete(final ItemArmury item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				ItemArmuryProviderAdapter.ITEMARMURY_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return ItemArmury
	 */
	public ItemArmury query(final int id) {
		ItemArmury result = null;
		ItemArmurySQLiteAdapter adapt =
					new ItemArmurySQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		ItemArmuryCriterias crits =
				new ItemArmuryCriterias(GroupType.AND);
		crits.add(ItemArmurySQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			ItemArmuryProviderAdapter.ITEMARMURY_URI,
			ItemArmurySQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<ItemArmury>
	 */
	public ArrayList<ItemArmury> queryAll() {
		ArrayList<ItemArmury> result =
					new ArrayList<ItemArmury>();
		ItemArmurySQLiteAdapter adapt =
					new ItemArmurySQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				ItemArmuryProviderAdapter.ITEMARMURY_URI,
				ItemArmurySQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<ItemArmury>
	 */
	public ArrayList<ItemArmury> query(
				CriteriasBase<ItemArmury> criteria) {
		ArrayList<ItemArmury> result =
					new ArrayList<ItemArmury>();
		ItemArmurySQLiteAdapter adapt =
					new ItemArmurySQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				ItemArmuryProviderAdapter.ITEMARMURY_URI,
				ItemArmurySQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item ItemArmury
	 * @return number of rows updated
	 */
	public int update(final ItemArmury item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ItemArmurySQLiteAdapter adapt =
				new ItemArmurySQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(item);

		Uri uri = Uri.withAppendedPath(
				ItemArmuryProviderAdapter.ITEMARMURY_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());


		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item ItemArmury
	 * @param characterPlayerarmuryEquipedItemsInternalId characterPlayerarmuryEquipedItemsInternal Id
	 * @return number of rows updated
	 */
	public int update(final ItemArmury item,
							 final int characterPlayerarmuryEquipedItemsInternalId) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ItemArmurySQLiteAdapter adapt =
				new ItemArmurySQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item,
				characterPlayerarmuryEquipedItemsInternalId);

		Uri uri = Uri.withAppendedPath(
				ItemArmuryProviderAdapter.ITEMARMURY_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());



		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/** Relations operations. */
}
