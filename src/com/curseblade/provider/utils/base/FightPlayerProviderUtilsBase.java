/**************************************************************************
 * FightPlayerProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.FightPlayerCriterias;
import com.curseblade.criterias.FightCriterias;
import com.curseblade.criterias.CharacterPlayerCriterias;
import com.curseblade.criterias.ItemWeaponCriterias;
import com.curseblade.criterias.SpellCriterias;
import com.curseblade.criterias.base.Criteria;
import com.curseblade.criterias.base.Criteria.Type;
import com.curseblade.criterias.base.value.ArrayValue;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.FightPlayerSQLiteAdapter;
import com.curseblade.data.FightSQLiteAdapter;
import com.curseblade.data.CharacterPlayerSQLiteAdapter;
import com.curseblade.data.ItemWeaponSQLiteAdapter;
import com.curseblade.data.SpellSQLiteAdapter;

import com.curseblade.entity.FightPlayer;
import com.curseblade.entity.Fight;
import com.curseblade.entity.CharacterPlayer;
import com.curseblade.entity.ItemWeapon;
import com.curseblade.entity.Spell;

import com.curseblade.provider.FightPlayerProviderAdapter;
import com.curseblade.provider.FightProviderAdapter;
import com.curseblade.provider.CharacterPlayerProviderAdapter;
import com.curseblade.provider.ItemWeaponProviderAdapter;
import com.curseblade.provider.SpellProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * FightPlayer Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class FightPlayerProviderUtilsBase
			extends ProviderUtilsBase<FightPlayer> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "FightPlayerProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public FightPlayerProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final FightPlayer item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		FightPlayerSQLiteAdapter adapt =
				new FightPlayerSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(FightPlayerSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI)
						.withValues(itemValues)
						.build());

		if (item.getBaseSpells() != null && item.getBaseSpells().size() > 0) {
			String baseSpellsSelection = SpellSQLiteAdapter.COL_ID + " IN (";
			String[] baseSpellsSelectionArgs = new String[item.getBaseSpells().size()];
			for (int i = 0; i < item.getBaseSpells().size(); i++) {
				baseSpellsSelectionArgs[i] = String.valueOf(item.getBaseSpells().get(i).getId());
				baseSpellsSelection += "? ";
				if (i != item.getBaseSpells().size() - 1) {
					 baseSpellsSelection += ", ";
				}
			}
			baseSpellsSelection += ")";

			operations.add(ContentProviderOperation.newUpdate(SpellProviderAdapter.SPELL_URI)
					.withValueBackReference(
							SpellSQLiteAdapter
									.COL_FIGHTPLAYERBASESPELLSINTERNAL,
							0)
					.withSelection(baseSpellsSelection, baseSpellsSelectionArgs)
					.build());
		}

		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}


	/**
	 * Delete from DB.
	 * @param item FightPlayer
	 * @return number of row affected
	 */
	public int delete(final FightPlayer item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return FightPlayer
	 */
	public FightPlayer query(final int id) {
		FightPlayer result = null;
		FightPlayerSQLiteAdapter adapt =
					new FightPlayerSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		FightPlayerCriterias crits =
				new FightPlayerCriterias(GroupType.AND);
		crits.add(FightPlayerSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			FightPlayerProviderAdapter.FIGHTPLAYER_URI,
			FightPlayerSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

			if (result.getFight() != null) {
				result.setFight(
					this.getAssociateFight(result));
			}
			if (result.getBaseCharacter() != null) {
				result.setBaseCharacter(
					this.getAssociateBaseCharacter(result));
			}
			if (result.getBaseWeapon() != null) {
				result.setBaseWeapon(
					this.getAssociateBaseWeapon(result));
			}
			result.setBaseSpells(
				this.getAssociateBaseSpells(result));
		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<FightPlayer>
	 */
	public ArrayList<FightPlayer> queryAll() {
		ArrayList<FightPlayer> result =
					new ArrayList<FightPlayer>();
		FightPlayerSQLiteAdapter adapt =
					new FightPlayerSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<FightPlayer>
	 */
	public ArrayList<FightPlayer> query(
				CriteriasBase<FightPlayer> criteria) {
		ArrayList<FightPlayer> result =
					new ArrayList<FightPlayer>();
		FightPlayerSQLiteAdapter adapt =
					new FightPlayerSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				FightPlayerSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item FightPlayer
	 
	 * @return number of rows updated
	 */
	public int update(final FightPlayer item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		FightPlayerSQLiteAdapter adapt =
				new FightPlayerSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item);

		Uri uri = Uri.withAppendedPath(
				FightPlayerProviderAdapter.FIGHTPLAYER_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());


		if (item.getBaseSpells() != null && item.getBaseSpells().size() > 0) {
			// Set new baseSpells for FightPlayer
			SpellCriterias baseSpellsCrit =
						new SpellCriterias(GroupType.AND);
			Criteria crit = new Criteria();
			ArrayValue values = new ArrayValue();
			crit.setType(Type.IN);
			crit.setKey(SpellSQLiteAdapter.COL_ID);
			crit.addValue(values);
			baseSpellsCrit.add(crit);


			for (int i = 0; i < item.getBaseSpells().size(); i++) {
				values.addValue(String.valueOf(
						item.getBaseSpells().get(i).getId()));
			}

			operations.add(ContentProviderOperation.newUpdate(
					SpellProviderAdapter.SPELL_URI)
						.withValue(
								SpellSQLiteAdapter
										.COL_FIGHTPLAYERBASESPELLSINTERNAL,
								item.getId())
					.withSelection(
							baseSpellsCrit.toSQLiteSelection(),
							baseSpellsCrit.toSQLiteSelectionArgs())
					.build());

			// Remove old associated baseSpells
			crit.setType(Type.NOT_IN);
			baseSpellsCrit.add(SpellSQLiteAdapter.COL_FIGHTPLAYERBASESPELLSINTERNAL,
					String.valueOf(item.getId()),
					Type.EQUALS);
			

			operations.add(ContentProviderOperation.newUpdate(
					SpellProviderAdapter.SPELL_URI)
						.withValue(
								SpellSQLiteAdapter
										.COL_FIGHTPLAYERBASESPELLSINTERNAL,
								null)
					.withSelection(
							baseSpellsCrit.toSQLiteSelection(),
							baseSpellsCrit.toSQLiteSelectionArgs())
					.build());
		}


		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/** Relations operations. */
	/**
	 * Get associate Fight.
	 * @param item FightPlayer
	 * @return Fight
	 */
	public Fight getAssociateFight(
			final FightPlayer item) {
		Fight result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor fightCursor = prov.query(
				FightProviderAdapter.FIGHT_URI,
				FightSQLiteAdapter.ALIASED_COLS,
				FightSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getFight().getId())},
				null);

		if (fightCursor.getCount() > 0) {
			fightCursor.moveToFirst();
			FightSQLiteAdapter fightAdapt =
					new FightSQLiteAdapter(this.getContext());
			result = fightAdapt.cursorToItem(fightCursor);
		} else {
			result = null;
		}
		fightCursor.close();

		return result;
	}

	/**
	 * Get associate BaseCharacter.
	 * @param item FightPlayer
	 * @return CharacterPlayer
	 */
	public CharacterPlayer getAssociateBaseCharacter(
			final FightPlayer item) {
		CharacterPlayer result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor characterPlayerCursor = prov.query(
				CharacterPlayerProviderAdapter.CHARACTERPLAYER_URI,
				CharacterPlayerSQLiteAdapter.ALIASED_COLS,
				CharacterPlayerSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getBaseCharacter().getId())},
				null);

		if (characterPlayerCursor.getCount() > 0) {
			characterPlayerCursor.moveToFirst();
			CharacterPlayerSQLiteAdapter characterPlayerAdapt =
					new CharacterPlayerSQLiteAdapter(this.getContext());
			result = characterPlayerAdapt.cursorToItem(characterPlayerCursor);
		} else {
			result = null;
		}
		characterPlayerCursor.close();

		return result;
	}

	/**
	 * Get associate BaseWeapon.
	 * @param item FightPlayer
	 * @return ItemWeapon
	 */
	public ItemWeapon getAssociateBaseWeapon(
			final FightPlayer item) {
		ItemWeapon result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor itemWeaponCursor = prov.query(
				ItemWeaponProviderAdapter.ITEMWEAPON_URI,
				ItemWeaponSQLiteAdapter.ALIASED_COLS,
				ItemWeaponSQLiteAdapter.COL_ID + "= ?",
				new String[]{String.valueOf(item.getBaseWeapon().getId())},
				null);

		if (itemWeaponCursor.getCount() > 0) {
			itemWeaponCursor.moveToFirst();
			ItemWeaponSQLiteAdapter itemWeaponAdapt =
					new ItemWeaponSQLiteAdapter(this.getContext());
			result = itemWeaponAdapt.cursorToItem(itemWeaponCursor);
		} else {
			result = null;
		}
		itemWeaponCursor.close();

		return result;
	}

	/**
	 * Get associate BaseSpells.
	 * @param item FightPlayer
	 * @return Spell
	 */
	public ArrayList<Spell> getAssociateBaseSpells(
			final FightPlayer item) {
		ArrayList<Spell> result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor spellCursor = prov.query(
				SpellProviderAdapter.SPELL_URI,
				SpellSQLiteAdapter.ALIASED_COLS,
				SpellSQLiteAdapter.COL_FIGHTPLAYERBASESPELLSINTERNAL
						+ "= ?",
				new String[]{String.valueOf(item.getId())},
				null);

		SpellSQLiteAdapter spellAdapt =
				new SpellSQLiteAdapter(this.getContext());
		result = spellAdapt.cursorToItems(
						spellCursor);
		spellCursor.close();

		return result;
	}

}
