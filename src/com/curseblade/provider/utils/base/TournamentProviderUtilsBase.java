/**************************************************************************
 * TournamentProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.TournamentCriterias;
import com.curseblade.criterias.TournamentPoolFightCriterias;
import com.curseblade.criterias.base.Criteria;
import com.curseblade.criterias.base.Criteria.Type;
import com.curseblade.criterias.base.value.ArrayValue;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.TournamentSQLiteAdapter;
import com.curseblade.data.TournamentPoolFightSQLiteAdapter;

import com.curseblade.entity.Tournament;
import com.curseblade.entity.TournamentPoolFight;

import com.curseblade.provider.TournamentProviderAdapter;
import com.curseblade.provider.TournamentPoolFightProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * Tournament Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class TournamentProviderUtilsBase
			extends ProviderUtilsBase<Tournament> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "TournamentProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public TournamentProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final Tournament item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		TournamentSQLiteAdapter adapt =
				new TournamentSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(TournamentSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				TournamentProviderAdapter.TOURNAMENT_URI)
						.withValues(itemValues)
						.build());

		if (item.getPoolFights() != null && item.getPoolFights().size() > 0) {
			String poolFightsSelection = TournamentPoolFightSQLiteAdapter.COL_ID + " IN (";
			String[] poolFightsSelectionArgs = new String[item.getPoolFights().size()];
			for (int i = 0; i < item.getPoolFights().size(); i++) {
				poolFightsSelectionArgs[i] = String.valueOf(item.getPoolFights().get(i).getId());
				poolFightsSelection += "? ";
				if (i != item.getPoolFights().size() - 1) {
					 poolFightsSelection += ", ";
				}
			}
			poolFightsSelection += ")";

			operations.add(ContentProviderOperation.newUpdate(TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI)
					.withValueBackReference(
							TournamentPoolFightSQLiteAdapter
									.COL_TOURNAMENTPOOLFIGHTSINTERNAL,
							0)
					.withSelection(poolFightsSelection, poolFightsSelectionArgs)
					.build());
		}

		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}


	/**
	 * Delete from DB.
	 * @param item Tournament
	 * @return number of row affected
	 */
	public int delete(final Tournament item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				TournamentProviderAdapter.TOURNAMENT_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return Tournament
	 */
	public Tournament query(final int id) {
		Tournament result = null;
		TournamentSQLiteAdapter adapt =
					new TournamentSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		TournamentCriterias crits =
				new TournamentCriterias(GroupType.AND);
		crits.add(TournamentSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			TournamentProviderAdapter.TOURNAMENT_URI,
			TournamentSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

			result.setPoolFights(
				this.getAssociatePoolFights(result));
		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<Tournament>
	 */
	public ArrayList<Tournament> queryAll() {
		ArrayList<Tournament> result =
					new ArrayList<Tournament>();
		TournamentSQLiteAdapter adapt =
					new TournamentSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				TournamentProviderAdapter.TOURNAMENT_URI,
				TournamentSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<Tournament>
	 */
	public ArrayList<Tournament> query(
				CriteriasBase<Tournament> criteria) {
		ArrayList<Tournament> result =
					new ArrayList<Tournament>();
		TournamentSQLiteAdapter adapt =
					new TournamentSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				TournamentProviderAdapter.TOURNAMENT_URI,
				TournamentSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item Tournament
	 
	 * @return number of rows updated
	 */
	public int update(final Tournament item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		TournamentSQLiteAdapter adapt =
				new TournamentSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item);

		Uri uri = Uri.withAppendedPath(
				TournamentProviderAdapter.TOURNAMENT_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());


		if (item.getPoolFights() != null && item.getPoolFights().size() > 0) {
			// Set new poolFights for Tournament
			TournamentPoolFightCriterias poolFightsCrit =
						new TournamentPoolFightCriterias(GroupType.AND);
			Criteria crit = new Criteria();
			ArrayValue values = new ArrayValue();
			crit.setType(Type.IN);
			crit.setKey(TournamentPoolFightSQLiteAdapter.COL_ID);
			crit.addValue(values);
			poolFightsCrit.add(crit);


			for (int i = 0; i < item.getPoolFights().size(); i++) {
				values.addValue(String.valueOf(
						item.getPoolFights().get(i).getId()));
			}

			operations.add(ContentProviderOperation.newUpdate(
					TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI)
						.withValue(
								TournamentPoolFightSQLiteAdapter
										.COL_TOURNAMENTPOOLFIGHTSINTERNAL,
								item.getId())
					.withSelection(
							poolFightsCrit.toSQLiteSelection(),
							poolFightsCrit.toSQLiteSelectionArgs())
					.build());

			// Remove old associated poolFights
			crit.setType(Type.NOT_IN);
			poolFightsCrit.add(TournamentPoolFightSQLiteAdapter.COL_TOURNAMENTPOOLFIGHTSINTERNAL,
					String.valueOf(item.getId()),
					Type.EQUALS);
			

			operations.add(ContentProviderOperation.newUpdate(
					TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI)
						.withValue(
								TournamentPoolFightSQLiteAdapter
										.COL_TOURNAMENTPOOLFIGHTSINTERNAL,
								null)
					.withSelection(
							poolFightsCrit.toSQLiteSelection(),
							poolFightsCrit.toSQLiteSelectionArgs())
					.build());
		}


		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	/** Relations operations. */
	/**
	 * Get associate PoolFights.
	 * @param item Tournament
	 * @return TournamentPoolFight
	 */
	public ArrayList<TournamentPoolFight> getAssociatePoolFights(
			final Tournament item) {
		ArrayList<TournamentPoolFight> result;
		ContentResolver prov = this.getContext().getContentResolver();
		Cursor tournamentPoolFightCursor = prov.query(
				TournamentPoolFightProviderAdapter.TOURNAMENTPOOLFIGHT_URI,
				TournamentPoolFightSQLiteAdapter.ALIASED_COLS,
				TournamentPoolFightSQLiteAdapter.COL_TOURNAMENTPOOLFIGHTSINTERNAL
						+ "= ?",
				new String[]{String.valueOf(item.getId())},
				null);

		TournamentPoolFightSQLiteAdapter tournamentPoolFightAdapt =
				new TournamentPoolFightSQLiteAdapter(this.getContext());
		result = tournamentPoolFightAdapt.cursorToItems(
						tournamentPoolFightCursor);
		tournamentPoolFightCursor.close();

		return result;
	}

}
