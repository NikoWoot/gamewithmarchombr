/**************************************************************************
 * HarmonyRssItemProviderUtilsBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils.base;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.database.Cursor;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;
import android.util.Log;

import com.curseblade.criterias.HarmonyRssItemCriterias;
import com.curseblade.criterias.base.CriteriasBase;
import com.curseblade.criterias.base.CriteriasBase.GroupType;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;
import com.curseblade.data.NewsSQLiteAdapter;

import com.curseblade.entity.HarmonyRssItem;

import com.curseblade.provider.HarmonyRssItemProviderAdapter;
import com.curseblade.provider.CursebladeProvider;

/**
 * HarmonyRssItem Provider Utils Base.
 *
 * DO NOT MODIFY THIS CLASS AS IT IS REGENERATED
 *
 * This class is a utility class helpful for complex provider calls.
 * ex : inserting an entity and its relations alltogether, etc.
 */
public abstract class HarmonyRssItemProviderUtilsBase
			extends ProviderUtilsBase<HarmonyRssItem> {
	/**
	 * Tag for debug messages.
	 */
	public static final String TAG = "HarmonyRssItemProviderUtilBase";

	/**
	 * Constructor.
	 * @param context Context
	 */
	public HarmonyRssItemProviderUtilsBase(Context context) {
		super(context);
	}

	@Override
	public Uri insert(final HarmonyRssItem item) {
		Uri result = null;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		ContentResolver prov = this.getContext().getContentResolver();

		HarmonyRssItemSQLiteAdapter adapt =
				new HarmonyRssItemSQLiteAdapter(this.getContext());


		ContentValues itemValues = adapt.itemToContentValues(item);
		itemValues.remove(HarmonyRssItemSQLiteAdapter.COL_ID);

		operations.add(ContentProviderOperation.newInsert(
				HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI)
						.withValues(itemValues)
						.build());


		try {
			ContentProviderResult[] results = 
					prov.applyBatch(CursebladeProvider.authority, operations);
			if (results[0] != null) {
				result = results[0].uri;
			}
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}


	/**
	 * Delete from DB.
	 * @param item HarmonyRssItem
	 * @return number of row affected
	 */
	public int delete(final HarmonyRssItem item) {
		int result = -1;
		ContentResolver prov = this.getContext().getContentResolver();

		Uri uri = Uri.withAppendedPath(
				HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI,
				String.valueOf(item.getId()));
		result = prov.delete(uri,
			null,
			null);


		return result;
	}

	/**
	 * Query the DB.
	 * @param id The ID
	 * @return HarmonyRssItem
	 */
	public HarmonyRssItem query(final int id) {
		HarmonyRssItem result = null;
		HarmonyRssItemSQLiteAdapter adapt =
					new HarmonyRssItemSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		HarmonyRssItemCriterias crits =
				new HarmonyRssItemCriterias(GroupType.AND);
		crits.add(HarmonyRssItemSQLiteAdapter.ALIASED_COL_ID,
					String.valueOf(id));

		Cursor cursor = prov.query(
			HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI,
			HarmonyRssItemSQLiteAdapter.ALIASED_COLS,
			crits.toSQLiteSelection(),
			crits.toSQLiteSelectionArgs(),
			null);

		if (cursor.getCount() > 0) {
			cursor.moveToFirst();
			result = adapt.cursorToItem(cursor);
			cursor.close();

		}

		return result;
	}

	/**
	 * Query the DB to get all entities.
	 * @return ArrayList<HarmonyRssItem>
	 */
	public ArrayList<HarmonyRssItem> queryAll() {
		ArrayList<HarmonyRssItem> result =
					new ArrayList<HarmonyRssItem>();
		HarmonyRssItemSQLiteAdapter adapt =
					new HarmonyRssItemSQLiteAdapter(this.getContext());
		ContentResolver prov =
					this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI,
				HarmonyRssItemSQLiteAdapter.ALIASED_COLS,
				null,
				null,
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Query the DB to get the entities filtered by criteria.
	 * @param criteria The criteria defining the selection and selection args
	 * @return ArrayList<HarmonyRssItem>
	 */
	public ArrayList<HarmonyRssItem> query(
				CriteriasBase<HarmonyRssItem> criteria) {
		ArrayList<HarmonyRssItem> result =
					new ArrayList<HarmonyRssItem>();
		HarmonyRssItemSQLiteAdapter adapt =
					new HarmonyRssItemSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();

		Cursor cursor = prov.query(
				HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI,
				HarmonyRssItemSQLiteAdapter.ALIASED_COLS,
				criteria.toSQLiteSelection(),
				criteria.toSQLiteSelectionArgs(),
				null);

		result = adapt.cursorToItems(cursor);

		cursor.close();

		return result;
	}

	/**
	 * Updates the DB.
	 * @param item HarmonyRssItem
	 
	 * @return number of rows updated
	 */
	public int update(final HarmonyRssItem item) {
		int result = -1;
		ArrayList<ContentProviderOperation> operations =
				new ArrayList<ContentProviderOperation>();
		HarmonyRssItemSQLiteAdapter adapt =
				new HarmonyRssItemSQLiteAdapter(this.getContext());
		ContentResolver prov = this.getContext().getContentResolver();
		ContentValues itemValues = adapt.itemToContentValues(
				item);

		Uri uri = Uri.withAppendedPath(
				HarmonyRssItemProviderAdapter.HARMONYRSSITEM_URI,
				String.valueOf(item.getId()));


		operations.add(ContentProviderOperation.newUpdate(uri)
				.withValues(itemValues)
				.build());



		try {
			ContentProviderResult[] results = prov.applyBatch(CursebladeProvider.authority, operations);
			result = results[0].count;
		} catch (RemoteException e) {
			Log.e(TAG, e.getMessage());
		} catch (OperationApplicationException e) {
			Log.e(TAG, e.getMessage());
		}

		return result;
	}

	
}
