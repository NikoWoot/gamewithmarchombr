/**************************************************************************
 * ItemWeaponProviderUtils.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider.utils;

import android.content.Context;

import com.curseblade.provider.utils.base.ItemWeaponProviderUtilsBase;

/**
 * ItemWeapon Provider Utils.
 *
 * This class is an utility class for wrapping provider calls.
 * Feel free to modify it, add new methods to it, etc.
 */
public class ItemWeaponProviderUtils
	extends ItemWeaponProviderUtilsBase {

	/**
	 * Constructor.
	 * @param context The context
	 */
	public ItemWeaponProviderUtils(Context context) {
		super(context);
	}

}
