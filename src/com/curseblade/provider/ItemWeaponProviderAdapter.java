/**************************************************************************
 * ItemWeaponProviderAdapter.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/
package com.curseblade.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.curseblade.provider.base.ItemWeaponProviderAdapterBase;

/**
 * ItemWeaponProviderAdapter.
 *
 * A provider adapter is used to separate your provider requests for
 * each entity of your application.
 * You will find here basic methods for database manipulation.
 * Feel free to override any method here.
 */
public class ItemWeaponProviderAdapter
					extends ItemWeaponProviderAdapterBase {

	/**
	 * Constructor.
	 * @param ctx context
	 */
	public ItemWeaponProviderAdapter(final Context ctx) {
		this(ctx, null);
	}

	/**
	 * Constructor.
	 * @param ctx context
	 * @param db database
	 */
	public ItemWeaponProviderAdapter(final Context ctx,
												 final SQLiteDatabase db) {
		super(ctx, db);
	}
}

