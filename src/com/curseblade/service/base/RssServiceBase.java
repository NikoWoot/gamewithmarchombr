/**************************************************************************
 * RssServiceBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.service.base;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import com.curseblade.R;
import com.curseblade.service.base.RssServiceBase;
import com.curseblade.parser.rss.NewsRssSync;
import com.curseblade.entity.HarmonyRssItem;
import com.curseblade.parser.rss.base.RssSyncBase;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.IBinder;
import android.util.Log;
import android.util.Xml;
import android.util.Xml.Encoding;

/**
 * RssService base.
 */
public abstract class RssServiceBase extends Service implements IRssService {
	
	/** Tag for Log. */
	private static final String TAG = "RssServiceBase";
	
	/** Binder for RssService. */
	protected RssServiceBinder binder;
	
	/** RssService listeners. */
	protected List<IRssServiceListener> listeners = null;
	
	private boolean firstCheck = true;	
	protected Timer timer;
	protected long refreshDelay;
	protected long firstTimeDelay;
	
	protected final List<Class<? extends RssSyncBase<?>>> rssSyncs
		= new ArrayList<Class<? extends RssSyncBase<?>>>();	
	
	public static Encoding Encoder = Xml.Encoding.UTF_8;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		Log.i(TAG, "Create Service..");
		this.timer = new Timer();
		
		try {
			this.refreshDelay = Long.parseLong(this
					.getString(R.string.rss_service_refresh_delay));
			this.firstTimeDelay = Long.parseLong(this
					.getString(R.string.rss_service_refresh_delay_first));
		}
		catch (NotFoundException exNoFound) {
			Log.e(TAG, "Can't convert refresh_delay value !!");
			this.refreshDelay = 10;
			this.firstTimeDelay = 1;
		}
		
		this.loadSyncBases();
		this.binder = new RssServiceBinder(this);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return this.binder;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(TAG, String.format(
			"Start Service... (Check : first=%s secondes / all=%s minutes)",
			firstTimeDelay,
			firstTimeDelay));
		
		final long refreshDelayInMS = refreshDelay * 60000;
		final long firstTimeDelayInMS = firstTimeDelay * 1000;
		
		this.timer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				processData();
				
				if (firstCheck)
					firstCheck = false;
			}
		}, firstTimeDelayInMS, refreshDelayInMS);
		
		return START_NOT_STICKY;
	}
	
	protected void loadSyncBases() {
		this.rssSyncs.add(NewsRssSync.class);
	}
	
	private void processData() {
		Log.i(TAG, "Run Process Data... (check pulse)");
		
		if (!this.rssSyncs.isEmpty()) {
			for (Class<? extends RssSyncBase<?>> sync : this.rssSyncs) {
				RssSyncBase<?> syncBase = null;
				
				try {
					syncBase = sync.getDeclaredConstructor(Context.class)
						.newInstance(this.getApplicationContext());
				}
				catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (syncBase != null) {
					syncBase.sync(this);
				}
			}
		}
	}
	
	@Override
	public void onDestroy() {
		Log.d(TAG, "Destroy Service ..!");
		
		if (listeners != null)
			this.listeners.clear();
		
		this.timer.cancel();
	}
	
	@Override
	public void addListener(IRssServiceListener listener) {
		if (listeners == null) {
			listeners = new ArrayList<IRssServiceListener>();
		}
		
		listeners.add(listener);
	}
	
	@Override
	public void removeListener(IRssServiceListener listener) {
		if (listeners != null) {
			listeners.remove(listener);
		}
	}
	
	@Override
	public void fireDataChanged(HarmonyRssItem item) {
		if (listeners != null) {
			Log.d(TAG, "Event fireDataChanged call...");
			for (IRssServiceListener listener : listeners) {
				listener.dataChanged(item);
			}
		}
	}
}
