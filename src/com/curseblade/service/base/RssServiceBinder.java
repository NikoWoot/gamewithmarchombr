/**************************************************************************
 * RssServiceBinder.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.service.base;

import android.os.Binder;

/**
 * Binder for RssService.
 */
public class RssServiceBinder extends Binder {
	
	/** The RssService */
	private IRssService service = null;
	
	/**
	 * RssServiceBinder constructor.
	 * @param service The RssService.
	 */
	public RssServiceBinder(IRssService service) {
		super();
		this.service = service;
	}
	
	/**
	 * @return The RssService.
	 */
	public IRssService getService() {
		return service;
	}
}
