/**************************************************************************
 * IRssService.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.service.base;

import com.curseblade.entity.HarmonyRssItem;

/**
 * Rss Service interface.
 */
public interface IRssService {
	/**
	 * Add a listener.
	 * @param listener Listener to add.
	 */
	public void addListener(IRssServiceListener listener); 
	
	/**
	 * Remove a listener.
	 * @param listener Listener to remove.
	 */
	public void removeListener(IRssServiceListener listener);
	
	/**
	 * Notify that an item was updated.
	 * @param item Updated item.
	 */
	public void fireDataChanged(HarmonyRssItem item);
}
