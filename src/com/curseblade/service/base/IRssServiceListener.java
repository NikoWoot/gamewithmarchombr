/**************************************************************************
 * IRssServiceListener.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.service.base;

import com.curseblade.entity.HarmonyRssItem;

/**
 * RssService listener interface.
 */
public interface IRssServiceListener {
	/**
	 * Callback for updated item.
	 * @param item Udated item.
	 */
	public void dataChanged(HarmonyRssItem item); 
}
