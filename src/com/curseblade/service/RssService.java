/**************************************************************************
 * RssService.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.service;

import com.curseblade.service.base.RssServiceBase;

/**
 * RssService.
 */
public class RssService extends RssServiceBase {
	
	@Override
	protected void loadSyncBases() {
		super.loadSyncBases();
	}
}
