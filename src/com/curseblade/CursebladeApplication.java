/**************************************************************************
 * CursebladeApplication.java, curseblade Android
 *
 * Copyright 2014
 * Description : 
 * Author(s)   : Harmony
 * Licence     : 
 * Last update : Feb 7, 2014
 *
 **************************************************************************/
package com.curseblade;

/** 
 * Custom Curseblade Application context. 
 *
 * Feel free to modify this class.
 */
public class CursebladeApplication extends CursebladeApplicationBase {
	// Override or Create your custom method for your application
	// this file is just generate at first time, and never override...

	// on release mode use BuildConfig.DEBUG;
	/**
	 * DEBUG.
	 */
	public static final boolean DEBUG = true;
	
	/**
	 * Character use by IA for test
	 */
	public static final Integer debugCharacterId = 2;
	
	/**
	 * Character selected by player
	 */
	public static Integer selectedCharacterId = 1;

}
