/**************************************************************************
 * HarmonyFragment.java, curseblade Android
 *
 * Copyright 2014
 * Description : 
 * Author(s)   : Harmony
 * Licence     : 
 * Last update : Feb 7, 2014
 *
 **************************************************************************/
package com.curseblade.harmony.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.curseblade.menu.CursebladeMenu;

/**
 * Harmony custom Fragment. This fragment will help you use a lot of harmony's
 * functionnality (menu wrappers, etc.)
 */
public abstract class HarmonyFragment extends SherlockFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setHasOptionsMenu(true);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);

		try {
			CursebladeMenu.getInstance(this.getActivity(), this).clear(menu);
			CursebladeMenu.getInstance(this.getActivity(), this).updateMenu(
					menu, this.getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result;

		try {
			result = CursebladeMenu.getInstance(this.getActivity(), this)
					.dispatch(item, this.getActivity());
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			CursebladeMenu.getInstance(this.getActivity(), this)
					.onActivityResult(requestCode, resultCode, data,
							this.getActivity(), this);
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Assign onClickListener on all buttons in the array
	 * 
	 * @param buttons
	 *            Array of button ids
	 */
	public void assignOnClickEventOnButtons(View parentView,
			OnClickListener listener, Integer... viewIds) {
		
		Button btn = null;

		// While on all view
		for (Integer viewId : viewIds) {

			// Search Button indicated in the array on my view
			btn = (Button) parentView.findViewById(viewId);

			// If button exist
			if (btn != null) {
				btn.setOnClickListener(listener);
			}
		}

	}

}
