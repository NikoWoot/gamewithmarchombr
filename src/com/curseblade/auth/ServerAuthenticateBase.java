/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.auth;

/**
 * Class implement {@link IServerAuthenticate} 
 * It's a test for my application
 * Return permanently "token" 
 * @author Nicolas GAUTIER
 *
 */
public class ServerAuthenticateBase implements IServerAuthenticate {

	/** {@inheritDoc} */
	@Override
	public String userSignUp(String accountname, String password,
			String authTokenType) {
		return "token";
	}

}
