/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.auth.activity;

import com.curseblade.R;

import android.accounts.AccountAuthenticatorActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * Class Activity to manager an loginAction for AccountManager
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class LogInActivity extends AccountAuthenticatorActivity implements
		OnClickListener {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.activity_login);

		Button btn = (Button) this.findViewById(R.id.btn_cancel);
		btn.setOnClickListener(this);

		btn = (Button) this.findViewById(R.id.btn_login);
		btn.setOnClickListener(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.btn_login:
			this.loginAction(v);
			break;

		case R.id.btn_cancel:
			this.cancelAction(v);
			break;
		}

	}

	/**
	 * Launch an loginAction
	 * 
	 * @param sender
	 *            SenderView
	 */
	private void loginAction(View sender) {
		setResult(RESULT_OK);
		finish();
	}

	/**
	 * Launch an cancelAction
	 * 
	 * @param sender
	 *            SenderView
	 */
	private void cancelAction(View sender) {
		// End of my activity
		setResult(RESULT_CANCELED);
		finish();
	}

}
