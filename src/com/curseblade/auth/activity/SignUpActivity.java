/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.auth.activity;

import com.curseblade.auth.task.SignUpTask;
import com.curseblade.auth.*;
import com.curseblade.start.MainActivity;
import com.curseblade.CursebladeApplication;
import com.curseblade.R;
import com.google.common.base.Strings;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Activity used for SignUp an account
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class SignUpActivity extends AccountAuthenticatorActivity 
			implements	OnClickListener {

	private EditText userNameEdit;
	private EditText passwordEdit;
	private SignUpTask signupTask;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.setContentView(R.layout.activity_signup);

		// Add events on button
		Button btn = (Button) this.findViewById(R.id.btn_signup);
		btn.setOnClickListener(this);

		btn = (Button) this.findViewById(R.id.btn_cancel);
		btn.setOnClickListener(this);

		this.userNameEdit = (EditText) this.findViewById(R.id.editText_username);
		this.passwordEdit = (EditText) this.findViewById(R.id.editText_password);
		
		
		// Create signUp task
		this.signupTask = new SignUpTask(
				AccountManagerConstants.sServerAuthenticate) {
			@Override
			protected void onPostExecute(Intent result) {

				try {
					AccountUtils accountUtils = new AccountUtils(
							getBaseContext());

					String username = result
							.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
					String password = result
							.getStringExtra(AccountManager.KEY_PASSWORD);

					// Create an account
					Account account = accountUtils.createAccount(username,
							password);
					if (account == null) {
						throw new Exception("Invalid account");
					}

					// Create an authorisationToken for my app
					accountUtils.createAuthToken(account);

					// End of my activity
					setResult(RESULT_OK);
					SignUpActivity.this.finish();

					Intent intent = new Intent(SignUpActivity.this,
							MainActivity.class);

					SignUpActivity.this.startActivity(intent);

				} catch (Exception e) {
					Log.e(CursebladeApplication.TAG, e.getMessage());
				}

			}
		};

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btn_signup:
			this.signUpAction(v);
			break;

		case R.id.btn_cancel:
			this.cancelAction(v);
			break;
		}

	}

	/**
	 * Method launched by click on btn_cancel
	 * 
	 * @param arg0
	 *            Sender
	 */
	public void cancelAction(View arg0) {
		// End of my activity
		setResult(RESULT_CANCELED);
		finish();
	}

	/**
	 * Method launched by click on btn_signup
	 * 
	 * @param arg0
	 */
	public void signUpAction(View arg0) {

		String userName = this.userNameEdit.getText().toString();
		String password = this.passwordEdit.getText().toString();
		
		// Try is username and password is not null or empty
		if (Strings.isNullOrEmpty(userName) || Strings.isNullOrEmpty(password)) {
			
			Toast.makeText(this, R.string.signup_error, Toast.LENGTH_LONG).show();
			
		} else {
			Intent data = new Intent();
			data.putExtra(AccountManager.KEY_ACCOUNT_NAME, userName);
			data.putExtra(AccountManager.KEY_PASSWORD, password);
			data.putExtra(AccountManager.KEY_ACCOUNT_TYPE,
					AccountManagerConstants.ACCOUNT_TYPE);

			signupTask.execute(data);
		}

	}
}
