/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.auth;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Service to manage AccountManager in my software
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class AccountAuthentificatorService extends Service {

	/**
	 * Class of AccountManager
	 */
	private AccountAuthenticator accountAuthenticator;

	/** {@inheritDoc} */
	@Override
	public void onCreate() {
		super.onCreate();
		this.accountAuthenticator = new AccountAuthenticator(this);
	};

	/** {@inheritDoc} */
	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/** {@inheritDoc} */
	@Override
	public IBinder onBind(Intent arg0) {
		return this.accountAuthenticator.getIBinder();
	}

}
