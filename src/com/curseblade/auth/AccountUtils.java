/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.auth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.content.Context;
import android.os.Bundle;

/**
 * Helper class to Account Management easily
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class AccountUtils {
	/**
	 * Instance of Android AccountManager
	 */
	private AccountManager accountManager;

	/**
	 * Default constructor
	 * 
	 * @param context
	 *            Context of my service or my activity
	 * @throws Exception
	 *             Return an exception if context is null
	 */
	public AccountUtils(Context context) throws Exception {
		if (context == null)
			throw new Exception("Context is not available !");

		// Get account manager by context of my activity of service
		this.accountManager = AccountManager.get(context);
	}

	/**
	 * Return an createdAccount if is available
	 * 
	 * @param username
	 *            Username of newAccount
	 * @param password
	 *            Password of newAccount
	 * @return Null Or Instance of Account
	 */
	public Account createAccount(String username, String password) {
		Account myAccount = null;

		// Create an account with my username and password
		myAccount = new Account(username, AccountManagerConstants.ACCOUNT_TYPE);
		if (accountManager.addAccountExplicitly(myAccount, password, null) == false) {
			// Return null if account username is not available
			myAccount = null;
		}

		return myAccount;
	}

	/**
	 * Return password of my account
	 * 
	 * @param account
	 *            Account to operating this action
	 * @return Password of my account
	 */
	public String getPassword(Account account) {
		return accountManager.getPassword(account);
	}

	/**
	 * Generate a AuthToken for my account
	 * 
	 * @param account
	 *            Account to operating this action
	 */
	public void createAuthToken(Account account) {
		accountManager.setAuthToken(account,
				AccountManagerConstants.TOKEN_TYPE,
				AccountManagerConstants.ACCOUNT_TYPE);
	}

	/**
	 * Try to get AuthToken => Call method defined in AccountAuthentificator
	 * 
	 * @param account
	 *            Account
	 * @param callback
	 *            Callback for data
	 */
	public void getAuthToken(Account account,
			AccountManagerCallback<Bundle> callback) {
		accountManager.getAuthToken(account,
				AccountManagerConstants.TOKEN_TYPE, null, true, callback, null);
	}

	/**
	 * Add an key/value with my account
	 * 
	 * @param account
	 *            Account to operation this action
	 * @param key
	 *            Key of my value
	 * @param value
	 *            Value for my key
	 */
	public void setUserData(Account account, String key, String value) {
		// Put a userdata in my account
		accountManager.setUserData(account, key, value);
	}

	/**
	 * Return list of accounts for my app
	 * 
	 * @return AccountList of my app
	 */
	public Account[] getAccounts() {
		// Get all accounts with type needed
		return accountManager
				.getAccountsByType(AccountManagerConstants.ACCOUNT_TYPE);
	}

	/**
	 * Return the first account for my app
	 * 
	 * @return First account of my app
	 */
	public Account getFirstAccount() {
		Account[] accounts = this.getAccounts();
		Account result = null;

		// Get FirstAccount if accounts exist
		if (accounts.length > 0)
			result = accounts[0];

		return result;
	}
}
