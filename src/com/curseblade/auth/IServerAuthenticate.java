/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.auth;

/**
 * Interface to describe methods usely for SignUp and Login
 * 
 * @author Nicolas GAUTIER
 * 
 */
public interface IServerAuthenticate {
	/**
	 * Register method
	 * 
	 * @param accountname
	 *            Name of account
	 * @param password
	 *            Password of account
	 * @param authTokenType
	 *            account type
	 * @return Return token
	 */
	String userSignUp(String accountname, String password, String authTokenType);
}
