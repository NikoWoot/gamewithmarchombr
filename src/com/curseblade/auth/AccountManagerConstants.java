/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.auth;

import com.curseblade.auth.activity.LogInActivity;
import com.curseblade.auth.activity.PreferenceActivity;
import com.curseblade.auth.activity.SignUpActivity;

public class AccountManagerConstants {

	// ===========================
	// Constants for AccountManager
	// ===========================
	public static final String ACCOUNT_TYPE = "com.curseblade";
	public static final String TOKEN_TYPE = "game";
	// ===========================
	
	// ===========================
	// AuthToken type
	// ===========================
	protected static final String AUTHTOKEN_TYPE_READ_ONLY = "Read only";
	protected static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";

	// ===========================
	// Class for activity
	// ===========================
	protected static Class<?> logInActivity = LogInActivity.class;
	protected static Class<?> signUpActivity = SignUpActivity.class;
	protected static Class<?> propertiesActivity = PreferenceActivity.class;

	// ===========================
	// Class use to communicate for the token
	// ===========================
	public static final IServerAuthenticate sServerAuthenticate = 
										new ServerAuthenticateBase();

}
