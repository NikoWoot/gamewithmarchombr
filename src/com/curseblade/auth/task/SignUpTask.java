/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.auth.task;

import com.curseblade.auth.IServerAuthenticate;

import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

/**
 * Class AsyncTask usefull for Asynchrone signup with an external webservice or
 * others service
 * 
 * @author Nicolas GAUTIER
 * 
 */
public abstract class SignUpTask extends AsyncTask<Intent, Void, Intent> {

	/**
	 * Instance of authentificationWithServer
	 */
	private IServerAuthenticate serverAuthenticate;

	/**
	 * Default Constructor
	 * 
	 * @param serverAuthenticate
	 *            Interface for communication with external service
	 */
	public SignUpTask(IServerAuthenticate serverAuthenticate) {
		this.serverAuthenticate = serverAuthenticate;
	}

	/** {@inheritDoc} */
	@Override
	protected abstract void onPostExecute(Intent result);

	/** {@inheritDoc} */
	@Override
	protected Intent doInBackground(Intent... params) {

		// I need Intent to work
		if (params.length == 0)
			return null;

		// Get Intent with data for get a token
		Intent oldIntent = params[0];
		String userName = oldIntent
				.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
		String userPass = oldIntent.getStringExtra(AccountManager.KEY_PASSWORD);
		String accountType = oldIntent
				.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);
		String authToken = null;

		Bundle data = new Bundle();

		try {
			// Get an token by the webservice
			authToken = serverAuthenticate.userSignUp(userName, userPass,
					accountType);

			// Put datas in Intent to return result
			data.putString(AccountManager.KEY_ACCOUNT_NAME, userName);
			data.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
			data.putString(AccountManager.KEY_AUTHTOKEN, authToken);
			data.putString(AccountManager.KEY_PASSWORD, userPass);

		} catch (Exception e) {
			data.putString(AccountManager.KEY_ERROR_MESSAGE, e.getMessage());
		}

		// Create intent to stock Bundle with response
		final Intent res = new Intent();
		res.putExtras(data);
		return res;
	}

}
