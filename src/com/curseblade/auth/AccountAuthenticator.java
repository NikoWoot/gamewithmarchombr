/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.auth;

import com.curseblade.R;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

/**
 * Class of managing AccountManager on my app
 * 
 * @author Nicolas GAUTIER
 * 
 */
public class AccountAuthenticator extends AbstractAccountAuthenticator {
	/**
	 * Arguments for my Intent
	 */
	public final static String ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT";

	/**
	 * Context for my activity
	 */
	private Context context;

	/** {@inheritDoc} */
	public AccountAuthenticator(Context context) {
		super(context);
		this.context = context;
	}

	/** {@inheritDoc} */
	@Override
	public Bundle addAccount(AccountAuthenticatorResponse response,
			String accountType, String authTokenType,
			String[] requiredFeatures, Bundle options)
			throws NetworkErrorException {

		// Intent creation to change screen
		final Intent intent = new Intent(this.context,
				AccountManagerConstants.signUpActivity);
		intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType);
		intent.putExtra(AccountManager.KEY_AUTHENTICATOR_TYPES, authTokenType);
		intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
				response);
		intent.putExtra(AccountAuthenticator.ARG_IS_ADDING_NEW_ACCOUNT, true);

		// Make bundle for result
		final Bundle result = new Bundle();
		result.putParcelable(AccountManager.KEY_INTENT, intent);

		return result;
	}

	/** {@inheritDoc} */
	@Override
	public Bundle confirmCredentials(AccountAuthenticatorResponse response,
			Account account, Bundle options) throws NetworkErrorException {

		final Bundle result = new Bundle();
		result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, true);

		// TODO : Check if newPassword is the same as the old password

		return result;

	}

	/** {@inheritDoc} */
	@Override
	public Bundle editProperties(AccountAuthenticatorResponse response,
			String accountType) {
		// TODO : A revoir
		String tmpNameAccount = "";

		// Create intent to launch the Wizard, with response
		final Bundle result = new Bundle();

		final Intent intent = new Intent(context,
				AccountManagerConstants.propertiesActivity);
		intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, tmpNameAccount);
		intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType);
		intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
				response);
		intent.putExtra(AccountManager.KEY_AUTHENTICATOR_TYPES,
				AccountManagerConstants.TOKEN_TYPE);

		result.putParcelable(AccountManager.KEY_INTENT, intent);

		return result;
	}

	/** {@inheritDoc} */
	@Override
	public Bundle getAuthToken(AccountAuthenticatorResponse response,
			Account account, String authTokenType, Bundle options)
			throws NetworkErrorException {

		// Make a return bundle
		final Bundle result = new Bundle();

		// If token isn't know
		if (!authTokenType
				.equals(AccountManagerConstants.AUTHTOKEN_TYPE_READ_ONLY)
				&& !authTokenType
						.equals(AccountManagerConstants.AUTHTOKEN_TYPE_FULL_ACCESS)) {

			result.putString(AccountManager.KEY_ERROR_MESSAGE,
					this.context.getString(R.string.authtoken_type_invalid));
		} else {
			// Extract the username and password from the Account Manager, and
			// ask
			// the server for an appropriate AuthToken.
			final AccountManager am = AccountManager.get(this.context);

			// Gets an auth token from the AccountManager's cache
			String authToken = am.peekAuthToken(account, authTokenType);

			// Try to connect user if authtoken is not null and not empty
			if (TextUtils.isEmpty(authToken)) {

				final String password = am.getPassword(account);

				if (password != null) {
					try {
						// re-authenticating with the existing password
						authToken = AccountManagerConstants.sServerAuthenticate
								.userSignUp(account.name, password,
										authTokenType);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			// If I have token, I enter here
			if (TextUtils.isEmpty(authToken) == false) {

				result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
				result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
				result.putString(AccountManager.KEY_AUTHENTICATOR_TYPES,
						authTokenType);
				result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
			} else {
				// Here, we don't connected, Open the LoginActivity
				final Intent intent = new Intent(this.context,
						AccountManagerConstants.logInActivity);
				intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, account.name);
				intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, account.type);
				intent.putExtra(
						AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE,
						response);
				intent.putExtra(AccountManager.KEY_AUTHENTICATOR_TYPES,
						authTokenType);

				result.putParcelable(AccountManager.KEY_INTENT, intent);
			}
		}

		return result;
	}

	/** {@inheritDoc} */
	@Override
	public String getAuthTokenLabel(String authTokenType) {

		// ==> Ask the authenticator for a localized label
		// for the given authTokenType.
		String result = this.context.getString(R.string.authtoken_type_invalid);

		if (authTokenType
				.equals(AccountManagerConstants.AUTHTOKEN_TYPE_FULL_ACCESS)) {

			result = this.context
					.getString(R.string.authtoken_type_full_access);
		} else if (authTokenType
				.equals(AccountManagerConstants.AUTHTOKEN_TYPE_READ_ONLY)) {

			result = this.context.getString(R.string.authtoken_type_read_only);
		}

		return result;
	}

	/** {@inheritDoc} */
	@Override
	public Bundle hasFeatures(AccountAuthenticatorResponse response,
			Account account, String[] features) throws NetworkErrorException {

		// ==> Checks if the account supports all the specified
		// authenticator specific features.

		final Bundle result = new Bundle();
		result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);

		// Finds out whether a particular account has all the specified
		// features.
		return result;
	}

	/** {@inheritDoc} */
	@Override
	public Bundle updateCredentials(AccountAuthenticatorResponse response,
			Account account, String authTokenType, Bundle options)
			throws NetworkErrorException {

		// ==> Called by AccountManager.updateCredentials

		// Asks the user to enter a new password for an account,
		// updating the saved credentials for the account.
		final Bundle result = new Bundle();
		result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);

		return result;
	}

}
