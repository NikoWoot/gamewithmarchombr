/**************************************************************************
 * RssSyncBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.parser.rss.base;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import com.curseblade.entity.HarmonyRssItem;
import com.curseblade.service.base.IRssService;

import android.content.Context;
import android.util.Log;

/**
 * RssSync base.
 */
public abstract class RssSyncBase<T extends HarmonyRssItem> {
	/** Tag for Log. */
	private static final String TAG = "RssSyncBase";
	
	/** Android Context. */
	protected final Context context;
	
	/** Rss Feed Uri. */
	protected final String uri;
	
	/**
	 * RssSyncBase constructor.
	 * @param context The Android context.
	 * @param uri The Rss feed url.
	 */
	protected RssSyncBase(Context context, String uri) {
		this.context = context;
		this.uri = uri;
	}
	
	/** @return Get the Rss uri */
	public String getUri() {
		return this.uri;
	}
	
	/**
	 * Sync Rss content.
	 * @param service The Rss service.
	 */
	public void sync(IRssService service) {
		List<T> items = this.parseData();
		items = this.limitData(items);
		this.syncData(items, service);
	}
	
	/**
	 * Sync Rss content.
	 * @param items List of items to append.
	 * @param service The Rss service to callback.
	 */
	protected abstract void syncData(List<T> items, IRssService service);
	
	/**
	 * @return Get the feed parser.
	 * @param uri The Rss feed url.
	 */
	protected abstract IFeedParser<T> getFeedParser(String uri)
		throws MalformedURLException;
	
	/**
	 * Parse the Rss feed.
	 * @return The list of parsed items.
	 */
	protected List<T> parseData() {
		List<T> result = null;
		
		try {
			if (this.uri != null) {
				IFeedParser<T> parser = this.getFeedParser(this.uri);
				;
				result = parser.parse();
				
				parser = null;
			}
		}
		catch (Throwable t) {
			Log.e(TAG, t.getMessage(), t);
		}
		
		return result;
	}
	
	/**
	 * Limit the number or rss item received.
	 * @param items The list of items to append.
	 * @return The limited list of items.
	 */
	protected List<T> limitData(List<T> items) {
		List<T> result = new ArrayList<T>();
		int limit = 20;
		
		if (items.size() < 20)
			limit = items.size();
		
		for (int i = 0; i < limit; i++) {
			result.add(items.get(i));
		}
		
		return result;
	}
}
