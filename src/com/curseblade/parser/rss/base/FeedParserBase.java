/**************************************************************************
 * FeedParserBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.parser.rss.base;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.sax.Element;
import android.sax.EndTextElementListener;
import android.util.Log;

import com.curseblade.entity.HarmonyRssItem;


/**
 *	FeedParser base class.
 */
public abstract class FeedParserBase<T extends HarmonyRssItem>
		implements IFeedParser<T> {
	
	/** Tag for Log **/
	private String TAG = "BaseFeedParser";
	
	/** title. */
	protected static final String TITLE = "title";
	/** link. */
	protected static final String LINK = "link";
	/** description2. */
	protected static final String DESCRIPTION = "description";
	/** enclosure. */
	protected static final String ENCLOSURE = "enclosure";
	/** author. */
	protected static final String AUTHOR = "author";
	/** pubDate. */
	protected static final String PUBDATE = "pubDate";
	/** category. */
	protected static final String CATEGORY = "category";
	
	/** The rss feed url **/
	private final URL feedUrl;
	
	/** Current item for parsing **/
	protected T currentMessage = null;

	/**
	 * FeedParserBase constructor.
	 * @param feedUrl The Rss feed url.
	 */
	protected FeedParserBase(String feedUrl) throws MalformedURLException{
		Log.d(TAG, "Parse url : " + feedUrl);
		
		this.feedUrl = new URL(feedUrl);
	}
	
	/**
	 * Itinialize parser for each rss items.
	 * @param item rss item.
	 */
	protected void initializeParser(Element item) {
		item.getChild(TITLE).setEndTextElementListener(new EndTextElementListener() {
			public void end(String value) {
				currentMessage.setTitle(value);
			}
		});
		item.getChild(LINK).setEndTextElementListener(new EndTextElementListener() {
			public void end(String value) {
				currentMessage.setLink(value);
			}
		});
		item.getChild(DESCRIPTION).setEndTextElementListener(new EndTextElementListener() {
			public void end(String value) {
				currentMessage.setDescription(value);
			}
		});
		item.getChild(ENCLOSURE).setEndTextElementListener(new EndTextElementListener() {
			public void end(String value) {
				currentMessage.setEnclosure(value);
			}
		});
		item.getChild(AUTHOR).setEndTextElementListener(new EndTextElementListener() {
			public void end(String value) {
				currentMessage.setAuthor(value);
			}
		});
		item.getChild(CATEGORY).setEndTextElementListener(new EndTextElementListener() {
			public void end(String value) {
				String categories = currentMessage.getCategories();
				if (categories == null) {
					categories = "";
				} else {
					categories += ";";
				}
				categories += value;
				currentMessage.setCategories(categories);
			}
		});
		item.getChild(PUBDATE).setEndTextElementListener(new EndTextElementListener() {
			public void end(String value) {
				DateTimeFormatter format = 
					    DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss Z").withLocale(Locale.ENGLISH);
				DateTime dateTime = format.parseDateTime(value);
				currentMessage.setPubDate(dateTime);
			}
		});
	}
	
	/**
	 * Get the input stream from rss url.
	 */
	protected InputStream getInputStream() throws IOException {
		Log.d(TAG, "Connect to url : " + feedUrl);
		
		URLConnection urlConnection = feedUrl.openConnection();
		
		return urlConnection.getInputStream();
	}
}