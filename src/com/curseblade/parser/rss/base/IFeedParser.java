/**************************************************************************
 * IFeedParser.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.parser.rss.base;

import java.util.List;

import com.curseblade.entity.HarmonyRssItem;


/**
 * FeedParser interface.
 */
public interface IFeedParser<T extends HarmonyRssItem> {
	/**
	 * Parse the rss feed.
	 * @return a list of T.
	 */
	List<T> parse();
}
