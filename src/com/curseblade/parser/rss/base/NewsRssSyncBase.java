/**************************************************************************
 * NewsRssSyncBase.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.parser.rss.base;

import java.net.MalformedURLException;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.curseblade.R;
import com.curseblade.data.HarmonyRssItemSQLiteAdapter;
import com.curseblade.data.NewsSQLiteAdapter;
import com.curseblade.entity.News;
import com.curseblade.parser.rss.NewsAndroidSaxFeedParser;
import com.curseblade.parser.rss.base.RssSyncBase;
import com.curseblade.provider.NewsProviderAdapter;
import com.curseblade.service.base.IRssService;


/**
 * NewsRssSync.
 */
public class NewsRssSyncBase extends RssSyncBase<News> {
	/** Tag for Log. */
	private static final String TAG = "NewsRssSyncBase";
	
	/**
	 * NewsRssSyncBase constructor.
	 * @param context The Android context.
	 */
	public NewsRssSyncBase(Context context) {
		super(context, context.getString(R.string.rss_news_uri));
	}
	
	@Override
	protected IFeedParser<News> getFeedParser(String feedUrl)
			throws MalformedURLException {
		return new NewsAndroidSaxFeedParser(feedUrl);
	}
	
	@Override
	protected void syncData(List<News> items, IRssService service) {
		Log.i(TAG, "Sync content...");
		
		Uri provider = NewsProviderAdapter.NEWS_URI;
		String selection = HarmonyRssItemSQLiteAdapter.ALIASED_COL_HASH + " = ?";
		String[] selectionArgs = new String[1];
		
		for (News item : items) {
			
			String hash = String.valueOf(item.hashCode());
			selectionArgs[0] = String.valueOf(hash);
			
			Cursor c = this.context.getContentResolver().query(
					provider,
					null,
					selection,
					selectionArgs,
					null);
			
			boolean finded = (c.getCount() != 0);
			
			if (!finded) {
				NewsSQLiteAdapter adapter =
					new NewsSQLiteAdapter(this.context);
				
				ContentValues values = adapter.itemToContentValues(item);
				values.remove(NewsSQLiteAdapter.COL_ID);
				
				this.context.getContentResolver().insert(provider, values);
				
				if (service != null) {
					service.fireDataChanged(item);
				}
			}
			
			c.close();
		}
	}
}
