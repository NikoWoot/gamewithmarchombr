/**************************************************************************
 * NewsAndroidSaxFeedParser.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.parser.rss;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.xml.sax.Attributes;

import com.curseblade.entity.News;
import com.curseblade.service.RssService;
import com.curseblade.parser.rss.base.FeedParserBase;

import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Log;
import android.util.Xml;

/**
 * NewsAndroidSaxFeed Parser.
 */
public class NewsAndroidSaxFeedParser extends FeedParserBase<News> {
	private static final String TAG = "NewsAndroidSaxFeedParser";
	
	
	/**
	 * NewsAndroidSaxFeedParser constructor.
	 * @param feedUrl The Rss feed url.
	 */
	public NewsAndroidSaxFeedParser(String feedUrl)
			throws MalformedURLException {
		super(feedUrl);
	}

	@Override
	public List<News> parse() {
		RootElement root = new RootElement("rss");
		final List<News> messages = new ArrayList<News>();
		Element channel = root.getChild("channel");
		Element item = channel.getChild("item");
		
		super.initializeParser(item);
		
		item.setStartElementListener(new StartElementListener(){
			@Override
			public void start(Attributes arg0) {
				currentMessage = new News();
			}
		});
		
		
		item.setEndElementListener(new EndElementListener(){
			public void end() {
				messages.add(currentMessage);
			}
		});
		
		try {
			InputStream stream = this.getInputStream();
			Xml.parse(stream, RssService.Encoder, root.getContentHandler());
		} catch (Exception e) {
			//throw new RuntimeException(e);
			Log.d(TAG, e.getMessage());
		}
		
		return messages;
	}
}
