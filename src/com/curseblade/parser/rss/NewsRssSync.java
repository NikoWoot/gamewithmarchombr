/**************************************************************************
 * NewsRssSync.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.parser.rss;

import android.content.Context;

import com.curseblade.parser.rss.base.NewsRssSyncBase;

/**
 * NewsRssSync.
 */
public class NewsRssSync extends NewsRssSyncBase {
	/**
	 * NewsRssSync constructor.
	 */
	public NewsRssSync(Context context) {
		super(context);
	}
}
