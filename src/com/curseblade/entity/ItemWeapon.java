/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import com.tactfactory.harmony.annotation.Column;
import com.tactfactory.harmony.annotation.Entity;
import com.tactfactory.harmony.annotation.Id;
import com.tactfactory.harmony.annotation.Column.Type;

/**
 * Class to description a weapon for my character
 * @author Nicolas GAUTIER
 *
 */
@Entity
public class ItemWeapon implements Serializable, Parcelable {

	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = -5969757369788520618L;

	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "ItemWeapon";

	@Id
	@Column(unique = true, nullable = false, hidden = true)
	private Integer id;

	@Column
	private String name;

	@Column
	private Double baseAttack;

	@Column(type = Type.ENUM)
	private ItemWeaponType weaponType;

	/**
	 * Default constructor.
	 */
	public ItemWeapon() {

	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param value
	 *            the id to set
	 */
	public void setId(final Integer value) {
		this.id = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param value
	 *            the name to set
	 */
	public void setName(final String value) {
		this.name = value;
	}

	/**
	 * @return the baseAttack
	 */
	public Double getBaseAttack() {
		return this.baseAttack;
	}

	/**
	 * @param value
	 *            the baseAttack to set
	 */
	public void setBaseAttack(final Double value) {
		this.baseAttack = value;
	}

	/**
	 * @return the weaponType
	 */
	public ItemWeaponType getWeaponType() {
		return this.weaponType;
	}

	/**
	 * @param value
	 *            the weaponType to set
	 */
	public void setWeaponType(final ItemWeaponType value) {
		this.weaponType = value;
	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		dest.writeInt(this.getId());
		dest.writeString(this.getName());

		if (this.getWeaponType() != null) {
			dest.writeInt(1);
			dest.writeString(this.getWeaponType().name());
		} else {
			dest.writeInt(0);
		}
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		this.setId(parc.readInt());
		this.setName(parc.readString());

		int weaponTypeBool = parc.readInt();
		if (weaponTypeBool == 1) {
			this.setWeaponType(ItemWeaponType.valueOf(parc.readString()));
		}
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public ItemWeapon(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<ItemWeapon> CREATOR = new Parcelable.Creator<ItemWeapon>() {
		public ItemWeapon createFromParcel(Parcel in) {
			return new ItemWeapon(in);
		}

		public ItemWeapon[] newArray(int size) {
			return new ItemWeapon[size];
		}
	};

}
