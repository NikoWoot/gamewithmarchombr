/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.ArrayList;
import com.tactfactory.harmony.annotation.Column;
import com.tactfactory.harmony.annotation.Entity;
import com.tactfactory.harmony.annotation.Id;
import com.tactfactory.harmony.annotation.OneToMany;

/**
 * Class to manage a tournament by player
 * 
 * @author Nicolas GAUTIER
 * 
 */
@Entity
public class Tournament implements Serializable, Parcelable {

	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = -8484926703674095341L;

	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "Tournament";

	@Id
	@Column(unique = true, nullable = false, hidden = true)
	private Integer id;

	@Column(hidden = true, defaultValue = "10")
	private Integer maxElements;

	@OneToMany
	@Column(nullable = true)
	private ArrayList<TournamentPoolFight> poolFights;

	/**
	 * Add an element on poolFight to initialize tournament
	 * 
	 * @param element
	 *            element to add at collection
	 * @return False if element not add (CollectionFull) and True if entity are
	 *         added
	 */
	public boolean addElement(TournamentPoolFight element) {
		if (this.poolFights.size() == maxElements)
			return false;

		this.poolFights.add(element);
		return true;
	}

	/*
	 * private void createNextLevelPool() { PoolFight newPool = new PoolFight();
	 * 
	 * for (PoolFight pool : this.poolFights) { if (newPool.getLeftPool() ==
	 * null) { newPool.setLeftPool(pool); } else { newPool.setRightPool(pool);
	 * this.winnerElements.add(newPool); newPool = new
	 * PoolFight<CharacterPlayer>(); } } }
	 */

	/**
	 * Default constructor.
	 */
	public Tournament() {

	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param value
	 *            the id to set
	 */
	public void setId(final Integer value) {
		this.id = value;
	}

	/**
	 * @return the maxElements
	 */
	public Integer getMaxElements() {
		return this.maxElements;
	}

	/**
	 * @param value
	 *            the maxElements to set
	 */
	public void setMaxElements(final Integer value) {
		this.maxElements = value;
	}

	/**
	 * @return the poolFights
	 */
	public ArrayList<TournamentPoolFight> getPoolFights() {
		return this.poolFights;
	}

	/**
	 * @param value
	 *            the poolFights to set
	 */
	public void setPoolFights(final ArrayList<TournamentPoolFight> value) {
		this.poolFights = value;
	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		dest.writeInt(this.getId());
		dest.writeInt(this.getMaxElements());

		if (this.getPoolFights() != null) {
			dest.writeInt(this.getPoolFights().size());
			for (TournamentPoolFight item : this.getPoolFights()) {
				dest.writeParcelable(item, flags);
			}
		} else {
			dest.writeInt(-1);
		}
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		this.setId(parc.readInt());
		this.setMaxElements(parc.readInt());

		int nbPoolFights = parc.readInt();
		if (nbPoolFights > -1) {
			ArrayList<TournamentPoolFight> items = new ArrayList<TournamentPoolFight>();
			for (int i = 0; i < nbPoolFights; i++) {
				items.add((TournamentPoolFight) parc
						.readParcelable(TournamentPoolFight.class
								.getClassLoader()));
			}
			this.setPoolFights(items);
		}
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public Tournament(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<Tournament> CREATOR = new Parcelable.Creator<Tournament>() {
		public Tournament createFromParcel(Parcel in) {
			return new Tournament(in);
		}

		public Tournament[] newArray(int size) {
			return new Tournament[size];
		}
	};

}
