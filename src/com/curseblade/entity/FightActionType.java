/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

/**
 * Type of Action during a fight
 * 
 * @author Nicolas GAUTIER
 * 
 */
public enum FightActionType {
	START_FIGHT, END_FIGHT, START_TURN, END_TURN, DEATH_FIGHTER, ACTION_FIGHTER
}
