/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

/**
 * Class use for persistance of the type of ItemWeapon So usely because is for
 * the roleplay (an magician not have an sword)
 * 
 * @author Nicolas GAUTIER
 * 
 */
public enum ItemWeaponType {
	STAFF, BLADE, BOW
}
