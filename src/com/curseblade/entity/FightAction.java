/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import com.tactfactory.harmony.annotation.Column;
import com.tactfactory.harmony.annotation.Entity;
import com.tactfactory.harmony.annotation.Id;
import com.tactfactory.harmony.annotation.Column.Type;
import com.tactfactory.harmony.annotation.ManyToOne;

/**
 * Class to the storage the history of a fight
 * 
 * @author Nicolas GAUTIER
 * 
 */
@Entity(hidden = true)
public class FightAction implements Serializable, Parcelable {

	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = 4183105259409665297L;

	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "FightAction";

	@Id
	@Column(unique = true, nullable = false, hidden = true)
	private Integer id;

	@ManyToOne
	private Fight fight;

	@ManyToOne
	private FightPlayer sender;

	@ManyToOne
	private FightPlayer receiver;

	@Column(type = Type.ENUM)
	private FightActionType actionType;

	@Column
	private Integer beforeValue;

	@Column
	private Integer afterValue;

	/**
	 * Default constructor.
	 */
	public FightAction() {

	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param value
	 *            the id to set
	 */
	public void setId(final Integer value) {
		this.id = value;
	}

	/**
	 * @return the fight
	 */
	public Fight getFight() {
		return this.fight;
	}

	/**
	 * @param value
	 *            the fight to set
	 */
	public void setFight(final Fight value) {
		this.fight = value;
	}

	/**
	 * @return the sender
	 */
	public FightPlayer getSender() {
		return this.sender;
	}

	/**
	 * @param value
	 *            the sender to set
	 */
	public void setSender(final FightPlayer value) {
		this.sender = value;
	}

	/**
	 * @return the receiver
	 */
	public FightPlayer getReceiver() {
		return this.receiver;
	}

	/**
	 * @param value
	 *            the receiver to set
	 */
	public void setReceiver(final FightPlayer value) {
		this.receiver = value;
	}

	/**
	 * @return the actionType
	 */
	public FightActionType getActionType() {
		return this.actionType;
	}

	/**
	 * @param value
	 *            the actionType to set
	 */
	public void setActionType(final FightActionType value) {
		this.actionType = value;
	}

	/**
	 * @return the beforeValue
	 */
	public Integer getBeforeValue() {
		return this.beforeValue;
	}

	/**
	 * @param value
	 *            the beforeValue to set
	 */
	public void setBeforeValue(final Integer value) {
		this.beforeValue = value;
	}

	/**
	 * @return the afterValue
	 */
	public Integer getAfterValue() {
		return this.afterValue;
	}

	/**
	 * @param value
	 *            the afterValue to set
	 */
	public void setAfterValue(final Integer value) {
		this.afterValue = value;
	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		dest.writeInt(this.getId());

		dest.writeParcelable(this.getFight(), flags);

		dest.writeParcelable(this.getSender(), flags);

		dest.writeParcelable(this.getReceiver(), flags);

		if (this.getActionType() != null) {
			dest.writeInt(1);
			dest.writeString(this.getActionType().name());
		} else {
			dest.writeInt(0);
		}
		dest.writeInt(this.getBeforeValue());
		dest.writeInt(this.getAfterValue());
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		this.setId(parc.readInt());

		this.setFight((Fight) parc.readParcelable(Fight.class.getClassLoader()));

		this.setSender((FightPlayer) parc.readParcelable(FightPlayer.class
				.getClassLoader()));

		this.setReceiver((FightPlayer) parc.readParcelable(FightPlayer.class
				.getClassLoader()));

		int actionTypeBool = parc.readInt();
		if (actionTypeBool == 1) {
			this.setActionType(FightActionType.valueOf(parc.readString()));
		}
		this.setBeforeValue(parc.readInt());
		this.setAfterValue(parc.readInt());
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public FightAction(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<FightAction> CREATOR = new Parcelable.Creator<FightAction>() {
		public FightAction createFromParcel(Parcel in) {
			return new FightAction(in);
		}

		public FightAction[] newArray(int size) {
			return new FightAction[size];
		}
	};

}
