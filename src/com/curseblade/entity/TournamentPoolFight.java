/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import com.tactfactory.harmony.annotation.*;

/**
 * Class to manage the node of fight in a Tournament
 * 
 * @author Nicolas GAUTIER
 * 
 */
@Entity
public class TournamentPoolFight implements Serializable, Parcelable {

	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = 5779665612675701196L;

	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "TournamentPoolFight";

	@Id
	@Column(unique = true, nullable = false, hidden = true)
	private Integer id;

	@Column(nullable = false, hidden = true)
	private Integer nodeLevel;

	@OneToOne
	@Column(nullable = true)
	private FightPlayer winner;

	@OneToOne
	@Column(nullable = true)
	private TournamentPoolFight parentPool;

	@OneToOne(mappedBy = "parentPool")
	@Column(nullable = true)
	private TournamentPoolFight leftPool;

	@OneToOne(mappedBy = "parentPool")
	@Column(nullable = true)
	private TournamentPoolFight rightPool;

	/**
	 * Default constructor.
	 */
	public TournamentPoolFight() {

	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param value
	 *            the id to set
	 */
	public void setId(final Integer value) {
		this.id = value;
	}

	/**
	 * @return the nodeLevel
	 */
	public Integer getNodeLevel() {
		return this.nodeLevel;
	}

	/**
	 * @param value
	 *            the nodeLevel to set
	 */
	public void setNodeLevel(final Integer value) {
		this.nodeLevel = value;
	}

	/**
	 * @return the winner
	 */
	public FightPlayer getWinner() {
		return this.winner;
	}

	/**
	 * @param value
	 *            the winner to set
	 */
	public void setWinner(final FightPlayer value) {
		this.winner = value;
	}

	/**
	 * @return the parentPool
	 */
	public TournamentPoolFight getParentPool() {
		return this.parentPool;
	}

	/**
	 * @param value
	 *            the parentPool to set
	 */
	public void setParentPool(final TournamentPoolFight value) {
		this.parentPool = value;
	}

	/**
	 * @return the leftPool
	 */
	public TournamentPoolFight getLeftPool() {
		return this.leftPool;
	}

	/**
	 * @param value
	 *            the leftPool to set
	 */
	public void setLeftPool(final TournamentPoolFight value) {
		this.leftPool = value;
	}

	/**
	 * @return the rightPool
	 */
	public TournamentPoolFight getRightPool() {
		return this.rightPool;
	}

	/**
	 * @param value
	 *            the rightPool to set
	 */
	public void setRightPool(final TournamentPoolFight value) {
		this.rightPool = value;
	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		dest.writeInt(this.getId());
		dest.writeInt(this.getNodeLevel());

		dest.writeParcelable(this.getWinner(), flags);

		dest.writeParcelable(this.getParentPool(), flags);

		dest.writeParcelable(this.getLeftPool(), flags);

		dest.writeParcelable(this.getRightPool(), flags);
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		this.setId(parc.readInt());
		this.setNodeLevel(parc.readInt());

		this.setWinner((FightPlayer) parc.readParcelable(FightPlayer.class
				.getClassLoader()));

		this.setParentPool((TournamentPoolFight) parc
				.readParcelable(TournamentPoolFight.class.getClassLoader()));

		this.setLeftPool((TournamentPoolFight) parc
				.readParcelable(TournamentPoolFight.class.getClassLoader()));

		this.setRightPool((TournamentPoolFight) parc
				.readParcelable(TournamentPoolFight.class.getClassLoader()));
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public TournamentPoolFight(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<TournamentPoolFight> CREATOR = new Parcelable.Creator<TournamentPoolFight>() {
		public TournamentPoolFight createFromParcel(Parcel in) {
			return new TournamentPoolFight(in);
		}

		public TournamentPoolFight[] newArray(int size) {
			return new TournamentPoolFight[size];
		}
	};

}
