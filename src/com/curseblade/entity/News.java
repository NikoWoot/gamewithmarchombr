/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import com.tactfactory.harmony.annotation.Entity;
import com.tactfactory.harmony.bundles.rss.annotation.Rss;

/**
 * Class to present a news on our software (Use for RSS)
 * 
 * @author Nicolas GAUTIER
 * 
 */
@Entity
@Rss(uri = "http://www.feedforall.com/sample.xml")
public class News extends HarmonyRssItem implements Serializable, Parcelable {

	
	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = -3762032176595237807L;
	
	
	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "News";

	/**
	 * Default constructor.
	 */
	public News() {

	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		super.writeToParcelRegen(dest, flags);
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		super.readFromParcel(parc);
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public News(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
		public News createFromParcel(Parcel in) {
			return new News(in);
		}

		public News[] newArray(int size) {
			return new News[size];
		}
	};

}
