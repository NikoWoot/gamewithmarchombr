/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.ArrayList;

import com.tactfactory.harmony.annotation.Column;

import com.tactfactory.harmony.annotation.Entity;
import com.tactfactory.harmony.annotation.Id;
import com.tactfactory.harmony.annotation.ManyToOne;
import com.tactfactory.harmony.annotation.OneToMany;
import com.tactfactory.harmony.annotation.OneToOne;

/**
 * Class represent a Character in fightMode
 * 
 * @author Nicolas GAUTIER
 * 
 */
@Entity
public class FightPlayer implements Serializable, Parcelable {

	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = -6877642782779855395L;

	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "FightPlayer";

	@Id
	@Column(unique = true, nullable = false, hidden = true)
	private Integer id;

	@Column
	private Integer teamId;

	@OneToOne
	private Fight fight;

	@Column
	private String pseudo;

	@Column
	private Integer life;

	@Column
	private boolean dead;

	@ManyToOne
	private CharacterPlayer baseCharacter;

	@ManyToOne
	private ItemWeapon baseWeapon;

	@OneToMany
	private ArrayList<Spell> baseSpells;

	/**
	 * Default constructor.
	 */
	public FightPlayer() {

	}

	/**
	 * Constructor based on CharacterPlayer data
	 * 
	 * @param player
	 *            Character for data
	 */
	public FightPlayer(CharacterPlayer player, Fight fight, Integer teamId) {
		this.setBaseCharacter(player);
		this.setBaseSpells(new ArrayList<Spell>(player.getEquipedSpells()));
		this.setBaseWeapon(player.getWeaponUsed());
		this.setLife(player.getLife());
		this.setPseudo(player.getPseudo());
		this.setTeamId(teamId);
		this.setFight(fight);
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param value
	 *            the id to set
	 */
	public void setId(final Integer value) {
		this.id = value;
	}

	/**
	 * @return the teamId
	 */
	public Integer getTeamId() {
		return this.teamId;
	}

	/**
	 * @param value
	 *            the teamId to set
	 */
	public void setTeamId(final Integer value) {
		this.teamId = value;
	}

	/**
	 * @return the fight
	 */
	public Fight getFight() {
		return this.fight;
	}

	/**
	 * @param value
	 *            the fight to set
	 */
	public void setFight(final Fight value) {
		this.fight = value;
	}

	/**
	 * @return the pseudo
	 */
	public String getPseudo() {
		return this.pseudo;
	}

	/**
	 * @param value
	 *            the pseudo to set
	 */
	public void setPseudo(final String value) {
		this.pseudo = value;
	}

	/**
	 * @return the life
	 */
	public Integer getLife() {
		return this.life;
	}

	/**
	 * @param value
	 *            the life to set
	 */
	public void setLife(final Integer value) {
		this.life = value;
	}

	/**
	 * @return the dead
	 */
	public boolean isDead() {
		return this.dead;
	}

	/**
	 * @param value
	 *            the dead to set
	 */
	public void setDead(final boolean value) {
		this.dead = value;
	}

	/**
	 * @return the baseCharacter
	 */
	public CharacterPlayer getBaseCharacter() {
		return this.baseCharacter;
	}

	/**
	 * @param value
	 *            the baseCharacter to set
	 */
	public void setBaseCharacter(final CharacterPlayer value) {
		this.baseCharacter = value;
	}

	/**
	 * @return the baseWeapon
	 */
	public ItemWeapon getBaseWeapon() {
		return this.baseWeapon;
	}

	/**
	 * @param value
	 *            the baseWeapon to set
	 */
	public void setBaseWeapon(final ItemWeapon value) {
		this.baseWeapon = value;
	}

	/**
	 * @return the baseSpells
	 */
	public ArrayList<Spell> getBaseSpells() {
		return this.baseSpells;
	}

	/**
	 * @param value
	 *            the baseSpells to set
	 */
	public void setBaseSpells(final ArrayList<Spell> value) {
		this.baseSpells = value;
	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		dest.writeInt(this.getId());
		dest.writeInt(this.getTeamId());

		dest.writeParcelable(this.getFight(), flags);
		dest.writeString(this.getPseudo());
		dest.writeInt(this.getLife());

		if (this.isDead()) {
			dest.writeInt(1);
		} else {
			dest.writeInt(0);
		}

		dest.writeParcelable(this.getBaseCharacter(), flags);

		dest.writeParcelable(this.getBaseWeapon(), flags);

		if (this.getBaseSpells() != null) {
			dest.writeInt(this.getBaseSpells().size());
			for (Spell item : this.getBaseSpells()) {
				dest.writeParcelable(item, flags);
			}
		} else {
			dest.writeInt(-1);
		}
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		this.setId(parc.readInt());
		this.setTeamId(parc.readInt());

		this.setFight((Fight) parc.readParcelable(Fight.class.getClassLoader()));
		this.setPseudo(parc.readString());
		this.setLife(parc.readInt());
		this.setDead(parc.readInt() == 1);

		this.setBaseCharacter((CharacterPlayer) parc
				.readParcelable(CharacterPlayer.class.getClassLoader()));

		this.setBaseWeapon((ItemWeapon) parc.readParcelable(ItemWeapon.class
				.getClassLoader()));

		int nbBaseSpells = parc.readInt();
		if (nbBaseSpells > -1) {
			ArrayList<Spell> items = new ArrayList<Spell>();
			for (int i = 0; i < nbBaseSpells; i++) {
				items.add((Spell) parc.readParcelable(Spell.class
						.getClassLoader()));
			}
			this.setBaseSpells(items);
		}
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public FightPlayer(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<FightPlayer> CREATOR = new Parcelable.Creator<FightPlayer>() {
		public FightPlayer createFromParcel(Parcel in) {
			return new FightPlayer(in);
		}

		public FightPlayer[] newArray(int size) {
			return new FightPlayer[size];
		}
	};

}
