/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.ArrayList;

import com.tactfactory.harmony.annotation.*;

/**
 * Class to represent a fight in tournament mode or pvp mode
 * 
 * @author Nicolas GAUTIER
 * 
 */
@Entity
public class Fight implements Serializable, Parcelable {

	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = 3247324159934127661L;

	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "Fight";

	@Id
	@Column(unique = true, nullable = false, hidden = true)
	private Integer id;

	/**
	 * Fighter on start of fight
	 */
	@OneToMany(mappedBy = "fight")
	@Column(nullable = true)
	private ArrayList<FightPlayer> starterFighters;

	/**
	 * ArrayList of alternated FightPlayer by Team [Sender, Receiver, Sender,
	 * Receiver]
	 */
	@OneToMany(mappedBy = "fight")
	@Column(nullable = true)
	private ArrayList<FightPlayer> alternatedFighters;

	/**
	 * Fighter on end of fight
	 */
	@OneToMany(mappedBy = "fight")
	@Column(nullable = true)
	private ArrayList<FightPlayer> survivorFighters;

	/**
	 * Actions realised during fight
	 */
	@OneToMany
	@Column(nullable = true)
	private ArrayList<FightAction> actions;

	/**
	 * Actual senderPlayer
	 */
	@OneToOne(mappedBy = "fight")
	@Column(nullable = true)
	private FightPlayer senderPlayer;

	/**
	 * Actual receiverPlayer
	 */
	@OneToOne(mappedBy = "fight")
	@Column(nullable = true)
	private FightPlayer receiverPlayer;

	/**
	 * Actual index in AlternatedFighter
	 */
	@Column
	private Integer currentElement;

	/**
	 * Default constructor.
	 */
	public Fight() {
		this.starterFighters = new ArrayList<FightPlayer>();
		this.alternatedFighters = new ArrayList<FightPlayer>();
		this.actions = new ArrayList<FightAction>();
		this.currentElement = 0;
	}

	/**
	 * Return count for a team in the arrayList
	 * 
	 * @param teamNumber
	 *            number of the team
	 * @param fighters
	 *            array of fighters
	 * @return count fighters on this team
	 */
	public int countTeam(int teamNumber, ArrayList<FightPlayer> fighters) {

		int count = 0;

		for (FightPlayer player : fighters) {
			if (player.getTeamId() == teamNumber) {
				count += 1;
			}
		}

		return count;
	}

	/**
	 * Initialize mecanisme before start fight
	 * 
	 * @throws Exception
	 *             If count of alternatedFighters is not correct
	 */
	public void prepare() throws Exception {

		int maxCount = 0;

		this.alternatedFighters = new ArrayList<FightPlayer>();
		ArrayList<FightPlayer> survirorFirstTeam = new ArrayList<FightPlayer>();
		ArrayList<FightPlayer> survivorSecondTeam = new ArrayList<FightPlayer>();

		// Divise fightplayer in two list
		for (FightPlayer fp : this.getStarterFighters()) {
			if (fp.getTeamId() == 1) {
				survirorFirstTeam.add(fp);
			} else {
				survivorSecondTeam.add(fp);
			}
		}

		// Get the max size of concurrent teams
		if (survirorFirstTeam.size() > survivorSecondTeam.size()) {
			maxCount = survirorFirstTeam.size();
		} else {
			maxCount = survivorSecondTeam.size();
		}

		// Alternate player
		for (int i = 0; i <= maxCount; i++) {

			// Add fighter of the first team if it exist
			if (survirorFirstTeam.size() > i) {
				this.alternatedFighters.add(survirorFirstTeam.get(i));
			}

			// Add fighter of the second team if it exist
			if (survivorSecondTeam.size() > i) {
				this.alternatedFighters.add(survivorSecondTeam.get(i));
			}
		}

		// Test if fight is available
		if (this.alternatedFighters.size() < 2) {
			throw new Exception(
					"A fight with only one fighter is not possible !");
		}

		// Copy StarterFighter on SurvivorFighter
		this.survivorFighters = new ArrayList<FightPlayer>(this.starterFighters);
	}

	/**
	 * Initialize mecanisme before a turn
	 */
	public void startTurn() {

		this.selectNextSenderAndReceiver();
	}

	/**
	 * Save data effectued during a turn
	 */
	public void endTurn() {

		// Delete the receiver if is dead
		if (this.receiverPlayer.getLife() <= 0) {
			this.manageReceiverDeath();
		}

		// Add an endTurn action on log
		FightAction fa = new FightAction();
		fa.setFight(this);
		fa.setSender(this.getSenderPlayer());
		fa.setReceiver(this.getReceiverPlayer());
		fa.setActionType(FightActionType.END_TURN);

		this.actions.add(fa);
	}

	/**
	 * Delete the receiver of fighterList
	 */
	private void manageReceiverDeath() {
		this.receiverPlayer.setDead(true);
		this.receiverPlayer.setLife(0);

		// Delete FighterPlayer on ordered list
		this.getAlternatedFighters().remove(this.receiverPlayer);
		this.getSurvivorFighters().remove(this.receiverPlayer);

		// Add an Deathaction on log
		FightAction fa = new FightAction();
		fa.setFight(this);
		fa.setSender(this.getSenderPlayer());
		fa.setReceiver(this.getReceiverPlayer());
		fa.setActionType(FightActionType.DEATH_FIGHTER);

		this.actions.add(fa);
	}

	/**
	 * Select new sender and receiver
	 */
	private void selectNextSenderAndReceiver() {

		FightPlayer tempPlayer = null;

		do {
			this.senderPlayer = this.getAlternatedFighters().get(
					this.currentElement);

			// Return on the first element if i have realised a while on
			// collection
			if ((this.getAlternatedFighters().size() - 1) > this.currentElement) {
				this.currentElement += 1;
			} else {
				this.currentElement = 0;
			}

		} while (this.senderPlayer == null);

		do {
			tempPlayer = this.getAlternatedFighters().get(this.currentElement);

			// Verify if two players is not in the same team
			if (senderPlayer.getTeamId() != tempPlayer.getTeamId()) {
				this.receiverPlayer = tempPlayer;
			} else {
				// Return on the first element if i have realised a while on
				// collection
				if ((this.getAlternatedFighters().size() - 1) > this.currentElement) {
					this.currentElement += 1;
				} else {
					this.currentElement = 0;
				}
			}

		} while (receiverPlayer == null);

		// Add an StartTurnaction on log
		FightAction fa = new FightAction();
		fa.setFight(this);
		fa.setSender(this.getSenderPlayer());
		fa.setReceiver(this.getReceiverPlayer());
		fa.setActionType(FightActionType.START_TURN);

		this.actions.add(fa);
	}

	/**
	 * Method to attack a character with an spell
	 * 
	 * @param sender
	 *            Sender of this attack
	 * @param receiver
	 *            Receiver of this attack
	 * @param spell
	 *            Spell launch on this receiver
	 * @throws Exception
	 */
	public void attack(FightPlayer sender, FightPlayer receiver, Spell spell)
			throws Exception {

		// Sender and receiver not is in the same team
		if (sender.getTeamId() == receiver.getTeamId()) {
			throw new Exception(
					"Sender and Receiver dont must in the same team");
		}

		// Get armury, dmg and finalDmg		
		int dmg = (int) Math.round(spell.getBaseAttack());		
		int finalDmg = this.calculateFinalDamage(dmg, receiver.getBaseCharacter());
		
		// Substract dmg on life
		receiver.setLife(receiver.getLife() - finalDmg);

		// Add an fightAction
		FightAction fa = new FightAction();
		fa.setFight(this);
		fa.setSender(this.getSenderPlayer());
		fa.setReceiver(this.getReceiverPlayer());
		fa.setActionType(FightActionType.ACTION_FIGHTER);
		fa.setBeforeValue(dmg);
		fa.setAfterValue(finalDmg);

		this.actions.add(fa);
	}

	/**
	 * Method to attack a character with a weapon
	 * 
	 * @param sender
	 *            Sender of this attack
	 * @param receiver
	 *            Receiver of this attack
	 * @param weapon
	 *            Weapon use on this receiver
	 * @throws Exception
	 */
	public void attack(FightPlayer sender, FightPlayer receiver,
			ItemWeapon weapon) throws Exception {

		// Sender and receiver not is in the same team
		if (sender.getTeamId() == receiver.getTeamId()) {
			throw new Exception(
					"Sender and Receiver dont must in the same team");
		}

		// Get armury, dmg and finalDmg	
		int dmg = (int) Math.round(weapon.getBaseAttack());		
		int finalDmg = this.calculateFinalDamage(dmg, receiver.getBaseCharacter());

		// Substract dmg on life
		receiver.setLife(receiver.getLife() - finalDmg);

		// Add an fightAction
		FightAction fa = new FightAction();
		fa.setFight(this);
		fa.setSender(this.getSenderPlayer());
		fa.setReceiver(this.getReceiverPlayer());
		fa.setActionType(FightActionType.ACTION_FIGHTER);
		fa.setBeforeValue(dmg);
		fa.setAfterValue(finalDmg);

		this.actions.add(fa);
	}

	/**
	 * Method to end fight 
	 */
	public void endFight() {

		// Add an EndFightAction on log
		FightAction fa = new FightAction();
		fa.setFight(this);
		fa.setSender(this.getSenderPlayer());
		fa.setReceiver(this.getReceiverPlayer());
		fa.setActionType(FightActionType.END_FIGHT);

		this.actions.add(fa);
	}

	/**
	 * Return amount of damage calculated with armury
	 * @param damage Amount of initial damage
	 * @param receiver Receiver of this attack
	 * @return Final Damage amount
	 */
	public Integer calculateFinalDamage(int damage, CharacterPlayer receiver) {
		
		// Get armury, dmg and finalDmg
		int armury = receiver.calculArmury();
		if (armury > 50) {
			armury = 50;
		}
		
		double armuryCoef = (armury / 100d);
		double reduceDmg = (damage * armuryCoef);
		int finalDmg = (int) (damage - Math.round(reduceDmg));
		
		// Dont heal character
		if (finalDmg < 0) {
			finalDmg = 0;
		}
		
		return finalDmg;
	}
	
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param value
	 *            the id to set
	 */
	public void setId(final Integer value) {
		this.id = value;
	}

	/**
	 * @return the starterFighters
	 */
	public ArrayList<FightPlayer> getStarterFighters() {
		return this.starterFighters;
	}

	/**
	 * @param value
	 *            the starterFighters to set
	 */
	public void setStarterFighters(final ArrayList<FightPlayer> value) {
		this.starterFighters = value;
	}

	/**
	 * @return the alternatedFighters
	 */
	public ArrayList<FightPlayer> getAlternatedFighters() {
		return this.alternatedFighters;
	}

	/**
	 * @param value
	 *            the alternatedFighters to set
	 */
	public void setAlternatedFighters(final ArrayList<FightPlayer> value) {
		this.alternatedFighters = value;
	}

	/**
	 * @return the survivorFighters
	 */
	public ArrayList<FightPlayer> getSurvivorFighters() {
		return this.survivorFighters;
	}

	/**
	 * @param value
	 *            the survivorFighters to set
	 */
	public void setSurvivorFighters(final ArrayList<FightPlayer> value) {
		this.survivorFighters = value;
	}

	/**
	 * @return the actions
	 */
	public ArrayList<FightAction> getActions() {
		return this.actions;
	}

	/**
	 * @param value
	 *            the actions to set
	 */
	public void setActions(final ArrayList<FightAction> value) {
		this.actions = value;
	}

	/**
	 * @return the senderPlayer
	 */
	public FightPlayer getSenderPlayer() {
		return this.senderPlayer;
	}

	/**
	 * @param value
	 *            the senderPlayer to set
	 */
	public void setSenderPlayer(final FightPlayer value) {
		this.senderPlayer = value;
	}

	/**
	 * @return the receiverPlayer
	 */
	public FightPlayer getReceiverPlayer() {
		return this.receiverPlayer;
	}

	/**
	 * @param value
	 *            the receiverPlayer to set
	 */
	public void setReceiverPlayer(final FightPlayer value) {
		this.receiverPlayer = value;
	}

	/**
	 * @return the currentElement
	 */
	public Integer getCurrentElement() {
		return this.currentElement;
	}

	/**
	 * @param value
	 *            the currentElement to set
	 */
	public void setCurrentElement(final Integer value) {
		this.currentElement = value;
	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		dest.writeInt(this.getId());

		if (this.getStarterFighters() != null) {
			dest.writeInt(this.getStarterFighters().size());
			for (FightPlayer item : this.getStarterFighters()) {
				dest.writeParcelable(item, flags);
			}
		} else {
			dest.writeInt(-1);
		}

		if (this.getAlternatedFighters() != null) {
			dest.writeInt(this.getAlternatedFighters().size());
			for (FightPlayer item : this.getAlternatedFighters()) {
				dest.writeParcelable(item, flags);
			}
		} else {
			dest.writeInt(-1);
		}

		if (this.getSurvivorFighters() != null) {
			dest.writeInt(this.getSurvivorFighters().size());
			for (FightPlayer item : this.getSurvivorFighters()) {
				dest.writeParcelable(item, flags);
			}
		} else {
			dest.writeInt(-1);
		}

		if (this.getActions() != null) {
			dest.writeInt(this.getActions().size());
			for (FightAction item : this.getActions()) {
				dest.writeParcelable(item, flags);
			}
		} else {
			dest.writeInt(-1);
		}

		dest.writeParcelable(this.getSenderPlayer(), flags);

		dest.writeParcelable(this.getReceiverPlayer(), flags);
		dest.writeInt(this.getCurrentElement());
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		this.setId(parc.readInt());

		int nbStarterFighters = parc.readInt();
		if (nbStarterFighters > -1) {
			ArrayList<FightPlayer> items = new ArrayList<FightPlayer>();
			for (int i = 0; i < nbStarterFighters; i++) {
				items.add((FightPlayer) parc.readParcelable(FightPlayer.class
						.getClassLoader()));
			}
			this.setStarterFighters(items);
		}

		int nbAlternatedFighters = parc.readInt();
		if (nbAlternatedFighters > -1) {
			ArrayList<FightPlayer> items = new ArrayList<FightPlayer>();
			for (int i = 0; i < nbAlternatedFighters; i++) {
				items.add((FightPlayer) parc.readParcelable(FightPlayer.class
						.getClassLoader()));
			}
			this.setAlternatedFighters(items);
		}

		int nbSurvivorFighters = parc.readInt();
		if (nbSurvivorFighters > -1) {
			ArrayList<FightPlayer> items = new ArrayList<FightPlayer>();
			for (int i = 0; i < nbSurvivorFighters; i++) {
				items.add((FightPlayer) parc.readParcelable(FightPlayer.class
						.getClassLoader()));
			}
			this.setSurvivorFighters(items);
		}

		int nbActions = parc.readInt();
		if (nbActions > -1) {
			ArrayList<FightAction> items = new ArrayList<FightAction>();
			for (int i = 0; i < nbActions; i++) {
				items.add((FightAction) parc.readParcelable(FightAction.class
						.getClassLoader()));
			}
			this.setActions(items);
		}

		this.setSenderPlayer((FightPlayer) parc
				.readParcelable(FightPlayer.class.getClassLoader()));

		this.setReceiverPlayer((FightPlayer) parc
				.readParcelable(FightPlayer.class.getClassLoader()));
		this.setCurrentElement(parc.readInt());
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public Fight(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<Fight> CREATOR = new Parcelable.Creator<Fight>() {
		public Fight createFromParcel(Parcel in) {
			return new Fight(in);
		}

		public Fight[] newArray(int size) {
			return new Fight[size];
		}
	};

}
