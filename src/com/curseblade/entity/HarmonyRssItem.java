/**************************************************************************
 * HarmonyRssItem.java, curseblade Android
 *
 * Copyright 2014 Mickael Gaillard / TACTfactory
 * Description : 
 * Author(s)   : Harmony
 * Licence     : all right reserved
 * Last update : Feb 10, 2014
 *
 **************************************************************************/

package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.joda.time.DateTime;

import com.tactfactory.harmony.annotation.Column;
import com.tactfactory.harmony.annotation.Column.Type;
import com.tactfactory.harmony.annotation.Entity;
import com.tactfactory.harmony.annotation.GeneratedValue;
import com.tactfactory.harmony.annotation.Id;
import com.tactfactory.harmony.annotation.Table;
import com.tactfactory.harmony.bundles.rss.annotation.RssField;

@Entity(hidden = true)
@Table
public class HarmonyRssItem implements Serializable, Parcelable {

	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = 1594397415125280186L;

	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "HarmonyRssItem";

	/** Entity's technical id. */
	@Id
	@Column(type = Type.INTEGER, hidden = true)
	@GeneratedValue(strategy = "IDENTITY")
	private int id;

	/** Hash of the RssItem. **/
	@Column(hidden = true)
	private int hash;

	/** Rss Guid. **/
	@Column(nullable = true, hidden = true)
	@RssField
	private String guid; // Unused

	/** Rss Title. **/
	@Column
	@RssField
	private String title;

	/** Rss Link. **/
	@Column
	@RssField
	private String link;

	/** Rss Description. **/
	@Column
	@RssField
	private String description;

	/** Rss Enclosure. **/
	@Column(nullable = true)
	@RssField
	private String enclosure;

	/** Rss Author. **/
	@Column(nullable = true)
	@RssField
	private String author;

	/** Rss Publication Date. **/
	@Column(nullable = true)
	@RssField
	private DateTime pubDate;

	/** Rss Categories. **/
	@Column(nullable = true)
	@RssField(rssname = "category")
	private String categories;

	/**
	 * Default constructor.
	 */
	public HarmonyRssItem() {

	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @param value
	 *            the id to set
	 */
	public void setId(final int value) {
		this.id = value;
	}

	/**
	 * @return the hash
	 */
	public int getHash() {
		return this.hash;
	}

	/**
	 * @param value
	 *            the hash to set
	 */
	public void setHash(final int value) {
		this.hash = value;
	}

	/**
	 * @return the guid
	 */
	public String getGuid() {
		return this.guid;
	}

	/**
	 * @param value
	 *            the guid to set
	 */
	public void setGuid(final String value) {
		this.guid = value;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * @param value
	 *            the title to set
	 */
	public void setTitle(final String value) {
		this.title = value;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return this.link;
	}

	/**
	 * @param value
	 *            the link to set
	 */
	public void setLink(final String value) {
		this.link = value;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * @param value
	 *            the description to set
	 */
	public void setDescription(final String value) {
		this.description = value;
	}

	/**
	 * @return the enclosure
	 */
	public String getEnclosure() {
		return this.enclosure;
	}

	/**
	 * @param value
	 *            the enclosure to set
	 */
	public void setEnclosure(final String value) {
		this.enclosure = value;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return this.author;
	}

	/**
	 * @param value
	 *            the author to set
	 */
	public void setAuthor(final String value) {
		this.author = value;
	}

	/**
	 * @return the pubDate
	 */
	public DateTime getPubDate() {
		return this.pubDate;
	}

	/**
	 * @param value
	 *            the pubDate to set
	 */
	public void setPubDate(final DateTime value) {
		this.pubDate = value;
	}

	/**
	 * @return the categories
	 */
	public String getCategories() {
		return this.categories;
	}

	/**
	 * @param value
	 *            the categories to set
	 */
	public void setCategories(final String value) {
		this.categories = value;
	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		dest.writeInt(this.getId());
		dest.writeInt(this.getHash());
		dest.writeString(this.getGuid());
		dest.writeString(this.getTitle());
		dest.writeString(this.getLink());
		dest.writeString(this.getDescription());
		dest.writeString(this.getEnclosure());
		dest.writeString(this.getAuthor());
		if (this.getPubDate() != null) {
			dest.writeInt(1);
			dest.writeString(this.getPubDate().toString());
		} else {
			dest.writeInt(0);
		}
		dest.writeString(this.getCategories());
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		this.setId(parc.readInt());
		this.setHash(parc.readInt());
		this.setGuid(parc.readString());
		this.setTitle(parc.readString());
		this.setLink(parc.readString());
		this.setDescription(parc.readString());
		this.setEnclosure(parc.readString());
		this.setAuthor(parc.readString());
		if (parc.readInt() == 1) {
			this.setPubDate(new DateTime(parc.readString()));
		}
		this.setCategories(parc.readString());
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public HarmonyRssItem(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<HarmonyRssItem> CREATOR = new Parcelable.Creator<HarmonyRssItem>() {
		public HarmonyRssItem createFromParcel(Parcel in) {
			return new HarmonyRssItem(in);
		}

		public HarmonyRssItem[] newArray(int size) {
			return new HarmonyRssItem[size];
		}
	};

}
