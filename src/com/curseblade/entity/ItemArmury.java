/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import com.tactfactory.harmony.annotation.*;
import com.tactfactory.harmony.annotation.Column.Type;

/**
 * Class to describe an piece of Armury for my character
 * @author Nicolas GAUTIER
 *
 */
@Entity
public class ItemArmury implements Serializable, Parcelable {

	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = -5650029122603726282L;

	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "ItemArmury";

	@Id
	@Column(unique = true, nullable = false, hidden = true)
	private Integer id;

	@Column
	private String name;

	@Column
	private Integer defValue;

	@Column(type = Type.ENUM)
	private ItemArmuryType armuryType;

	/**
	 * Default constructor.
	 */
	public ItemArmury() {

	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param value
	 *            the id to set
	 */
	public void setId(final Integer value) {
		this.id = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param value
	 *            the name to set
	 */
	public void setName(final String value) {
		this.name = value;
	}

	/**
	 * @return the defValue
	 */
	public Integer getDefValue() {
		return this.defValue;
	}

	/**
	 * @param value
	 *            the defValue to set
	 */
	public void setDefValue(final Integer value) {
		this.defValue = value;
	}

	/**
	 * @return the armuryType
	 */
	public ItemArmuryType getArmuryType() {
		return this.armuryType;
	}

	/**
	 * @param value
	 *            the armuryType to set
	 */
	public void setArmuryType(final ItemArmuryType value) {
		this.armuryType = value;
	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		dest.writeInt(this.getId());
		dest.writeString(this.getName());
		dest.writeInt(this.getDefValue());

		if (this.getArmuryType() != null) {
			dest.writeInt(1);
			dest.writeString(this.getArmuryType().name());
		} else {
			dest.writeInt(0);
		}
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		this.setId(parc.readInt());
		this.setName(parc.readString());
		this.setDefValue(parc.readInt());

		int armuryTypeBool = parc.readInt();
		if (armuryTypeBool == 1) {
			this.setArmuryType(ItemArmuryType.valueOf(parc.readString()));
		}
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public ItemArmury(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<ItemArmury> CREATOR = new Parcelable.Creator<ItemArmury>() {
		public ItemArmury createFromParcel(Parcel in) {
			return new ItemArmury(in);
		}

		public ItemArmury[] newArray(int size) {
			return new ItemArmury[size];
		}
	};

}
