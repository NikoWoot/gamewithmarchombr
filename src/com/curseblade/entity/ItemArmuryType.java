/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

/**
 * Class use for persistance of the type of ItemArmury So usely because is for
 * the roleplay (not 2 helmet on 1 character)
 * 
 * @author Nicolas GAUTIER
 * 
 */
public enum ItemArmuryType {
	HELMET, BODY, LEGS, GAUNTLET, RING
}
