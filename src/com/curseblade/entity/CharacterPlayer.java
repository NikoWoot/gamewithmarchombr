/**
 *
 * Copyright Nicolas GAUTIER, Alexandre LEBRUN - 2013.
 * Description : CurseBlade AndroidGame created by two students of IIA.
 * Author(s)   : Nicolas GAUTIER, Alexandre LEBRUN.
 * Licence     : MIT.
 * Last update : 10/02/2014.
 *
**/
package com.curseblade.entity;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.ArrayList;

import org.joda.time.DateTime;
import com.tactfactory.harmony.annotation.Column;
import com.tactfactory.harmony.annotation.Column.Type;
import com.tactfactory.harmony.annotation.Entity;
import com.tactfactory.harmony.annotation.Id;
import com.tactfactory.harmony.annotation.OneToMany;
import com.tactfactory.harmony.annotation.OneToOne;
import com.tactfactory.harmony.bundles.social.annotation.Social;

/**
 * Class to present a character in the game
 * 
 * @author Nicolas GAUTIER
 * 
 */
@Entity
@Social
public class CharacterPlayer implements Serializable, Parcelable {

	/**
	 * UID for serialisation
	 */
	private static final long serialVersionUID = 5803532978871790472L;

	/** Key Constant for parcelable/serialization. */
	public static final String PARCEL = "CharacterPlayer";

	@Id
	@Column(unique = true, nullable = false, hidden = true)
	private Integer id;

	@Column
	private String pseudo;

	@Column(defaultValue = "50")
	private Integer life;

	@Column(type = Type.DATE)
	private DateTime createdAt;

	@Column(defaultValue = "1")
	private Integer level;

	@OneToMany
	private ArrayList<ItemArmury> armuryEquipedItems;

	@OneToMany
	private ArrayList<Spell> equipedSpells;

	@OneToOne
	private ItemWeapon weaponUsed;

	/**
	 * Default constructor.
	 */
	public CharacterPlayer() {
		this.createdAt = DateTime.now();
		this.life = 50;
		this.level = 1;
		this.armuryEquipedItems = new ArrayList<ItemArmury>();
		this.equipedSpells = new ArrayList<Spell>();
	}

	/**
	 * Calcul armury for my player
	 * 
	 * @return Mount of armury
	 */
	public int calculArmury() {

		int armur = 0;

		for (ItemArmury armury : this.getArmuryEquipedItems()) {
			armur += armury.getDefValue();
		}

		return armur;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @param value
	 *            the id to set
	 */
	public void setId(final Integer value) {
		this.id = value;
	}

	/**
	 * @return the pseudo
	 */
	public String getPseudo() {
		return this.pseudo;
	}

	/**
	 * @param value
	 *            the pseudo to set
	 */
	public void setPseudo(final String value) {
		this.pseudo = value;
	}

	/**
	 * @return the life
	 */
	public Integer getLife() {
		return this.life;
	}

	/**
	 * @param value
	 *            the life to set
	 */
	public void setLife(final Integer value) {
		this.life = value;
	}

	/**
	 * @return the createdAt
	 */
	public DateTime getCreatedAt() {
		return this.createdAt;
	}

	/**
	 * @param value
	 *            the createdAt to set
	 */
	public void setCreatedAt(final DateTime value) {
		this.createdAt = value;
	}

	/**
	 * @return the level
	 */
	public Integer getLevel() {
		return this.level;
	}

	/**
	 * @param value
	 *            the level to set
	 */
	public void setLevel(final Integer value) {
		this.level = value;
	}

	/**
	 * @return the armuryEquipedItems
	 */
	public ArrayList<ItemArmury> getArmuryEquipedItems() {
		return this.armuryEquipedItems;
	}

	/**
	 * @param value
	 *            the armuryEquipedItems to set
	 */
	public void setArmuryEquipedItems(final ArrayList<ItemArmury> value) {
		this.armuryEquipedItems = value;
	}

	/**
	 * @return the equipedSpells
	 */
	public ArrayList<Spell> getEquipedSpells() {
		return this.equipedSpells;
	}

	/**
	 * @param value
	 *            the equipedSpells to set
	 */
	public void setEquipedSpells(final ArrayList<Spell> value) {
		this.equipedSpells = value;
	}

	/**
	 * @return the weaponUsed
	 */
	public ItemWeapon getWeaponUsed() {
		return this.weaponUsed;
	}

	/**
	 * @param value
	 *            the weaponUsed to set
	 */
	public void setWeaponUsed(final ItemWeapon value) {
		this.weaponUsed = value;
	}

	/**
	 * This stub of code is regenerated. DO NOT MODIFY.
	 * 
	 * @param dest
	 *            Destination parcel
	 * @param flags
	 *            flags
	 */
	public void writeToParcelRegen(Parcel dest, int flags) {
		dest.writeInt(this.getId());
		dest.writeString(this.getPseudo());
		dest.writeInt(this.getLife());
		if (this.getCreatedAt() != null) {
			dest.writeInt(1);
			dest.writeString(this.getCreatedAt().toString());
		} else {
			dest.writeInt(0);
		}
		dest.writeInt(this.getLevel());

		if (this.getArmuryEquipedItems() != null) {
			dest.writeInt(this.getArmuryEquipedItems().size());
			for (ItemArmury item : this.getArmuryEquipedItems()) {
				dest.writeParcelable(item, flags);
			}
		} else {
			dest.writeInt(-1);
		}

		if (this.getEquipedSpells() != null) {
			dest.writeInt(this.getEquipedSpells().size());
			for (Spell item : this.getEquipedSpells()) {
				dest.writeParcelable(item, flags);
			}
		} else {
			dest.writeInt(-1);
		}

		dest.writeParcelable(this.getWeaponUsed(), flags);
	}

	/**
	 * Regenerated Parcel Constructor.
	 * 
	 * This stub of code is regenerated. DO NOT MODIFY THIS METHOD.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public void readFromParcel(Parcel parc) {
		this.setId(parc.readInt());
		this.setPseudo(parc.readString());
		this.setLife(parc.readInt());
		if (parc.readInt() == 1) {
			this.setCreatedAt(new DateTime(parc.readString()));
		}
		this.setLevel(parc.readInt());

		int nbArmuryEquipedItems = parc.readInt();
		if (nbArmuryEquipedItems > -1) {
			ArrayList<ItemArmury> items = new ArrayList<ItemArmury>();
			for (int i = 0; i < nbArmuryEquipedItems; i++) {
				items.add((ItemArmury) parc.readParcelable(ItemArmury.class
						.getClassLoader()));
			}
			this.setArmuryEquipedItems(items);
		}

		int nbEquipedSpells = parc.readInt();
		if (nbEquipedSpells > -1) {
			ArrayList<Spell> items = new ArrayList<Spell>();
			for (int i = 0; i < nbEquipedSpells; i++) {
				items.add((Spell) parc.readParcelable(Spell.class
						.getClassLoader()));
			}
			this.setEquipedSpells(items);
		}

		this.setWeaponUsed((ItemWeapon) parc.readParcelable(ItemWeapon.class
				.getClassLoader()));
	}

	/**
	 * Parcel Constructor.
	 * 
	 * @param parc
	 *            The parcel to read from
	 */
	public CharacterPlayer(Parcel parc) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.readFromParcel(parc);

		// You can implement your own parcel mechanics here.

	}

	/*
	 * This method is not regenerated. You can implement your own parcel
	 * mechanics here.
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// You can chose not to use harmony's generated parcel.
		// To do this, remove this line.
		this.writeToParcelRegen(dest, flags);

		// You can implement your own parcel mechanics here.
	}

	@Override
	public int describeContents() {
		// This should return 0
		// or CONTENTS_FILE_DESCRIPTOR if your entity is a FileDescriptor.
		return 0;
	}

	/**
	 * Parcelable creator.
	 */
	public static final Parcelable.Creator<CharacterPlayer> CREATOR = new Parcelable.Creator<CharacterPlayer>() {
		public CharacterPlayer createFromParcel(Parcel in) {
			return new CharacterPlayer(in);
		}

		public CharacterPlayer[] newArray(int size) {
			return new CharacterPlayer[size];
		}
	};

}
